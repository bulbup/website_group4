-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 03, 2017 at 11:37 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `english_unlimited`
--

-- --------------------------------------------------------

--
-- Table structure for table `details`
--

CREATE TABLE `details` (
  `detail_id` int(11) NOT NULL,
  `learn_id` int(11) NOT NULL,
  `listword_id` int(11) NOT NULL,
  `vocabulary_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `file_id` int(11) NOT NULL,
  `file_reading` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_audio` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_question` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`file_id`, `file_reading`, `file_audio`, `file_question`) VALUES
(0, 'files/read/family.csv', 'link', 'link'),
(1, 'files/read/sport.csv', 'link', 'link'),
(2, 'files/read/environment.csv', 'link', 'link');

-- --------------------------------------------------------

--
-- Table structure for table `file_of_lesson`
--

CREATE TABLE `file_of_lesson` (
  `file_of_lesson_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `file_of_lesson`
--

INSERT INTO `file_of_lesson` (`file_of_lesson_id`, `lesson_id`, `file_id`) VALUES
(1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `learns`
--

CREATE TABLE `learns` (
  `learn_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `level_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lessons`
--

CREATE TABLE `lessons` (
  `lesson_id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `lesson_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Ma` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lessons`
--

INSERT INTO `lessons` (`lesson_id`, `topic_id`, `lesson_name`, `Ma`, `image`) VALUES
(0, 2, 'Bài 1 (Thể thao)', 1, 'http://cms.kienthuc.net.vn/zoomh/500/uploaded/buimanhhung/2014_01_02/7/thuykieu_kienthuc_ecmv.jpg'),
(1, 1, 'Bài 1 ( Gia đình)', 1, 'http://images.tapchianhdep.net/15-10tai-bo-hinh-anh-gia-dinh-dep-va-y-nghia-nhat16.jpg'),
(2, 1, 'bài 2', 2, 'http://images.tapchianhdep.net/15-10tai-bo-hinh-anh-gia-dinh-dep-va-y-nghia-nhat1.jpg'),
(3, 2, 'Bài 2 (Thể thao)', 2, 'http://media1.tinngan.vn/archive/images/2014/12/23/092420_kk3.jpg'),
(4, 3, 'Bài 1 (thực phẩm sạch', 1, 'http://www.foodanddrinkdevon.co.uk/sites/default/files/styles/quicklink/public/quicklinks/food_producer_photo.jpg?itok=5_6Zniu8'),
(5, 3, 'bài 2 (thực phẩm tươi)', 2, 'https://static.independent.co.uk/s3fs-public/styles/article_small/public/thumbnails/image/2016/12/19/18/sush0istock-gkrphoto.jpg'),
(6, 4, 'Bài 1', 1, 'http://kinhdoanhthongminh.net/images/product/goc/goc_nhung-gi-ta-hoc-duoc-sau-khi-dung-len-tu-that-bai.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `lesson_detail`
--

CREATE TABLE `lesson_detail` (
  `lesson_detail_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `listword_id` int(11) NOT NULL,
  `file_of_lesson_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lesson_detail`
--

INSERT INTO `lesson_detail` (`lesson_detail_id`, `lesson_id`, `level_id`, `listword_id`, `file_of_lesson_id`) VALUES
(1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `level_id` int(11) NOT NULL,
  `level_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`level_id`, `level_name`) VALUES
(1, 'cơ bản'),
(2, 'nâng cao');

-- --------------------------------------------------------

--
-- Table structure for table `listwords`
--

CREATE TABLE `listwords` (
  `listword_id` int(11) NOT NULL,
  `level_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `listwords`
--

INSERT INTO `listwords` (`listword_id`, `level_name`, `file_name`) VALUES
(1, 'cơ bản', 'files/read/family_word.csv'),
(2, 'cơ bản', 'files/read/sport_word.csv');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `log_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `login_time` datetime NOT NULL,
  `login_count` int(11) NOT NULL,
  `login_ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`) VALUES
('nv', 'Nhân viên'),
('qt', 'Quản trị');

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `topic_id` int(11) NOT NULL,
  `topic_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `describe_short` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`topic_id`, `topic_name`, `level_name`, `describe_short`, `image`, `create_time`) VALUES
(1, 'Gia đình', 'cơ bản', 'mô tả gia đình', 'http://nhungcaunoihay.net/wp-content/uploads/2016/01/hinh-anh-dep-ve-gia-dinh-hanh-phuc-y-nghia-nhat12.png', '2017-06-27 01:57:46'),
(2, 'Thể thao', 'cơ bản', 'mô tả thể thao', 'http://taihinhnen.net/hnimages/10/hinh-nen-hinh-anh-nu-van-dong-vien-bong-chuyen-xinh-dep.63687.jpg', '2017-06-27 01:58:28'),
(3, 'Thực phẩm', 'cơ bản', 'mô tả thực phẩm', 'https://vietjet.net/includes/uploads/2015/02/cach-mua-va-bao-quan-thuc-pham-tet-nguyen-dan-phu-hop2.jpg', '0000-00-00 00:00:00'),
(4, 'Môi trường', 'cơ bản', 'Mô tả môi trường', 'https://sites.google.com/site/cd1310050094/_/rsrc/1467121534347/home/hinh-anh-ve-moi-truong/tu-vung-ve-bao-ve-moi-truong-protecting-the-environment.jpg', '0000-00-00 00:00:00'),
(5, 'Sức khỏe', 'cơ bản', 'Mô tả môi trường', 'http://daotaokythuat.com/wp-content/uploads/%E1%BA%A2nh-ch%E1%BB%A5p-m%C3%A0n-h%C3%ACnh_2015-11-02_084513.png', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `role_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `job` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `role_id`, `first_name`, `last_name`, `email`, `password`, `birthday`, `gender`, `job`, `image`, `created`, `modified`) VALUES
(3, 'nv', 'Luân', 'Nguyễn', 'luan@gmail.com', '$2y$10$6HOAa3J9QQwH76pgnA8azOavYUdihGQ/Y366CefnUIKz4i73oZB6S', NULL, NULL, NULL, NULL, '2017-06-23 01:27:24', '0000-00-00 00:00:00'),
(15, 'nv', 'Tuân', 'Phạm', 'tuan@gmail.com', '$2y$10$auuvVMu/ZGgocHrT3ItE2OlLPg1.5u01R/zvafCR06eiiCrC/SfX6', NULL, NULL, NULL, NULL, '2017-06-25 07:06:21', '2017-06-25 07:06:21'),
(18, 'nv', 'Bình', 'Bùi', 'binh@gmail.com', '$2y$10$LeKIuMhpLnRpOa19ajAr2.t4CxdszY7WzLYBzC/Afzyu.Dsf7B1Cm', NULL, NULL, NULL, NULL, '2017-06-25 07:35:20', '2017-06-25 07:35:20'),
(19, 'nv', 'Đang', 'Phạm', 'dang@gmail.com', '$2y$10$imML7PcwnXUMzDAu5uyb7u6BlHUr8t4NYJFDmekiu6kn/c.oXWAHe', NULL, NULL, NULL, NULL, '2017-06-25 07:36:39', '2017-06-25 07:36:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `details`
--
ALTER TABLE `details`
  ADD PRIMARY KEY (`detail_id`),
  ADD KEY `learn_id` (`learn_id`),
  ADD KEY `listword_id` (`listword_id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`file_id`);

--
-- Indexes for table `file_of_lesson`
--
ALTER TABLE `file_of_lesson`
  ADD PRIMARY KEY (`file_of_lesson_id`),
  ADD KEY `lesson_id` (`lesson_id`),
  ADD KEY `fk_file_lesson` (`file_id`);

--
-- Indexes for table `learns`
--
ALTER TABLE `learns`
  ADD PRIMARY KEY (`learn_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `lesson_id` (`lesson_id`);

--
-- Indexes for table `lessons`
--
ALTER TABLE `lessons`
  ADD PRIMARY KEY (`lesson_id`),
  ADD KEY `topic_id` (`topic_id`);

--
-- Indexes for table `lesson_detail`
--
ALTER TABLE `lesson_detail`
  ADD PRIMARY KEY (`lesson_detail_id`),
  ADD KEY `level_id` (`level_id`),
  ADD KEY `file_of_lesson_id` (`file_of_lesson_id`),
  ADD KEY `lesson_id` (`lesson_id`),
  ADD KEY `listword_id` (`listword_id`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`level_id`);

--
-- Indexes for table `listwords`
--
ALTER TABLE `listwords`
  ADD PRIMARY KEY (`listword_id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`topic_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `topic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `details`
--
ALTER TABLE `details`
  ADD CONSTRAINT `details_ibfk_1` FOREIGN KEY (`learn_id`) REFERENCES `learns` (`learn_id`);

--
-- Constraints for table `file_of_lesson`
--
ALTER TABLE `file_of_lesson`
  ADD CONSTRAINT `fk_file_lesson` FOREIGN KEY (`file_id`) REFERENCES `file` (`file_id`);

--
-- Constraints for table `learns`
--
ALTER TABLE `learns`
  ADD CONSTRAINT `learns_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `learns_ibfk_2` FOREIGN KEY (`lesson_id`) REFERENCES `lessons` (`lesson_id`);

--
-- Constraints for table `lessons`
--
ALTER TABLE `lessons`
  ADD CONSTRAINT `lessons_ibfk_1` FOREIGN KEY (`topic_id`) REFERENCES `topics` (`topic_id`);

--
-- Constraints for table `lesson_detail`
--
ALTER TABLE `lesson_detail`
  ADD CONSTRAINT `lesson_detail_ibfk_1` FOREIGN KEY (`level_id`) REFERENCES `levels` (`level_id`),
  ADD CONSTRAINT `lesson_detail_ibfk_2` FOREIGN KEY (`file_of_lesson_id`) REFERENCES `file_of_lesson` (`file_of_lesson_id`),
  ADD CONSTRAINT `lesson_detail_ibfk_3` FOREIGN KEY (`lesson_id`) REFERENCES `lessons` (`lesson_id`),
  ADD CONSTRAINT `lesson_detail_ibfk_4` FOREIGN KEY (`listword_id`) REFERENCES `listwords` (`listword_id`);

--
-- Constraints for table `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `logs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
