<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Learns Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Learns
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Lessons
 *
 * @method \App\Model\Entity\Learn get($primaryKey, $options = [])
 * @method \App\Model\Entity\Learn newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Learn[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Learn|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Learn patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Learn[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Learn findOrCreate($search, callable $callback = null, $options = [])
 */
class LearnsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('learns');
        $this->setDisplayField('learn_id');
        $this->setPrimaryKey('learn_id');

        $this->belongsTo('Learns', [
            'foreignKey' => 'learn_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Lessons', [
            'foreignKey' => 'lesson_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('level_name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['learn_id'], 'Learns'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['lesson_id'], 'Lessons'));

        return $rules;
    }
}
