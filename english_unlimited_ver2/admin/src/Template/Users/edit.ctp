<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">    <div class="row">
<div class="users index col-sm-12 col-lg-12 columns content">
<br><br>
<!-- <div class="users form large-9 medium-8 columns content"> -->
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Chỉnh sửa thông tin học viên') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('role_id', ['options' => $roles]);
            echo $this->Form->control('Tên');
            echo $this->Form->control('Họ');
            echo $this->Form->control('email');
            echo $this->Form->control('Mật Khẩu');
            echo $this->Form->control('Ngày Sinh', ['empty' => true]);
            echo $this->Form->control('Giới Tính');
            echo $this->Form->control('Nghề nghiệp');
            echo $this->Form->control('Hình ảnh');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Lưu thay đổi')) ?>
    <?= $this->Form->end() ?>
<!-- </div> -->
</div></div></div>