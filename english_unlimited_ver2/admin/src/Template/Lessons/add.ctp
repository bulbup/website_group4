<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <!-- <div class="row"> -->
<!-- <div class="users index col-sm-12 col-lg-12 columns content"> -->
<br><br><br>
    <?= $this->Form->create($lesson) ?>
    <fieldset>
        <legend><?= __('Thêm Bài Học') ?></legend>
        <?php
            echo $this->Form->control('topic_id', ['options' => $topics]);
            echo $this->Form->control('lesson_name');
            echo $this->Form->control('image');
        ?>

        <?= $this->Form->file('submittedfile', ['class' => 'form-control input-upload', 'style' => 'height:50px']) ?>
    </fieldset>
    <?= $this->Form->button(__('Thêm')) ?>
    <?= $this->Form->end() ?>

    
</div>
