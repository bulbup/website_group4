<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <!-- <div class="row"> -->
<!-- <div class="users index col-sm-12 col-lg-12 columns content"> -->
<br><br><br>
    <h3><?= h($lesson->lesson_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Lesson') ?></th>
            <td><?= $lesson->has('lesson') ? $this->Html->link($lesson->lesson->lesson_id, ['controller' => 'Lessons', 'action' => 'view', $lesson->lesson->lesson_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Topic') ?></th>
            <td><?= $lesson->has('topic') ? $this->Html->link($lesson->topic->topic_id, ['controller' => 'Topics', 'action' => 'view', $lesson->topic->topic_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lesson Name') ?></th>
            <td><?= h($lesson->lesson_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Image') ?></th>
            <td><?= h($lesson->image) ?></td>
        </tr>
    </table>
</div>
