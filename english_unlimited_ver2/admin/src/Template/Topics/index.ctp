<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <!-- <div class="row"> -->
<!-- <div class="users index col-sm-12 col-lg-12 columns content"> -->
<br><br><br>
    <h3><?= __('Topics') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('topic_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('topic_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('level_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('describe_short') ?></th>
                <th scope="col"><?= $this->Paginator->sort('image') ?></th>
                <th scope="col"><?= $this->Paginator->sort('create_time') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($topics as $topic): ?>
            <tr>
                <td><?= h($topic ->topic_id) ?></td>
                <td><?= h($topic->topic_name) ?></td>
                <td><?= h($topic->level_name) ?></td>
                <td><?= h($topic->describe_short) ?></td>
                <td><?= h($topic->image) ?></td>
                <td><?= h($topic->create_time) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $topic->topic_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $topic->topic_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $topic->topic_id], ['confirm' => __('Are you sure you want to delete # {0}?', $topic->topic_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
     <button class="btn btn-sm btn-success"><?= $this->Html->link(__('Tạo Chủ Đề'), ['action' => 'add']) ?></button>
</div>
