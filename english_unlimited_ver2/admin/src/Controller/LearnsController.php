<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Learns Controller
 *
 * @property \App\Model\Table\LearnsTable $Learns
 *
 * @method \App\Model\Entity\Learn[] paginate($object = null, array $settings = [])
 */
class LearnsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users','Lessons']
        ];
        $learns = $this->paginate($this->Users);

        $this->set(compact('learns'));
        $this->set('_serialize', ['learns']);
         // $this -> render('/Users/index');
          // $this->render('Users/view');
    }

    /**
     * View method
     *
     * @param string|null $id Learn id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $learn = $this->Learns->get($id, [
            'contain' => ['Users', 'Lessons']
        ]);

        $this->set('learn', $learn);
        $this->set('_serialize', ['learn']);
        // $this->render('index');
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $learn = $this->Learns->newEntity();
        if ($this->request->is('post')) {
            $learn = $this->Learns->patchEntity($learn, $this->request->getData());
            if ($this->Learns->save($learn)) {
                $this->Flash->success(__('The learn has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The learn could not be saved. Please, try again.'));
        }
        $learns = $this->Learns->Learns->find('list', ['limit' => 200]);
        $users = $this->Learns->Users->find('list', ['limit' => 200]);
        $lessons = $this->Learns->Lessons->find('list', ['limit' => 200]);
        $this->set(compact('learn', 'learns', 'users', 'lessons'));
        $this->set('_serialize', ['learn']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Learn id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $learn = $this->Learns->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $learn = $this->Learns->patchEntity($learn, $this->request->getData());
            if ($this->Learns->save($learn)) {
                $this->Flash->success(__('The learn has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The learn could not be saved. Please, try again.'));
        }
        $learns = $this->Learns->Learns->find('list', ['limit' => 200]);
        $users = $this->Learns->Users->find('list', ['limit' => 200]);
        $lessons = $this->Learns->Lessons->find('list', ['limit' => 200]);
        $this->set(compact('learn', 'learns', 'users', 'lessons'));
        $this->set('_serialize', ['learn']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Learn id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $learn = $this->Learns->get($id);
        if ($this->Learns->delete($learn)) {
            $this->Flash->success(__('The learn has been deleted.'));
        } else {
            $this->Flash->error(__('The learn could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
