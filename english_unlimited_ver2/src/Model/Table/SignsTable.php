<?php
// src/Model/Table/PostsTable.php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class SignsTable extends Table
{
    public function initialize(array $config)
    {
        $this->addBehavior('Timestamp');
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmpty('ho')
            ->notEmpty('ten')
            ->notEmpty('email')
            ->notEmpty('password');
        return $validator;
    }
}