<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * homes Controller
 *
 * @property \App\Model\Table\homesTable $homes
 *
 * @method \App\Model\Entity\home[] paginate($object = null, array $settings = [])
 */
class LearnsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users','Lessons']
        ];

        $learns = $this->paginate($this->Learns);

        // $learn = $this->Learns->get($id, [
        //     'contain' => []
        // ]);

        $this->set(compact('learns'));
        $this->set('_serialize', ['learns']);

        // $homes = $this->paginate($this->Homes);

        //$this->set(compact('learns'));
        // $this->set('_serialize', ['homes']);

    }



    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->viewBuilder()->layout('frontend_short');
        
    }

    /**
     * View method
     *
     * @param string|null $id Learn id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $learn = $this->Learns->get($id, [
            'contain' => ['Learns', 'Users', 'Lessons']
        ]);

        $this->set('learn', $learn);
        $this->set('_serialize', ['learn']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $learn = $this->Learns->newEntity();
        if ($this->request->is('post')) {
            $learn = $this->Learns->patchEntity($learn, $this->request->getData());
            if ($this->Learns->save($learn)) {
                $this->Flash->success(__('The learn has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The learn could not be saved. Please, try again.'));
        }
        $learns = $this->Learns->Learns->find('list', ['limit' => 200]);
        $users = $this->Learns->Users->find('list', ['limit' => 200]);
        $lessons = $this->Learns->Lessons->find('list', ['limit' => 200]);
        $this->set(compact('learn', 'learns', 'users', 'lessons'));
        $this->set('_serialize', ['learn']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Learn id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $learn = $this->Learns->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $learn = $this->Learns->patchEntity($learn, $this->request->getData());
            if ($this->Learns->save($learn)) {
                $this->Flash->success(__('The learn has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The learn could not be saved. Please, try again.'));
        }
        $learns = $this->Learns->Learns->find('list', ['limit' => 200]);
        $users = $this->Learns->Users->find('list', ['limit' => 200]);
        $lessons = $this->Learns->Lessons->find('list', ['limit' => 200]);
        $this->set(compact('learn', 'learns', 'users', 'lessons'));
        $this->set('_serialize', ['learn']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Learn id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // public function delete($id = null)
    // public function initialize()

    // {
    //     parent::initialize();

    //     $this->loadComponent('RequestHandler');
    //     $this->loadComponent('Flash');
    //       $this->viewBuilder()->layout('frontend_short');
    // }
    
  
 
}
