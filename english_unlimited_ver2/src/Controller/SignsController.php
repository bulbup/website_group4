<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * homes Controller
 *
 * @property \App\Model\Table\homesTable $homes
 *
 * @method \App\Model\Entity\home[] paginate($object = null, array $settings = [])
 */
class SignsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        // $homes = $this->paginate($this->Homes);

         //$this->set(compact('index'));
        // $this->set('_serialize', ['homes']);
    }
    // public function initialize()
    // {
    //     parent::initialize();

    //     $this->loadComponent('RequestHandler');
    //     $this->loadComponent('Flash');
    //       //$this->viewBuilder()->layout('frontend_short');
    //     $this->Auth->allow(['logout']);
    // }
//login

    public function initialize()
    {
        $this->loadComponent('Flash');
        // $this->loadComponent('Auth');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'signs',
                'action' => 'login'
            ],
            'unauthorizedRedirect' => $this->referer() // If unauthorized, return them to page they were just on
        ]);

        // Allow the display action so our pages controller
        // continues to work.

        $this->Auth->allow(['display','login','register']);
            // 18 Chapter 2. Quick Start Guide
            // CakePHP Cookbook Documentation, Release 3.4
    }
//.login

    public function login()
    {
       

        $user = $this->Auth->user('id');

        if (!empty($user)) {
            return $this->redirect('/Topics');
            
        }else{

            if ($this->request->is('post')) {

                $user = $this->Auth->identify(); // mã hóa dữ liệu

                if ($user) {

                    $this->Auth->setUser($user);
                    //return $this->redirect($this->Auth->redirectUrl());
                    return $this->redirect('/Topics');
                }
                //$this->Flash->error('Your username or password is incorrect.');
                    echo '<script> alert("Tài khoản hoặc mật khẩu không hợp lệ!") </script>';
            }
            //else {
            //     // $this->layout = 'login';
            //     //$this->render('/Signs/login');
            // }
        // $this->set('user',$user);
        }
    }

    public function register(){

       $user = $this->Signs->newEntity(); // tạo đối tượng $user với trường dữ liệu có trong csdl
        if ($this->request->is('post')) {
            $user = $this->Signs->patchEntity($user, $this->request->getData()); // đổ dữ liệu trên form vào đối tượng $user
            if ($this->Signs->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['controller' => 'homes', 'action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }


      public function userprofile(){
        $userId = $this->Auth->user('id');

        $user = $this->Signs->get($userId, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Signs->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }


    public function logout()
    {
        // $this->loadComponent('Auth');
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
        // return $this->redirect('/Signs/login');
    }

}
