<?php
namespace App\Controller;

use App\Controller\AppController;

class LessonsController extends AppController
{

    public function index($id = null, $bh = null)
    {

        $this->paginate = [
            'contain' => ['Topics','Lesson_detail','file']
            ];
        // $lessons = $this->paginate($this->Lessons);

        // $this->set(compact('lessons'));
        // $this->set('_serialize', ['lessons']);

        // ==================== //
            // $this->loadModel('lesson_detail');
            // $query = $this->Lessons->find('all')->contain(['Topics','lesson_detail']);
            $this->loadModel('Topics');
            $this->loadModel('lesson_detail');
            // $this->loadModel('file_of_lesson');
            $this->loadModel('file');

            $chuong = $this->Topics->get($id);
            //echo $chuong;
            $baihoc = $this->Lessons->get($bh);
            //echo $baihoc;

            $lesson = $this->Lessons->find('all');
            $topic = $this->Topics->find('all');
            $lesson_detail = $this->lesson_detail->find('all');
            $file = $this->file->find('all');
            // $file_of_lesson = $this->file_of_lesson->find('all');


            $this->set(compact('chuong'));
            $this->set(compact('baihoc'));
            $this->set(compact('lesson'));
            $this->set(compact('topic'));
            $this->set(compact('query'));
            $this->set(compact('lesson_detail'));
            // $this->set(compact('file_of_lesson'));
            $this->set(compact('file'));
        // ==================== //

        $data_read = $this->read_file($chuong->topic_id, $baihoc->lesson_id);
        $this->set('data_read', $data_read);

    }

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
          $this->viewBuilder()->layout('frontend_short');
    }
    

    /**
     * View method
     *
     * @param string|null $id Lesson id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $lesson = $this->Lessons->get($id, [
            'contain' => ['Topics']
        ]);

        $this->set('lesson', $lesson);
        $this->set('_serialize', ['lesson']);

        ///////////////////////
        // $data_read = $this->read_file();
        //$data_write = $this->readingcsv();
        // $this->set('data_read', $data_read);
        //$this->set('data_write', $data_read);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $lesson = $this->Lessons->newEntity();
        if ($this->request->is('post')) {
            $lesson = $this->Lessons->patchEntity($lesson, $this->request->getData());

            if ($this->Lessons->save($lesson)) {
                $this->Flash->success(__('The lesson has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The lesson could not be saved. Please, try again.'));
        }

        $lessons = $this->Lessons->Lessons->find('list', ['limit' => 200]);
        $topics = $this->Lessons->Topics->find('list', ['limit' => 200]);
        $this->set(compact('lesson', 'lessons', 'topics'));
        $this->set('_serialize', ['lesson']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Lesson id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $lesson = $this->Lessons->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $lesson = $this->Lessons->patchEntity($lesson, $this->request->getData());
            if ($this->Lessons->save($lesson)) {
                $this->Flash->success(__('The lesson has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The lesson could not be saved. Please, try again.'));
        }
        $lessons = $this->Lessons->Lessons->find('list', ['limit' => 200]);
        $topics = $this->Lessons->Topics->find('list', ['limit' => 200]);
        $this->set(compact('lesson', 'lessons', 'topics'));
        $this->set('_serialize', ['lesson']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Lesson id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $lesson = $this->Lessons->get($id);
        if ($this->Lessons->delete($lesson)) {
            $this->Flash->success(__('The lesson has been deleted.'));
        } else {
            $this->Flash->error(__('The lesson could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    /////////////////////////////////////////////////////////////////////
    public function read_file($chuong_id = null, $baihoc_id = null){
        $file_name = '';
        $count = 0;
        $data_read = array();
        echo $chuong_id.' - '.$baihoc_id;
        // lấy id lesson cứ so sánh với id lesson bên lesson detail và bắt bài học bằng cái so sánh id lesson với mã
        $this->loadModel('lessons');
        $lessons = $this->lessons->find('all');
        foreach ($lessons as $key => $value) {
            if(($baihoc_id == $value->lesson_id) && ($chuong_id == $value->topic_id)){ 
                // echo $value->lesson_id.' - ';
                // echo $value->topic_id.' - ';
                $this->loadModel('lesson_detail');
                $this->loadModel('file');
                // mã bằng với lesson_id thì sẽ tìm đc bài học sau đó dựa vào baihoc_id để xác định chính xác bài học nào ///$query = $articles->find('all', ['conditions' => ['Articles.title LIKE' => '%Ovens%']
                $lesson_detail = $this->lesson_detail->find('all');
                foreach ($lesson_detail as $key => $value_detail) {
                    $file_id = $this->file->find('all');
                    // echo $value_detail->file_id.' _ ';
                    if($value->lesson_id == $value_detail->lesson_id){
                        foreach ($file_id as $key => $value_file) {
                            if ($value_detail->file_id == $value_file->file_id) {
                                $file_name = $value_file->file_reading;
                                // echo $file_name;
                                // exit();
                                $file = fopen($file_name, 'r');
                                while (($emapData = fgetcsv($file, 10000,';')) !== false) {
                                    $count++;
                                    if($count > 1){
                                        $data_read[$count]['id'] = $emapData[0];
                                        $data_read[$count]['content'] = $emapData[1];
                                        $data_read[$count]['stt'] = $emapData[2];
                                        $data_read[$count]['file_id'] = $emapData[3];
                                        $data_read[$count]['answer'] = $emapData[4];
                                        
                                    }
                                }
                                return $data_read;
                            }
                        }
                    }
                }
                    
            }
                        
                // $file_id = $this->file->find('all',['conditions' => ['file_id' => $lesson_detail->file_id]);
         
        }

        //return iconv("Windows-1252", "UTF-8", $data_read);
    }






// ///////////
//     public function read_file($chuong_id = null){
//         $file_name = '';
//         $count = 0;
//         $data_read = array();
        
//         switch ($chuong_id) {
//             case '1':
//                 $file_name = 'files/reading/1giadinh/giadinh1.csv';
//                 break;
//             case '2':
//                 $file_name = 'files/reading/2thethao/thethao1.csv';
//                 break;
//             case '3':
//                 $file_name = 'files/read/family.csv';
//                 break;
//             case '4':
//                 $file_name = 'files/read/family.csv';
//                 break;
            
//             default:
//                 $file_name = 'files/read/1giadinh/giadinh1.csv';
//                 break;
//         }

//         // lấy id lesson cứ so sánh với id lesson bên lesson detail và bắt bài học bằng cái so sánh id lesson với mã
//         //$file_name = 'files/read/family_read1.csv';
// //         $lessons = $this->lessons->find('all');
// //         foreach ($lessons as $key => $value) {
// //             if(($baihoc_id == $value->Ma) && ($chuong_id == $value->topic_id)){ 
// //                 $this->loadModel('lesson_detail');
// //                 $this->loadModel('file');
// //                 $lesson_detail = $this->lesson_detail->find('all',['conditions' => ['lesson_id' => $baihoc_id]);// mã bằng với lesson_id thì sẽ tìm đc bài học sau đó dựa vào baihoc_id để xác định chính xác bài học nào ///$query = $articles->find('all', ['conditions' => ['Articles.title LIKE' => '%Ovens%']
// // ]);
// //                 $file = $this->file->find('all',['conditions' => ['file_id' => $lesson_detail->file_id]);

// //                 $file_name = $file->file_reading;
// //                 $file = fopen($file_name, 'r');
// //                 while (($emapData = fgetcsv($file, 10000,';')) !== false) {
// //                     $count++;
// //                     if($count > 1){
// //                         $data_read[$count]['id'] = $emapData[0];
// //                         $data_read[$count]['content'] = $emapData[1];
// //                         $data_read[$count]['file_id'] = $emapData[3];
// //                         $data_read[$count]['answer'] = $emapData[4];
// //                         $data_read[$count]['stt'] = $emapData[2];
// //                     }
// //                 }
// //                 return $data_read;

// //            }
// //        }

//         $file = fopen($file_name, 'r');
//         while (($emapData = fgetcsv($file, 10000,';')) !== false) {
//             $count++;
//             if($count > 1){
//                 $data_read[$count]['id'] = $emapData[0];
//                 $data_read[$count]['content'] = $emapData[1];
//                 $data_read[$count]['file_id'] = $emapData[3];
//                 $data_read[$count]['answer'] = $emapData[4];
//                 $data_read[$count]['stt'] = $emapData[2];
//             }
//         }
//         return $data_read;

//         //return iconv("Windows-1252", "UTF-8", $data_read);
//     }




}
