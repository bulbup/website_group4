<?php
namespace App\Controller;

use App\Controller\AppController;

class TopicsController extends AppController
{
    public function index()
    {
        $this->paginate = [
            'contain' => ['Lessons']
        ];
        
        $topics = $this->paginate($this->Topics);
        $topics = $this->search();
        if (empty($topics)) {
            $topics = $this->Topics->find("all");
        }
        
        $this->set(compact('topics'));
        $this->set('_serialize', ['topics']);
        
    }


    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
          $this->viewBuilder()->layout('frontend');
    }

    public function view($id = null)
    {
        $topic = $this->Topics->get($id, [
            'contain' => ['Lessons']
        ]);

        $this->set('topic', $topic);
        $this->set('_serialize', ['topic']);
    }

    public function add()
    {
        $topic = $this->Topics->newEntity();
        if ($this->request->is('post')) {
            $topic = $this->Topics->patchEntity($topic, $this->request->getData());
            if ($this->Topics->save($topic)) {
                $this->Flash->success(__('The topic has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The topic could not be saved. Please, try again.'));
        }
        $topics = $this->Topics->Topics->find('list', ['limit' => 200]);
        $this->set(compact('topic', 'topics'));
        $this->set('_serialize', ['topic']);
    }



    public function edit($id = null)
    {
        $topic = $this->Topics->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $topic = $this->Topics->patchEntity($topic, $this->request->getData());
            if ($this->Topics->save($topic)) {
                $this->Flash->success(__('The topic has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The topic could not be saved. Please, try again.'));
        }
        $topics = $this->Topics->Topics->find('list', ['limit' => 200]);
        $this->set(compact('topic', 'topics'));
        $this->set('_serialize', ['topic']);
    }

   

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $topic = $this->Topics->get($id);
        if ($this->Topics->delete($topic)) {
            $this->Flash->success(__('The topic has been deleted.'));
        } else {
            $this->Flash->error(__('The topic could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    public function search(){
        if($this->request->is('post')){
            $search = $this->request->getData('search');
            // $result = $this->Topics->find('all')->where(['Topics.topic_name like %' => $search]);
            $result = $this->Topics->find('all', array(
                    'conditions'=> array(
                        // 'or'=> array(
                                'topic_name like'=>'%'.$search.'%'
                        //         'Lessons.lesson_name'=>'%'.$search.'%'
                        //     ),
                        // 'join'=> array(
                        //         'table'=> 'Lessons',
                        //         'alias'=> 'lessons',
                        //         'conditions'=> 'Topics.topic_id = lessons.topic_id'
                        //     )
                        ),
                    )
                );
            
            return $result;
        }
    }


}
