<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $learn->learn_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $learn->learn_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Learns'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Learns'), ['controller' => 'Learns', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Learn'), ['controller' => 'Learns', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Lessons'), ['controller' => 'Lessons', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Lesson'), ['controller' => 'Lessons', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="learns form large-9 medium-8 columns content">
    <?= $this->Form->create($learn) ?>
    <fieldset>
        <legend><?= __('Edit Learn') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('lesson_id', ['options' => $lessons]);
            echo $this->Form->control('level_name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
