<?php
	use Cake\ORM\Query;
?>
<!-- tìm kiếm -->
<form method="post" name="frmsearch" action="<?= $this->url->build(['controller'=>'topics', 'action'=>'index']) ?>">
	<input type="text" name="search" placeholder="Bạn muốn tìm gì?">
	<button type="submit" name="btnsearch">Search</button>

</form> 
<!-- .tìm kiếm -->

<div id="fh5co-main">
	<div class="container">

		<div class="row" id="fh5co-features">

			<!-- Page Content -->
			<div id="page-content-wrapper">
				<div class="container">
					<div class="row">
						<?php
							foreach ($topics as $key => $topic) {
								
						?>
						<div class="col-sm-3">
							<div class="thumbnail" style="padding: 0">
								
								<!-- class="caption" -->
								<div align="center" class="caption" >
									<!-- <h2>Gia Đình</h2> -->
									<h2><?php echo h($topic->topic_name)?></h2>
									<!-- <p>My project description</p> -->
									<p><?php echo h($topic->describe_short)?></p>
									<!-- <a href="<?php //echo $this->url->build(['controller'=>'lessons','action'=>'index', $topic->topic_id, 1]) ?>"><button class="button"><span>Vào học </span></button></a> -->
									<a href="<?php echo $this->url->build(['controller'=>'lessons','action'=>'index', $topic->topic_id, 1]) ?>" class="button">Vào học</a>
								</div>
								<div style="padding:4px">
									<!-- <img alt="300x200" style="width: 100%" src="http://placehold.it/200x150"> -->
									<img alt="300x200" style="width: 100%; height: 200px;" src="<?php echo h($topic->image); ?>">
									<?php
										// echo $value->topic_id;
										//echo h($topic->topic_id)
									?>
								<div class="modal-footer" style="text-align: left">
									<div class="progress progress-striped active" style="background: #ddd">
										<div class="bar bar-warning" style="width: 60%;"></div>
									</div>
									
								</div>
							</div>
						</div>
						</div>
						<?php
							}
						?>
						
					</div>
				</div>
			</div>
			<!-- /#page-content-wrapper -->

			
			
				<!-- END row -->
		<div class="clearfix visible-md-block visible-lg-block"></div> 
		<div class="clearfix visible-md-block visible-lg-block"></div> 
		 	<br>
	 	<br>
	 		<br>
	 	<br>
		</div>
	</div>
</div>
		