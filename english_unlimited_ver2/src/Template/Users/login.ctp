    <title>Sign-Up/Login Form</title>
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <?= $this->Html->css('login/style') ?>

    <div class="form">
        <ul class="tab-group">
          <li class="tab"><a href="#signup">Đăng ký</a></li>
          <li class="tab active"><a href="#login">Đăng nhập</a></li>
        </ul>
    <div class="tab-content">
        <div id="login">
            <h1>Chào mừng bạn đến với EU</h1>
            <form action="<?php echo $this->url->build(['controller'=>'Users', 'action'=>'login']) ?>" method="post">
                <div class="field-wrap">
                    <label>
                      Email <span class="req">*</span>
                    </label>
                    <input type="email" name="email" required autocomplete="off"/>
                </div>
          
                <div class="field-wrap">
                    <label>
                      Mật khẩu<span class="req">*</span>
                    </label>
                    <input type="password" name="password" required autocomplete="off"/>
                </div>
          <!-- class="forgot"  -->
                <p class="tab forgot"><a href="#forgetpw" class="forget">Quên mật khẩu?</a></p>
          
                <button type="submit" class="button button-block"/>Đăng nhập</button>
            </form>
        </div>
       
        <div id="forgetpw" hidden>
            <h1>Vui lòng nhập email đã đăng ký</h1>
            <form action="<?php echo $this->url->build(['controller'=>'Users', 'action'=>'login']) ?>" method="post">
                <div class="field-wrap">
                    <label>
                      Email <span class="req">*</span>
                    </label>
                    <input type="email" name="email" required autocomplete="off"/>
                </div>
                <!-- <p class="tab forgot"><a href="#login">Quay lại?</a></p> -->
          
                <button type="submit" class="button button-block"/>OK</button>
            </form>
        </div>

        <div id="signup">   
            <h1>Hãy là thành viên của EU </h1>
            <form action="<?php echo $this->url->build(['controller'=>'Users', 'action'=>'register']) ?>" method="post">

                <div class="top-row">
                    <div class="field-wrap">
                        <label>
                            Họ <span class="req">*</span>
                        </label>
                        <input type="text" name="ho" required autocomplete="off" />
                    </div>

                    <div class="field-wrap">
                        <label>
                            Tên <span class="req">*</span>
                        </label>
                        <input type="text" name="ten" required autocomplete="off"/>
                    </div>
                </div>

                <div class="field-wrap">
                    <label>
                        Email <span class="req">*</span>
                    </label>
                    <input type="email" name="email" required autocomplete="off"/>
                </div>
              
                <div class="field-wrap">
                    <label>
                    Mật khẩu <span class="req">*</span>
                    </label>
                    <input type="password" name="password" required autocomplete="off"/>
                </div>
              
                <button type="submit" class="button button-block"/>Đăng ký</button>
          
            </form>
        </div>
<!--  -->

    </div><!-- tab-content -->

  </div> <!-- /form -->

  <script type="text/javascript">
     
    $(document).ready(function(){
        $(".forget").click(function(){
            $(".tab-group").html("<li class='tab active'><a style='width: 100% !important;' href='#'>Quên mật khẩu</a></li>");
        });
    });

  </script>

  <?= $this->Html->script('login/jquery.min') ?>
  <?= $this->Html->script('login/index') ?>
