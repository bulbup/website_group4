
    <title>Sign-Up/Login Form</title>
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <?= $this->Html->css('login/style') ?>
    <div class="form">
        <ul class="tab-group">
          <li class="tab active"><a href="#signup">Đăng ký</a></li>
          <li class="tab"><a href="#login">Đăng nhập</a></li>
        </ul>
    <div class="tab-content">
        
        <div id="signup">   
            <h1>Hãy là thành viên của EU</h1>
            <form action="<?php echo $this->url->build(['controller'=>'Users', 'action'=>'register']) ?>" method="post">
                
                <!-- <input type="text" name="role_id" value="nv" readonly="" /> -->
                
                <div class="top-row">
                    <div class="field-wrap">
                        <label>
                            Họ <span class="req">*</span>
                        </label>
                        <input type="text" name="last_name" required autocomplete="off" />
                    </div>

                    <div class="field-wrap">
                        <label>
                            Tên <span class="req">*</span>
                        </label>
                        <input type="text" name="first_name" required autocomplete="off"/>
                    </div>
                </div>

                <div class="field-wrap">
                    <label>
                        Email <span class="req">*</span>
                    </label>
                    <input type="email" name="email" required autocomplete="off"/>
                </div>
              
                <div class="field-wrap">
                    <label>
                    Mật khẩu <span class="req">*</span>
                    </label>
                    <input type="password" name="password" required autocomplete="off"/>
                </div>
              
                <button type="submit" class="button button-block"/>Đăng ký</button>
          
            </form>
        </div>
        <div id="login">
            <h1>Chào mừng bạn đến với EU</h1>
            <form action="<?php echo $this->url->build(['controller'=>'Users', 'action'=>'login']) ?>" method="post">
                <div class="field-wrap">
                    <label>
                      Email <span class="req">*</span>
                    </label>
                    <input type="email" name="email" required autocomplete="off"/>
                </div>
          
                <div class="field-wrap">
                    <label>
                      Mật khẩu<span class="req">*</span>
                    </label>
                    <input type="password" name="password" required autocomplete="off"/>
                </div>
          
                <p class="forgot"><a href="#">Quên mật khẩu?</a></p>
          
                <button type="submit" class="button button-block"/>Đăng nhập</button>
            </form>
        </div>

    </div><!-- tab-content -->

  </div> <!-- /form -->
  <?= $this->Html->script('login/jquery.min') ?>
  <?= $this->Html->script('login/index') ?>
