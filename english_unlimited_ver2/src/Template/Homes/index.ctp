
	<div id="fh5co-main">

		<div class="container">
			<div class="row" id="fh5co-features">
				<h2 class="dung" style="text-align: center;">4 Yếu tố học tốt tiếng anh</h2>
				<div class="fh5co-spacer fh5co-spacer-sm"></div>
				<div class="col-md-3 col-sm-3 col-xs-3 col-lg-3 text-center fh5co-feature feature-box">
					<div class="fh5co-feature-icon">
						<?= $this->Html->image('niemtin.png',['class'=>'img-responsive'])?>
					</div>
					<h3 class="heading">Niềm tin</h3>
					<p>Niềm tin là yếu tố quan trọng nhất giúp bạn thực hiện bất kì công việc gì trong mọi lĩnh vực.</p>
				</div>

				<div class="col-md-3 col-sm-3 col-xs-3 col-lg-3 text-center fh5co-feature feature-box"> 
					<div class="fh5co-feature-icon">
						<?= $this->Html->image('kehoa.png',['class'=>'img-responsive'])?>
					</div>
					<h3 class="heading">Kế họach</h3>
					<p>Liệt kê các công việc cần phải làm khi học tiếng anh, các mục tiêu cần hướng tới theo một trình tự nhất định và được thực hiện trong một khoảng thời gian nhất định, cụ thể. </p>
				</div>

				<!-- 	<div class="clearfix visible-sm-block"></div> -->

				<div class="col-md-3 col-sm-3 col-xs-3 col-lg-3 text-center fh5co-feature feature-box"> 
					<div class="fh5co-feature-icon">
						<!-- <i class="fa fa-video-camera"></i> -->
						<?= $this->Html->image('thuc hien.png',['class'=>'img-responsive'])?>
					</div>
					<h3 class="heading">Thực hiện</h3>
					<p> Nếu bạn đã hoàn thành được 2 bước đầu thì việc khó khăn cuối cùng chỉ là bạn hãy thực hiện và kiên trì với kế hoạch của mình. Bí quyết để thực hiện được việc khó khăn này là hãy luôn suy nghĩ tích cực và đặt niềm tin vào chính bản thân mình.</p>
				</div>

				<!-- <div class="clearfix visible-md-block visible-lg-block"></div> -->

				<div class="col-md-3 col-sm-3 col-xs-3 col-lg-3 text-center fh5co-feature feature-box">
					<div class="fh5co-feature-icon">
						<?= $this->Html->image('damme.png',['class'=>'img-responsive'])?>
					</div>
					<h3 class="heading">Động lực và đam mê</h3>
					<p>Để luôn giữ ngọn lửa đam mê trong mình hãy liệt kê hết những lý do bạn phải học tiếng anh. Hãy chọn ra một vài lý do mà bạn thực sự tâm đắc nhất để lấy đó làm kim chỉ nam cho những bước đi tiếp theo trên con đường chinh phục tiếng Anh. </p>
				</div>

				<div class="clearfix visible-sm-block"></div>

			<!-- 		<div class="col-md-4 col-sm-6 text-center fh5co-feature feature-box"> 
						<div class="fh5co-feature-icon">
							<i class="ti-palette"></i>
						</div>
						<h3 class="heading">Pallete</h3>
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste sunt porro delectus cum officia magnam.</p>
					</div>
					<div class="col-md-4 col-sm-6 text-center fh5co-feature feature-box"> 
						<div class="fh5co-feature-icon">
							<i class="ti-truck"></i>
						</div>
						<h3 class="heading">Deliver</h3>
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste sunt porro delectus cum officia magnam.</p>
					</div> -->
				</div>
				<!-- END row -->

				<div class="fh5co-spacer fh5co-spacer-md"></div>
				<!-- End Spacer -->

				<div class="row" id="fh5co-works" >
					<div class="col-md-8 col-md-offset-2 text-center fh5co-section-heading work-box">
						<h2 class="fh5co-lead">3 Bước học hiệu quả của<br> English Unlimited</h2>
						<!-- <p class="fh5co-sub">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit est facilis maiores, perspiciatis accusamus asperiores sint consequuntur debitis. Ut, dolores sit amet consectetur adipisicing elit.</p> -->
						<!-- <div class="fh5co-spacer fh5co-spacer-sm"></div> -->
					</div>
<!-- col-md-4 col-sm-6 col-xs-6 col-xxs-12 -->
					<div class="col-md-4 col-sm-12  text-center fh5co-work-item work-box">
						<div class="container">

							<blockquote class="quote-box">
								<p class="quotation-mark">
									“
								</p>
								<p class="quote-text">
									Bạn có thể lựa chọn các chủ đề để học theo sở thích và mong muốn. 
								</p>

								<hr>
								<div class="blog-post-actions">
									<p class="blog-post-bottom pull-left">
										Học theo chủ đề
									</p>
									<p class="blog-post-bottom pull-right">
										<!-- <span class="badge quote-badge">896</span>  ❤ -->
									</p>
								</div>
							</blockquote>
						</div>
					</div>
<!-- col-md-4 col-sm-6 col-xs-6 col-xxs-12 -->
					<div class="col-md-4 col-sm-12   fh5co-work-item work-box"> 
						<div class="container">
							<blockquote class="quote-box">
								<p class="quotation-mark">
									“
								</p>
								<p class="quote-text">
										Với mỗi bài học mà english unlimited đem lại có đầy đủ các kĩ năng: nghe, nói, đọc, viết. Mỗi kĩ năng được áp dụng phương pháp học thông minh như: mindmap, video, flashcard  
								</p>

								<hr>
								<div class="blog-post-actions">
									<p class="blog-post-bottom pull-left">
									Học theo Kỹ năng
									</p>
									<p class="blog-post-bottom pull-right">
										<!-- <span class="badge quote-badge">896</span>  ❤ -->
									</p>
								</div>
							</blockquote>
						</div>
					</div>

					<div class="clearfix visible-sm-block visible-xs-block"></div>
							
					<div class="col-md-4 col-sm-12  text-center fh5co-work-item work-box"> 
						<div class="container">
							<blockquote class="quote-box">
								<p class="quotation-mark">
									“
								</p>
								<p class="quote-text">
									Bài kiểm tra giúp đánh giá tiến độ của bạn.
So sánh trước và sau khi học một cách rõ ràng và cụ thể.
								</p>

								<hr>
								<div class="blog-post-actions">
									<p class="blog-post-bottom pull-left">
										Kiểm tra
									</p>
									<p class="blog-post-bottom pull-right">
										<!-- <span class="badge quote-badge">896</span>  ❤ -->
									</p>
								</div>
							</blockquote>
						</div>
					</div>

					<!-- <div class="clearfix visible-md-block visible-lg-block"></div> -->

				<div class="col-md-4 col-md-offset-4 text-center work-box">
						<p style="color: black"><a href="#" class="btn btn-outline btn-md">Học ngay thôi</a></p>
					</div>
				</div>
				<!-- END row -->


				

			</div>
			<!-- END container -->
		</div>
		<!-- END fhtco-main -->
<!-- ====================================about========================================= -->
<div class="container"  id="aboutt" >
				<!-- <div class="fh5co-spacer fh5co-spacer-md"></div> -->
				<h2 class="fh5co-lead" style="text-align: center; color: #f86942"><b>Quản Trị Của English Unlimited</b></h2>

  <div class="fh5co-spacer fh5co-spacer-sm"></div>
				<div class="row" >
	<div class="col-lg-3 col-sm-6">
			<div class="card hovercard">
				<div class="cardheader">

				</div>
				<div class="avatar">
					<!-- <img alt="" src="/webroot/img/tien.png"> -->
					<?= $this->Html->image('luan.jpg') ?>
				</div>
				<div class="info">
					<div class="title">
						<a target="_blank" href="http://scripteden.com/">Tho</a>
					</div>
					<div class="desc">Nguyễn Văn Luân</div>
					<div class="">Nhà Quản Trị</div>
					
				</div>
				<div class="bottom">
					
					<a class="btn btn-danger btn-sm" rel="publisher"
					   href="https://plus.google.com/+ahmshahnuralam">
						<i class="fa fa-google-plus"></i>
					</a>
					<a class="btn btn-primary btn-sm" rel="publisher"
					   href="https://plus.google.com/shahnuralam">
						<i class="fa fa-facebook"></i>
					</a>
					<a class="btn btn-warning btn-sm" rel="publisher" href="https://plus.google.com/shahnuralam">
						<i class="fa fa-envelope-o"></i>
					</a>
				</div>
			</div>

		</div>

	<div class="col-lg-3 col-sm-6">
			<div class="card hovercard">
				<div class="cardheader">

				</div>
				<div class="avatar">
					<?= $this->Html->image('dung.jpg') ?>
				</div>
				<div class="info">
					<div class="title">
						<a target="_blank" href="http://scripteden.com/">Dung</a>
					</div>
					<div class="desc">Trần Dương Ngọc Dung</div>
					<div class="">Nhà Quản Trị</div>
					
				</div>
				<div class="bottom">
				
					<a class="btn btn-danger btn-sm" rel="publisher"
					   href="https://plus.google.com/+ahmshahnuralam">
						<i class="fa fa-google-plus"></i>
					</a>
					<a class="btn btn-primary btn-sm" rel="publisher"
					   href="https://plus.google.com/shahnuralam">
						<i class="fa fa-facebook"></i>
					</a>
					<a class="btn btn-warning btn-sm" rel="publisher" href="https://plus.google.com/shahnuralam">
						<i class="fa fa-envelope-o"></i>
					</a>
				</div>
			</div>

		</div>

	<div class="col-lg-3 col-sm-6">
			<div class="card hovercard">
				<div class="cardheader">

				</div>
				<div class="avatar">
					 <?= $this->Html->image('tho.jpg') ?>
				</div>
				<div class="info">
					<div class="title">
						<a target="_blank" href="http://scripteden.com/">Luan</a>
					</div>
					<div class="desc">Nguyễn Thị Anh Thơ</div>
					<div class="">Nhà Quản Trị</div>
				
				</div>
				<div class="bottom">
					
					<a class="btn btn-danger btn-sm" rel="publisher"
					   href="https://plus.google.com/+ahmshahnuralam">
						<i class="fa fa-google-plus"></i>
					</a>
					<a class="btn btn-primary btn-sm" rel="publisher"
					   href="https://plus.google.com/shahnuralam">
						<i class="fa fa-facebook"></i>
					</a>
					<a class="btn btn-warning btn-sm" rel="publisher" href="https://plus.google.com/shahnuralam">
						<i class="fa fa-envelope-o"></i>
					</a>
				</div>
			</div>

		</div>

	<div class="col-lg-3 col-sm-6">
			<div class="card hovercard">
				<div class="cardheader">

				</div>
				<div class="avatar">
					<?= $this->Html->image('tien.jpg') ?>
				</div>
				<div class="info">
					<div class="title">
						<a target="_blank" href="http://scripteden.com/">Tien</a>
					</div>
					<div class="desc">Lê Thị Cẩm Tiên</div>
					<div class="">Nhà Quản Trị</div>
					
				</div>
				<div class="bottom">
					
					<a class="btn btn-danger btn-sm" rel="publisher"
					   href="https://plus.google.com/+ahmshahnuralam">
						<i class="fa fa-google-plus"></i>
					</a>
					<a class="btn btn-primary btn-sm" rel="publisher"
					   href="https://plus.google.com/shahnuralam">
						<i class="fa fa-facebook"></i>
					</a>
					<a class="btn btn-warning btn-sm" rel="publisher" href="https://plus.google.com/shahnuralam">
						<i class="fa fa-envelope-o"></i>
					</a>
				</div>
			</div>

		</div>

  </div>
</div>

			<!-- END row -->
			<div class="fh5co-spacer fh5co-spacer-md"></div>
				<!-- 
				///////////////////////////////////
				Features
				///////////////////////////////////
				-->
				
				<!-- END row -->

				<div class="fh5co-spacer fh5co-spacer-md"></div>
				<!-- End Spacer -->

				<!-- 
				///////////////////////////////////
				Portfolio 
				///////////////////////////////////
				-->
<div class="container">
				<div class="row" id="fh5co-works">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-section-heading work-box">
						<h2 class="fh5co-lead">Tại Sao Bạn Nên Học Tiếng Anh?</h2>
						<p class="fh5co-sub">Hình ảnh được chia sẻ bởi các thành viên của English Unlimited trong quá trình xây dựng cũng như nâng cấp Website cũng như những trải nghiệm của chúng tôi với các bài học tại English Unlimited</p>
						<div class="fh5co-spacer fh5co-spacer-sm"></div>
					</div>

<div class="container">
  <div class="row">
	<div class="col-sm-5">
	<h2>Tiếng Anh là một trong những ngôn ngữ dễ học nhất hành tinh</h2>
	<h3>Với hệ thống bảng chữ cái đơn giản; tiếng Anh là một trong số những ngôn ngữ dễ dàng học và sử dụng thành thạo. Hãy nhìn sang bản chữ cái của Nhật hay Hàn, bạn sẽ cảm thấy may mắn biết bao khi chúng không phải là ngôn ngữ toàn cầu!</h3></div>
	<div class="col-sm-7"> <?= $this->Html->image('book.png',['class'=>'img-responsive'])?></div>   
  </div>
<div class="fh5co-spacer fh5co-spacer-sm"></div>
<div class="row">
	<div class="col-sm-7"><?= $this->Html->image('Success.png',['class'=>'img-responsive'])?></div>   
	<div class="col-sm-5">
		<h2>Học tiếng Anh là để cải thiện bản thân, cải thiện cuộc sống và tương lai của chính mình.</h2>
		<h3>Học tiếng Anh cũng là điều kiện quan trọng để bạn có thể tiếp cận, cập nhật những nguồn tri thức từ khắp thế giới.Có tới hơn 1 tỷ trang Web sử dụng tiếng Anh. Thật kinh ngạc khi chỉ cần học một ngôn ngữ là có thể khai thác hầu hết kho tri thức ấy. </h3>
	</div>
	
  </div>
<div class="fh5co-spacer fh5co-spacer-sm"></div>
   <div class="row">
	<div class="col-sm-6">
	<h2>Giúp bạn năng động trong môi trường xã hội, tự tin trong giao tiếp</h2>
	<h3>Để không trở thành một vị khách, một người bạn “không thể giao tiếp”, bạn cần phải học để sử dụng tiếng Anh. Nhất là khi Việt Nam, quốc gia của chúng ta đã gia nhập kinh tế thế giới, bạn sẽ không muốn mình phải lắc đầu lắc tay khi một người bạn nước ngoài tươi cười hỏi đường bạn chứ?</h3> </div>
	<div class="col-sm-6"><?= $this->Html->image('Communication.png',['class'=>'img-responsive'])?>	</div>   
  </div>
<div class="fh5co-spacer fh5co-spacer-sm"></div>
  <div class="row">
	  <div class="col-sm-6"><?= $this->Html->image('happy2.png',['class'=>'img-responsive'])?></div> 
	<div class="col-sm-6"><h2>Cảm giác hài lòng</h2>
	<h3>Cảm giác tiến bộ thật tuyệt. Bạn sẽ không bao giờ quên được giây phút phát hiện ra mình có thể nói chuyện với người Mỹ hoặc xem các kênh tivi tiếng Anh</h3>
	</div>
  
  </div>

</div>

					
				</div>
				<!-- END row -->

			</div>
			<!-- END container -->
			
<div class="container" id="contact">		
			<div class="row">
					<div class="col-md-12 animate-box">
						<h2 class="fh5co-lead" style="text-align: center; color: #f86942"><b>Bạn Có Thể Liên Hệ Với English Unlimited</b></h2>
						
						<div class="fh5co-spacer fh5co-spacer-sm"></div>

						<div id="fh5co-tab-feature-center" class="fh5co-tab text-center">
							<ul class="resp-tabs-list hor_1">
								<li><i class="fh5co-tab-menu-icon fa  fa-phone" style="font-size:24px;"></i>Liên Hệ</li>
								<li><i class="fh5co-tab-menu-icon fa fa-home"  style="font-size:24px;"></i>Địa Chỉ</li>
								<li><i class="fh5co-tab-menu-icon fa fa-map-marker" style="font-size:24px;"></i> Bản Đồ</li>
							</ul>
							<div class="resp-tabs-container hor_1">
								<div>
									<div class="row">
										
										<div class="col-md-12">
										<!-- form contact -->
										


<div class="container">
			<div class="row">
				<div class="col-sm-8 col-centered">
					<div class="panel panel-default">
						<div class="panel-heading" >
							<h1>Thông Tin</h1>
						</div>
					<form action="" method="POST">
					<div class="panel-body">
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user blue"></i></span>
								<input type="text" name="InputName" placeholder="Họ Tên" class="form-control"  required>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope blue"></i></span>
								<input type="email" name="InputEmail" placeholder="Email" class="form-control" required>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-phone blue"></i></span>
								<input type="tex" name="InputCno" placeholder="Số điện thoại" class="form-control" required>
							</div>
						</div>
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-comment blue"></i></span>
								<textarea name="InputMessage" rows="6" class="form-control" type="text" required></textarea>
							</div>
						</div>
						<div class="">
						<button type="submit" class="btn btn-info pull-right">Gửi <span class="fa fa-send"></span></button>
							<button type="reset" value="Reset" name="reset" class="btn">Làm lại <span class="fa fa-refresh"></span></button>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
		
		
										<!-- .form contact -->
										</div>
										
									</div>
								</div>
								<div>
									<div class="row">
										<div class="col-md-3" ></div>
										<div class="col-md-3" >
											 <form>
											<legend><span class="fa fa-globe"></span>Địa Chỉ</legend>
											<address>
												<strong>Đại học Cần Thơ</strong><br>
												Đường 3/2, quận Ninh Kiều<br>
												Thành phố Cần Thơ<br>
												<abbr title="Phone">
													P:</abbr>
												0989 999 999
											</address>
											<address>
												<strong>Địa chỉ email</strong><br>
												<a href="mailto:#" style="color: #3fb0ac">LTDT@englishunlimited.com</a>
											</address>
											</form>
										</div>
										<div class="col-md-6" >
										<?= $this->Html->image('Can-tho.gif',['class'=>'img-responsive'])?>
										
										</div>
									</div>
								</div>
								<div>
									<div class="row">
										
										<div class="col-md-6 col-centered" >
										<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3928.8289571160703!2d105.7667154140422!3d10.030969375254312!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31a0883d0dac6b15%3A0xf6ae5b1bd18625!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBD4bqnbiBUaMah!5e0!3m2!1svi!2s!4v1498103427630" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
										</div>	
										
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<hr class="fh5co-spacer fh5co-spacer-sm">

					
					
					<!-- END Tabs -->

				<!-- END .row -->

				<!-- <div class="fh5co-spacer fh5co-spacer-md"></div> -->
				
</div>
</div>
				
			<!-- END row -->
<div class="container">
				
				<div class="row">
					<!-- Start Slider Testimonial -->
					<h2 class="fh5co-uppercase-heading-sm text-center animate-box">Châm ngôn của chúng tôi</h2>
					<div class="fh5co-spacer fh5co-spacer-xs"></div>
					<div class="owl-carousel-fullwidth animate-box">
						<div class="item">
							<p class="text-center quote" style="color: black">&ldquo;Life is 10% what happens to you and 90% how you respond to it &rdquo; <cite class="author">&mdash; Lou Holtz</cite></p>
						</div>
						<div class="item">
							<p class="text-center quote " style="color: black">&ldquo;Never give up. Today is hard, tomorrow will be worse, but the day after tomorrow will be sunshine. &rdquo;<cite class="author">&mdash;  JACK MA</cite></p>
						</div>
						<div class="item">
							<p class="text-center quote" style="color: black">&ldquo;Success is only meaningful and enjoynable if it fells like your own&rdquo;<cite class="author">&mdash; Michelle Obama</cite></p>
						</div>
					</div>
					<!-- End Slider Testimonial -->
				</div>
				</div>
				<!-- END row -->
				<div class="fh5co-spacer fh5co-spacer-md"></div>
				<div class="fh5co-spacer fh5co-spacer-md"></div>
