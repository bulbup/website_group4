<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
header("content-type:application/csv;charset=UTF-8");
header('Content-Type: text/xml, charset=UTF-8; encoding=UTF-8');

?>


<div class="container">    
    <div class="container-fluid">
        <div class="row">
            <!-- cot trai -->
            <div class="col-sm-3" style="background-color:white;" id="fh5co-sidebar">

                <div class="panel-group" id="accordion">
                    <?php 
                    foreach ($topic as $value_topic) {
                    ?>    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $value_topic->topic_id; ?>"><?php echo h($value_topic->topic_name) ?></a>
                            </h4>
                        </div>
                        <div id="collapse<?php echo $value_topic->topic_id; ?>" class="panel-collapse collapse <?php if($chuong->topic_id == $value_topic->topic_id ){ echo 'in'; }?>">
                            <div class="panel-body">
                                <table class="table">
                                <?php 
                                    foreach ($lesson as $value_lesson) {
                                        if($value_topic->topic_id == $value_lesson->topic_id){

                                ?>
                                    <tr>
                                        <td>
                                            <a href="<?php echo $this->url->build(['controller'=>'lessons', 'action'=>'index', $value_topic->topic_id, $value_lesson->lesson_id]) ?>" style="color: black;"><h5><?php echo h($value_lesson->lesson_name) ?></h5></a>
                                            <!-- <a href="<?php //echo $this->url->build(['controller'=>'lessons', 'action'=>'index', $value_topic->topic_id, $value_lesson->Ma]) ?>" style="color: black;"><h5><?php //echo h($value_lesson->lesson_name) ?></h5></a> -->
                                        </td>
                                    </tr>

                                    <?php
                                        }
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php 
                            
                        }

                     ?>
                    
                </div>
            </div>
            <!-- end cot trai -->

<!-- ////// -->
<!-- cot phai -->
            <div class="col-sm-9" style="background-color:#f8f8f8;"> 
                <div class="panel panel-default" style="margin-top: 2%; margin-bottom: 2%">
                    <div class="panel-heading">
                        <h5 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            <h3>Chủ đề: <?php 
                                    echo $chuong->topic_name.' ';
                                    foreach ($lesson as $key => $value) {
                                        if(($baihoc->lesson_id == $value->Ma) && ($chuong->topic_id == $value->topic_id)){ 
                                            echo ' '.$value->lesson_name;
                                        }
                                    }
                                    

                             ?>
                                
                            </h3></a>
                        </h5>
                    </div>
                    <?php
                    foreach ($lesson as $key => $value) {
                        if(($baihoc->lesson_id == $value->lesson_id) && ($chuong->topic_id == $value->topic_id)){ 
                    ?>
                            <img src="<?php echo $value->image ?>" style="width: 100%;" />
                    <?php
                        }
                    }
                    ?>
                    
                    
                    <p class="loading">

                        <em><img src="http://www.ifmo.ru/images/loader.gif" style="width: 3%;" alt="Initializing audio"> Loading audio…
                        <?php //$this->Html->image('loader.gif',['class'=>'img-responsive'])?></em>
                    </p>

                    <p class="passage-audio" >
                        <!--  <audio controls id="passage-audio" class="passage">
                            <source src="../media/Luke.2.1-Luke.2.20.mp3" type="audio/mp3">
                            <source src="../media/Luke.2.1-Luke.2.20.ogg" type="audio/ogg"> 
                            <source src="../media/Luke.2.1-Luke.2.20.wav" type="audio/wav"> 
                                <em class="error"><strong>Error:</strong> Your browser doesn't appear to support HTML5 Audio.</em> 
                        </audio>  -->
                        <?=  $this->Html->media(['audio/Luke.2.1-Luke.2.20.mp3'],['controls']); ?>
       
                    </p>
                    <p class="passage-audio-unavailable" hidden>
                        <em class="error"><strong>Error:</strong> You will not be able to do the read-along audio because your browser is not able to play MP3, Ogg, or WAV audio formats.</em>
                    </p>

                    <p class="playback-rate" hidden title="Note that increaseing the reading rate will decrease accuracy of word highlights">
                        <label for="playback-rate">Reading rate:</label>
                            <input id="playback-rate" type="range" min="0.5" max="2.0" value="1.0" step="0.1" style="width: 25% !important;" disabled onchange='this.nextElementSibling.textContent = String(Math.round(this.valueAsNumber * 10) / 10) + "\u00D7";'> <!-- Chỗ tính này là seo??? -->
                                    <output>1&times;</output>
                    </p>
                    <p class="playback-rate-unavailable" hidden>
                        <em>(It seems your browser does not support <code>HTMLMediaElement.playbackRate</code>, so you will not be able to change the speech rate.)</em>
                    </p>
                    <p class="autofocus-current-word" hidden>
                                    <input type="checkbox" id="autofocus-current-word" checked>
                                    <label for="autofocus-current-word">Auto-focus/auto-scroll</label>
                    </p>

                    <noscript>
                        <p class="error"><em><strong>Notice:</strong> You must have JavaScript enabled/available to try this HTML5 Audio read along.</em></p>
                    </noscript>

                    <div id="passage-text" class="passage">


                        <h3 class="section-heading">The Birth of Jesus Christ</h3>

                <!-- bắt đầu ở giây thứ 'data-begin' và kéo dài trong 'data-dur' giây, từ sau sẽ bắt đầu ở giây 'data-begin'+'data-dur' của từ trước đó
                
            -->
            <?php foreach ($file as $key => $value) {
                echo 'abc'.$value['file_reading'];
            } ?>
            <p><sup class="verse-start">1</sup><span data-dur="0.154" data-begin="0.775">In</span> <span data-dur="0.28" data-begin="0.929">those</span> <span data-dur="0.29" data-begin="1.218">days</span> <span data-dur="0.131" data-begin="1.508">a</span> <span data-dur="0.525" data-begin="1.639">decree</span> <span data-dur="0.191" data-begin="2.165">went</span> <span data-dur="0.225" data-begin="2.355">out</span> <span data-dur="0.245" data-begin="2.583">from</span> <span data-dur="0.438" data-begin="2.828">Caesar</span> <span data-dur="0.637" data-begin="3.267">Augustus</span> <span data-dur="0.166" data-begin="4.03">that</span> <span data-dur="0.268" data-begin="4.216">all</span> <span data-dur="0.111" data-begin="4.486">the</span> <span data-dur="0.411" data-begin="4.594">world</span> <span data-dur="0.205" data-begin="5.006">should</span> <span data-dur="0.134" data-begin="5.211">be</span> <span data-dur="0.529" data-begin="5.344">registered</span>. <sup class="verse-start">2</sup><span data-dur="0.201" data-begin="6.675">This</span> <span data-dur="0.124" data-begin="6.876">was</span> <span data-dur="0.11" data-begin="7">the</span> <span data-dur="0.321" data-begin="7.11">first</span> <span data-dur="0.762" data-begin="7.431">registration</span> <span data-dur="0.164" data-begin="8.193">when</span> <span data-dur="0.474" data-begin="8.357">Quirinius</span> <span data-dur="0.206" data-begin="8.834">was</span> <span data-dur="0.338" data-begin="9.041">governor</span> <span data-dur="0.082" data-begin="9.379">of</span> <span data-dur="0.477" data-begin="9.46">Syria</span>. <sup class="verse-start">3</sup><span data-dur="0.119" data-begin="10.676">And</span> <span data-dur="0.24" data-begin="10.794">all</span> <span data-dur="0.186" data-begin="11.034">went</span> <span data-dur="0.087" data-begin="11.22">to</span> <span data-dur="0.139" data-begin="11.307">be</span> <span data-dur="0.592" data-begin="11.446">registered</span>, <span data-dur="0.251" data-begin="12.284">each</span> <span data-dur="0.093" data-begin="12.572">to</span> <span data-dur="0.134" data-begin="12.665">his</span> <span data-dur="0.275" data-begin="12.799">own</span> <span data-dur="0.467" data-begin="13.074">town</span>. <sup class="verse-start">4</sup><span data-dur="0.184" data-begin="14.369">And</span> <span data-dur="0.358" data-begin="14.553">Joseph</span> <span data-dur="0.351" data-begin="14.911">also</span> <span data-dur="0.128" data-begin="15.262">went</span> <span data-dur="0.152" data-begin="15.39">up</span> <span data-dur="0.215" data-begin="15.595">from</span> <span data-dur="0.541" data-begin="15.811">Galilee</span>, <span data-dur="0.074" data-begin="16.557">from</span> <span data-dur="0.121" data-begin="16.632">the</span> <span data-dur="0.236" data-begin="16.752">town</span> <span data-dur="0.097" data-begin="16.988">of</span> <span data-dur="0.559" data-begin="17.085">Nazareth</span>, <span data-dur="0.154" data-begin="17.966">to</span> <span data-dur="0.575" data-begin="18.12">Judea</span>, <span data-dur="0.129" data-begin="18.823">to</span> <span data-dur="0.059" data-begin="18.952">the</span> <span data-dur="0.31" data-begin="19.011">city</span> <span data-dur="0.166" data-begin="19.321">of</span> <span data-dur="0.393" data-begin="19.487">David</span>, <span data-dur="0.161" data-begin="20.029">which</span> <span data-dur="0.109" data-begin="20.19">is</span> <span data-dur="0.307" data-begin="20.321">called</span> <span data-dur="0.642" data-begin="20.628">Bethlehem</span>, <span data-dur="0.317" data-begin="21.76">because</span> <span data-dur="0.116" data-begin="22.077">he</span> <span data-dur="0.104" data-begin="22.193">was</span> <span data-dur="0.166" data-begin="22.297">of</span> <span data-dur="0.059" data-begin="22.463">the</span> <span data-dur="0.412" data-begin="22.522">house</span> <span data-dur="0.155" data-begin="22.935">and</span> <span data-dur="0.384" data-begin="23.09">lineage</span> <span data-dur="0.175" data-begin="23.474">of</span> <span data-dur="0.421" data-begin="23.648">David</span>, <sup class="verse-start">5</sup><span data-dur="0.127" data-begin="24.714">to</span> <span data-dur="0.172" data-begin="24.84">be</span> <span data-dur="0.53" data-begin="25.013">registered</span> <span data-dur="0.125" data-begin="25.543">with</span> <span data-dur="0.515" data-begin="25.668">Mary</span>, <span data-dur="0.172" data-begin="26.183">his</span> <span data-dur="0.607" data-begin="26.355">betrothed</span>, <span data-dur="0.123" data-begin="27.134">who</span> <span data-dur="0.166" data-begin="27.257">was</span> <span data-dur="0.167" data-begin="27.423">with</span> <span data-dur="0.513" data-begin="27.59">child</span>. <sup class="verse-start">6</sup><span data-dur="0.161" data-begin="29.448">And</span> <span data-dur="0.362" data-begin="29.609">while</span> <span data-dur="0.159" data-begin="29.97">they</span> <span data-dur="0.166" data-begin="30.129">were</span> <span data-dur="0.436" data-begin="30.295">there</span>, <span data-dur="0.159" data-begin="31.072">the</span> <span data-dur="0.431" data-begin="31.231">time</span> <span data-dur="0.277" data-begin="31.662">came</span> <span data-dur="0.161" data-begin="31.939">for</span> <span data-dur="0.093" data-begin="32.1">her</span> <span data-dur="0.107" data-begin="32.193">to</span> <span data-dur="0.233" data-begin="32.299">give</span> <span data-dur="0.352" data-begin="32.522">birth</span>. <sup class="verse-start">7</sup><span data-dur="0.133" data-begin="33.972">And</span> <span data-dur="0.213" data-begin="34.105">she</span> <span data-dur="0.277" data-begin="34.318">gave</span> <span data-dur="0.253" data-begin="34.596">birth</span> <span data-dur="0.069" data-begin="34.888">to</span> <span data-dur="0.171" data-begin="34.957">her</span> <span data-dur="0.602" data-begin="35.128">firstborn</span> <span data-dur="0.56" data-begin="35.73">son</span> <span data-dur="0.166" data-begin="36.491">and</span> <span data-dur="0.342" data-begin="36.657">wrapped</span> <span data-dur="0.153" data-begin="36.998">him</span> <span data-dur="0.119" data-begin="37.152">in</span> <span data-dur="0.55" data-begin="37.271">swaddling</span> <span data-dur="0.542" data-begin="37.82">cloths</span> <span data-dur="0.154" data-begin="38.644">and</span> <span data-dur="0.287" data-begin="38.798">laid</span> <span data-dur="0.176" data-begin="39.085">him</span> <span data-dur="0.087" data-begin="39.261">in</span> <span data-dur="0.092" data-begin="39.348">a</span> <span data-dur="0.604" data-begin="39.44">manger</span>, <span data-dur="0.277" data-begin="40.182">because</span> <span data-dur="0.131" data-begin="40.46">there</span> <span data-dur="0.151" data-begin="40.591">was</span> <span data-dur="0.213" data-begin="40.742">no</span> <span data-dur="0.312" data-begin="40.975">place</span> <span data-dur="0.121" data-begin="41.287">for</span> <span data-dur="0.158" data-begin="41.408">them</span> <span data-dur="0.116" data-begin="41.566">in</span> <span data-dur="0.111" data-begin="41.683">the</span> <span data-dur="0.406" data-begin="41.794">inn</span>.</p>
            <?php

                foreach ($data_read as $key => $value) {
                    //if ($value['id'] == $baihoc->lesson_id) {
                       
            ?>
                    <p>

                        <?php //echo $value['id'] ?>
                        <?php echo $value['content'] ?>
                        <br>
                        <?php echo $value['answer'] ?>
                        <br>
                        
                    </p>
            <?php
                    //}
                }
            ?>

        </div>
    </div> <!-- end panel default -->


    <div class="panel panel-default"><!-- panel default 2 -->
        <div class="panel-heading">
            <h5 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion2" href="#collapse"><h3>Hãy chọn từ bạn thích học để tạo sơ đồ tư duy của riêng bạn nhé!</h3></a>
            </h5>
        </div>
        <!-- TEXT DE CHON -->
         <div class="row"> 
            <div class="col-md-4" style="border-right: 2px solid #797979; color: black !important">
                Từ đã chọn
                <textarea name="txtMessage" id="txtMessage" class="txtMessage txtDropTarget" style="width: 100%; height: 350px; color: black !important;"></textarea>
            </div>
            <div class="col-md-8">

                <!-- Map -->
                <!-- <img src="img/mindmap1.png" class="img-responsive" alt="Planets" usemap="#planetmap"  style=""> -->
                <?= $this->Html->image('mindmap1.png',['class'=>'img-responsive', 'usemap'=>'planetmap', 'alt'=>'Planets']) ?>

                <map name="planetmap" >

                    <div style="position: absolute; top: 180px; left: 50px;" data-toggle="popover" title="User Info">
                        <a data-placement="top" data-content="Content">
                            <area shape="poly" coords="38,148,56,145,66,155,82,157,96,166,117,173,121,188,121,194,121,203,121,216,121,222,104,230,91,239,79,243,69,238,63,234,49,237,33,231,23,228,16,225,14,214,7,209,-1,195,15,172,15,169">sister</a>
                        </div>


                        <div  style="position: absolute; top: 80px; left: 110px;" data-toggle="popover" title="User Info"> 
                            <a data-placement="top" data-content="Content" >
                                <area shape="poly" coords="98,53,110,39,129,44,135,46,148,48,159,53,166,58,182,63,185,73,190,85,183,91,190,102,189,109,181,120,174,123,167,123,152,135,137,132,130,126,119,133,106,127,99,122,85,120,79,113,77,104,69,94,74,79,79,61">father</a>
                            </div>

                            <div style="position: absolute; top: 40px; left:270px;" data-toggle="popover" title="User Info">
                                <a  data-placement="top" data-content="Content">
                                    <area shape="poly" coords="294,80,281,87,263,78,246,68,235,52,242,37,256,16,273,6,284,4,294,9,310,11,323,18,340,22,348,31,354,41,353,63,344,77,316,90,304,86">Daughter</a>
                                </div>

                                <div style="position: absolute; top: 115px; left:420px;" data-toggle="popover" title="User Info">
                                    <a  data-placement="top" data-content="Content">
                                        <area shape="poly" coords="438,158,412,155,396,151,386,130,377,118,388,107,396,94,413,86,430,76,443,84,458,90,474,94,488,101,496,111,496,117,494,129,499,135,497,144,488,149,462,162">brother</a>
                                    </div>
                                                             
<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="popover"]').popover({
            placement : 'top',
            trigger : 'hover',
            html : true,
            content : '<div class="media"><a href="#" class="pull-left"><?= $this->Html->image('cd_gia_dinh-brother.jpg',['class'=>'img-responsive media-object'])?></a><div class="media-body"><h4 class="media-heading">Jhon Carter</h4><p>Excellent Bootstrap popover! I really love it.</p></div></div>'
        });
    });
</script>
 <!-- <div class="bs-example">
        <button type="button" class="btn btn-primary" data-toggle="popover">Popover without Title</button>
        <button type="button" class="btn btn-primary" data-toggle="popover" title="User Info">Popover with Title</button>
    </div> -->


    <div style="position: absolute; top: 250px; left:400px;" data-toggle="popover" title="User Info">
        <a  data-placement="top" data-content="Content">
            <area shape="poly" coords="380,238,397,226,416,234,435,236,447,241,465,249,468,259,468,274,458,291,438,295,425,297,409,290,399,296,382,291,370,283,361,274,357,266,367,245"> mother</a>
        </div>


        <div style="position: absolute; top: 300px; left:180px;" data-toggle="popover" title="User Info">
            <a data-placement="top" data-content="Content">
                <area shape="poly" coords="160,263,177,255,203,262,222,263,238,273,256,284,261,293,261,303,263,319,260,324,251,331,238,335,227,343,218,347,195,336,174,341,164,334,147,328,140,316,134,309,132,296,144,277">son</a>
            </div>

        </map>

        <!-- end map -->
    </div>
</div>



</div>

<!-- end panel default 2 -->


<!-- speaking -->
<div class="panel panel-default">
<?php
 // foreach ($query as $key => $value) {
 //    echo $value->file_of_lesson_id;
 //     # code...
 // }
?>
    <div class="panel-heading">
        <h5 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion2" href="#collapse"><h3>Speaking</h3></a>
        </h5>
    </div>
    <div class="row">
        <div class="col-sm-7">
            <!--  
            <div class="cauhoi">
                <?php //$this->Html->image('speaker-icon.png',['onclick'=>"play()"]); ?>
                <?php //$this->Html->media('audio/what-is-your-name.mp3',['id'=>'audio'])?>
                
                <?php //echo $this->Html->image('microphone-icon.png'); ?>
                <input type="text" x-webkit-speech>
                <?php //echo $this->Html->image('false.jpg'); ?>
            </div>
         

            <div class="cauhoi">
                <?php //$this->Html->image('speaker-icon.png',['onclick'=>"play()"]); ?>
                <?php //$this->Html->media('audio/what-is-your-name.mp3',['id'=>'audio'])?>

                <?php //echo $this->Html->image('microphone-icon.png'); ?>
                <input type="text" x-webkit-speech>
                <?php //echo $this->Html->image('false.jpg'); ?>
            </div>

            <div class="cauhoi">
                <?php //$this->Html->image('speaker-icon.png',['onclick'=>"play()"]); ?>
                <?php //$this->Html->media('audio/what-is-your-name.mp3',['id'=>'audio'])?>

                <?php //echo $this->Html->image('microphone-icon.png'); ?>
                <input type="text" x-webkit-speech>
                <?php //echo $this->Html->image('true.jpg'); ?>
                
            </div>
            -->
            <?php
                $count = 0;
                foreach ($data_read as $key => $value) {
                    $count++;
                    
            ?>
            <!-- cau noi 4 -->
            <div class="cauhoi">
                <?php //$this->Html->image('speaker-icon.png',['onclick'=>"play()"]); ?>
                <button onclick="play(<?php echo $count; ?>)" class="fa fa-headphones" style="background-color: #fff !important; "></button>

                <?php //$this->Html->media('audio/what-is-your-name.mp3',['id'=>'audio'])?>
                <?= $this->Html->media($value['file_id'],['id'=>$count])?>

                <!-- <button onclick="startConverting(<?php //echo $count; ?>);setTimeout(check(<?php //echo $count; ?>), 10000);" class="fa fa-microphone btn-record" style="background-color: #fff !important;"></button> -->
                <button onclick="startConverting(<?php echo $count; ?>);" class="fa fa-microphone btn-record" style="background-color: #fff !important;"></button>

                <?php //echo $this->Html->image('microphone-icon.png'); ?>
                <!-- <input type="text" x-webkit-speech>            -->
                <textarea class="txtResult" id="result<?php echo $count; ?>" style="height: 40px; width: 250px"></textarea>
                <input type="hidden" value="<?php echo $count; ?>" class="stt">
                <!-- <?php //echo $this->Html->image('false.jpg'); ?> -->
                <div class="true">
                    
                </div>
            </div>
            <!-- end cot 1 -->
            <!-- end cau noi 4 -->


            <?php
                }
            ?>
                       <!-- audio -->
<script>
    function play(id){
     var audio = document.getElementById(id);
     audio.play();
 }
</script>


<!-- ghi âm giọng nói -->
        <script type="text/javascript">

            //var r = document.getElementById('result'+<?php //echo $count; ?>);

            function startConverting (id) {
                //alert(id);
                var r = document.getElementById('result'+id);
                if('webkitSpeechRecognition' in window){
                    var speechRecognizer = new webkitSpeechRecognition();
                    speechRecognizer.continuous = true;
                    speechRecognizer.interimResults = true;
                    speechRecognizer.lang = 'en-IN';
                    speechRecognizer.start();

                    var finalTranscripts = '';

                    speechRecognizer.onresult = function(event){
                        var interimTranscripts = '';
                        for(var i = event.resultIndex; i < event.results.length; i++){
                            var transcript = event.results[i][0].transcript;
                            transcript.replace("\n", "<br>");
                            if(event.results[i].isFinal){
                                finalTranscripts += transcript;
                            }else{
                                interimTranscripts += transcript;
                            }
                        }
                        // r.innerHTML = finalTranscripts + '<span style="color:#999">' + interimTranscripts + '</span>';
                        r.innerHTML = finalTranscripts + interimTranscripts;
                        
                    };

                    
                    speechRecognizer.onerror = function (event) {
                    };
                }else{
                    r.innerHTML = 'Your browser is not supported. If google chrome, please upgrade!';
                }
                
            }

            // function check(id){
            //     var check = document.getElementById('result'+ id);
            //     alert(check.innerHTML);
            //     if(check.innerHTML != ''){
            //         if(check.innerHTML == '<?php //echo $value['answer']; ?>'){
            //             alert('đúng');
            //         }else{
            //             alert('sai');
            //         }
            //     }else{
            //         alert('chưa có dữ liệu');
            //     }
            // }

            $(".btn-record").on( "click", function() {
              var btn=$(this);
              setTimeout(
                function() {
                    var answer=btn.parent().find("textarea").val();
                    var stt=btn.parent().find(".stt").val();
                    <?php foreach ($data_read as $key => $value) { ?>
                            if((answer == '') && (stt == '<?php echo $value['stt']; ?>')){
                                alert('Chưa nhận được giọng nói. Vui lòng trả lời lại!');
                                var img = btn.parent().find(".true").html('<img src="https://t3.ftcdn.net/jpg/00/12/88/28/500_F_12882805_6WZBYZZj9ZijCRTfCRGLpgMNh0GFI3TF.jpg" width="50" height="50">');
                            }else if((answer == '<?php echo $value['answer']; ?>') && (stt == '<?php echo $value['stt']; ?>')){
                                //$(".true").html('<img src="img/true.jpg">');
                                var img = btn.parent().find(".true").html('<img src="http://www.gaestezimmer-hagen.de/images/haken_rot.gif" width="50" height="50">');
                            }else if((answer != '<?php echo $value['answer']; ?>') && (stt == '<?php echo $value['stt']; ?>')){
                                var img = btn.parent().find(".true").html('<img src="https://yt3.ggpht.com/-b7vGFzVjJwg/AAAAAAAAAAI/AAAAAAAAAAA/msUtINENTWA/s900-c-k-no-mo-rj-c0xffffff/photo.jpg" width="50" height="50">');
                            }
                            

                    <?php } ?>
                },
                5000);
            });

        </script>
<!-- .ghi âm giọng nói -->
            
        </div>
        <div class="col-sm-5">
            <!-- <img src="images/chibi.png" class="img-responsive"> -->
            <?= $this->Html->image('chibi.png',['class'=>'img-responsive'])?>
        </div>
    </div>

    <!-- the audio -->



    <!-- end speaking -->
</div> <!-- het cot 2 -->
</div>

</div> 


</div>
</div>

<!-- end container -->
<!-- video gợi ý -->
<div class="container">
    <h1 align="center" style="margin-top: 5%">Các bài học gợi ý</h1>
    <div class="row">
        <section class="col-xs-12">
            <div id="carousel-example-generic" class="carousel slide box" data-ride="carousel">
                <ol class="carousel-indicators" id="icon-next">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="row">
                            <h2 class="center-block"></h2>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <!-- thu truong hoc -->
                                <div class="thumbnail">
                                    <div class="caption">
                                        <h4>Trường học</h4>
                                        <p>Student, teacher, school,...</p>
                                        <p><a href="" rel="tooltip"><button class="button"><span>Vào học </span></button></a>
                                        </p>
                                    </div>
                                    <!-- <img src="images/truong.jpg" class="img-responsive"> -->
                                    <?= $this->Html->image('truong.jpg',['class'=>'img-responsive'])?>
                                </div>
                                <!-- end -->
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="thumbnail">
                                    <div class="caption">
                                        <h4>Bệnh viện</h4>
                                        <p>Nurse, doctor, tire...</p>
                                        <p><a href="" rel="tooltip"><button class="button"><span>Vào học </span></button></a>
                                        </p>
                                    </div>
                                    <!-- <img src="images/hospital(1).png" class="img-responsive"> -->
                                    <?= $this->Html->image('hospital(1).png',['class'=>'img-responsive'])?>
                                </div>
                                <!-- end -->
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="thumbnail">
                                    <div class="caption">
                                        <h4>Thức ăn</h4>
                                        <p>Fast-food, rice, fish,...</p>
                                        <p><a href="" rel="tooltip"><button class="button"><span>Vào học </span></button></a>
                                        </p>
                                    </div>
                                    <!-- <img src="images/food.jpg" class="img-responsive"> -->
                                    <?= $this->Html->image('food.jpg',['class'=>'img-responsive'])?>
                                </div>
                            </div>
                            <!-- end -->
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="thumbnail">
                                    <div class="caption">
                                        <h4>Trái cây</h4>
                                        <p>Apple, orange, banana,...</p>
                                        <p><a href="" rel="tooltip"><button class="button"><span>Vào học </span></button></a>
                                        </p>
                                    </div>
                                    <!-- <img src="images/traicay.jpg" class="img-responsive"> -->
                                    <?= $this->Html->image('traicay.jpg',['class'=>'img-responsive'])?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <h2 class="center-block"></h2>
                            <div class="col-xs-12 col-sm-6">
                                <div class="thumbnail">
                                    <div class="caption">
                                        <h4>Môi trường</h4>
                                        <p>Tree, forest, water,...</p>
                                        <p><a href="" rel="tooltip"><button class="button"><span>Vào học </span></button></a>
                                        </p>
                                    </div>
                                    <!-- <img src="images/mt1.jpg" class="img-responsive"> -->
                                    <?= $this->Html->image('mt1.jpg',['class'=>'img-responsive'])?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="thumbnail">
                                    <div class="caption">
                                        <h4>Mỹ phẩm</h4>
                                        <p>Skin care, anti-sunshine, tonner,...</p>
                                        <p><a href="" rel="tooltip"><button class="button"><span>Vào học </span></button></a>
                                        </p>
                                    </div>
                                    <!-- <img src="images/mypham1.jpg" class="img-responsive"> -->
                                    <?= $this->Html->image('mypham1.jpg',['class'=>'img-responsive'])?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <h2 class="center-block"></h2>
                            <div class="col-xs-12 col-sm-6 col-md-3">

                                <div class="thumbnail">
                                    <div class="caption">
                                        <h4>Điện thoại</h4>
                                        <p>Cellphone, smartphone,...</p>
                                        <p><a href="" rel="tooltip"><button class="button"><span>Vào học </span></button></a>
                                        </p>
                                    </div>
                                    <!-- <img src="images/smartphone.jpg" class="img-responsive"> -->
                                    <?= $this->Html->image('smartphone.jpg',['class'=>'img-responsive'])?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3">

                                <div class="thumbnail">
                                    <div class="caption">
                                        <h4>Chữ cái</h4>
                                        <p>Alphabet, a, b, c,...</p>
                                        <p><a href="" rel="tooltip"><button class="button"><span>Vào học </span></button></a>
                                        </p>
                                    </div>
                                    <!-- <img src="images/chucai.jpg" class="img-responsive"> -->
                                    <?= $this->Html->image('chucai.jpg',['class'=>'img-responsive'])?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="thumbnail">
                                    <div class="caption">
                                        <h4>Nghề Nghiệp</h4>
                                        <p>Doctor, teacher,...</p>
                                        <p><a href="" rel="tooltip"><button class="button"><span>Vào học </span></button></a>
                                        </p>
                                    </div>
                                    <!-- <img src="images/nghe.jpg" class="img-responsive"> -->
                                    <?= $this->Html->image('nghe.jpg',['class'=>'img-responsive'])?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="thumbnail">
                                    <div class="caption">
                                        <h4>Số đếm</h4>
                                        <p>First, second, third,...</p>
                                        <p><a href="" rel="tooltip"><button class="button"><span>Vào học </span></button></a>
                                        </p>
                                    </div>
                                    <!-- <img src="images/number.jpg" class="img-responsive"> -->
                                    <?= $this->Html->image('number.jpg',['class'=>'img-responsive'])?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div> 
<br>
<br>
<br>


<!-- end goi y -->
