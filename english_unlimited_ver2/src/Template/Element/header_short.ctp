
<style type="text/css">
	#fh5co-header-section{
		background-color: #3fb0ac !important;
	}
</style>

<!-- START #fh5co-header -->
<header id="fh5co-header-section" role="header" class="" >
	<div class="container">
	
		<h1 id="fh5co-logo" class="pull-left"><a href="<?php echo $this->url->build(['controller'=>'Homes']) ?>"><?php echo $this->Html->image('logo2.png',['English unlimited']); ?>english unlimited</a></h1>

		<!-- START #fh5co-menu-wrap -->
		<nav id="fh5co-menu-wrap" role="navigation">


			<ul class="sf-menu" id="fh5co-primary-menu">
				<li><a href="<?php echo $this->url->build(['controller'=>'Homes']) ?>">Trang Chủ</a></li>
				<li><a href="<?php echo $this->url->build(['controller'=>'Topics']) ?>" >Chủ đề</a></li>
				<li><a href="#Contacts">Liên hệ</a></li>
				<li><a href="#Abouts">Giới thiệu</a></li>
				<li class="fh5co-special"><a href="#">Đăng nhập</a></li>
				<!-- <li class="fh5co-special" style="margin-left: 15px"><a href="contact.html">Đăng nhập</a></li> -->
			</ul>
		</nav>
		<!-- </div> -->

	</div>
</header>
<div class="fh5co-spacer fh5co-spacer-sm"></div>

<div class="fh5co-spacer fh5co-spacer-sm"></div>
