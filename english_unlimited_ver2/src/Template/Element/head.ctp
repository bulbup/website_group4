<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<title>Slant &mdash; English Unlimited</title>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
<meta name="author" content="FREEHTML5.CO" />
<!-- Facebook and Twitter integration -->
<meta property="og:title" content=""/>
<meta property="og:image" content=""/>
<meta property="og:url" content=""/>
<meta property="og:site_name" content=""/>
<meta property="og:description" content=""/>
<meta name="twitter:title" content="" />
<meta name="twitter:image" content="" />
<meta name="twitter:url" content="" />
<meta name="twitter:card" content="" />


<?= $this->Html->css(array('themify-icons.css','bootstrap.css','owl.carousel.min.css','owl.theme.default.min.css','magnific-popup.css','superfish.css','easy-responsive-tabs.css','animate.css','style.css','chu_de.css','https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css','http://fonts.googleapis.com/css?family=Lato:300,400,700','lien_he.css', 'goiy.css','nexus.css','animate.css','font-awesome.css','responsive.css','custom.css','http://fonts.googleapis.com/css?family=Roboto:400,300','http://fonts.googleapis.com/css?family=Open+Sans:400,300'));

?>
<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="favicon.ico">

<?= $this->Html->script(array('modernizr-2.6.2.min.js','jquery-1.10.2.min.js','jquery.easing.1.3.js','bootstrap.js','owl.carousel.min.js','jquery.magnific-popup.min.js','hoverIntent.js','superfish.js','superfish.js','easyResponsiveTabs.js','fastclick.js','jquery.waypoints.min.js','jquery.waypoints.min.js','main.js','read-along.js','clickwordlist.js','goiybaihoc.js','main.js','hieuungchu.js','mainhieuungchu.js','jquery.min.js','bootstrap.min.js','scripts.js','jquery.isotope.js','jquery.slicknav.js','jquery.visible.js','jquery.sticky.js','slimbox2.js','modernizr.custom.js'));

?>
