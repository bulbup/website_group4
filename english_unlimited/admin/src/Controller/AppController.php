<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

class AppController extends Controller
{


    // public function initialize()
    // {
    //     parent::initialize();

    //     $this->loadComponent('RequestHandler');
    //     $this->loadComponent('Flash');

    //     //$this->loadComponent('Security');
    //     //$this->loadComponent('Csrf');
    // }

//login
    public function initialize()
    {
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authenticate' => [
            'Form' => [
            'fields' => [
            'username' => 'email',
            'password' => 'password'
            ]
            ]
            ],
            'loginAction' => [
            'controller' => 'users',
            'action' => 'login'
            ],
                'unauthorizedRedirect' => $this->referer() // If unauthorized, return them to page they were just on
                ]);
// Allow the display action so our pages controller
// continues to work.
        $this->Auth->allow(['display']);
            // 18 Chapter 2. Quick Start Guide
            // CakePHP Cookbook Documentation, Release 3.4
    }
//.login

    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
            ) {
            $this->set('_serialize', true);
    }
}
}
