<footer role="contentinfo" id="fh5co-footer">
			<a href="#" class="fh5co-arrow fh5co-gotop footer-box"><i class="fa fa-angle-up"></i></a>
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-6 footer-box">
						<h3 class="fh5co-footer-heading">GIỚI THIỆU</h3>
						<p id="text-footer-color" style="color: #dddfd4">Bạn có thể tìm hiểu thêm về chúng tôi cũng như các công việc tại English Unlimited.</p>
						<p><a href="#" class="btn btn-outline btn-sm">Xem thêm</a></p>

					</div>
					<div class="col-md-3 col-sm-6 footer-box">
						<h3 class="fh5co-footer-heading">Riêng tư &amp; ĐIỀU KHOẢN</h3>
						<ul class="fh5co-footer-links">
							<li><a href="#">Qui định &amp; chính sách</a></li>
							<li><a href="#">Hỗ trợ</a></li>
							<li><a href="#">Đăng nhập</a></li>
							<li><a href="#">Đăng ký</a></li>
							</ul>
							
					</div>
					<div class="col-md-3 col-sm-6 footer-box">
						<h3 class="fh5co-footer-heading">DANH MỤC</h3>
						<ul class="fh5co-footer-links">
							<li><a href="#">Thư viện chủ đề</a></li>
							<li><a href="#">Thư viện bài học</a></li>
							<li><a href="#">Trò chơi</a></li>
							<li><a href="#">Kiểm tra</a></li>
							</ul>

					</div>
					
					<div class="col-md-3 col-sm-6 footer-box">
						<h3 class="fh5co-footer-heading">KẾT NỐI VỚI CHÚNG TÔI</h3>
						<ul class="fh5co-social-icons">
							
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>	
							<li><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
						</ul>
						<ul class="fh5co-footer-links" style="color: #dddfd4">
							<li>Địa chỉ: 123/4 Quán Xí Muội, Cầu Rạch Ngỗng</li>
							<li>Email: LTDT@englishulimited.com</li>
							<li>SĐT: 0123456789</li>
							</ul>
					</div>
					<div class="col-md-12 footer-box">
						<div class="fh5co-copyright">
						<p style="color: #dddfd4">&copy; 2017 English Unlimited</p>
						</div>
					</div>
			
				</div>
				<!-- END row -->
				
			</div>
		</footer>
		<!-- jQuery -->
		<script src="js/jquery-1.10.2.min.js"></script>
		<!-- jQuery Easing -->
		<script src="js/jquery.easing.1.3.js"></script>
		<!-- Bootstrap -->
		<script src="js/bootstrap.js"></script>
		<!-- Owl carousel -->
		<script src="js/owl.carousel.min.js"></script>
		<!-- Magnific Popup -->
		<script src="js/jquery.magnific-popup.min.js"></script>
		<!-- Superfish -->
		<script src="js/hoverIntent.js"></script>
		<script src="js/superfish.js"></script>
		<!-- Easy Responsive Tabs -->
		<script src="js/easyResponsiveTabs.js"></script>
		<!-- FastClick for Mobile/Tablets -->
		<script src="js/fastclick.js"></script>
		<!-- Parallax -->
		<script src="js/jquery.parallax-scroll.min.js"></script>
		<!-- Waypoints -->
		<script src="js/jquery.waypoints.min.js"></script>
		<!-- Main JS -->
		<script src="js/main.js"></script>
