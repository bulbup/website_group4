<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Slant &mdash; English Unlimited</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
<meta name="author" content="FREEHTML5.CO" />
<!-- Facebook and Twitter integration -->
<meta property="og:title" content=""/>
<meta property="og:image" content=""/>
<meta property="og:url" content=""/>
<meta property="og:site_name" content=""/>
<meta property="og:description" content=""/>
<meta name="twitter:title" content="" />
<meta name="twitter:image" content="" />
<meta name="twitter:url" content="" />
<meta name="twitter:card" content="" />


<?php
echo $this->Html->css(array(
'themify-icons.css','bootstrap.css','owl.carousel.min.css',
'owl.theme.default.min.css','magnific-popup.css','superfish.css','easy-responsive-tabs.css','animate.css','style.css','chu_de.css','https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css','http://fonts.googleapis.com/css?family=Lato:300,400,700','lien_he.css'
));
?>
<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="favicon.ico">

<!-- Google Webfont -->
<!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
<!-- Themify Icons -->
<!-- <link rel="stylesheet" href="css/themify-icons.css"> -->
<!-- Bootstrap -->
<!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
<!-- Owl Carousel -->
<!-- <link rel="stylesheet" href="css/owl.carousel.min.css"> -->
<!-- <link rel="stylesheet" href="css/owl.theme.default.min.css"> -->
<!-- Magnific Popup -->
<!-- <link rel="stylesheet" href="css/magnific-popup.css"> -->
<!-- Superfish -->
<!-- <link rel="stylesheet" href="css/superfish.css"> -->
<!-- Easy Responsive Tabs -->
<!-- <link rel="stylesheet" href="css/easy-responsive-tabs.css"> -->
<!-- Animate.css -->
<!-- <link rel="stylesheet" href="css/animate.css"> -->
<!-- Theme Style -->
<!-- <link rel="stylesheet" href="css/style.css"> -->
<!-- <link rel="stylesheet" type="text/css" href="css/chu_de.css"> -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4s.7.0/css/font-awesome.min.css"> -->

<?php
echo $this->Html->script(array(
	'modernizr-2.6.2.min.js','jquery-1.10.2.min.js','jquery.easing.1.3.js','bootstrap.js','owl.carousel.min.js','jquery.magnific-popup.min.js','hoverIntent.js','superfish.js','superfish.js','easyResponsiveTabs.js','fastclick.js','jquery.parallax-scroll.min.js','jquery.waypoints.min.js','jquery.waypoints.min.js','main.js'
));
?>
<link rel="stylesheet" type="text/css" href="css/lien_he.css">
<link rel="stylesheet" type="text/css" href="css/goiy.css">
<script src="js/goiybaihoc.js"></script>

<script src="js/read-along.js"></script>
<script src="js/main.js"></script>
<script src="js/clickwordlist.js"></script>
<!-- Modernizr JS -->
<!-- <script src="js/modernizr-2.6.2.min.js"></script> -->

<!-- jQuery -->
<!-- <script src="js/jquery-1.10.2.min.js"></script> -->
<!-- jQuery Easing -->
<!-- <script src="js/jquery.easing.1.3.js"></script> -->
<!-- Bootstrap -->
<!-- <script src="js/bootstrap.js"></script> -->
<!-- Owl carousel -->
<!-- <script src="js/owl.carousel.min.js"></script> -->
<!-- Magnific Popup -->
<!-- <script src="js/jquery.magnific-popup.min.js"></script> -->
<!-- Superfish -->
<!-- <script src="js/hoverIntent.js"></script> -->
<!-- <script src="js/superfish.js"></script> -->
<!-- Easy Responsive Tabs -->
<!-- <script src="js/easyResponsiveTabs.js"></script> -->
<!-- FastClick for Mobile/Tablets -->
<!-- <script src="js/fastclick.js"></script> -->
<!-- Parallax -->
 <!-- <script src="js/jquery.parallax-scroll.min.js"></script>  -->
<!-- Waypoints -->
<!-- <script src="js/jquery.waypoints.min.js"></script> -->
<!-- Main JS -->
<!-- <script src="js/main.js"></script> -->
