<nav class="navbar fixed-top navbar-toggleable-md navbar-inverse" id="mainNav">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand" href="#page-top"> Unlimit English</a>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="#services">Phương pháp</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#portfolio">Chủ đề</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#about">Giới thiệu</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#team">Nhóm</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact">Liên hệ</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="#dangky">Đăng ký</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#dangnhap">Đăng nhập</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
<!--  -->