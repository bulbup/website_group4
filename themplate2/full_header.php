<header class="masthead">
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in">Welcome To Our Website!</div>
                <div class="intro-heading">It's Nice To Meet You</div>
                <a href="#services" class="btn btn-xl">Tell Me More</a>
            </div>
        </div>
    </header>