<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>jQuery Simulate Extended Plugin - jsFiddle demo</title>


<!-- CSS Sheets -->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/gosigner.css">

    <!-- THIS IS WHAT CAUSES THE ISSUE 
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap.css"> -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/dataTables.css"> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/dropzone.css">


<!-- Global JS Files -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<style>
    body {
        background-color: #EAEAEA;
    }

    #view-port {
        margin: 0 auto;
        width: 852px;
    }

    .droppableShape {
        z-index: 10;
    }

    #signature-add-canvas {
        background-image: url("/app/img/sign_bg.png");
    }

    .page {
        margin: 10px 0 10px 0;
        background-repeat: no-repeat;
        width: 850px; 
        height:1100px;
        border: 1px solid #DADADA;
        position: relative;
        background-color: #fff;
        border-radius: 2px;
    }
</style>
</head>

<body>
    <div class="container">
        <div id="view-port">
            <div id="placeme" class="droppableShape">
                <img src="https://upload.wikimedia.org/wikipedia/en/6/6f/Smiley_Face.png" width="25" height="25" />
            </div>
            <button id="testme">Simulate Drop</button>
            <div class="page" id="page1" style="background-image: url(http://images.huffingtonpost.com/2012-08-15-1CanvasPanelInstall5psd.jpg);"></div>
            <!-- Pages Here -->
        </div>
    </div>
</body>


<!-- jQuery UI -->

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script type='text/javascript' src="http://j-ulrich.github.com/jquery-simulate-ext/jquery.simulate.js"></script>
<script type='text/javascript' src="http://j-ulrich.github.com/jquery-simulate-ext/jquery.simulate.ext.js"></script>
<script type='text/javascript' src="http://j-ulrich.github.com/jquery-simulate-ext/bililiteRange.js"></script>
<script type='text/javascript' src="http://j-ulrich.github.com/jquery-simulate-ext/jquery.simulate.key-sequence.js"></script>
<script type='text/javascript' src="http://j-ulrich.github.com/jquery-simulate-ext/jquery.simulate.key-combo.js"></script>
<script type='text/javascript' src="http://j-ulrich.github.com/jquery-simulate-ext/jquery.simulate.drag-n-drop.js"></script>

<script>

     $(document).ready(function(){

        $(".droppableShape").draggable({
            helper:'clone'
        });

        $(".page").droppable({
            accept: ".droppableShape",
            tolerance: 'fit',
            drop: function(event,ui){
                alert("Drop Detected");
                // Set variables
                var new_field = $(ui.helper).clone().removeClass('droppableShape');
                var droppable_page = $(this);
                var droppableOffset = $(this).offset();
                new_field.css('top', ui.position.top - droppableOffset.top);
                new_field.css('left', ui.position.left - droppableOffset.left);

                // Set Draggable Options
                new_field.draggable({
                    containment: droppable_page,
                    stop: function(event, ui) {
                       // Save position after dragging stops
                       $(this).data("x_cord", ui.position.left);
                       $(this).data("y_cord", ui.position.top);
                    }
                });

                // Add to drop area
                $(this).append(new_field);
            }
        });

        $( "#testme" ).click(function() {
            $('#placeme').simulate("drag-n-drop", {
                dragTarget: $("#page1")
            });
            console.log("Drag Simulated");
        });
    });

</script>
