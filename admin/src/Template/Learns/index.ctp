<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Learn'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Lessons'), ['controller' => 'Lessons', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Lesson'), ['controller' => 'Lessons', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="learns index large-9 medium-8 columns content">
    <h3><?= __('Learns') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('learn_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('lesson_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('level_name') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($learns as $learn): ?>
            <tr>
                <td><?= $learn->has('learn') ? $this->Html->link($learn->learn->learn_id, ['controller' => 'Learns', 'action' => 'view', $learn->learn->learn_id]) : '' ?></td>
                <td><?= $learn->has('user') ? $this->Html->link($learn->user->id, ['controller' => 'Users', 'action' => 'view', $learn->user->id]) : '' ?></td>
                <td><?= $learn->has('lesson') ? $this->Html->link($learn->lesson->lesson_id, ['controller' => 'Lessons', 'action' => 'view', $learn->lesson->lesson_id]) : '' ?></td>
                <td><?= h($learn->level_name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $learn->learn_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $learn->learn_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $learn->learn_id], ['confirm' => __('Are you sure you want to delete # {0}?', $learn->learn_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
