<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Topic'), ['action' => 'edit', $topic->topic_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Topic'), ['action' => 'delete', $topic->topic_id], ['confirm' => __('Are you sure you want to delete # {0}?', $topic->topic_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Topics'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Topic'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Topics'), ['controller' => 'Topics', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Topic'), ['controller' => 'Topics', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="topics view large-9 medium-8 columns content">
    <h3><?= h($topic->topic_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Topic') ?></th>
            <td><?= $topic->has('topic') ? $this->Html->link($topic->topic->topic_id, ['controller' => 'Topics', 'action' => 'view', $topic->topic->topic_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Topic Name') ?></th>
            <td><?= h($topic->topic_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Level Name') ?></th>
            <td><?= h($topic->level_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Describe Short') ?></th>
            <td><?= h($topic->describe_short) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Image') ?></th>
            <td><?= h($topic->image) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Create Time') ?></th>
            <td><?= h($topic->create_time) ?></td>
        </tr>
    </table>
</div>
