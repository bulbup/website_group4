<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../sty.css"  rel="stylesheet"type="text/css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<title>Drag and Drop</title>
</head>



<body>
<!--footer-->
<footer class="mt-5">
  <div class="container-fluid bg-faded mt-5">
    <div class="container">
      <div class="row py-3">
        <!-- footer column 1 start -->
        <div class="col-md-4">
          <!-- row start -->
          <div class="row py-2">
            <div class="col-sm-3 hidden-md-down">
              <a class="bg-circle bg-info" href="https://twitter.com/ ">
                <i class="fa fa-2x fa-fw fa-twitter" aria-hidden="true "></i>
              </a>
            </div>
            <div class="col-sm-9">
              <h4>Tweets</h4>
              Embed here?
            </div>
          </div>
          <!-- row end -->
        </div>
        <!-- footer column 1 end -->
        <!-- footer column 2 start -->
        <div class="col-md-4">
          <!-- row start -->
          <div class="row py-2">
            <div class="col-sm-3 hidden-md-down">
              <a class="bg-circle bg-info" href="#">
                <i class="fa fa-2x fa-fw fa-address-card" aria-hidden="true "></i>
              </a>
            </div>
            <div class="col-sm-9">
              <h4>Contact us</h4>
              <p>Why not?</p>
            </div>
          </div>
          <!-- row end -->
          <!-- row start -->
          <div class="row py-2">
            <div class="col-sm-3 hidden-md-down">
              <a class="bg-circle bg-info" href="#">
                <i class="fa fa-2x fa-fw fa-laptop" aria-hidden="true "></i>
              </a>
            </div>
            <div class="col-sm-9">
              <h4>Cookie policy</h4>
              <p class=" ">We use <a class=" " href="/# ">cookies </a></p>
            </div>
          </div>
          <!-- row end -->
        </div>
        <!-- footer column 2 end -->
        <!-- footer column 3 start -->
        <div class="col-md-4">
          <!-- row starting  -->
          <div class="row py-2">
            <div class="col-sm-3 hidden-md-down">
              <a class="bg-circle bg-danger" href="# ">
                <i class="fa fa-2x fa-fw fa-file-pdf-o" aria-hidden="true "></i>
              </a>
            </div>
            <div class="col-sm-9">
              <h4>Download pdf</h4>
              <p> You like print?</a></p>

            </div>
          </div>
          <!-- row ended -->
          <!-- row starting  -->
          <div class="row py-2">
            <div class="col-sm-3 hidden-md-down">
              <a class="bg-circle bg-info" href="https://twitter.com/ ">
                <i class="fa fa-2x fa-fw fa-info" aria-hidden="true "></i>
              </a>
            </div>
            <div class="col-sm-9">
              <h4>Info</h4>
              About us.
            </div>
          </div>
          <!-- row ended -->
        </div>
        <!-- footer column 3 end -->
      </div>
    </div>
  </div>


  <div class="container-fluid bg-primary py-3">
    <div class="container" style="height:350px">
    <a  href="https://www.facebook.com/"class="fa fa-facebook-square" aria-hidden="true" style="font-size:50px",></a>
    <a  href="https://www.twitter.com/"class="fa fa-twitter-square" aria-hidden="true" style="font-size:50px"></a>
    <a  href="https://www.instagram.com/"class="fa fa-instagram" aria-hidden="true" style="font-size:50px"></a>
    <a  href="https://www.pinterest.com/"class="fa fa-pinterest-square" aria-hidden="true"  style="font-size:50px"></a>
   	  <div class="jumbotron text-center" style="height:250px; background-color:rgba(153,153,153,0.5)">
      
  <h1>Company</h1> 
  <p>We specialize in blablabla</p> 
  <form class="form-inline" >
    <div class="input-group">
      <input type="email" class="form-control" size="50" placeholder="Email Address" required />
      <div class="input-group-btn" style="color:#c0dfd9">
      <input type="submit" class="btn btn-info" value="Submit Button">
      </div>
    </div>
  </form>
	</div>
      
    </div>
  </div>
  
</footer>




</body>
</html>
