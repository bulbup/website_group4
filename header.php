<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>DemoPHP</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
    </head>
    <body>
        <div class="site-contain">    
        <!--MENU-->
            <nav class="navbar navbar-default" style="position: fixed; width: 100%; z-index: 9000;">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">Welcome!</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#Home">Home</a></li>
                            <li><a href="#AboutUs">About Us</a></li>
                            <li><a href="#Services">Services</a></li>
                            <li><a href="#Technologies">Technologies</a></li>
                            <li><a href="#ContactUs">Contact Us</a></li>                        
                        </ul>                       
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container -->
            </nav><!-- /.navbar -->
       