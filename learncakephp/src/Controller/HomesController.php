<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * homes Controller
 *
 * @property \App\Model\Table\homesTable $homes
 *
 * @method \App\Model\Entity\home[] paginate($object = null, array $settings = [])
 */
class HomesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        // $homes = $this->paginate($this->Homes);

         $this->set(compact('homes'));
        // $this->set('_serialize', ['homes']);
    }
public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
          $this->viewBuilder()->layout('frontend');
    }
    /**
     * View method
     *
     * @param string|null $id home id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
  

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
  

    /**
     * Edit method
     *
     * @param string|null $id home id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
   

    /**
     * Delete method
     *
     * @param string|null $id home id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
 
}
