<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * homes Controller
 *
 * @property \App\Model\Table\homesTable $homes
 *
 * @method \App\Model\Entity\home[] paginate($object = null, array $settings = [])
 */
class ContactsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        // $homes = $this->paginate($this->Homes);

         $this->set(compact('contacts'));
        // $this->set('_serialize', ['homes']);
    }
public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
          $this->viewBuilder()->layout('frontend');
    }
    
  
 
}
