<?php
   
  // <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

 //<link rel="shortcut icon" href="favicon.ico"> 
echo $this->html->ico('favicon');
// <!-- Google Webfont -->
// echo $this->html->css('http://fonts.googleapis.com/css?family=Lato:300,400,700');
// <!-- Themify Icons -->s
 echo $this->html->css('thessmify-icons');
// <!-- Bootstrap -->
echo $this->html->css('bootstrap');
// <!-- Owl Carousel -->
echo $this->html->css('owl.carousel.min');
echo $this->html->css('owl.theme.default.min');
// <!-- Magnific Popup -->
echo $this->html->css('magnific-popup');
// <!-- Superfish -->
echo $this->html->css('superfish');
// <!-- Easy Responsive Tabs -->
echo $this->html->css('easy-responsive-tabs');
// <!-- Animate.css -->
echo $this->html->css('animate');
// <!-- Theme Style -->
echo $this->html->css('style');
// <!-- Modernizr JS -->
echo $this->html->script('modernizr-2.6.2.min');
// <!-- jQuery -->
echo $this->html->script('jquery-1.10.2.min');
// <!-- jQuery Easing -->
echo $this->html->script('jquery.easing.1.3');
// <!-- Bootstrap -->
echo $this->html->script('bootstrap');
// <!-- Owl carousel -->
echo $this->html->script('owl.carousel.min');
// <!-- Magnific Popup -->
echo $this->html->script('jquery.magnific-popup.min');
// <!-- Superfish -->
echo $this->html->script('hoverIntent');
echo $this->html->script('superfish');
// <!-- Easy Responsive Tabs -->
echo $this->html->script('easyResponsiveTabs');
// <!-- FastClick for Mobile/Tablets -->
echo $this->html->script('fastclick');
// <!-- Parallax -->
echo $this->html->script('jquery.parallax-scroll.min');
// <!-- Waypoints -->
echo $this->html->script('jquery.waypoints.min');
// <!-- Main JS -->
echo $this->html->script('main');

?>
<title>English Unlimited</title>