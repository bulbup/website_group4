	<div id="fh5co-main">

		<div class="container">
			<div class="row" id="fh5co-features">
				<h2 class="dung" style="text-align: center;">4 Yếu tố học tốt tiếng anh</h2>
				<div class="fh5co-spacer fh5co-spacer-sm"></div>
				<div class="col-md-3 col-sm-6 text-center fh5co-feature feature-box">
					<div class="fh5co-feature-icon">
						<i class="ti-mobile"></i>
					</div>
					<h3 class="heading">Niềm tin</h3>
					<p>Niềm tin là yếu tố quan trọng nhất giúp bạn thực hiện bất kì công việc gì trong mọi lĩnh vực. Bạn sẽ chẳng thể thành công nều luôn để sự nghi ngờ tồn tại trong tâm trí. Vì nó chính là rào cản lớn nhất khiến bạn không thể tự tin đưa ra và thực thi bất cứ dự định gì. Vì vậy, hãy loại bỏ những suy nghĩ như “ mình dốt”, “ mình không có năng khiếu”,… ra khỏi đầu và đặt niềm tin vào chính bản thân mình.</p>
				</div>

				<div class="col-md-3 col-sm-6 text-center fh5co-feature feature-box"> 
					<div class="fh5co-feature-icon">
						<i class="ti-lock"></i>
					</div>
					<h3 class="heading">Kế họach</h3>
					<p>Liệt kê các công việc cần phải làm khi học tiếng anh, các mục tiêu cần hướng tới theo một trình tự nhất định và được thực hiện trong một khoảng thời gian nhất định, cụ thể. </p>
				</div>

				<!-- 	<div class="clearfix visible-sm-block"></div> -->

				<div class="col-md-3 col-sm-6 text-center fh5co-feature feature-box"> 
					<div class="fh5co-feature-icon">
						<i class="ti-video-camera"></i>
					</div>
					<h3 class="heading">Thực hiện</h3>
					<p> Nếu bạn đã hoàn thành được 2 bước đầu thì việc khó khăn cuối cùng chỉ là bạn hãy thực hiện và kiên trì với kế hoạch của mình. Bí quyết để thực hiện được việc khó khăn này là hãy luôn suy nghĩ tích cực và đặt niềm tin vào chính bản thân mình.</p>
				</div>

				<!-- <div class="clearfix visible-md-block visible-lg-block"></div> -->

				<div class="col-md-3 col-sm-6 text-center fh5co-feature feature-box">
					<div class="fh5co-feature-icon">
						<i class="ti-shopping-cart"></i>
					</div>
					<h3 class="heading">Động lực và đam mê</h3>
					<p>Chắc hẳn nếu bạn đã đọc đến đây thì có lẽ bạn đã thực sự mong muốn tự học tiếng anh. Để luôn giữ ngọn lửa đam mê trong mình hãy liệt kê hết những lý do bạn phải học tiếng anh như: thăng tiến trong công việc, đi du lịch nước ngoài, xem bất kì chương trình tiếng anh mà không cần phụ đề…Hãy chọn ra một vài lý do mà bạn thực sự tâm đắc nhất để lấy đó làm kim chỉ nam cho những bước đi tiếp theo trên con đường chinh phục tiếng Anh. </p>
				</div>

				<div class="clearfix visible-sm-block"></div>

			<!-- 		<div class="col-md-4 col-sm-6 text-center fh5co-feature feature-box"> 
						<div class="fh5co-feature-icon">
							<i class="ti-palette"></i>
						</div>
						<h3 class="heading">Pallete</h3>
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste sunt porro delectus cum officia magnam.</p>
					</div>
					<div class="col-md-4 col-sm-6 text-center fh5co-feature feature-box"> 
						<div class="fh5co-feature-icon">
							<i class="ti-truck"></i>
						</div>
						<h3 class="heading">Deliver</h3>
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste sunt porro delectus cum officia magnam.</p>
					</div> -->
				</div>
				<!-- END row -->

				<div class="fh5co-spacer fh5co-spacer-md"></div>
				<!-- End Spacer -->

				<div class="row" id="fh5co-works">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-section-heading work-box">
						<h2 class="fh5co-lead">3 Bước học hiệu quả của<br> English Unlimited</h2>
						<!-- <p class="fh5co-sub">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit est facilis maiores, perspiciatis accusamus asperiores sint consequuntur debitis. Ut, dolores sit amet consectetur adipisicing elit.</p> -->
						<!-- <div class="fh5co-spacer fh5co-spacer-sm"></div> -->
					</div>

					<div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 text-center fh5co-work-item work-box">
						<div class="container">
							<blockquote class="quote-box">
								<p class="quotation-mark">
									“
								</p>
								<p class="quote-text">
									Bạn có thể lựa chọn các chủ đề để học theo sở thích và mong muốn. 
								</p>

								<hr>
								<div class="blog-post-actions">
									<p class="blog-post-bottom pull-left">
										Học theo chủ đề
									</p>
									<p class="blog-post-bottom pull-right">
										<!-- <span class="badge quote-badge">896</span>  ❤ -->
									</p>
								</div>
							</blockquote>
						</div>
					</div>

					<div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 text-center fh5co-work-item work-box"> 
						<div class="container">
							<blockquote class="quote-box">
								<p class="quotation-mark">
									“
								</p>
								<p class="quote-text">
										Với mỗi bài học mà english unlimited đem lại có đầy đủ các kĩ năng: nghe, nói, đọc, viết. Mỗi kĩ năng được áp dụng phương pháp học thông minh như: mindmap, video, flashcard  
								</p>

								<hr>
								<div class="blog-post-actions">
									<p class="blog-post-bottom pull-left">
									Học theo Kỹ năng
									</p>
									<p class="blog-post-bottom pull-right">
										<!-- <span class="badge quote-badge">896</span>  ❤ -->
									</p>
								</div>
							</blockquote>
						</div>
					</div>

					<div class="clearfix visible-sm-block visible-xs-block"></div>

					<div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 text-center fh5co-work-item work-box"> 
						<div class="container">
							<blockquote class="quote-box">
								<p class="quotation-mark">
									“
								</p>
								<p class="quote-text">
									Bài kiểm tra giúp đánh giá tiến độ của bạn.
So sánh trước và sau khi học một cách rõ ràng và cụ thể.
								</p>

								<hr>
								<div class="blog-post-actions">
									<p class="blog-post-bottom pull-left">
										Kiểm tra
									</p>
									<p class="blog-post-bottom pull-right">
										<!-- <span class="badge quote-badge">896</span>  ❤ -->
									</p>
								</div>
							</blockquote>
						</div>
					</div>

					<div class="clearfix visible-md-block visible-lg-block"></div>

			<br><br>

					<div class="col-md-4 col-md-offset-4 text-center work-box">
						<p><a href="#" class="btn btn-outline btn-md">Học ngay thôi</a></p>
					</div>
				</div>
				<!-- END row -->
				
				<div class="fh5co-spacer fh5co-spacer-md"></div>
				<div class="row">
					<!-- Start Slider Testimonial -->
					<h2 class="fh5co-uppercase-heading-sm text-center animate-box">Châm ngôn của chúng tôi</h2>
					<div class="fh5co-spacer fh5co-spacer-xs"></div>
					<div class="owl-carousel-fullwidth animate-box">
						<div class="item">
							<p class="text-center quote">&ldquo;Life is 10% what happens to you and 90% how you respond to it &rdquo; <cite class="author">&mdash; Lou Holtz</cite></p>
						</div>
						<div class="item">
							<p class="text-center quote">&ldquo;Never give up. Today is hard, tomorrow will be worse, but the day after tomorrow will be sunshine. &rdquo;<cite class="author">&mdash;  JACK MA</cite></p>
						</div>
						<div class="item">
							<p class="text-center quote">&ldquo;Success is only meaningful and enjoynable if it fells like your own&rdquo;<cite class="author">&mdash; Michelle Obama</cite></p>
						</div>
					</div>
					<!-- End Slider Testimonial -->
				</div>
				<!-- END row -->
				<div class="fh5co-spacer fh5co-spacer-md"></div>

			</div>
			<!-- END container -->


		</div>
		<!-- END fhtco-main -->