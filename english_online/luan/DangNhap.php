<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Đăng nhập</title>
<!-- Phần labraries -->
    <link rel="stylesheet" href="labraries/css/bootstrap.min.css">
    <link rel="stylesheet" href="labraries/css/bootstrap-theme.min.css">
    <script type="text/javascript" src="labraries/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="labraries/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="labraries/font-awesome-4.7.0/css/font-awesome.min.css">
<!-- End phần labraries -->

<!-- Phần labrary -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="labrary/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>     
    <script src="labrary/js/boopstrap.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- End phần labrary -->
    <!-- <link rel="stylesheet" type="text/css" href="css/dangnhap.css"> -->

    
  </head>
  <body>
  <!-- Đăng nhập -->
	<a href="#myModal" class="btn" data-toggle="modal">Login here...</a>
	<div class="modal fade in" id="myModal" aria-hidden="true" style="height: 450px !important; margin-top: -40px; margin-bottom: 20px;">
	  
		<div class="modal-body">
		  	<!-- <button class="btn" data-dismiss="modal" aria-hidden="true" style="float: right; margin-top: -15px; margin-right: -15px;">Close</button> -->
            <button type="button" class="close" data-dismiss="modal">&times;</button>
		    <form name="frmdangnhap" action="xuly_DN.php" method="post" style="text-align: center;">
		        <h2 style="color: #33b5e5; text-align: center;">Đăng nhập</h2>
		        <input type="text" name="tendangnhap" required="" placeholder="Tên đăng nhập" class="span5 search-query" style="height: 50px !important; margin-bottom: 20px; border-radius: 100px !important; margin-top: 30px;">
		         
		        <input type="password" name="matkhau" required="" placeholder="Mật khẩu" class="span5 search-query" style="height: 50px !important; margin-bottom: 20px; border-radius: 100px !important;">
		        <br/><a href="#myModalQMK" style="float: right; margin-right: 80px;" data-toggle="modal">Quên mật khẩu?</a><br/>
		            
		        <input type="submit" name="sbmdangnhap" value="Đăng nhập" class="span5" style="height: 50px !important; margin-top: 20px; border-radius: 100px !important; background: red;">
		    </form>
		    <p style="float: right; margin-right: 80px;">Chưa có tài khoản? <a href="#myModalDK" data-toggle="modal">Đăng ký</a></p>

		</div>
	</div>

<!-- End Đăng nhập -->

<!-- Quên mật khẩu -->
	<div class="modal fade in" id="myModalQMK" aria-hidden="true" style="height: 450px !important; margin-top: -40px; margin-bottom: 20px;">
	  
		<div class="modal-body">
		  	<!--<button class="btn" data-dismiss="modal" aria-hidden="true" style="float: right; margin-top: -15px; margin-right: -15px;">Close</button> -->
            <button type="button" class="close" data-dismiss="modal">&times;</button>
		    <form name="frmquenmatkhau" action="xuly_QMK.php" method="post" style="text-align: center;">
		        <h2 style="color: #33b5e5; text-align: center;">Quên mật khẩu</h2>
		        <p style="color: red; margin-top: 30px;">Vui lòng nhập Email đăng ký để nhận lại mật khẩu</p>
		        <input type="email" name="email" required="" placeholder="Email" class="span5 search-query" style="height: 50px !important; margin-bottom: 20px; border-radius: 100px !important;">
		            
		        <input type="submit" name="sbmquenmatkhau" value="Xác nhận" class="span5" style="height: 50px !important; margin-top: 20px; border-radius: 100px !important; background: red;">
		    </form>
		    <!-- <p style="float: right; margin-right: 80px;">Chưa có tài khoản? <a href="#myModalDK" data-toggle="modal">Đăng ký</a></p> -->

		</div>
	</div>

<!-- End Quên mật khẩu -->

<!-- Đăng ký -->
	<!-- <a href="#myModalDK" class="btn" data-toggle="modal">Đăng ký</a> -->
    <div class="modal fade in" id="myModalDK" aria-hidden="true" style="height: 600px !important; margin-top: -40px; margin-bottom: 20px; text-align: center;">
      
      <div class="modal-body" style="max-height: 590px !important;">
        <!-- <button class="btn" data-dismiss="modal" aria-hidden="true" style="float: right; margin-top: -15px; margin-right: -15px;">Close</button> -->
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <form name="frmdangky" action="xuly_DK.php" method="post">
          	<h2 style="color: #33b5e5; text-align: center; margin-bottom: 50px;">Đăng ký thành viên</h2>
			

          <div style="padding-left: 30px;padding-right: 30px; margin-top: 20px;">
            <!-- <legend>Thông tin người dùng</legend> -->

            <input type="text" name="hoten" required="" placeholder="Họ và tên" class="span5 search-query" style="height: 50px !important; margin-bottom: 20px; border-radius: 100px !important;">
            <!-- <span style="color: red;"><?php //if (!empty($errors) && in_array('hoten', $errors)) { echo 'Vui lòng nhập Họ và tên';} ?></span> -->
           
            <!-- <input type="date" name="ngaysinh" required="" class="span5 search-query" style="height: 50px !important; margin-bottom: 20px; border-radius: 100px !important;">
           
            <select name="gioitinh" class="search-query" style="height: 50px !important; width: 365px; margin-bottom: 20px; border-radius: 100px !important;">
                <option value="1">Nam</option>
                <option value="0">Nữ</option>
            </select>
            

            <input type="text" name="diachi" required="" placeholder="Địa chỉ" class="span5 search-query" style="height: 50px !important; margin-bottom: 20px; border-radius: 100px !important;">
             -->
            <input type="Email" name="email" required="" placeholder="Email" class="span5 search-query" style="height: 50px !important; margin-bottom: 20px; border-radius: 100px !important;">
            
            <input type="password" name="matkhau" required="" placeholder="Mật khẩu" class="span5 search-query" style="height: 50px !important; margin-bottom: 20px; border-radius: 100px !important;">

            <input type="number" name="sdt" required="" placeholder="Số điện thoại" class="span5 search-query" style="height: 50px !important; margin-bottom: 20px; border-radius: 100px !important;">
            
        </div>

        	<div style="display: inline-block; width: 365px;">
        		<input type="text" name="captcha" required="" placeholder="Mã captcha" class="span3 search-query" style="height: 50px !important; margin-bottom: 20px; float: left !important; border-radius: 100px !important;">

            	<label style="float: right !important; margin-right: 50px; height: 50px; line-height: 50px;"><img id="captcha" src="captcha_code.php" onclick="document.getElementById('captcha').src='captcha_code.php?sid='+Math.random();" title="Đổi mã khác"></label>
        	</div>
            

            <div style="display: inline-block; width: 365px;">
            	<input type="submit" name="sbmdangky" value="Đăng ký" class="search-query" style="height: 50px !important; margin-bottom: 20px; border-radius: 100px !important; background: red; float: left; width: 150px; ">

            	<button type="Reset" class="btn form-control" class="span2 search-query" style="height: 50px !important; width: 150px; margin-bottom: 20px; border-radius: 100px !important; float: right;">Reset</button>
            </div>
            
        </form>
      </div>
    </div>
<!-- End Đăng ký -->

  </body>
</html>
