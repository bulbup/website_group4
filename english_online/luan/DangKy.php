<?php session_start(); ?>
<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Đăng ký thành viên</title>
    <link rel="stylesheet" href="labraries/css/bootstrap.min.css">
    <link rel="stylesheet" href="labraries/css/bootstrap-theme.min.css">
    <script type="text/javascript" src="labraries/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="labraries/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="labraries/font-awesome-4.7.0/css/font-awesome.min.css">
    <style type="text/css">
      label{
        font-weight: 100 !important;
        color: #33b5e5;
      }
    </style>
    
  </head>

  <body style="background: #e9e9e9;">

<a href="#myModal" class="btn" data-toggle="modal">Login here...</a>
<div class="modal fade in" id="myModal" aria-hidden="true">
  
  <div class="modal-body">

    <form name="frmDangKy" name="DangKy" method="post">
      <!-- <div class="row" style="width: 50%; margin: 0px auto; border: 1px solid #fff; margin-bottom: 30px;">
        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12"> -->
          <h2 style="color: #33b5e5; text-align: center;">Đăng ký thành viên</h2>

          <fieldset style="padding-left: 30px;padding-right: 30px; margin-top: 20px;">
            <legend>Thông tin tài khoản</legend>
            <div class="form-group">
              <label>Tên đăng nhập</label>
              <input type="text" name="tendangnhap" required="" placeholder="Nguyễn Văn A" class="span5">
            </div>
            <div class="form-group">
              <label>Mật khẩu</label>
              <input type="password" name="matkhau" required="" placeholder="Mật khẩu" class="form-control span5">
            </div>
            <div class="form-group">
              <label>Nhập lại mật khẩu</label>
              <input type="password" name="xnmatkhau" required="" placeholder="Nhập lại mật khẩu" class="form-control">
            </div>
          </fieldset>

          <fieldset style="padding-left: 30px;padding-right: 30px; margin-top: 20px;">
            <legend>Thông tin người dùng</legend>
            <div class="form-group">
              <label>Họ và tên</label>
              <input type="text" name="hoten" required="" placeholder="Họ và tên" class="form-control">
            </div>
            <div class="form-group">
              <label>Ngày sinh</label>
              <input type="date" name="ngaysinh" required="" class="form-control">
            </div>
            <div class="form-group">
              <label style="display: block;">Giới tính</label>
              <select name="gioitinh" class="form-control">
                <option value="1">Nam</option>
                <option value="0">Nữ</option>
              </select>
            </div>
            <div class="form-group">
              <label>Địa chỉ</label>
              <input type="text" name="diachi" required="" placeholder="Địa chỉ" class="form-control">
            </div>
            <div class="form-group">
              <label>Email</label>
              <input type="Email" name="email" required="" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
              <label>SĐT</label>
              <input type="number" name="sdt" required="" placeholder="Số điện thoại" class="form-control">
            </div>
          </fieldset>


          <div class="form-group" style="display: inline-block;">
            <input type="text" name="captcha" required="" placeholder="Mã captcha" class="form-control" style="width: 300px !important; float: left; margin-left: 30px;">

            <label style="float: right !important; margin-left: 50px;"><img id="captcha" src="captcha_code.php" onclick="document.getElementById('captcha').src='captcha_code.php?sid='+Math.random();" title="Đổi mã khác"></label>

          </div>

          <div class="form-group" style="text-align: center;">
            <input type="submit" name="submit" value="Đăng ký" class="btn btn-primary form-control" style="background: #33b5e5; width: 200px !important;">
          </div>
           
        <!-- </div>
      </div> -->
    </form>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
 
</div>

 <!--  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
 -->
</div>

  </body>
</html>
