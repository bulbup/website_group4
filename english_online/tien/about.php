<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>

 <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    
    
<link rel="stylesheet" href="https://dc8hdnsmzapvm.cloudfront.net/assets/styles/old/old.css?d518045" type="text/css" _cdn="true" />
  
 


</head>

<body>
<div class="about-banner sawtooth" style="background-image: url(../images/tt.jpg)">
  <div class="container parent">
    <div class="row" >
      <nav class="span3">
  <ul class="nav nav-vertical nav-side nav-about caps center" style="margin-top:50%">
    <li class="item item-story navItem">
      <a href="#ourstory" onclick="_gaq.push(['_trackEvent', 'About Page', 'button', 'Our Story']);"><span class="icon-container"><i class="icon icon-about-nav-story about-nav-story_active "></i></span><br>Our Story</a>
    </li>  
    <br />
    
    <li class="item item-team ">
      <a href="#" onclick="_gaq.push(['_trackEvent', 'About Page', 'button', 'Teams']);"><span class="icon-container"><i class="icon icon-about-nav-team"></i></span><br>Team</a>
    </li> 
  
    
     
  </ul>
</nav>
      <div class="span12">
        <div class="center">
          <h1 class="about-story-headline center" style="background: rgba(255, 255, 255,0.5); margin-top:20%">
            <font color="#990000">Making the Web a Better Place<br />
            <small>to learn English</small></font>
          </h1>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container content">
  <section class="row" id="ourstory">
    <div class="span11 offset2 center">
      <h1 class="center headline-bordered">Our story</h1>
      <div class="row">
        <div class="span9 offset1">
          <p class="intro">Moz started up in 2004, and we’ve been on an epic ride ever since. From our beginnings as an SEO consulting company to launching the first Pro app in 2007, we’ve tried to stay true to our core beliefs—TAGFEE—and to deliver an exceptional experience for our community and subscribers. We owe a huge thanks to our community for joining us on this awesome journey, and we hope that you’ll continue to be a part of our story.</p>
          <p class="intro">Our co-founder, Rand Fishkin, has always put the T (for transparency) in TAGFEE. Read his personal thoughts about Moz on his blog.</p>
        </div>
      </div>
    </div>
  </section>
</div>
</body>
</html>