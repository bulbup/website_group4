/*==============================================================*/
/* DBMS name:      Sybase SQL Anywhere 12                       */
/* Created on:     09/06/2017 1:41:49 CH                        */
/*==============================================================*/


-- if exists(select 1 from sys.sysforeignkey where role='FK_COMMENTS_POSTS_COM_POSTS') then
--     alter table COMMENTS
--        delete foreign key FK_COMMENTS_POSTS_COM_POSTS
-- end if;

-- if exists(select 1 from sys.sysforeignkey where role='FK_DETAIL_DETAIL_LE_LEARNS') then
--     alter table DETAIL
--        delete foreign key FK_DETAIL_DETAIL_LE_LEARNS
-- end if;

-- if exists(select 1 from sys.sysforeignkey where role='FK_DETAIL_DETAIL_LI_LISTWORD') then
--     alter table DETAIL
--        delete foreign key FK_DETAIL_DETAIL_LI_LISTWORD
-- end if;

-- if exists(select 1 from sys.sysforeignkey where role='FK_FILE_FILE_OF_L_FILE_OF_') then
--     alter table FILE
--        delete foreign key FK_FILE_FILE_OF_L_FILE_OF_
-- end if;

-- if exists(select 1 from sys.sysforeignkey where role='FK_FILE_OF__FILE_OF_L_LESSONS') then
--     alter table FILE_OF_LESSON
--        delete foreign key FK_FILE_OF__FILE_OF_L_LESSONS
-- end if;

-- if exists(select 1 from sys.sysforeignkey where role='FK_FILE_TYP_FILE_TYPE_FILE') then
--     alter table FILE_TYPE
--        delete foreign key FK_FILE_TYP_FILE_TYPE_FILE
-- end if;

-- if exists(select 1 from sys.sysforeignkey where role='FK_LEARNS_LEARN_LES_LESSONS') then
--     alter table LEARNS
--        delete foreign key FK_LEARNS_LEARN_LES_LESSONS
-- end if;

-- if exists(select 1 from sys.sysforeignkey where role='FK_LEARNS_USER_LEAR_USERS') then
--     alter table LEARNS
--        delete foreign key FK_LEARNS_USER_LEAR_USERS
-- end if;

-- if exists(select 1 from sys.sysforeignkey where role='FK_LESSONS_LESONS_LE_LEVELS') then
--     alter table LESSONS
--        delete foreign key FK_LESSONS_LESONS_LE_LEVELS
-- end if;

-- if exists(select 1 from sys.sysforeignkey where role='FK_LESSONS_LESSON_OF_TOPICS') then
--     alter table LESSONS
--        delete foreign key FK_LESSONS_LESSON_OF_TOPICS
-- end if;

-- if exists(select 1 from sys.sysforeignkey where role='FK_LOGS_USERS_LOG_USERS') then
--     alter table LOGS
--        delete foreign key FK_LOGS_USERS_LOG_USERS
-- end if;

-- if exists(select 1 from sys.sysforeignkey where role='FK_POSTS_USERS_POS_USERS') then
--     alter table POSTS
--        delete foreign key FK_POSTS_USERS_POS_USERS
-- end if;

-- if exists(select 1 from sys.sysforeignkey where role='FK_USERS_USERS_ROL_ROLES') then
--     alter table USERS
--        delete foreign key FK_USERS_USERS_ROL_ROLES
-- end if;

-- drop index if exists COMMENTS.POSTS_COMMENTS_FK;

-- drop index if exists COMMENTS.COMMENTS_PK;

-- drop table if exists COMMENTS;

-- drop index if exists DETAIL.DETAIL_LISTWORD_FK;

-- drop index if exists DETAIL.DETAIL_LEARN_FK;

-- drop index if exists DETAIL.DETAIL_PK;

-- drop table if exists DETAIL;

-- drop index if exists DICTS.DICTS_PK;

-- drop table if exists DICTS;

-- drop index if exists FILE.FILE_OF_LESSON_FK;

-- drop index if exists FILE.FILE_PK;

-- drop table if exists FILE;

-- drop index if exists FILE_OF_LESSON.FILE_OF_LESSION_FK;

-- drop index if exists FILE_OF_LESSON.FILE_OF_LESSON_PK;

-- drop table if exists FILE_OF_LESSON;

-- drop index if exists FILE_TYPE.FILE_TYPE_OF_FILE_FK;

-- drop index if exists FILE_TYPE.FILE_TYPE_PK;

-- drop table if exists FILE_TYPE;

-- drop index if exists LEARNS.LEARN_LESSON_FK;

-- drop index if exists LEARNS.USER_LEARN_FK;

-- drop index if exists LEARNS.LEARNS_PK;

-- drop table if exists LEARNS;

-- drop index if exists LESSONS.LESSON_OF_TOPIC_FK;

-- drop index if exists LESSONS.LESONS_LEVELS_FK;

-- drop index if exists LESSONS.LESSONS_PK;

-- drop table if exists LESSONS;

-- drop index if exists LEVELS.LEVELS_PK;

-- drop table if exists LEVELS;

-- drop index if exists LISTWORDS.LISTWORDS_PK;

-- drop table if exists LISTWORDS;

-- drop index if exists LOGS.USERS_LOGS_FK;

-- drop index if exists LOGS.LOGS_PK;

-- drop table if exists LOGS;

-- drop index if exists POSTS.USERS_POSTS_FK;

-- drop index if exists POSTS.POSTS_PK;

-- drop table if exists POSTS;

-- drop index if exists ROLES.ROLES_PK;

-- drop table if exists ROLES;

-- drop index if exists TOPICS.TOPICS_PK;

-- drop table if exists TOPICS;

-- drop index if exists USERS.USERS_ROLES_FK;

-- drop index if exists USERS.USERS_PK;

-- drop table if exists USERS;

/*==============================================================*/
/* Table: COMMENTS                                              */
/*==============================================================*/
create table COMMENTS 
(
   COMMENT_ID           integer                        not null,
   POST_ID              integer                        not null,
   CONTENT              long varchar                   not null,
   TIME_LOGIN           timestamp                      not null,
   USER_COMMENT         varchar(50)                    not null,
   constraint PK_COMMENTS primary key (COMMENT_ID)
);

/*==============================================================*/
/* Index: COMMENTS_PK                                           */
/*==============================================================*/
create unique index COMMENTS_PK on COMMENTS (
COMMENT_ID ASC
);

/*==============================================================*/
/* Index: POSTS_COMMENTS_FK                                     */
/*==============================================================*/
create index POSTS_COMMENTS_FK on COMMENTS (
POST_ID ASC
);

/*==============================================================*/
/* Table: DETAIL                                                */
/*==============================================================*/
create table DETAIL 
(
   LESSON_ID            integer                        not null,
   USER_ID              integer                        not null,
   LEARNS_ID            integer                        not null,
   LISTWORD_ID          integer                        not null,
   DETAIL_ID            char(10)                       not null,
   NAME_VOCABULARY      varchar(50)                    not null,
   constraint PK_DETAIL primary key (LESSON_ID, USER_ID, LEARNS_ID, LISTWORD_ID, DETAIL_ID)
);

/*==============================================================*/
/* Index: DETAIL_PK                                             */
/*==============================================================*/
create unique index DETAIL_PK on DETAIL (
LESSON_ID ASC,
USER_ID ASC,
LEARNS_ID ASC,
LISTWORD_ID ASC,
DETAIL_ID ASC
);

/*==============================================================*/
/* Index: DETAIL_LEARN_FK                                       */
/*==============================================================*/
create index DETAIL_LEARN_FK on DETAIL (
LESSON_ID ASC,
USER_ID ASC,
LEARNS_ID ASC
);

/*==============================================================*/
/* Index: DETAIL_LISTWORD_FK                                    */
/*==============================================================*/
create index DETAIL_LISTWORD_FK on DETAIL (
LISTWORD_ID ASC
);

/*==============================================================*/
/* Table: DICTS                                                 */
/*==============================================================*/
create table DICTS 
(
   DICT_ID              integer                        not null,
   FILE_NAME            varchar(50)                    not null,
   constraint PK_DICTS primary key (DICT_ID)
);

/*==============================================================*/
/* Index: DICTS_PK                                              */
/*==============================================================*/
create unique index DICTS_PK on DICTS (
DICT_ID ASC
);

/*==============================================================*/
/* Table: FILE                                                  */
/*==============================================================*/
create table FILE 
(
   DICT_ID7             integer                        not null,
   FILE_OF_LESSON_ID    integer                        not null,
   FILE_NAME            varchar(50)                    not null,
   TYPE                 long varchar                   not null,
   URL                  long varchar                   null,
   constraint PK_FILE primary key (DICT_ID7)
);

/*==============================================================*/
/* Index: FILE_PK                                               */
/*==============================================================*/
create unique index FILE_PK on FILE (
DICT_ID7 ASC
);

/*==============================================================*/
/* Index: FILE_OF_LESSON_FK                                     */
/*==============================================================*/
create index FILE_OF_LESSON_FK on FILE (
FILE_OF_LESSON_ID ASC
);

/*==============================================================*/
/* Table: FILE_OF_LESSON                                        */
/*==============================================================*/
create table FILE_OF_LESSON 
(
   FILE_OF_LESSON_ID    integer                        not null,
   LESSON_ID            integer                        not null,
   NAME_FILE_OF_LESSON  varchar(50)                    not null,
   constraint PK_FILE_OF_LESSON primary key (FILE_OF_LESSON_ID)
);

/*==============================================================*/
/* Index: FILE_OF_LESSON_PK                                     */
/*==============================================================*/
create unique index FILE_OF_LESSON_PK on FILE_OF_LESSON (
FILE_OF_LESSON_ID ASC
);

/*==============================================================*/
/* Index: FILE_OF_LESSION_FK                                    */
/*==============================================================*/
create index FILE_OF_LESSION_FK on FILE_OF_LESSON (
LESSON_ID ASC
);

/*==============================================================*/
/* Table: FILE_TYPE                                             */
/*==============================================================*/
create table FILE_TYPE 
(
   FILE_TYPE_ID         integer                        not null,
   DICT_ID7             integer                        not null,
   FILE_TYPE_NAME       varchar(50)                    not null,
   constraint PK_FILE_TYPE primary key (FILE_TYPE_ID)
);

/*==============================================================*/
/* Index: FILE_TYPE_PK                                          */
/*==============================================================*/
create unique index FILE_TYPE_PK on FILE_TYPE (
FILE_TYPE_ID ASC
);

/*==============================================================*/
/* Index: FILE_TYPE_OF_FILE_FK                                  */
/*==============================================================*/
create index FILE_TYPE_OF_FILE_FK on FILE_TYPE (
DICT_ID7 ASC
);

/*==============================================================*/
/* Table: LEARNS                                                */
/*==============================================================*/
create table LEARNS 
(
   LESSON_ID            integer                        not null,
   USER_ID              integer                        not null,
   LEARNS_ID            integer                        not null,
   NAME_LEVEL           char(50)                       not null,
   constraint PK_LEARNS primary key (LESSON_ID, USER_ID, LEARNS_ID)
);

/*==============================================================*/
/* Index: LEARNS_PK                                             */
/*==============================================================*/
create unique index LEARNS_PK on LEARNS (
LESSON_ID ASC,
USER_ID ASC,
LEARNS_ID ASC
);

/*==============================================================*/
/* Index: USER_LEARN_FK                                         */
/*==============================================================*/
create index USER_LEARN_FK on LEARNS (
USER_ID ASC
);

/*==============================================================*/
/* Index: LEARN_LESSON_FK                                       */
/*==============================================================*/
create index LEARN_LESSON_FK on LEARNS (
LESSON_ID ASC
);

/*==============================================================*/
/* Table: LESSONS                                               */
/*==============================================================*/
create table LESSONS 
(
   LESSON_ID            integer                        not null,
   LEVEL_ID             integer                        not null,
   TOPIC_ID             integer                        not null,
   NAME_LESSON          char(50)                       not null,
   constraint PK_LESSONS primary key (LESSON_ID)
);

/*==============================================================*/
/* Index: LESSONS_PK                                            */
/*==============================================================*/
create unique index LESSONS_PK on LESSONS (
LESSON_ID ASC
);

/*==============================================================*/
/* Index: LESONS_LEVELS_FK                                      */
/*==============================================================*/
create index LESONS_LEVELS_FK on LESSONS (
LEVEL_ID ASC
);

/*==============================================================*/
/* Index: LESSON_OF_TOPIC_FK                                    */
/*==============================================================*/
create index LESSON_OF_TOPIC_FK on LESSONS (
TOPIC_ID ASC
);

/*==============================================================*/
/* Table: LEVELS                                                */
/*==============================================================*/
create table LEVELS 
(
   LEVEL_ID             integer                        not null,
   NAME_LEVEL           char(50)                       not null,
   constraint PK_LEVELS primary key (LEVEL_ID)
);

/*==============================================================*/
/* Index: LEVELS_PK                                             */
/*==============================================================*/
create unique index LEVELS_PK on LEVELS (
LEVEL_ID ASC
);

/*==============================================================*/
/* Table: LISTWORDS                                             */
/*==============================================================*/
create table LISTWORDS 
(
   LISTWORD_ID          integer                        not null,
   NAME_LEVEL           char(50)                       null,
   FILE_NAME            varchar(50)                    not null,
   constraint PK_LISTWORDS primary key (LISTWORD_ID)
);

/*==============================================================*/
/* Index: LISTWORDS_PK                                          */
/*==============================================================*/
create unique index LISTWORDS_PK on LISTWORDS (
LISTWORD_ID ASC
);

/*==============================================================*/
/* Table: LOGS                                                  */
/*==============================================================*/
create table LOGS 
(
   LOG_ID               integer                        not null,
   USER_ID              integer                        not null,
   TIME_LOGIN           timestamp                      not null,
   COUNT_LOGIN          integer                        not null,
   IP_LOGIN             char(50)                       not null,
   ACTION               varchar(255)                   not null,
   constraint PK_LOGS primary key (LOG_ID)
);

/*==============================================================*/
/* Index: LOGS_PK                                               */
/*==============================================================*/
create unique index LOGS_PK on LOGS (
LOG_ID ASC
);

/*==============================================================*/
/* Index: USERS_LOGS_FK                                         */
/*==============================================================*/
create index USERS_LOGS_FK on LOGS (
USER_ID ASC
);

/*==============================================================*/
/* Table: POSTS                                                 */
/*==============================================================*/
create table POSTS 
(
   POST_ID              integer                        not null,
   USER_ID              integer                        not null,
   TITLE                varchar(1000)                  not null,
   CONTENT              long varchar                   not null,
   constraint PK_POSTS primary key (POST_ID)
);

/*==============================================================*/
/* Index: POSTS_PK                                              */
/*==============================================================*/
create unique index POSTS_PK on POSTS (
POST_ID ASC
);

/*==============================================================*/
/* Index: USERS_POSTS_FK                                        */
/*==============================================================*/
create index USERS_POSTS_FK on POSTS (
USER_ID ASC
);

/*==============================================================*/
/* Table: ROLES                                                 */
/*==============================================================*/
create table ROLES 
(
   ROLE_ID              integer                        not null,
   NAME_LEVEL           char(50)                       null,
   constraint PK_ROLES primary key (ROLE_ID)
);

/*==============================================================*/
/* Index: ROLES_PK                                              */
/*==============================================================*/
create unique index ROLES_PK on ROLES (
ROLE_ID ASC
);

/*==============================================================*/
/* Table: TOPICS                                                */
/*==============================================================*/
create table TOPICS 
(
   TOPIC_ID             integer                        not null,
   NAME_LEVEL           char(50)                       not null,
   TIME_CREATE          timestamp                      not null,
   constraint PK_TOPICS primary key (TOPIC_ID)
);

/*==============================================================*/
/* Index: TOPICS_PK                                             */
/*==============================================================*/
create unique index TOPICS_PK on TOPICS (
TOPIC_ID ASC
);

/*==============================================================*/
/* Table: USERS                                                 */
/*==============================================================*/
create table USERS 
(
   USER_ID              integer                        not null,
   ROLE_ID              integer                        not null,
   EMAIL                varchar(255)                   not null,
   PASSWORD             varchar(255)                   not null,
   BIRTHDAY             date                           null,
   GENDER               smallint                       null,
   JOB                  varchar(255)                   null,
   IMAGE                long varchar                   null,
   constraint PK_USERS primary key (USER_ID)
);

/*==============================================================*/
/* Index: USERS_PK                                              */
/*==============================================================*/
create unique index USERS_PK on USERS (
USER_ID ASC
);

/*==============================================================*/
/* Index: USERS_ROLES_FK                                        */
/*==============================================================*/
create index USERS_ROLES_FK on USERS (
ROLE_ID ASC
);

alter table COMMENTS
   add constraint FK_COMMENTS_POSTS_COM_POSTS foreign key (POST_ID)
      references POSTS (POST_ID)
      on update restrict
      on delete restrict;

alter table DETAIL
   add constraint FK_DETAIL_DETAIL_LE_LEARNS foreign key (LESSON_ID, USER_ID, LEARNS_ID)
      references LEARNS (LESSON_ID, USER_ID, LEARNS_ID)
      on update restrict
      on delete restrict;

alter table DETAIL
   add constraint FK_DETAIL_DETAIL_LI_LISTWORD foreign key (LISTWORD_ID)
      references LISTWORDS (LISTWORD_ID)
      on update restrict
      on delete restrict;

alter table FILE
   add constraint FK_FILE_FILE_OF_L_FILE_OF_ foreign key (FILE_OF_LESSON_ID)
      references FILE_OF_LESSON (FILE_OF_LESSON_ID)
      on update restrict
      on delete restrict;

alter table FILE_OF_LESSON
   add constraint FK_FILE_OF__FILE_OF_L_LESSONS foreign key (LESSON_ID)
      references LESSONS (LESSON_ID)
      on update restrict
      on delete restrict;

alter table FILE_TYPE
   add constraint FK_FILE_TYP_FILE_TYPE_FILE foreign key (DICT_ID7)
      references FILE (DICT_ID7)
      on update restrict
      on delete restrict;

alter table LEARNS
   add constraint FK_LEARNS_LEARN_LES_LESSONS foreign key (LESSON_ID)
      references LESSONS (LESSON_ID)
      on update restrict
      on delete restrict;

alter table LEARNS
   add constraint FK_LEARNS_USER_LEAR_USERS foreign key (USER_ID)
      references USERS (USER_ID)
      on update restrict
      on delete restrict;

alter table LESSONS
   add constraint FK_LESSONS_LESONS_LE_LEVELS foreign key (LEVEL_ID)
      references LEVELS (LEVEL_ID)
      on update restrict
      on delete restrict;

alter table LESSONS
   add constraint FK_LESSONS_LESSON_OF_TOPICS foreign key (TOPIC_ID)
      references TOPICS (TOPIC_ID)
      on update restrict
      on delete restrict;

alter table LOGS
   add constraint FK_LOGS_USERS_LOG_USERS foreign key (USER_ID)
      references USERS (USER_ID)
      on update restrict
      on delete restrict;

alter table POSTS
   add constraint FK_POSTS_USERS_POS_USERS foreign key (USER_ID)
      references USERS (USER_ID)
      on update restrict
      on delete restrict;

alter table USERS
   add constraint FK_USERS_USERS_ROL_ROLES foreign key (ROLE_ID)
      references ROLES (ROLE_ID)
      on update restrict
      on delete restrict;

