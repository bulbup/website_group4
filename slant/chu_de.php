<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Slant &mdash; A free HTML5 Template by FREEHTML5.CO</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="FREEHTML5.CO" />
<link rel="stylesheet" type="text/css" href="css/chu_de.css">
  	<!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
	
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 	https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

  	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
  	<link rel="shortcut icon" href="favicon.ico">

  	<!-- Google Webfont -->
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
	<!-- Themify Icons -->
	<link rel="stylesheet" href="css/themify-icons.css">
	<!-- Bootstrap -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">
	<!-- Superfish -->
	<link rel="stylesheet" href="css/superfish.css">
	<!-- Easy Responsive Tabs -->
	<link rel="stylesheet" href="css/easy-responsive-tabs.css">
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Theme Style -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>

		<!-- START #fh5co-header -->
<!-- header -->
<?php include('header_short.php');?>
<!-- .header -->

<!-- tìm kiếm -->
<form>
  <input type="text" name="search" placeholder="Bạn muốn tìm gì?">
</form> 
<!-- .tìm kiếm -->

		<div id="fh5co-main">
	
			<div class="container">

				<div class="row" id="fh5co-features">
					


				        <!-- Page Content -->
        <div id="page-content-wrapper">
                   <div class="container">
               <div class="row">
		<div class="col-sm-3">
			<div class="thumbnail" style="padding: 0">
				<div style="padding:4px">
					<img alt="300x200" style="width: 100%" src="images/giadinh.jpg">
				</div>
				<div class="caption">
					<h2>Gia Đình</h2>
					<p>My project description</p>
				</div>
				<div class="modal-footer" style="text-align: left">
					<div class="progress progress-striped active" style="background: #ddd">
						<div class="bar bar-warning" style="width: 60%;"></div>
					</div>
					<a href="noi_dung_bai_hoc.php"><button class="button"><span>Vào học </span></button></a>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="thumbnail" style="padding: 0">
				<div style="padding:4px">
					<img alt="300x200" style="width: 100%" src="images/thoitiet.jpg">
				</div>
				<div class="caption">
					<h2>Thời tiết</h2>
					<p>My project description</p>
				</div>
				<div class="modal-footer" style="text-align: left">
					<div class="progress" style="background: #ddd">
						<div class="bar bar-success" style="width: 100%;"></div>
					</div>					
		<button class="button"><span>Vào học </span></button>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
            <div class="thumbnail" style="padding: 0">
              <div style="padding:4px">
                <img alt="300x200" style="width: 100%" src="images/suckhoe.jpg">
              </div>
              <div class="caption">
                <h2>Sức Khỏe</h2>
                <p>My project description</p>
              
              </div>
              <div class="modal-footer" style="text-align: left">
                <div class="progress progress-striped active" style="background: #ddd">
                  <div class="bar bar-danger" style="width: 30%;"></div>
                </div>
               <button class="button"><span>Vào học </span></button>
              </div>
            </div>
        </div>
    	<div class="col-sm-3">
            <div class="thumbnail" style="padding: 0">
              <div style="padding:4px">
                <img alt="300x200" style="width: 100%" src="images/thethao.jpg">
              </div>
              <div class="caption">
                <h2>Thể Thao</h2>
                <p>My project description</p>
              
              </div>
              <div class="modal-footer" style="text-align: left">
                <div class="progress" style="background: #ddd">
                  <div class="bar bar-info" style="width: 40%;"></div>
                </div>
               <button class="button"><span>Vào học </span></button>
              </div>
            </div>
        </div>
	</div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
					<!-- <div class="clearfix visible-md-block visible-lg-block"></div> -->

			
				</div>
				<!-- END row -->

				
				<!-- End Spacer -->


				
				
				<div class="fh5co-spacer fh5co-spacer-md"></div>
				<div class="row">
					<!-- Start Slider Testimonial -->
	            <h2 class="fh5co-uppercase-heading-sm text-center animate-box">Châm ngôn của chúng tôi</h2>
	            <div class="fh5co-spacer fh5co-spacer-xs"></div>
	            <div class="owl-carousel-fullwidth animate-box">
	            <div class="item">
	              <p class="text-center quote">&ldquo;Life is 10% what happens to you and 90% how you respond to it &rdquo; <cite class="author">&mdash; Lou Holtz</cite></p>
	            </div>
	            <div class="item">
	              <p class="text-center quote">&ldquo;Never give up. Today is hard, tomorrow will be worse, but the day after tomorrow will be sunshine. &rdquo;<cite class="author">&mdash;  JACK MA</cite></p>
	            </div>
	            <div class="item">
	              <p class="text-center quote">&ldquo;Success is only meaningful and enjoynable if it fells like your own&rdquo;<cite class="author">&mdash; Michelle Obama</cite></p>
	            </div>
	          </div>
	           <!-- End Slider Testimonial -->
				</div>
				<!-- END row -->
				<div class="fh5co-spacer fh5co-spacer-md"></div>

			</div>
			<!-- END container -->

		
		</div>
		<!-- END fhtco-main -->


	<!-- footer -->
		<?php include('footer.php');?>
<!--.footer  -->
			
			
		<!-- jQuery -->
		<script src="js/jquery-1.10.2.min.js"></script>
		<!-- jQuery Easing -->
		<script src="js/jquery.easing.1.3.js"></script>
		<!-- Bootstrap -->
		<script src="js/bootstrap.js"></script>
		<!-- Owl carousel -->
		<script src="js/owl.carousel.min.js"></script>
		<!-- Magnific Popup -->
		<script src="js/jquery.magnific-popup.min.js"></script>
		<!-- Superfish -->
		<script src="js/hoverIntent.js"></script>
		<script src="js/superfish.js"></script>
		<!-- Easy Responsive Tabs -->
		<script src="js/easyResponsiveTabs.js"></script>
		<!-- FastClick for Mobile/Tablets -->
		<script src="js/fastclick.js"></script>
		<!-- Parallax -->
		<script src="js/jquery.parallax-scroll.min.js"></script>
		<!-- Waypoints -->
		<script src="js/jquery.waypoints.min.js"></script>
		<!-- Main JS -->
		<script src="js/main.js"></script>

	</body>
</html>
