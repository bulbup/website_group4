<?php echo $this->html->css('lien_he.css'); ?>

<br><br>
<div class="jumbotron jumbotron-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h1 class="h1">
                    Liên hệ <small>Hãy để lại liên lạc cho chúng tôi</small></h1>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="well well-sm">
                <form>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Họ Tên</label>
                            <input type="text" class="form-control" id="name" placeholder="Tên của bạn" required="required" />
                        </div>
                        <div class="form-group">
                            <label for="email">
                                Email</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                </span>
                                <input type="email" class="form-control" id="email" placeholder="Email của bạn" required="required" /></div>
                        </div>
                        <div class="form-group">
                            <label for="subject">
                                Subject</label>
                            <select id="subject" name="subject" class="form-control" required="required">
                                <option value="na" selected="">Choose One:</option>
                                <option value="service">General Customer Service</option>
                                <option value="suggestions">Suggestions</option>
                                <option value="product">Product Support</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Nội dung</label>
                            <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required"
                                placeholder="Nội dung..."></textarea>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">
                            Send Message</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <form>
            <legend><span class="glyphicon glyphicon-globe"></span>Địa Chỉ</legend>
            <address>
                <strong>Đại học Cần Thơ</strong><br>
                Đường 3/2, quận Ninh Kiều<br>
                Thành phố Cần Thơ<br>
                <abbr title="Phone">
                    P:</abbr>
                0989 999 999
            </address>
            <address>
                <strong>Địa chỉ email</strong><br>
                <a href="mailto:#">ngocdung7896@gmail.com</a>
            </address>
            </form>
        </div>
    </div>
</div>
<br><br><br><br>