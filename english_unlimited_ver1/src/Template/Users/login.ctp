 <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        <!-- <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet"> -->
        <link rel="stylesheet" type="text/css" href="css/dang_nhap.css">
 <div class="container">
           
            <header>
                <h1>Đăng nhập vào English Unlimited</h1>
				
            </header>
            <section>				
                <div id="container_demo" >
                    <!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
                    <a class="hiddenanchor" id="toregister"></a>
                    <a class="hiddenanchor" id="tologin"></a>
                    <div id="wrapper">
                        <div id="login" class="animate form">
                            <form  action="/english_unlimited/users/login" method="post" autocomplete="on"> 
                                <h1>Đăng nhập</h1> 
                                <p> 
                                    <label for="username" class="uname" >Tên tài khoản hoặc Email </label>
                                    <input id="username" name="username" required="required" type="text" placeholder="nhập vào email của bạn"/>
                                </p>
                                <p> 
                                    <label for="password" class="youpasswd"> Mật Khẩu </label>
                                    <input id="password" name="password" required="required" type="password" placeholder="nhập vào mật khẩu" /> 
                                </p>
                                <p class="keeplogin"> 
									<input type="checkbox" name="loginkeeping" id="loginkeeping" value="loginkeeping" /> 
									<label for="loginkeeping">Lưu tài khoản</label>
								</p>
                                <p class="login button"> 
                                  <!--  <a href="http://cookingfoodsworld.blogspot.in/" target="_blank" ></a> -->
                                <p class="signin button"> 
                                    <input type="submit" value="Đăng Nhập"/> 
                                </p>
								</p>
                                <p class="change_link">
									Không phải thành viên-->
									<a href="#toregister" class="to_register">Đăng ký ngay</a>
								</p>
                            </form>
                        </div>

                        <div id="register" class="animate form">
                            <form  action="/english_unlimited/users/register" method="post" autocomplete="on"> 
                                <h1> Đăng ký </h1> 
                                <p> 
                                    <label for="usernamesignup" class="uname" >Tên tài khoản</label>
                                    <input id="usernamesignup" name="usernamesignup" required="required" type="text" placeholder="tên tài khoản của bạn" />
                                </p>
                                <p> 
                                    <label for="emailsignup" class="youmail"  >Email</label>
                                    <input id="emailsignup" name="emailsignup" required="required" type="email" placeholder="nhập vào email của bạn"/> 
                                </p>
                                <p> 
                                    <label for="passwordsignup" class="youpasswd" >Mật khẩu </label>
                                    <input id="passwordsignup" name="passwordsignup" required="required" type="password" placeholder="nhập vào mật khẩu"/>
                                </p>
                                <p> 
                                    <label for="passwordsignup_confirm" class="youpasswd" >Nhập lại mật khẩu </label>
                                    <input id="passwordsignup_confirm" name="passwordsignup_confirm" required="required" type="password" placeholder="nhập vào mật khẩu"/>
                                </p>
                                <p class="signin button"> 
									<input type="submit" value="Đăng Nhập"/> 
								</p>
                                <p class="change_link">  
									Tôi là thành viên -->
									<a href="#tologin" class="to_register"> Đăng nhập </a>
								</p>
                            </form>
                        </div>
						
                    </div>
                </div>  
            </section>
        </div>