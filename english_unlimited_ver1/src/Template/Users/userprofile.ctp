


<!-- <link rel="stylesheet" type="text/css" href="css/profile.css"> -->
<?= $this->Html->css('profile') ?>

<!-- <script type="js/profile.js"></script> -->
<?= $this->Html->script('profile') ?>


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://bootswatch.com/cosmo/bootstrap.min.css">
<script src="js/jquery-1.10.2.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
<!-- <body>
  <?php //include('header_short.php');?> -->
  <!-- Simple post content example. -->
  <div class="media-body">
   <div class="container" style="padding-top: 60px;">
    <h1 class="page-header">Trang cá nhân</h1>
    <a href="#" class="btn btn-default btn-sm pull-right" style="margin-top: -70px"><i class="fa fa-power-off" aria-hidden="true"></i> Sign Out</a>
    <div class="row">
      <!-- left column -->
      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="text-center">
          <img src="images/shin.png" class="avatar img-circle img-thumbnail" alt="avatar">
          <h6>Thay đổi hình ảnh....</h6>
          <input type="file" class="text-center center-block well well-sm">
        </div>
      </div>
      <!-- edit form column -->
      <div class="col-md-8 col-sm-6 col-xs-12 personal-info">

        <form class="form-horizontal" role="form">
          <div class="form-group">
            <label class="col-lg-3 control-label" style="color:black">Họ&amp;tên:</label>
            <div class="col-lg-8">
             <input id="name" name="name" type="text" placeholder="Họ&tên" class="form-control input-md" value="<?= h($user->email)?>"> 
           </div>
         </div>
         <div class="form-group">
          <label class="col-lg-3 control-label" style="color:black">Sinh nhật:</label>
          <div class="col-lg-8">
            <input id="Date Of Birth" name="Date Of Birth" type="text" placeholder="Ngày-tháng-năm" class="form-control input-md">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label" for="Gender" style="color:black">Giới tính</label>
          <div class="col-md-4"> 
            <label class="radio-inline" for="Gender-0" style="color:black">
              <input type="radio" name="Gender" id="Gender-0" value="1" checked="checked" >
              Nam
            </label> 
            <label class="radio-inline" for="Gender-1" style="color:black">
              <input type="radio" name="Gender" id="Gender-1" value="2" >
              Nữ
            </label> 
          </div>
        </div>

        <div class="form-group">
          <label class="col-lg-3 control-label" style="color:black">Nghề nghiệp:</label>
          <div class="col-lg-8">
            <input id="job" name="job" type="text" placeholder="học sinh, sinh viên,...." class="form-control input-md">
          </div>
        </div>
        <div class="form-group">
          <label class="col-lg-3 control-label" style="color:black">Email:</label>
          <div class="col-lg-8">
            <input id="email" name="email" type="text" placeholder="xxx@.com" class="form-control input-md">
          </div>
        </div>
        <div class="form-group">
          <label class="col-lg-3 control-label" style="color:black">SĐT:</label>
          <div class="col-lg-8">
            <input id="phone" name="phone" type="text" placeholder="0xxxxxxxx" class="form-control input-md">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label" style="color:black">Tên tài khoản:</label>
          <div class="col-md-8">
            <input class="form-control"  type="text" placeholder="tho tho" class="form-control input-md">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label"  style="color:black">Mật khẩu:</label>
          <div class="col-md-8">
            <input class="form-control"  type="password">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label"  style="color:black">Nhập lại mật khẩu:</label>
          <div class="col-md-8">
            <input class="form-control"  type="password">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label"></label>
          <div class="col-md-8">
            <input class="btn btn-primary" value="Lưu thay đổi" type="button">
            <span></span>
            <input class="btn btn-default" value="Cancel" type="reset">
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
<!--upload image -->
<br/> <br/>
<br/> <br/>
<br/> <br/>

<!-- <?php //include('footer.html');?> -->
</body>

