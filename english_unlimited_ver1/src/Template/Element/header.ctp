

<header id="fh5co-header-section" role="header" class="" >
    <div class="container">



        <!-- <div id="fh5co-menu-logo"> -->
        <!-- START #fh5co-logo -->
        <h1 id="fh5co-logo" class="pull-left"><a href="index.php"><img src="/learncakephp/webroot/img/logo4.png" alt="Slant Free HTML5 Template"></a></h1>

        <!-- START #fh5co-menu-wrap -->
        <nav id="fh5co-menu-wrap" role="navigation">


            <ul class="sf-menu" id="fh5co-primary-menu">
                <li><a href="Homes">Trang Chủ</a></li>
                <li><a href="Topics" >Chủ đề</a></li>
                <li><a href="Contacts">Liên hệ</a></li>
                <li><a href="">Giới thiệu</a></li>
                <li class="fh5co-special"><a href="<?php echo $this->Url->build(["controller" => "Signs", "action" => "login"]); ?>">Đăng nhập</a></li>
                <!-- <li class="fh5co-special" style="margin-left: 15px"><a href="contact.html">Đăng nhập</a></li> -->
            </ul>
        </nav>
        <!-- </div> -->

    </div>
</header>
    <div id="fh5co-hero">
            <a href="#fh5co-main" class="smoothscroll fh5co-arrow to-animate hero-animate-4"><i class=" fa fa-sort-desc"></i></a>
            <!-- End fh5co-arrow -->
            <div class="container">
                <div class="col-md-8 col-md-offset-2">
                    <div class="fh5co-hero-wrap">
                        <div class="fh5co-hero-intro">
                            <h1 class="to-animate hero-animate-1">Chào mừng bạn đến với </h1>
                            <h1 class="to-animate hero-animate-1" style="background-color: #66a3ff;margin-top: 40px;padding: 10px">English Unlimited</h1>
                            <h2 class="to-animate hero-animate-2">Cùng nhau học tiếng anh bạn nhé</h2>
                            <p class="to-animate hero-animate-3"><a href="<?php echo $this->Url->build(["controller" => "Signs", "action" => "register"]); ?>" class="btn btn-outline btn-lg">ĐĂNG KÝ</a></p>
                        </div>
                    </div>
                </div>
            </div>      
        </div>