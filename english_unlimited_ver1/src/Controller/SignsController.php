<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * homes Controller
 *
 * @property \App\Model\Table\homesTable $homes
 *
 * @method \App\Model\Entity\home[] paginate($object = null, array $settings = [])
 */
class SignsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        // $homes = $this->paginate($this->Homes);

         //$this->set(compact('index'));
        // $this->set('_serialize', ['homes']);
    }
    // public function initialize()
    // {
    //     parent::initialize();

    //     $this->loadComponent('RequestHandler');
    //     $this->loadComponent('Flash');
    //       //$this->viewBuilder()->layout('frontend_short');
    //     $this->Auth->allow(['logout']);
    // }

        //login
        public function initialize()
        {
            $this->loadComponent('Flash');
            $this->loadComponent('Auth', [
                'authenticate' => [
                    'Form' => [
                        'fields' => [
                            'username' => 'email',
                            'password' => 'password'
                        ]
                    ]
                ],
                'loginAction' => [
                    'controller' => 'signs',
                    'action' => 'login'
                ],
                'unauthorizedRedirect' => $this->referer() // If unauthorized, return them to page they were just on
            ]);
// Allow the display action so our pages controller
// continues to work.
            $this->Auth->allow( 
                'index',
                'login',
                'register'
            );
            // 18 Chapter 2. Quick Start Guide
            // CakePHP Cookbook Documentation, Release 3.4
        }
//.login

    public function login()
    {
         echo '<script> alert("ver 1 signs/login!") </script>';


        $user = '';
        if ($this->request->is('post')) {
            echo '<script> alert("ver 1 signs/login is post!") </script>';

            $user = $this->Auth->identify(); // mã hóa dữ liệu
            if ($user) {
                echo "da vao toi day doi";
                $this->Auth->setUser($user);
                //return $this->redirect($this->Auth->redirectUrl());
                echo '<script> alert("chuyển hướng ver 1!") </script>';

                return $this->redirect(
                    //['Controller' => 'Slants', 'action' => 'index']
                    '/users/add'
                    );
            }else{
                echo "Chu kiem tra duoc du lieu";
            }
            $this->Flash->error('Your username or password is incorrect.');
        }
        $this->set('user',$user);
    }

    public function register(){
        
    }

    // khởi tạo
 

    public function logout()
    {
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }

 
}
