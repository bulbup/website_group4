<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Chủ đề</title>

 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"/> -->
	 <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="../css/chude.css">
		
</head>
<body>
    <div id="wrapper">
        <div class="overlay"></div>
    
        <!-- Sidebar -->
        <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
            <ul class="nav sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">
                       Brand
                    </a>
                </li>
                <li>
                    <a href="#">Home</a>
                </li>
                <li>
                    <a href="#">About</a>
                </li>
                <li>
                    <a href="#">Events</a>
                </li>
                <li>
                    <a href="#">Team</a>
                </li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Works <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li class="dropdown-header">Dropdown heading</li>
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li><a href="#">Separated link</a></li>
                    <li><a href="#">One more separated link</a></li>
                  </ul>
                </li>
                <li>
                    <a href="#">Services</a>
                </li>
                <li>
                    <a href="#">Contact</a>
                </li>
                <li>
                    <a href="https://twitter.com/maridlcrmn">Follow me</a>
                </li>
            </ul>
        </nav>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                <span class="hamb-top"></span>
    			<span class="hamb-middle"></span>
				<span class="hamb-bottom"></span>
            </button>
            <div class="container">
               <div class="row">
		<div class="col-sm-3">
			<div class="thumbnail" style="padding: 0">
				<div style="padding:4px">
					<img alt="300x200" style="width: 100%" src="http://placehold.it/200x150">
				</div>
				<div class="caption">
					<h2>Project A</h2>
					<p>My project description</p>
					<p><i class="icon icon-map-marker"></i> Place, Country</p>
				</div>
				<div class="modal-footer" style="text-align: left">
					<div class="progress progress-striped active" style="background: #ddd">
						<div class="bar bar-warning" style="width: 60%;"></div>
					</div>
					<div class="row">
						<div class="col-sm-4"><b>60%</b><br/><small>FUNDED</small></div>
						<div class="col-sm-4"><b>$400</b><br/><small>PLEDGED</small></div>
						<div class="col-sm-4"><b>18</b><br/><small>DAYS</small></div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
			<div class="thumbnail" style="padding: 0">
				<div style="padding:4px">
					<img alt="300x200" style="width: 100%" src="http://placehold.it/200x150">
				</div>
				<div class="caption">
					<h2>Project B</h2>
					<p>My project description</p>
					<p><i class="icon icon-map-marker"></i> Place, Country</p>
				</div>
				<div class="modal-footer" style="text-align: left">
					<div class="progress" style="background: #ddd">
						<div class="bar bar-success" style="width: 100%;"></div>
					</div>
					<div class="row">
						<div class="col-sm-4"><b>250%</b><br/><small>FUNDED</small></div>
						<div class="col-sm-4"><b>$50000</b><br/><small>PLEDGED</small></div>
						<div class="col-sm-4"><b>FUNDED</b><br/><small>AUG 5</small></div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-3">
            <div class="thumbnail" style="padding: 0">
              <div style="padding:4px">
                <img alt="300x200" style="width: 100%" src="http://placehold.it/200x150">
              </div>
              <div class="caption">
                <h2>Project C</h2>
                <p>My project description</p>
                <p><i class="icon icon-map-marker"></i> Place, Country</p>
              </div>
              <div class="modal-footer" style="text-align: left">
                <div class="progress progress-striped active" style="background: #ddd">
                  <div class="bar bar-danger" style="width: 30%;"></div>
                </div>
                <div class="row">
                  <div class="col-sm-4"><b>60%</b><br/><small>FUNDED</small></div>
                  <div class="col-sm-4"><b>$1000</b><br/><small>PLEDGED</small></div>
                  <div class="col-sm-4"><b>NOT FUNDED</b><br/><small></small></div>
                </div>
              </div>
            </div>
        </div>
    	<div class="col-sm-3">
            <div class="thumbnail" style="padding: 0">
              <div style="padding:4px">
                <img alt="300x200" style="width: 100%" src="http://placehold.it/200x150">
              </div>
              <div class="caption">
                <h2>Project D</h2>
                <p>My project description (FROZEN, INACTIVE)</p>
                <p><i class="icon icon-map-marker"></i> Place, Country</p>
              </div>
              <div class="modal-footer" style="text-align: left">
                <div class="progress" style="background: #ddd">
                  <div class="bar bar-info" style="width: 40%;"></div>
                </div>
                <div class="row">
                  <div class="col-sm-4"><b>60%</b><br/><small>FUNDED</small></div>
                  <div class="col-sm-4"><b>$1000</b><br/><small>PLEDGED</small></div>
                  <div class="col-sm-4"><b>NOT FUNDED</b><br/><small></small></div>
                </div>
              </div>
            </div>
        </div>
	</div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

<script type="text/javascript">
	
	$(document).ready(function () {
  var trigger = $('.hamburger'),
      overlay = $('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    function hamburger_cross() {

      if (isClosed == true) {          
        overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
      } else {   
        overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
      }
  }
  
  $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
  });  
});
</script>

</div>
</body>
</html>