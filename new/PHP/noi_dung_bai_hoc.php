<!DOCTYPE html>
<html lang="en">
<head>
	<title>Bootstrap Example</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link href="../css/noi_dung_bai_hoc-css.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>

	<div class="tab_container">
		<input id="tab1" type="radio" name="tabs" checked>
		<label for="tab1"><i class="fa fa-code"></i><span>Dễ</span></label>

		<input id="tab2" type="radio" name="tabs">
		<label for="tab2"><i class="fa fa-pencil-square-o"></i><span>Trung Bình</span></label>

		<input id="tab3" type="radio" name="tabs">
		<label for="tab3"><i class="fa fa-bar-chart-o"></i><span>Khó</span></label>

		
		<section id="content1" class="tab-content">

			<div class="notice notice-warning">
				<strong> Hãy nghe và theo dõi đoạn đối thoại sau</strong>
			</div>
			<div class="row">
				<div class="col-sm-4" style="background-color:lavender;text-align: center;">
					<img src="../image/topic_food.jpg" width="" height="" alt=""/>
				</div>
				<div class="col-sm-8" style="background-color:lavenderblush;">
				
	<audio controls>
	  <source src="../image/audio.m4a" type="audio/ogg">
	  <source src="horse.mp3" type="audio/mpeg">
	Your browser does not support the audio element.
	</audio>

				<pre>
A: to see please thank you
B: i made a few cookies yesterday wanna bite
A: I sure wow that's delicious
B: so fast
A: i'm glad you like it to cook my other 
B: well i do actually but mosstly choose Vietnamese food
A: so tell me more about it I miss but what is coming
B: well we lot of rice and vegetables but noodles are very common to eat
A: what your favorite friend 
B: I have to say- beef noodle soup and now i'm good at making it- how about you  you cook ofen
A: no no no i don't i usually just eat out- same time
sometimes i do that to come by often well i do wish me but mostly choose vietnamese food
B: so tell me more about it no means but what is common 
well we love rice and vegetables but noodles are very common-and what do you what's your favorite thing 
A: I have to say it's a beef noodle soup and now i'm good at making it you
				</pre>
				</div>


				<div class="col-sm-8">
					<!--bài dịch-->      
					<div class="bai_dich">
						<button type="button" class="btn btn-info btn-lg btn3d"  data-toggle="collapse" data-target="#demo">
							<span class="glyphicon glyphicon-question-sign"></span>
							xem bài dịch
						</button>


						<div id="demo" class="collapse">
							Lorem ipsum dolor text....Lorem ipsum dolor text....Lorem ipsum dolor text....Lorem ipsum dolor text....Lorem ipsum dolor text....Lorem ipsum dolor text....Lorem ipsum dolor text....Lorem ipsum dolor text....Lorem ipsum dolor text....Lorem ipsum dolor text....Lorem ipsum dolor text....Lorem ipsum dolor text....Lorem ipsum dolor text....Lorem ipsum dolor text....Lorem ipsum dolor text....Lorem ipsum dolor text....
						</div>		
					</div>
					<!--.bài dịch-->
				</div>

			</div>   

			<!--câu hỏi-->

			<div class="notice notice-info">
				<strong>Hãy lắng nghe và trả lời bốn câu hỏi sau</strong> 
			</div>

			
			<div class="row">
				<div class="col-sm-3">
					<div style="background-image: url('../image/note1.png'); background-repeat: repeat-x; width: 230px; height: 250px;line-height: 200px; text-align: center; margin-bottom: 50px;">
						<i class="fa fa-headphones" style="font-size:36px"></i>Lắng nge
						<i class="fa fa-microphone" style="font-size:36px"></i>Trả lời
					</div>
				</div>
				<div class="col-sm-3">
					<div style="background-image: url('../image/note2.png'); background-repeat: repeat-x; width: 230px; height: 250px; line-height: 200px; text-align: center; margin-bottom: 50px;">
						<i class="fa fa-headphones" style="font-size:36px"></i>Lắng nge
						<i class="fa fa-microphone" style="font-size:36px"></i>Trả lời
					</div>
				</div>
				<div class="col-sm-3">
					<div style="background-image: url('../image/note3.png'); background-repeat: repeat-x; width: 230px; height: 250px; line-height: 200px; text-align: center; margin-bottom: 50px;">
						<i class="fa fa-headphones" style="font-size:36px"></i>Lắng nge
						<i class="fa fa-microphone" style="font-size:36px"></i>Trả lời
					</div>
				</div>
				<div class="col-sm-3">
					<div style="background-image: url('../image/note4.png'); background-repeat: repeat-x; width: 230px; height: 250px; line-height: 200px; text-align: center; margin-bottom: 50px;">
						<i class="fa fa-headphones" style="font-size:36px"></i>Lắng nge
						<i class="fa fa-microphone" style="font-size:36px"></i>Trả lời
					</div>
				</div>
			</div>
			<!--.câu hỏi-->

			

			
			<div class="notice notice-success">
				<strong>Sơ đồ tư duy sau sẽ giúp bạn dễ học hơn</strong><br>
			</div>		
			<!--mindmap-->
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-5">
					<img src="../image/mindmap1.png" alt="Planets" usemap="#planetmap"  style="">

					<map name="planetmap" >

						<div style="position: absolute; top: 180px; left: 50px;">
							<a title="Header" data-toggle="popover" data-placement="top" data-content="Content">
								<area shape="poly" coords="38,148,56,145,66,155,82,157,96,166,117,173,121,188,121,194,121,203,121,216,121,222,104,230,91,239,79,243,69,238,63,234,49,237,33,231,23,228,16,225,14,214,7,209,-1,195,15,172,15,169">sister</a>
							</div>


							<div  style="position: absolute; top: 80px; left: 110px;">
								<a  title="Header" data-toggle="popover" data-placement="top" data-content="Content">
									<area shape="poly" coords="98,53,110,39,129,44,135,46,148,48,159,53,166,58,182,63,185,73,190,85,183,91,190,102,189,109,181,120,174,123,167,123,152,135,137,132,130,126,119,133,106,127,99,122,85,120,79,113,77,104,69,94,74,79,79,61">father</a>
								</div>

								<div style="position: absolute; top: 40px; left:270px;">
									<a   title="Header" data-toggle="popover" data-placement="top" data-content="Content">
										<area shape="poly" coords="294,80,281,87,263,78,246,68,235,52,242,37,256,16,273,6,284,4,294,9,310,11,323,18,340,22,348,31,354,41,353,63,344,77,316,90,304,86">Daughter</a>
									</div>

									<div style="position: absolute; top: 115px; left:420px;">
										<a  target="#map" title="Header" data-toggle="popover" data-placement="top" data-content="Content">
											<area shape="poly" coords="438,158,412,155,396,151,386,130,377,118,388,107,396,94,413,86,430,76,443,84,458,90,474,94,488,101,496,111,496,117,494,129,499,135,497,144,488,149,462,162">brother</a>
										</div>


										<div style="position: absolute; top: 250px; left:400px;">
											<a  target="#map" title="Header" data-toggle="popover" data-placement="top" data-content="Content">
												<area shape="poly" coords="380,238,397,226,416,234,435,236,447,241,465,249,468,259,468,274,458,291,438,295,425,297,409,290,399,296,382,291,370,283,361,274,357,266,367,245">mother</a>
											</div>


											<div style="position: absolute; top: 300px; left:180px;">
												<a title="Header" data-toggle="popover" data-placement="top" data-content="Content">
													<area shape="poly" coords="160,263,177,255,203,262,222,263,238,273,256,284,261,293,261,303,263,319,260,324,251,331,238,335,227,343,218,347,195,336,174,341,164,334,147,328,140,316,134,309,132,296,144,277">son</a>
												</div>

											</map>

										</div>  

										<script>
											$(document).ready(function(){
												$('[data-toggle="popover"]').popover();   
											});
										</script>
									</div>
									<div class="col-md-4"></div>
<br><br>
									<!--play game-->
									<div align="center">
									<button class="button"><span>Play game </span></button>
									</div>
									<!--.play game--> 
								
								<div style="position: relative;" >


									     

									<div class="notice notice-danger">
										<strong>Bài học gợi ý</strong> 
									</div>

									<!-- video gợi ý -->
									<div class="container">
										<div class="row">
											<div class="span12">
												<div class="well"> 
													<div id="myCarousel" class="carousel slide">

														<ol class="carousel-indicators">
															<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
															<li data-target="#myCarousel" data-slide-to="1"></li>
															<li data-target="#myCarousel" data-slide-to="2"></li>
														</ol>

														<!-- Carousel items -->
														<div class="carousel-inner">

															<div class="item active">
																<div class="row-fluid">
																	<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
																	<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
																	<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
																	<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
																</div><!--/row-fluid-->
															</div><!--/item-->

															<div class="item">
																<div class="row-fluid">
																	<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
																	<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
																	<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
																	<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
																</div><!--/row-fluid-->
															</div><!--/item-->

															<div class="item">
																<div class="row-fluid">
																	<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
																	<div class="col-sm-3""><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
																	<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
																	<div class="col-sm-3"><a href="#x" class="thumbnail"><img src="http://placehold.it/250x250" alt="Image" style="max-width:100%;" /></a></div>
																</div><!--/row-fluid-->
															</div><!--/item-->

														</div><!--/carousel-inner-->

														<a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
														<a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
													</div><!--/myCarousel-->

												</div><!--/well-->   
											</div>
										</div>
									</div>
									<!-- .video  gợi ý -->
									<script type="text/javascript">
										$(document).ready(function() {
											$('#myCarousel').carousel({
												interval: 3000
											})
										});
									</script>


								</section>

							</section>

							<section id="content2" class="tab-content">
								<div class="row">
									<div class="col-sm-4" style="background-color:lavender;">.col-sm-4</div>
									<div class="col-sm-8" style="background-color:lavenderblush;">.col-sm-8</div>
								</div>	

							</section>

							<section id="content3" class="tab-content">
								<div class="row">
									<div class="col-sm-4" style="background-color:lavender;">.col-sm-4</div>
									<div class="col-sm-8" style="background-color:lavenderblush;">.col-sm-8</div>
								</div>	

							</section>

							
						</div>
						</div>
					</body>
					</html>
