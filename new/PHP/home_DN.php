<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Home English Online</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"/>
 	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="../css/home_DN.css">
  <script rel="stylesheet" src="../js/home_new.js"></script>

  <!-- đăng ký đăng nhập -->
  <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="../login-signup-modal/css/reset.css"> <!-- CSS reset -->
  <link rel="stylesheet" href="../login-signup-modal/css/style.css"> <!-- Gem style -->
  <script src="../login-signup-modal/js/modernizr.js"></script> <!-- Modernizr -->
  <!-- end đăng ký đăng nhập -->

  <!-- icon -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style type="text/css">

    li a:hover {
      text-decoration: underline !important;
    }
  </style>

</head>

<body>
<div class="slider-container">

<!-- đăng ký đăng nhập -->
  <header role="banner">
    <nav class="main-nav">
      <ul>
        <!-- inser more links here -->
        <i class="fa fa-user-circle-o" style="font-size: 30px; color: black; position: relative; top: 3px;"></i>
        <li><a class="cd-signin" href="google.com">Nguyễn Văn Luân</a>|</li>
        <li><a class="cd-signup" href="#0">Thoát</a></li>
      </ul>
    </nav>
  </header>
<!-- end đăng ký đăng nhập -->

  <div class="slider-control left inactive"></div>
  <div class="slider-control right"></div>
  <ul class="slider-pagi"></ul>

  <div class="slider">
    <div class="slide slide-0 active">
      <div class="slide__bg"></div>
      <div class="slide__content">
        <svg class="slide__overlay" viewBox="0 0 720 405" preserveAspectRatio="xMaxYMax slice">
          <path class="slide__overlay-path" d="M0,0 150,0 500,405 0,405" />
        </svg>
        <div class="slide__text">
          <h2 class="slide__text-heading">Chào mừng đến English Online</h2>
          <p class="slide__text-desc">Làm sao cho mỗi người Việt chúng ta giỏi tiếng Anh là ước mơ và sứ mệnh của chúng tôi. Chúng tôi xin chia sẻ với tất cả các bạn bằng cả trái tim mình để ước mơ này trở thành hiện thực.</p>
          <a href="cachhoc.php"
          class="slide__text-link">Xem cách học</a>
        </div>
      </div>
    </div>
    <div class="slide slide-1 ">
      <div class="slide__bg"></div>
      <div class="slide__content">
        <svg class="slide__overlay" viewBox="0 0 720 405" preserveAspectRatio="xMaxYMax slice">
          <path class="slide__overlay-path" d="M0,0 150,0 500,405 0,405" />
        </svg>
        <div class="slide__text">
          <h2 class="slide__text-heading">Thư viện LTDT</h2>
          <p class="slide__text-desc">Nhiều chủ đề khác nhau ở hầu hết các lĩnh vực. Các bài học trong chủ đề được thiết kế theo 4 kỹ năng: nghe, nói, đọc, viết và chia thành 3 cấp độ: Cơ Bản, Trung Bình, Nâng Cao giúp người học phát triển toàn diện khả năng tiếng Anh.</p>
          <a href="#" 
          class="slide__text-link">Vào thư viện</a>
        </div>
      </div>
    </div>
    <div class="slide slide-2">
      <div class="slide__bg"></div>
      <div class="slide__content">
        <svg class="slide__overlay" viewBox="0 0 720 405" preserveAspectRatio="xMaxYMax slice">
          <path class="slide__overlay-path" d="M0,0 150,0 500,405 0,405" />
        </svg>
        <div class="slide__text">
          <h2 class="slide__text-heading">Tư vấn nhanh chóng</h2>
          <p class="slide__text-desc">Nếu có bất cứ thắc mắc gì cần được hỗ trợ về phương pháp cũng như bài học, hãy liên hệ với chúng tôi ngay!!!</p>
          <a href="" class="slide__text-link">Liên hệ</a>
        </div>
      </div>
    </div>
    <div class="slide slide-3">
      <div class="slide__bg"></div>
      <div class="slide__content">
        <svg class="slide__overlay" viewBox="0 0 720 405" preserveAspectRatio="xMaxYMax slice">
          <path class="slide__overlay-path" d="M0,0 150,0 500,405 0,405" />
        </svg>
        <div class="slide__text">
          <h2 class="slide__text-heading">Về chúng tôi</h2>
          <p class="slide__text-desc">Đội ngũ nhân viên nhiệt tình, sẵn sàng đồng hành cùng bạn trên con đường học tiếng Anh sắp tới.</p>
          <a href="" class="slide__text-link">Gặp chúng tôi</a>
        </div>
      </div>
    </div>
  </div>
</div>


</body>
</html>
