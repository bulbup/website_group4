<div id="pricing">
<h2 align="center"; style="color:rgba(255,0,0,1)"><strong>3 BƯỚC HỌC TIẾNG ANH HIỆU QUẢ</strong></h2><br /><br />
	<div class="price_card alpha">
		<div class="header">
			<span class="price">Học theo chủ đề</span>
		
		</div>
		<ul class="features">
			<li>Bạn có thể lựa chọn các chủ đề để học theo sở thích và mong muốn.</li>
			<li>Mỗi chủ đề cung cấp nhiều bài học thú vị. </li>
		</ul>
		
		
	</div>
	<div class="price_card bravo">
		<div class="header">
			<span class="price">Học theo kỹ năng</span>
			</div>
		<ul class="features">
			<li>Với mỗi bài học mà GoodEnglish4sK.com đem lại có đầy đủ các kĩ năng: nghe, nói, đọc, viết. </li>
			
			<li> Mỗi kĩ năng được áp dụng phương pháp học thông minh như: mindmap, video, flashcard</li>
		</ul>
		
		
	</div>
	<div class="price_card charlie">
		<div class="header">
			<span class="price">Kiểm tra</span>
			
		</div>
		<ul class="features">
			<li>Bài kiểm tra giúp đánh giá tiến độ của bạn.</li>
			<li> So sánh trước và sau khi học một cách rõ ràng và cụ thể.</li>
		</ul>
		
	</div>
</div>