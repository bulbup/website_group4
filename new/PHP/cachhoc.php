<link rel="stylesheet" type="text/css" href="../css/buochoc.css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
 	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <h2 class="text-center" style="color: red"><B>4 YẾU TỐ HỌC TỐT TIẾNG ANH</B></h2>
      
      <ul class="timeline">
        <li>
          <div class="timeline-image">
            <img class="img-circle img-responsive" src="../image/niem_tin1.jpeg" alt="">
          </div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4></h4>
              <h4 class="subheading"  style="color:#240B7A"><b>Niềm tin</b></h4>
            </div>
            <div class="timeline-body">
              <p class="text-muted">
                Niềm tin là yếu tố quan trọng nhất giúp bạn thực hiện bất kì công việc gì trong mọi lĩnh vực. Bạn sẽ chẳng thể thành công nều luôn để sự nghi ngờ tồn tại trong tâm trí. Vì nó chính là rào cản lớn nhất khiến bạn không thể tự tin đưa ra và thực thi bất cứ dự định gì. Vì vậy, hãy loại bỏ những suy nghĩ như “ mình dốt”, “ mình không có năng khiếu”,… ra khỏi đầu và đặt niềm tin vào chính bản thân mình.
              </p>
            </div>
          </div>
          <div class="line"></div>
        </li>
        <li class="timeline-inverted">
          <div class="timeline-image">
            <img class="img-circle img-responsive" src="../image/ke_hoach.jpg" alt="">
          </div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4></h4>
              <h4 class="subheading"  style="color:#240B7A"><b>Kế hoạch</b></h4>
            </div>
            <div class="timeline-body">
              <p class="text-muted">
                Liệt kê các công việc cần phải làm khi học tiếng anh, các mục tiêu cần hướng tới theo một trình tự nhất định và được thực hiện trong một khoảng thời gian nhất định, cụ thể.
              </p>
            </div>
          </div>
          <div class="line"></div>
        </li>
        <li>
          <div class="timeline-image">
            <img class="img-circle img-responsive" src="../image/thuc_hien.png" alt="">
          </div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4></h4>
              <h4 class="subheading"  style="color:#240B7A"><b>Thực hiện</b></h4>
            </div>
            <div class="timeline-body">
              <p class="text-muted">
               Nếu bạn đã hoàn thành được 2 bước đầu thì việc khó khăn cuối cùng chỉ là bạn hãy thực hiện và kiên trì với kế hoạch của mình. Bí quyết để thực hiện được việc khó khăn này là hãy luôn suy nghĩ tích cực và đặt niềm tin vào chính bản thân mình.
              </p>
            </div>
          </div>
          <div class="line"></div>
        </li>
        <li class="timeline-inverted">
          <div class="timeline-image">
            <img class="img-circle img-responsive" src="../image/niem_tin.jpg" alt="">
          </div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h4></h4>
              <h4 class="subheading"  style="color:#240B7A"><b>Động lực và đam mê</b></h4>
            </div>
            <div class="timeline-body">
              <p class="text-muted">
                Chắc hẳn nếu bạn đã đọc đến đây thì có lẽ bạn đã thực sự mong muốn tự học tiếng anh. Để luôn giữ ngọn lửa đam mê trong mình hãy liệt kê hết những lý do bạn phải học tiếng anh như: thăng tiến trong công việc, đi du lịch nước ngoài, xem bất kì chương trình tiếng anh mà không cần phụ đề…Hãy chọn ra một vài lý do mà bạn thực sự tâm đắc nhất để lấy đó làm kim chỉ nam cho những bước đi tiếp theo trên con đường chinh phục tiếng Anh.abcde
              </p>
            </div>
          </div>
        
        </li>
       
      </ul>
    </div>
  </div>
</div>