<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
<div style="position: relative">

	<img src="../image/mindmap1.png" alt="Planets" usemap="#planetmap">
	
<map name="planetmap">

<div style="position: absolute; top: 180px; left: 50px;">
	 <a href="" title="Header" data-toggle="popover" data-placement="top" data-content="Content">
	 <area shape="poly" coords="38,148,56,145,66,155,82,157,96,166,117,173,121,188,121,194,121,203,121,216,121,222,104,230,91,239,79,243,69,238,63,234,49,237,33,231,23,228,16,225,14,214,7,209,-1,195,15,172,15,169" href="#">sister</a>
  </div>


<div  style="position: absolute; top: 80px; left: 110px;">
	  <a href="" title="Header" data-toggle="popover" data-placement="top" data-content="Content">
	  <area shape="poly" coords="98,53,110,39,129,44,135,46,148,48,159,53,166,58,182,63,185,73,190,85,183,91,190,102,189,109,181,120,174,123,167,123,152,135,137,132,130,126,119,133,106,127,99,122,85,120,79,113,77,104,69,94,74,79,79,61" href="#">father</a>
</div>

  <div style="position: absolute; top: 40px; left:270px;">
	  <a href="" title="Header" data-toggle="popover" data-placement="top" data-content="Content">
	  <area shape="poly" coords="294,80,281,87,263,78,246,68,235,52,242,37,256,16,273,6,284,4,294,9,310,11,323,18,340,22,348,31,354,41,353,63,344,77,316,90,304,86" href="#">Daughter</a>
  </div>
  
  <div style="position: absolute; top: 115px; left:420px;">
	  <a href="" title="Header" data-toggle="popover" data-placement="top" data-content="Content">
	  <area shape="poly" coords="438,158,412,155,396,151,386,130,377,118,388,107,396,94,413,86,430,76,443,84,458,90,474,94,488,101,496,111,496,117,494,129,499,135,497,144,488,149,462,162" href="#">brother</a>
  </div>


  <div style="position: absolute; top: 250px; left:400px;">
	  <a href="" title="Header" data-toggle="popover" data-placement="top" data-content="Content">
	  <area shape="poly" coords="380,238,397,226,416,234,435,236,447,241,465,249,468,259,468,274,458,291,438,295,425,297,409,290,399,296,382,291,370,283,361,274,357,266,367,245" href="#">mother</a>
  </div>

		
  <div style="position: absolute; top: 300px; left:180px;">
	  <a href="" title="Header" data-toggle="popover" data-placement="top" data-content="Content">
	  <area shape="poly" coords="160,263,177,255,203,262,222,263,238,273,256,284,261,293,261,303,263,319,260,324,251,331,238,335,227,343,218,347,195,336,174,341,164,334,147,328,140,316,134,309,132,296,144,277" href="#">son</a>
  </div>
  
</map>

</div>




<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();   
});
	</script>


</body>
</html>
