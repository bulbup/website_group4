<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Home English Online</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"/>
 	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css"/>
  <link rel="stylesheet" type="text/css" href="../css/home_new.css">
  <script rel="stylesheet" src="../js/home_new.js"></script>

  <!-- đăng ký đăng nhập -->
  <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="../login-signup-modal/css/reset.css"> <!-- CSS reset -->
  <link rel="stylesheet" href="../login-signup-modal/css/style.css"> <!-- Gem style -->
  <script src="../login-signup-modal/js/modernizr.js"></script> <!-- Modernizr -->
  <!-- end đăng ký đăng nhập -->

  <!-- icon -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <style type="text/css">

    li a:hover {
      text-decoration: underline !important;
    }
  </style>

</head>

<body>
<div class="slider-container">

<!-- đăng ký đăng nhập -->
  <header role="banner">
    <nav class="main-nav">
      <ul>
        <!-- inser more links here -->
        <i class="fa fa-user-circle-o" style="font-size: 30px; color: black; position: relative; top: 3px;"></i>
        <li><a class="cd-signin" href="#0">Đăng nhập</a>|</li>
        <li><a class="cd-signup" href="#0">Đăng ký</a></li>
      </ul>
    </nav>
  </header>
<!-- .đăng ký đăng nhập -->

<!--   <div class="slider-control left inactive"></div>
  <div class="slider-control right"></div> -->
 <!--  <ul class="slider-pagi"></ul> -->

  <div class="slider">
    <div class="slide slide-0 active">
      <div class="slide__bg"></div>
      <div class="slide__content">
        <svg class="slide__overlay" viewBox="0 0 720 405" preserveAspectRatio="xMaxYMax slice">
          <path class="slide__overlay-path" d="M0,0 150,0 500,405 0,405" />
        </svg>
        <div class="slide__text">
          <h2 class="slide__text-heading">Chào mừng đến English Online</h2>
          <p class="slide__text-desc">Làm sao cho mỗi người Việt chúng ta giỏi tiếng Anh là ước mơ và sứ mệnh của chúng tôi. Chúng tôi xin chia sẻ với tất cả các bạn bằng cả trái tim mình để ước mơ này trở thành hiện thực.</p>
          <a href="cachhoc.php"
          class="slide__text-link">Xem cách học</a>
        </div>
      </div>
    </div>
    <div class="slide slide-1 ">
      <div class="slide__bg"></div>
      <div class="slide__content">
        <svg class="slide__overlay" viewBox="0 0 720 405" preserveAspectRatio="xMaxYMax slice">
          <path class="slide__overlay-path" d="M0,0 150,0 500,405 0,405" />
        </svg>
        <div class="slide__text">
          <h2 class="slide__text-heading">Thư viện LTDT</h2>
          <p class="slide__text-desc">Nhiều chủ đề khác nhau ở hầu hết các lĩnh vực. Các bài học trong chủ đề được thiết kế theo 4 kỹ năng: nghe, nói, đọc, viết và chia thành 3 cấp độ: Cơ Bản, Trung Bình, Nâng Cao giúp người học phát triển toàn diện khả năng tiếng Anh.</p>
          <a href="chu_de.php" 
          class="slide__text-link">Vào thư viện</a>
        </div>
      </div>
    </div>
    <div class="slide slide-2">
      <div class="slide__bg"></div>
      <div class="slide__content">
        <svg class="slide__overlay" viewBox="0 0 720 405" preserveAspectRatio="xMaxYMax slice">
          <path class="slide__overlay-path" d="M0,0 150,0 500,405 0,405" />
        </svg>
        <div class="slide__text">
          <h2 class="slide__text-heading">Tư vấn nhanh chóng</h2>
          <p class="slide__text-desc">Nếu có bất cứ thắc mắc gì cần được hỗ trợ về phương pháp cũng như bài học, hãy liên hệ với chúng tôi ngay!!!</p>
          <a href="contact.php" class="slide__text-link">Liên hệ</a>
        </div>
      </div>
    </div>
    <div class="slide slide-3">
      <div class="slide__bg"></div>
      <div class="slide__content">
        <svg class="slide__overlay" viewBox="0 0 720 405" preserveAspectRatio="xMaxYMax slice">
          <path class="slide__overlay-path" d="M0,0 150,0 500,405 0,405" />
        </svg>
        <div class="slide__text">
          <h2 class="slide__text-heading">Về chúng tôi</h2>
          <p class="slide__text-desc">Đội ngũ nhân viên nhiệt tình, sẵn sàng đồng hành cùng bạn trên con đường học tiếng Anh sắp tới.</p>
          <a href="" class="slide__text-link">Gặp chúng tôi</a>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Đăng ký Đăng nhập -->
  <div class="cd-user-modal"> <!-- this is the entire modal form, including the background -->
    <div class="cd-user-modal-container"> <!-- this is the container wrapper -->
      <ul class="cd-switcher">
        <li><a href="#0">Sign in</a></li>
        <li><a href="#0">New account</a></li>
      </ul>

      <div id="cd-login"> <!-- log in form -->
        <form class="cd-form" name="frmdangnhap" method="post" action="">
          <p class="fieldset">
            <label class="image-replace cd-email" for="signin-email">E-mail</label>
            <input class="full-width has-padding has-border" id="signin-email" type="email" placeholder="E-mail">
            <span class="cd-error-message">Error message here!</span>
          </p>

          <p class="fieldset">
            <label class="image-replace cd-password" for="signin-password">Password</label>
            <input class="full-width has-padding has-border" id="signin-password" type="text"  placeholder="Password">
            <a href="#0" class="hide-password">Hide</a>
            <span class="cd-error-message">Error message here!</span>
          </p>

          <p class="fieldset">
            <input type="checkbox" id="remember-me" checked>
            <label for="remember-me">Remember me</label>
          </p>

          <p class="fieldset">
            <input class="full-width" type="submit" value="Login">
          </p>
        </form>
        
        <p class="cd-form-bottom-message"><a href="#0">Forgot your password?</a></p>
        <a href="#0" class="cd-close-form">Close</a>
      </div> <!-- cd-login -->

      <div id="cd-signup"> <!-- sign up form -->
        <form class="cd-form">
          <p class="fieldset">
            <label class="image-replace cd-username" for="signup-username">Username</label>
            <input class="full-width has-padding has-border" id="signup-username" type="text" placeholder="Username">
            <span class="cd-error-message">Error message here!</span>
          </p>

          <p class="fieldset">
            <label class="image-replace cd-email" for="signup-email">E-mail</label>
            <input class="full-width has-padding has-border" id="signup-email" type="email" placeholder="E-mail">
            <span class="cd-error-message">Error message here!</span>
          </p>

          <p class="fieldset">
            <label class="image-replace cd-password" for="signup-password">Password</label>
            <input class="full-width has-padding has-border" id="signup-password" type="text"  placeholder="Password">
            <a href="#0" class="hide-password">Hide</a>
            <span class="cd-error-message">Error message here!</span>
          </p>

          <p class="fieldset">
            <input type="checkbox" id="accept-terms">
            <label for="accept-terms">I agree to the <a href="#0">Terms</a></label>
          </p>

          <p class="fieldset">
            <input class="full-width has-padding" type="submit" value="Create account">
          </p>
        </form>

        <a href="#0" class="cd-close-form">Close</a>
      </div> <!-- cd-signup -->

      <div id="cd-reset-password"> <!-- reset password form -->
        <p class="cd-form-message">Lost your password? Please enter your email address. You will receive a link to create a new password.</p>

        <form class="cd-form">
          <p class="fieldset">
            <label class="image-replace cd-email" for="reset-email">E-mail</label>
            <input class="full-width has-padding has-border" id="reset-email" type="email" placeholder="E-mail">
            <span class="cd-error-message">Error message here!</span>
          </p>

          <p class="fieldset">
            <input class="full-width has-padding" type="submit" value="Reset password">
          </p>
        </form>

        <p class="cd-form-bottom-message"><a href="#0">Back to log-in</a></p>
      </div> <!-- cd-reset-password -->
      <a href="#0" class="cd-close-form">Close</a>
    </div> <!-- cd-user-modal-container -->
  </div> <!-- cd-user-modal -->
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="../login-signup-modal/js/main.js"></script> <!-- Gem jQuery -->
<!--  -->


<h1>asdfghjkl;</h1>
</body>
</html>
