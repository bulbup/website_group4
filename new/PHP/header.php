 <!DOCTYPE html>
 <html>
 <head>
 	<title>website</title>

 	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
 	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
 	<!-- header -->
 	<link rel="stylesheet" type="text/css" href="css/header.css">

    <link href="https://fonts.googleapis.com/css?family=Lora|Roboto+Slab" rel="stylesheet">
 	<!-- footer -->
	<link rel="stylesheet" type="text/css" href="css/footer.css">
	<!-- kỹ năng -->
	<link rel="stylesheet" type="text/css" href="baihoc/kynang.css">
	<!-- chủ đê -->
<!--	<link rel="stylesheet" type="text/css" href="css/chude.css">-->
	<!-- bước học -->
	<link rel="stylesheet" type="text/css" href="css/buochoc.css">
	<!-- yếu tố 1 -->
	<link rel="stylesheet" type="text/css" href="css/yeuto1.css">
	<!-- giới thiệu -->
	<link rel="stylesheet" type="text/css" href="css/gioithieu.css">
	<!-- liên hệ -->
	<link rel="stylesheet" type="text/css" href="css/lienhe.css">
	<!-- trở lại -->
	<link rel="stylesheet" type="text/css" href="css/trolai_top.css">
 	<!-- jquery -->
 	<script type="text/javascript" src="js/jquery.min.js"></script>
 	<!-- slider -->
 	<link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet" media="all">
 	
 	<!-- end slider -->

 	<!-- đăng nhập-->
	
  	<style type="text/css">
  		#frmdk, #frmcaptcha{
  			height: 50px;
  			line-height: 50px;
  			margin-bottom: 10px;

  		}
  		/*#frmcaptcha{
  			border-radius: 100px;
  			height: 50px;
  			line-height: 50px;
  			margin-bottom: 10px;
  		}*/

  		#frmOK{
  			/*border-radius: 100px;*/
  			height: 50px;
  			line-height: 50px;
  			margin-bottom: 10px;
  			
  		}
  		#frmMXH{
  			/*border-radius: 100px;*/
  			height: 50px;
  			line-height: 50px;
  			margin-bottom: 10px;
  		}

  	</style>
 	<!-- end đăng nhập -->

 </head>
 <body>
 	<div class="navbar-more-overlay"></div>
 	<nav class="navbar navbar-inverse navbar-fixed-top animate" style="margin-bottom: 0px !important;">
 		<div class="container navbar-more visible-xs">
 			<form class="navbar-form navbar-left" role="search">
 				<div class="form-group">
 					<div class="input-group">
 						<input type="text" class="form-control" placeholder="Search for...">
 						<span class="input-group-btn">
 							<button class="btn btn-default" type="submit">Submit</button>
 						</span>
 					</div>
 				</div>
 			</form>
 			<ul class="nav navbar-nav">
 				<li>
 					<a href="#">
 						<span class="menu-icon fa fa-picture-o"></span>
 						Photos
 					</a>
 				</li>
 				<li>
 					<a href="#">
 						<span class="menu-icon fa fa-bell-o"></span>
 						Reservations
 					</a>
 				</li>
 				<li>
 					<a href="#">
 						<span class="menu-icon fa fa-picture-o"></span>
 						Photos
 					</a>
 				</li>
 				<li>
 					<a href="#">
 						<span class="menu-icon fa fa-bell-o"></span>
 						Reservations
 					</a>
 				</li>
 				<li>
 					<a href="#">
 						<span class="menu-icon fa fa-picture-o"></span>
 						Photos
 					</a>
 				</li>
 				
 			</ul>
 		</div>
 		<div class="container">
 			<div class="navbar-header hidden-xs">
 				<a class="navbar-brand" href="#">English_Online</a>
 			</div>

 			<ul class="nav navbar-nav navbar-right mobile-bar">
 				<li>
 					<a href="#">
 						<span class="menu-icon fa fa-home"></span>
 						Trang chủ
 					</a>
 				</li>
 				<li>
 					<a href="#">
 						<span class="menu-icon fa fa-info"></span>
 						<span class="hidden-xs">Giới thiệu</span>
 						<span class="visible-xs">Giới thiệu</span>
 					</a>
 				</li>
 				
 				<li class="hidden-xs">
 					<a data-toggle="modal" data-target="#myModal" style="cursor: pointer;">
 						<span class="menu-icon fa fa-bell-o"></span>
 						Đăng ký
 					</a>
 				</li>

 				<li class="hidden-xs">
 					<a href="#">
 						<span class="menu-icon fa fa-bell-o"></span>
 						Đăng nhập
 					</a>
 				</li>
 				<li>
 					<a href="#">
 						<span class="menu-icon fa fa-phone"></span>						
 						<span class="hidden-xs">Liên hệ</span>
 						<span class="visible-xs">Liên hệ</span>
 					</a>
 				</li>
 				<li class="visible-xs">
 					<a href="#navbar-more-show">
 						<span class="menu-icon fa fa-bars"></span>
 						More
 					</a>
 				</li>
 			</ul>
 		</div>
 	</nav> 
<!-- đăng ký -->
 	<div class="modal fade" id="myModal" role="dialog">
 		<div class="modal-dialog">

 			<!-- Modal content-->
 			<div class="modal-content" style="text-align: center; background-color: #ffffff;">
 				<div class="modal-header">
 					<button type="button" class="close" data-dismiss="modal" style="width: 25px;">&times;</button>
 					<h2 class="modal-title" style="color: blue;">Đăng ký thành viên</h2>
 				</div>

 				<div class="modal-body">
 					<form method="post" name="frmdangky" action=""  style="padding-left: 50px; padding-right: 50px;">
	 					 <div class="form-group">

	 					 	<div class="row"  class="frmdk">
		 					 	<div class="col-md-4">
		 					 		<label >Họ và tên</label>
		 					 	</div>
	 					 		
	 					 		<div class="col-md-8">
	 					 			<input id="frmdk" type="text" name="hoten" placeholder="Họ và tên" required="" class="form-control">
	 					 		</div>
	 					 	</div>

	 					 	<div class="row"  class="frmdk">
		 					 	<div class="col-md-4">
		 					 		<label >Email</label>
		 					 	</div>
	 					 		
	 					 		<div class="col-md-8">
	 					 			<input id="frmdk" type="text" name="hoten" placeholder="Họ và tên" required="" class="form-control">
	 					 		</div>
	 					 	</div>

	 					 	<div class="row"  class="frmdk">
		 					 	<div class="col-md-4">
		 					 		<label >Mật khẩu</label>
		 					 	</div>
	 					 		
	 					 		<div class="col-md-8">
	 					 			<input id="frmdk" type="text" name="hoten" placeholder="Họ và tên" required="" class="form-control">
	 					 		</div>
	 					 	</div>
	 					 	<div class="row"  class="frmdk">
		 					 	<div class="col-md-4">
		 					 		<label >Nhập lại mật khẩu</label>
		 					 	</div>
	 					 		
	 					 		<div class="col-md-8">
	 					 			<input id="frmdk" type="text" name="hoten" placeholder="Họ và tên" required="" class="form-control">
	 					 		</div>
	 					 	</div>

	 					 	<!-- captcha -->
	 					 	<div class="row"  class="frmdk">
		 					 	<div class="col-md-4">
		 					 		<!-- <label >Mật khẩu</label> -->
		 					 		<label id="frmcaptcha" style="margin: 0px auto;"><img id="captcha" src="php/captcha_code.php" onclick="document.getElementById('captcha').src='php/captcha_code.php?sid='+Math.random();" title="Đổi mã khác"></label>
		 					 	</div>
	 					 		
	 					 		<div class="col-md-8">
	 					 			<!-- <input id="frmdk" type="text" name="hoten" placeholder="Họ và tên" required="" class="form-control"> -->
	 					 			<input id="frmcaptcha" type="text" name="captcha" placeholder="Nhập mã captcha" required="" class="form-control">
	 					 		</div>
	 					 	</div>

	 					 	<div class="row">
	 					 		<br>
	 					 		<div class="col-md-3"></div>
	 					 		<div class="col-md-3">
	 					 			<!-- <button id="frmOK" type="submit" name="dangky" class="btn btn-primary" class="form-control">Đăng ký</button> -->
	 					 			<input type="submit" name="dangky" class="form-control">
	 					 		</div>
	 					 		<div class="col-md-3">
	 					 			<!-- <button id="frmOK" type="submit" name="dangky" class="btn btn-primary" class="form-control">Đăng ký</button> -->
	 					 			<input type="reset" name="reset" class="form-control">
	 					 		</div>
	 					 		<div class="col-md-3"></div>
	 					 	</div>

	 					 	<!-- mạng xã hội -->
	 					 	<div class="row">
	 					 		<div class="col-md-12">
	 					 			<br>
	 					 			<h5 style="color: blue;">Đăng ký bằng tài khoản xã hôi?</h5>
	 					 		</div>
	 					 	</div>

	 					 	<div class="row">
	 					 		<div class="col-md-12">
	 					 			<a class="caret" data-toggle="collapse" data-target="#mxh"></a>
	 					 		</div>
	 					 	</div>

	 					 	<div class="row">
	 					 		<div id="mxh" class="collapse" >
	 					 			<div class="col-md-2"></div>
		 					 		<div class="col-md-4">
		 					 			<button type="submit" name="dangky" class="form-control">Facebook</button>
		 					 		</div>
		 					 		<!-- <div class="col-md-4">
		 					 			<button type="submit" name="dangky" class="form-control">Zalo</button>
		 					 		</div> -->
									<div class="col-md-4">
		 					 			<button type="submit" name="dangky" class="form-control">Google+</button>
		 					 		</div>
		 					 		<div class="col-md-2"></div>

		 					 	</div>
	 					 	</div>


	 					</div>
	 						

 						<!-- đăng ký reset -->
 						<!-- <div class="form-group" style="margin-bottom: 5px; height: 50px !important;">
 							<button id="frmOK" style=" float: left; color: #fff; background-color: #CC00FF;" type="submit" name="dangky">Đăng ký</button>

 							<button id="frmOK" style="float: right;" type="reset">Reset</button>
 						</div> -->

 						<!-- mạng xã hội -->
 						<!-- <div class="form-group">
 							<br/>
 							<h5 style="color: blue;">Đăng ký bằng tài khoản xã hôi?</h5>
 							<a class="caret" data-toggle="collapse" data-target="#mxh"></a>
 							<div id="mxh" class="collapse" >
 								<div class="row">
 									<div class="col-md-4" style="background-color:yellow;">
	 								<button id="frmMXH" style="/*float: left; margin-left: 35px; background-color: #AB82FF; color: #fff;*/" type="submit" name="dangky">Facebook</button>

	 							</div>
 								<div class="col-md-4" style="background-color:red;">
	 								<button id="frmMXH" style="/*float: left; margin-left: 20px; background-color: green; color: #fff;*/" type="reset">Zalo</button>
	 							</div>
 								<div class="col-md-4" style="background-color:yellow;">
	 								<button id="frmMXH"  style="/*float: left; margin-left: 20px; background-color: red; color: #fff;*/" type="submit">Google+</button>
	 							</div>
 								</div>
	 							
 							</div> 
 						</div> -->
 			
 						
 					</form>
 				</div>
 				<div class="modal-footer" style="text-align: center;"> 							
 						<h5>Bạn đã có tài khản?<a data-toggle="modal" data- data-target="#myModalDN" style="cursor: pointer;">Đăng nhập</a></h5>
 						
 				</div>

 				</div>
 			</div>

 		</div>
 	</div>
<!-- end đăng ký -->

<!-- đăng nhập -->
	<!-- <div class="modal fade" id="myModalDN" role="dialog">
 		<div class="modal-dialog"> -->

 			<!-- Modal content-->
 			<!-- <div class="modal-content" style="text-align: center; background-color: #ffffff;">
 				<div class="modal-header">
 					<button type="button" class="close" data-dismiss="modal" style="width: 25px;">&times;</button>
 					<h2 class="modal-title" style="color: blue;">Đăng ký thành viên</h2>
 				</div>

 				<div class="modal-body">
 					<form method="post" name="frmdangky" action=""  style="padding-left: 50px; padding-right: 50px;">
	 					 <div class="form-group">

	 						<input id="frmdk" type="Email" name="email" placeholder="Email" required="" class="form-control">

	 						<input id="frmdk" type="Password" name="Password" placeholder="Mật khẩu" required="" class="form-control">
	 					</div> -->
	 					

 						<!-- đăng ký reset -->
 						<!-- <div class="form-group" style="margin-bottom: 5px; height: 50px !important;">
 							<button id="frmOK" style=" float: left; color: #fff; background-color: #CC00FF;" type="submit" name="dangky">Đăng nhập</button>

 							<button id="frmOK" style="float: right;" type="reset">Reset</button>
 						</div> -->

 						<!-- mạng xã hội -->
 						<!-- <div class="form-group">
 							<br/>
 							<h5 style="color: blue;">Đăng ký bằng tài khoản xã hôi?</h5>
 							<a class="caret" data-toggle="collapse" data-target="#mxh"></a>
 							<div id="mxh" class="collapse">
 								<button id="frmMXH" style=" float: left; margin-left: 35px; background-color: #AB82FF; color: #fff;" type="submit" name="dangky">Facebook</button>

 							<button id="frmMXH" style="float: left; margin-left: 20px; background-color: green; color: #fff;" type="reset">Zalo</button>

 							<button id="frmMXH" style="float: left; margin-left: 20px; background-color: red; color: #fff;" type="submit">Google+</button>

 							</div> 
 						</div>
 			
 						
 					</form>
 				</div>
 				<div class="modal-footer" style="text-align: center;"> 							
 						<h5>Bạn đã có tài khản?<a href="">Đăng nhập</a></h5>
 						
 				</div>

 				</div>
 			</div>

 		</div>
 	</div> -->
<!-- end đăng nhập -->

<!-- quên mật khẩu -->
<!-- end quên mật khẩu -->

 <!-- </body>
 </html>
-->
<script type="text/javascript">
	$(document).ready(function() {
		$('a[href="#navbar-more-show"], .navbar-more-overlay').on('click', function(event) {
			event.preventDefault();
			$('body').toggleClass('navbar-more-show');
			if ($('body').hasClass('navbar-more-show'))	{
				$('a[href="#navbar-more-show"]').closest('li').addClass('active');
			}else{
				$('a[href="#navbar-more-show"]').closest('li').removeClass('active');
			}
			return false;
		});
	});
</script>

