<footer class="container-fluid text-center bg-lightgray">

	<div class="copyrights" style="margin-top:25px;">
		<p>English Online © 2017, All Rights Reserved
			<br>
			<span>Web Design By: Group 4</span></p>
			<p><a href="https://www.linkedin.com/in/michael-clark-webdeveloper" target="_blank">Linkedin <i class="fa fa-linkedin-square" aria-hidden="true"></i> </a></p>
		</div>
	</footer>
</body>
</html>
     