<?php include('includes/header.php'); ?>

	<div class="row" style="width: 80%; float:right; margin: 50px 50px 0px 0px;">
		<div class=" col-lg-12 col-md-12 col-xs-12 col-sm-12">
		<?php
			$errors = array();
			if ($_SERVER['REQUEST_METHOD'] == 'POST') {
				$taikhoan = $_POST['taikhoan'];
				$matkhau = md5($_POST['matkhau']);
				$xn_matkhau = md5($_POST['xn_matkhau']);
				$hoten = $_POST['hoten'];
				$diachi = $_POST['diachi'];
				$email = $_POST['email'];
				$sdt = $_POST['sdt'];
				$trangthai = $_POST['trangthai'];

				include('../inc/myconnect.php');
				include('../inc/function.php');

				// kiểm tra tài khoản đã tồn tại hay chưa
				$query1 = "select taikhoan from tbluser where taikhoan = '{$taikhoan}'";
				$result1 = mysqli_query($dbc, $query1);
				kt_query($result1, $query1);

				// kiểm tra email đã tồn tại hay chưa
				$query2 = "select email from tbluser where email = '{$email}'";
				$result2 = mysqli_query($dbc, $query2);
				kt_query($result2, $query2);

				// kiểm tra tài khoản và email đã tồn tại hay chưa
				if($_POST['matkhau'] != $_POST['xn_matkhau']){
					$errors[] = 'xn_matkhau';
				}
				if(mysqli_num_rows($result1) == 1){
					$errors[] = 'taikhoan';
				}
				if(mysqli_num_rows($result2) == 1){
					$errors[] = 'email';
				}
				if(empty($errors)){

					$sql = "INSERT INTO tbluser(taikhoan, matkhau, hoten, diachi, email, sđt, status) VALUES('{$taikhoan}', '{$matkhau}', '{$hoten}', '{$diachi}', '{$email}', '{$sdt}', '{$trangthai}' )";
					$result = mysqli_query($dbc, $sql);
					kt_query($result, $sql);
					if(mysqli_affected_rows($dbc) == 1){
						$taikhoan = '';
						$matkhau = '';
						$xn_matkhau = '';
						$hoten = '';
						$diachi = '';
						$email = '';
						$sdt = '';
						echo '<script> alert("Thêm mới User thành công!") </script>';
					}else{
						echo '<script> alert("Lỗi cú pháp SQL!") </script>';
					}
				}//else{
					//echo var_dump($errors);
					//echo '<script> alert("Còn lỗi!") </script>';

				//}
				

			}
		?>

		<form name="frm_adduser" method="post" action="">
			<h3>Thêm mới User</h3>

			<div class="form-group">
				<label>Tài khoản</label>
				<input type="text" name="taikhoan" class="form-control" value="<?php echo isset($taikhoan)?$taikhoan:'';?>" placeholder="Tài khoản" required=" ">
				<?php if(isset($errors) && in_array('taikhoan',$errors)){ echo '<p style="color:red;">Tài khoản đã tồn tại<p>';} ?>
			</div>

			<div class="form-group">
				<label>Mật khẩu</label>
				<input type="password" name="matkhau" class="form-control" value="<?php //echo isset($matkhau)?$matkhau:'';?>"  placeholder="Mật khẩu" required=" ">
			</div>

			<div class="form-group">
				<label>Xác nhận mật khẩu</label>
				<input type="password" name="xn_matkhau" class="form-control" value="<?php //echo isset($xn_matkhau)?$xn_matkhau:'';?>"  placeholder="Nhập lại mật khẩu" required=" ">
				<?php if(isset($errors) && in_array('xn_matkhau',$errors)){ echo '<p style="color:red;">Mật khẩu xác nhận chưa đúng</p>';} ?>
			</div>

			<div class="form-group">
				<label>Họ tên</label>
				<input type="text" name="hoten" class="form-control" value="<?php echo isset($hoten)?$hoten:'';?>" placeholder="Họ tên" required=" ">
			</div>
			<div class="form-group">
				<label>Địa chỉ</label>
				<input type="text" name="diachi" class="form-control" value="<?php echo isset($diachi)?$diachi:'';?>" placeholder="Địa chỉ" required=" ">
			</div>
			<div class="form-group">
				<label>Email</label>
				<input type="Email" name="email" class="form-control" value="<?php echo isset($email)?$email:'';?>" placeholder="Email" required=" ">
				<?php if(isset($errors) && in_array('email',$errors)){ echo '<p style="color:red;">Email đã tồn tại</p>';} ?>
			</div>
			<div class="form-group">
				<label>Số điện thoại</label>
				<input type="number" name="sdt" class="form-control" value="<?php echo isset($sdt)?$sdt:'';?>" placeholder="Số điện thoại" required=" ">
			</div>
			<div class="form-group">
				<label style="display: block;">Trạng thái</label>
				<label class="radio-inline"><input type="radio" name="trangthai"  value="1" checked="checked">Hiển thị</label>
				<label class="radio-inline"><input type="radio" name="trangthai" value="0">Không hiển thị</label>
			</div>
			<div class="form-group">
				<input type="submit" name="submit" value="Thêm user" class="btn btn-primary">
			</div>

		</form>
			
		</div>
	</div>



<?php include('includes/footer.php'); ?>