<?php include('includes/header.php');?>
	<div class="row" style="width: 80%; margin: 50px 50px 0px 0px; float: right;">
		<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
			<?php
				include('../inc/myconnect.php');
				include('../inc/function.php');
// load dữ liệu lên form
				$id = '';
				if(isset($_GET['id']) && filter_var($_GET['id'],FILTER_VALIDATE_INT,array('min_range'=>1))){
					$id = $_GET['id'];
					$sql = "select title,anh,link,ordernum,status from tblslider where id = '{$id}'";
					$result = mysqli_query($dbc,$sql);
					kt_query($result,$sql);

					if(mysqli_affected_rows($dbc) == 1){
						list($title,$anh,$link,$ordernum,$status) = mysqli_fetch_array($result);
					}else{
						echo '<script> alert("Lỗi truy vấn!"); </script>';
					}

				}else{
					echo '<script> alert("ID không tồn tại!"); </script>';
				}
// kết thúc phần load dữ liệu lên form
				$errors = array();
				if($_SERVER['REQUEST_METHOD']=='POST'){
					$img = '';
					$link_img = '';
					$title = $_POST['title'];
					//$anh = $_POST['img'];
					$link = $_POST['link'];
					$ordernum = $_POST['ordernum'];
					$status = $_POST['status'];


						if ($_FILES['img']['size'] == ''){
							$errors[] = 'img';

							$query="update tblslider set title ='{$title}', link = '{$link}', ordernum = $ordernum, status = $status where id = '{$id}'";
							$result=mysqli_query($dbc,$query);
							kt_query($result,$query);

							if(mysqli_affected_rows($dbc)==1){
								$title='';
								$link='';
								$ordernum='';
								echo '<script> alert("Updata Slider thành công!"); </script>';
							
							}else{
								echo '<script> alert("Lỗi! Update Slider thất bại"); </script>';

							}

						}elseif($_FILES['img']['size'] != ''){

							if(
								($_FILES['img']['type'] != 'image/gif') &&
								($_FILES['img']['type'] != 'image/png') &&
								($_FILES['img']['type'] != 'image/jpeg') &&
								($_FILES['img']['type'] != 'image/jpg')
								){

								echo '<script> alert("File upload không đúng định dạng!") </script>';

							}elseif ($_FILES['img']['size'] > 1000000) {
								echo '<script> alert("File upload phải nhỏ hơn 1MB!") </script>';
							}else{
								// xóa ảnh cũ
								$sql_anh = "select anh,anh_thumb from tblslider where id = {$id}";
								$result_anh = mysqli_query($dbc, $sql_anh);
								$slider_anh = mysqli_fetch_assoc($result_anh);
								unlink('../'.$slider_anh['anh']);
								unlink('../'.$slider_anh['anh_thumb']);

								//
								$img = $_FILES['img']['name'];
								$link_img = 'upload/'.$img;
								move_uploaded_file($_FILES['img']['tmp_name'], '../upload/'.$img);
								//
								

								// câu lệnh SQL
								$query="update tblslider set title = '{$title}', anh = '{$link_img}', link = '{$link}', ordernum = $ordernum, status = $status where id = '{$id}'";
								$result=mysqli_query($dbc,$query);
								kt_query($result,$query);

								if(mysqli_affected_rows($dbc)==1){
									$title='';
									$link='';
									$ordernum='';
									echo '<script> alert("Updata Slider thành công!"); </script>';
								
								}else{
									echo '<script> alert("Lỗi! Update Slider thất bại"); </script>';

								}

							}

						}

				}

			?>
			<form name="frmadd_video" method="POST" enctype="multipart/form-data">
			<h3>Sửa Slider</h3>
				<div class="form-group">
					<label>Title</label>
					<input type="text" name="title" value="<?php echo isset($title)?$title:''; ?>" class="form-control" placeholder="Title" required=''>
				</div>
				<div class="form-group">
					<label>Ảnh đại diện</label>
					<p><img width="150" src="../<?php echo $anh;?>"></p>
					<input type="file" name="img" value="">
					<?php 
						if(isset($errors) && in_array('img', $errors)){
							echo '<p style="color: blue;" >Image không thay đổi!</p>';
						} 
					?>
				</div>

				<div class="form-group">
					<label>Link</label>
					<input type="text" name="link" value="<?php echo isset($link)?$link:''; ?>" class="form-control" placeholder="link slider" required=''>			
				</div>

				<div class="form-group">
					<label>Thứ tự</label>
					<input type="text" name="ordernum" value="<?php echo isset($ordernum)?$ordernum:''; ?>" class="form-control" placeholder="Thứ tự">
				</div>

				<div class="form-group">
					<?php
						$hienthi = '';
						$khonghienthi = '';
						if($status == 1){
							$hienthi = 'checked';
						}
						if($status == 0){
							$khonghienthi = 'checked';
						}
					?>
					<label style="display: block;">Trạng thái</label>
					<label class="radio-inline"><input type="radio" <?php echo isset($hienthi)?$hienthi:''; ?> name="status" value="1">Hiển thị</label>
					<label class="radio-inline"><input type="radio" <?php echo isset($khonghienthi)?$khonghienthi:''; ?> name="status" value="0">Không hiển thị</label>
				</div>

				<input type="submit" name="submit" class="btn btn-primary" value="Sửa" style="margin-bottom: 20px">

			</form>
			
		</div>
	</div>

<?php include('includes/footer.php');?>
