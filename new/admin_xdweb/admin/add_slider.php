<?php
	//error_reporting(0);
?>

<style type="text/css">
.required
{
	color: red;
}
</style>
<?php include('includes/header.php') ?>
<div class="row" style="width: 80%; float: right; margin: 50px 50px 0px 0px;">
	<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
	<?php 
		include('../inc/myconnect.php');
		include('../inc/function.php');
		include('../inc/images_helper.php');

		if($_SERVER['REQUEST_METHOD']=='POST'){
				$title = $_POST['title'];
				$link = $_POST['link'];
				$ordernum = $_POST['ordernum'];
				$status = $_POST['status'];

			if(is_numeric($status)  && is_string($link) && is_string($title) ){
				$sql = "select * from tblslider";
				$result_sql = mysqli_query($dbc,$sql);
				kt_query($result_sql, $sql);
				foreach ($result_sql as $key => $value) {
					if($ordernum == $value['ordernum']){
						$ordernum++;
					}
				}

				if(
					($_FILES['img']['type'] != 'image/gif') &&
					($_FILES['img']['type'] != 'image/png') &&
					($_FILES['img']['type'] != 'image/jpeg') &&
					($_FILES['img']['type'] != 'image/jpg') 
					){

					echo '<script> alert("File upload không đúng định dạng! "); </script>';
				}else if($_FILES['img']['size'] > 1000000){
					echo '<script> alert("Kích thước file upload phải nhỏ hơn 1MB! "); </script>';

				}else if($_FILES['img']['size'] == ''){
					echo '<script> alert("Bạn chưa chọn file ảnh! "); </script>';

				}else{
					$img = $_FILES['img']['name'];
					$link_img = 'upload/'.$img;
					move_uploaded_file($_FILES['img']['tmp_name'], '../upload/'.$img);
				// xử lý resize và crip hình ảnh
					//hàm cắt chuỗi dựa vào dấu chấm
					$temp = explode('.', $img); // lấy được cái đuôi định dạng file ảnh
					if($temp[1] == 'jpeg' or $temp[1] == 'JPEG'){ // khi cắt ra thì $temp[1] là cái đuôi ảnh $temp[0] là cái tên ảnh
						$temp[1] = 'jpg';
					}
					// strtolower() hàm chuyển đổi thành chữ thường
					$temp[1] = strtolower($temp[1]);
					$thumb = 'upload/resized/'.$temp[0].'_thumb'.'.'.$temp[1];
					$image_thumb = new Image('../'.$link_img);
					// resize ảnh
					// if($image_thumb->getWidth()>700){
					// 	$image_thumb->resize(500,'resize');
					// }

					// crop ảnh khi muốn cố định chiều cao và chiều dọc của bức ảnh
					$image_thumb->resize(160,150,'crop');
					$image_thumb->save($temp[0].'_thumb','../upload/resized');

					//  

					$query="INSERT INTO tblslider(title,anh,anh_thumb,link,ordernum,status) VALUES ('{$title}','{$link_img}','{$thumb}','{$link}',$ordernum,$status)";
					$result=mysqli_query($dbc,$query);

					kt_query($result,$query);
				
					if (mysqli_affected_rows($dbc)==1){

						$title='';
						$link='';
						$ordernum='';
						echo '<script> alert("Thêm mới thành công!"); </script>';
						
					}else{
						echo '<script> alert("Lỗi! Không thành công!"); </script>';
					}
					
				}
	
			}else{
					if(!is_numeric(isset($ordernum)?$ordernum:'')){
						$ordernum = '';
					}
					if(!is_numeric($status)) {
						$status = '';
					}
					if(!is_string($link)) {
						$link = '';
					}
					if(!is_string($title)) {
						$title = '';
					}
					echo '<script> alert("Kiểu dữ liệu không hợp lệ!"); </script>';

			}
		}
		
	?>
		<form name="frmadd_video" method="POST" enctype="multipart/form-data">
		
		<h3>Thêm mới Slider</h3>
			<div class="form-group">
				<label>Title</label>
				<input type="text" name="title" value="<?php echo isset($title)?$title:''; ?>" class="form-control" placeholder="Title" required=''>
			</div>
			<div class="form-group">
				<label>Ảnh đại diện</label>
				<input type="file" name="img" value="">
			</div>

			<div class="form-group">
				<label>Link</label>
				<input type="text" name="link" value="<?php echo isset($link)?$link:''; ?>" class="form-control" placeholder="link slider" required=''>			
			</div>

			<div class="form-group">
				<label>Thứ tự</label>
				<input type="text" name="ordernum" value="<?php echo isset($ordernum)?$ordernum:''; ?>" class="form-control" placeholder="Thứ tự">
			</div>

			<div class="form-group">
				<label style="display: block;">Trạng thái</label>
				<label class="radio-inline"><input type="radio" checked="checked" name="status" value="1">Hiển thị</label>
				<label class="radio-inline"><input type="radio" name="status" value="0">Không hiển thị</label>
			</div>

			<input type="submit" name="submit" class="btn btn-primary" value="Thêm mới">

		</form>
	</div>
</div>
<?php include('includes/footer.php') ?>