<?php
	ob_start();
	session_start();
?>
<?php
	if(!isset($_SESSION['username']) && !isset($_SESSION['pass'])){
		//header('location: login.php');

		echo '<script> alert("Chưa có session username và pass") </script>';
		header('location: login.php');
	}

?>


<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE-edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="atuthor" content="">
	<title>Quản trị hệ thống</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
	<script type="text/javascript" src="../js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<!-- ckeditor -->
	<script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.replace('noidung');
	</script>

	<link rel="stylesheet" type="text/css" href="design_demo.css">
	<link rel="stylesheet" href="../font-awesome-4.7.0/css/font-awesome.min.css">
</head>
<body>
	<div id="wrapper">
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="home.php">QUẢN TRỊ HỆ THỐNG</a>
			</div>
			<!-- //Login, Logout -->	
			<ul class="nav navbar-right top-nav">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa user"></i>Xin chào:&nbsp;<?php echo isset($_SESSION['username'])?$_SESSION['username']:'không có giá trị'; ?><b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li>
							<a href="thongtin.php?id=<?php echo isset($_SESSION['uid'])?$_SESSION['uid']:'';?>"><i class="fa fa-fw fa-user"></i>Thông tin</a>
						</li>
						<li>
							<a href="doimatkhau.php"><i class="fa fa-fw fa-gear"></i>Đổi mật khẩu</a>
						</li>
						<li class="divider"></li>
						<li>
							<a href="logout.php"><i class="fa fa-fw fa-power-off"></i>Thoát</a>
						</li>
					</ul>
				</li>
			</ul>			
		</nav>
<?php include('includes/sidebar.php') ?>																																															