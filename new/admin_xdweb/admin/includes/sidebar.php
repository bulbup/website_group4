<div class="aside navbar-inverse">
			<ul class="nav navbar-nav slide-nav">
				<li style="background:#1b926c; color: #fff; padding-right: 82px;">
					<a href="demo.php" style="color:#fff"><i class="glyphicon glyphicon-home"></i>&nbsp;Dashboard</a>
				</li>
				<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo_bv">
						<i class="glyphicon glyphicon-book"></i>&nbsp;Chủ đề
						<i class="fa fa-fw fa-caret-down"></i>
					</a>
					<ul id="demo_bv" class="collapse">
						<li>
							<a href="#">Thêm mới</a>
						</li>
						<li>
							<a href="#">Danh sách</a>
						</li>
						
					</ul>
				</li>

				<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo_dm">
						<i class="glyphicon glyphicon-list-alt"></i>&nbsp;Bài học
						<i class="fa fa-fw fa-caret-down"></i>
					</a>
					<ul id="demo_dm" class="collapse">
						<li>
							<a href="#">Thêm mới</a>
						</li>
						<li>
							<a href="#">Danh sách</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo_support">
						<i class="glyphicon glyphicon-book"></i>&nbsp;Hỗ trợ trực tuyến
						<i class="fa fa-fw fa-caret-down"></i>
					</a>
					<ul id="demo_support" class="collapse">
						<li>
							<a href="#">Thêm mới</a>
						</li>
						<li>
							<a href="#">Danh sách</a>
						</li>
						
					</ul>
				</li>
<!-- 				<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo_slider">
						<i class="glyphicon glyphicon-book"></i>&nbsp;Slider
						<i class="fa fa-fw fa-caret-down"></i>
					</a>
					<ul id="demo_slider" class="collapse">
						<li>
							<a href="add_slider.php">Thêm mới</a>
						</li>
						<li>
							<a href="list_slider.php">Danh sách</a>
						</li>
						
					</ul>
				</li> -->
				<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo_vd">
						<i class="glyphicon glyphicon-book"></i>&nbsp;audio
						<i class="fa fa-fw fa-caret-down"></i>
					</a>
					<ul id="demo_vd" class="collapse">
						<li>
							<a href="add_video.php">Thêm mới</a>
						</li>
						<li>
							<a href="list_video.php">Danh sách</a>
					    </li>
						
					</ul>
				</li>	
				<li>
					<a href="javascript:;" data-toggle="collapse" data-target="#demo_u">
						<i class="glyphicon glyphicon-user"></i>&nbsp;User
						<i class="fa fa-fw fa-caret-down"></i>
					</a>
					<ul id="demo_u" class="collapse">
						<li>
							<a href="add_user.php">Thêm mới</a>
						</li>
						<li>
							<a href="list_user.php">Danh sách</a>
					    </li>
						
					</ul>
				</li>	
			</ul>
		</div>