<?php include('includes/header.php') ?>
<?php 
include('../inc/myconnect.php');
include('../inc/function.php'); 
?>
<style type="text/css">
	th{
		text-align: center;
	}
</style>
<div class="row pull-right" style="width: 80%; margin-right: 50px">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h3>Danh sách video</h3>
		<table class="table table-hover">
			<thead>
				<tr>
					<th>Mã</th>
					<th>Title</th>
					<th>Link</th>
					<th>Thứ tự</th>
					<th>Trạng thái</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
			<?php 
				$query="SELECT * FROM tblvideo ORDER BY ordernum ASC";
				$result=mysqli_query($dbc,$query);
				kt_query($result,$query);
				while ($video=mysqli_fetch_array($result,MYSQLI_ASSOC)) {
					//MYSQLI_ASSOC : Đưa ra một mảng với chỉ số của mảng là tên trường ví dụ tên trường là id, title....
			?>
			<tr>
				 <td><?php echo $video['id']; ?></td>
				 <td><?php echo $video['title']; ?></td>
				 <td><?php echo $video['link']; ?></td>
				 <td><?php echo $video['ordernum']; ?></td>
				<td>
					<?php  
						if($video['status']==1) 
						{
							echo "Hiển thị";
						}
						else{echo "Không hiển thị";}
					?>						
				</td>
				 <td align="center"><a href="edit_video.php?id=<?php echo $video['id']; ?>"><img width="16" src="../images/icon_edit.png"></td>
				 <td align="center"><a onclick="return confirm('Bạn có thật sự muốn xóa không')" href="delete_video.php?id=<?php echo $video['id']; ?>"><img width="16" src="../images/icon_delete.png"></td>
			</tr>
			<?php		
				}
			?>
			</tbody>
		</table>
	</div>
</div>
<?php include('includes/footer.php') ?>
