<?php
  ob_start();
  session_start();
?>

<?php
  if(isset($_SESSION['username']) && isset($_SESSION['pass'])){
    header('location: home.php');
  }
?>

<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Flat Login Form 3.0</title>
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

  <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
<link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'><link rel="stylesheet" href="form_login/css/style.css">

  
</head>

<body>

<?php
    include('../inc/myconnect.php');
    include('../inc/function.php');
  if($_SERVER['REQUEST_METHOD']=='POST'){

    $username = $_POST['username'];
    $pass = md5($_POST['pass']);

    $sql = "select id,taikhoan,matkhau,hoten,diachi,email,sđt from tbluser where taikhoan='{$username}' and matkhau='{$pass}'";
    $result = mysqli_query($dbc,$sql);
    kt_query($result, $sql);
    if(mysqli_num_rows($result)==1){
      list($id,$username,$pass,$hoten,$diachi,$email,$sdt) = mysqli_fetch_array($result);
       $_SESSION['uid'] = $id;
       $_SESSION['username'] = $username;
       $_SESSION['pass'] = $pass;
       $_SESSION['hoten'] = $hoten;
       $_SESSION['diachi'] = $diachi;
       $_SESSION['email'] = $email;
       $_SESSION['sdt'] = $sdt;

      //setcookie('username',$username, time()+5000);
      //setcookie('pass',$pass, time()+5000);
      header('location: home.php');
    }else{
      echo '<script> alert("Tài khoản hoặc mật khẩu không hợp lệ!"); </script>';
    }
  }

?>
  
<!-- Form Mixin-->
<!-- Input Mixin-->
<!-- Button Mixin-->
<!-- Pen Title-->
<div class="pen-title">
  <h1>Flat Login Form</h1><span>Pen <i class='fa fa-paint-brush'></i> + <i class='fa fa-code'></i> by <a href='#'>LuanNguyen</a></span>
</div>
<!-- Form Module-->
<div class="module form-module">
  <div class="toggle"><i class="fa fa-times fa-pencil"></i>
    <div class="tooltip">Click Me</div>
  </div>
  <div class="form">
    <h2>Login to your account</h2>
    <form name="frmlogin" method="post" action="">
      <input type="text" name="username" placeholder="Username" required="" />
      <input type="password" name="pass" placeholder="Password" required="" />
      <button name="login">Login</button>
    </form>
  </div>
  <div class="form">
    <h2>Create an account</h2>
    <form>
      <input type="text" placeholder="Username"/>
      <input type="password" placeholder="Password"/>
      <input type="email" placeholder="Email Address"/>
      <input type="tel" placeholder="Phone Number"/>
      <button>Register</button>
    </form>
  </div>
  <div class="cta"><a href="#">Forgot your password?</a></div>
</div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://codepen.io/andytran/pen/vLmRVp.js'></script>

    <script src="form_login/js/index.js"></script>

</body>
</html>
