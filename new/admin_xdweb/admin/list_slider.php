<?php include('includes/header.php') ?>
<div class="row">
	 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="width: 80%; margin: 50px 50px 0px 0px;">
		<h3> Danh sách Slider </h3>

		<table class="table table-hover">
			<thead>
				<tr>
					<th>Mã</th>
					<th>Tiêu đề</th>
					<th>Ảnh</th>
					<th>Link</th>
					<th>Trạng thái</th>
					<th align="center">Sửa</th>
					<th align="center">Xóa</th>
				</tr>
			</thead>

			<tbody>
			<?php
				include('../inc/myconnect.php');
				include('../inc/function.php');
				$sql = "select * from tblslider order by ordernum ASC";
				$result = mysqli_query($dbc,$sql);
				kt_query($result, $sql);
				
				while ($slider = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
				//}
			?>
				<tr>
					<td><?php echo $slider['id']; ?></td>
					<td><?php echo $slider['title']; ?></td>
					<td><img width="50"s src="../<?php echo $slider['anh']; ?>"/></td>
					<td><?php echo $slider['link']; ?></td>
					<td>
					<?php  
						if($slider['status']==1) 
						{
							echo "Hiển thị";
						}
						else{echo "Không hiển thị";}
					?>
						
					</td>
					<td><a href="edit_slider.php?id=<?php echo $slider['id']; ?>"><img width="16" src="../images/icon_edit.png"></a></td>
					<td><a onclick="return confirm('Bạn có thật sự muốn xóa không')" href="delete_slider.php?id=<?php echo $slider['id']; ?>"><img width="16" src="../images/icon_delete.png"></td>
				</tr> 
				<?php } ?>
			</tbody>
				 
		</table>
	</div>
</div>
<?php include('includes/footer.php'); ?>