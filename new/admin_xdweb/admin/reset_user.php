<?php include('includes/header.php'); ?>
	<?php
		include('../inc/myconnect.php');
		include('../inc/function.php');
		$id = '';
		if(isset($_GET['id']) && filter_var($_GET['id'],FILTER_VALIDATE_INT,array('min_range'=>1))){
			$id = $_GET['id'];
			$sql = "select taikhoan,hoten from tbluser where id = '{$id}'";
			$result = mysqli_query($dbc,$sql);
			kt_query($result, $sql);
			list($taikhoan,$hoten) = mysqli_fetch_array($result);
		}

		$errors = array();
		if($_SERVER['REQUEST_METHOD']=='POST'){
			$matkhaumoi = md5($_POST['matkhaumoi']);
			$xn_matkhaumoi = md5($_POST['xn_matkhaumoi']);
			if($matkhaumoi != $xn_matkhaumoi){
				$errors[] = 'xn_matkhaumoi';
			}else{
				$sql = "update tbluser set matkhau = '{$matkhaumoi}' where id = '{$id}'";
				$result = mysqli_query($dbc, $sql);
				kt_query($result,$sql);

				if(($result == 1) && (mysqli_affected_rows($dbc) == 1)){
					echo '<script> alert("Reset password thành công!") </script>';
				}else{
					echo '<script> alert("Reset password thất bại!") </script>';
				}			
			}
		}
	?>

	<div class="row" style="width: 80%; margin: 50px 50px 0px 0px; float: right;">
		<form name="frm_reset_user" method="post" action="">
			<h3>Reset User: <?php echo isset($hoten)?$hoten:''; ?></h3>
			<div class="form-group">
				<label>Tài khoản</label>
				<input type="text" name="taikhoan" value="<?php echo isset($taikhoan)?$taikhoan:''; ?>" disabled="true" class="form-control">
			</div>
			<div class="form-group">
				<label>Mật khẩu mới</label>
				<input type="text" name="matkhaumoi" value="" class="form-control">
			</div>
			<div class="form-group">
				<label>Nhập lại mật khẩu mới</label>
				<input type="text" name="xn_matkhaumoi" value="" class="form-control">
				<?php 
					if(isset($errors) && in_array('xn_matkhaumoi', $errors)){
						echo 'Mật khẩu nhập lại không đúng!';
					}
				?>
			</div>
			<div class="form-group">
				<input type="submit" name="submit" value="Reset" class="btn btn-primary">
			</div>
			
		</form>
	</div>

<?php include('includes/footer.php'); ?>