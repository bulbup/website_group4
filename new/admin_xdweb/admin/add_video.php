<style type="text/css">
.required
{
	color: red;
}
</style>
<?php include('includes/header.php') ?>
<div class="row" style="width: 80%; float: right; margin: 50px 50px 0px 0px;">
	<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
	<?php 
		include('../inc/myconnect.php');
		include('../inc/function.php');
		if($_SERVER['REQUEST_METHOD']=='POST'){
				$title = $_POST['title'];
				$link = $_POST['link'];
				$ordernum = $_POST['ordernum'];
				$status = $_POST['status'];
			if(is_numeric($status)  && is_string($link) && is_string($title) ){
				$sql = "select * from tblvideo";
				$result_sql = mysqli_query($dbc,$sql);
				if(!$result_sql){
					die("Query {$sql} \n <br> MYSQL Error:".mysqli_error($dbc));
				}
				foreach ($result_sql as $key => $value) {
					if($ordernum == $value['ordernum']){
						$ordernum++;
					}
				}

				$query="INSERT INTO tblvideo(title,link,ordernum,status) VALUES ('{$title}','{$link}',$ordernum,$status)";
				$result=mysqli_query($dbc,$query);

				if(!$result){
					die("Query {$query} \n <br> MYSQL Error:".mysqli_error($dbc));
				}
				
				if (mysqli_affected_rows($dbc)==1){

					$title='';
					$link='';
					$ordernum='';
					echo '<script> alert("Thêm mới thành công!"); </script>';
					
				}else{
					echo '<script> alert("Lỗi! Không thành công!"); </script>';
				}	
			}else{
					if(!is_numeric(isset($ordernum)?$ordernum:'')){
						$ordernum = '';
					}
					if(!is_numeric($status)) {
						$status = '';
					}
					if(!is_string($link)) {
						$link = '';
					}
					if(!is_string($title)) {
						$title = '';
					}
					echo '<script> alert("Kiểu dữ liệu không hợp lệ!"); </script>';

			}
		}
		
	?>
		<form name="frmadd_video" method="POST">
		
		<h3>Thêm mới Video</h3>
			<div class="form-group">
				<label>Title</label>
				<input type="text" name="title" value="<?php echo isset($title)?$title:''; ?>" class="form-control" placeholder="Title" required=''>
			</div>

			<div class="form-group">
				<label>Link</label>
				<input type="text" name="link" value="<?php echo isset($link)?$link:''; ?>" class="form-control" placeholder="Video" required=''>			
			</div>

			<div class="form-group">
				<label>Thứ tự</label>
				<input type="text" name="ordernum" value="<?php echo isset($ordernum)?$ordernum:''; ?>" class="form-control" placeholder="Thứ tự">
			</div>

			<div class="form-group">
				<label style="display: block;">Trạng thái</label>
				<label class="radio-inline"><input type="radio" checked="checked" name="status" value="1">Hiển thị</label>
				<label class="radio-inline"><input type="radio" name="status" value="0">Không hiển thị</label>
			</div>

			<input type="submit" name="submit" class="btn btn-primary" value="Thêm mới">

		</form>
	</div>
</div>
<?php include('includes/footer.php') ?>