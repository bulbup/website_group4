<?php
	ob_start();
?>

<?php include('includes/header.php'); ?>

	<div class="row" style="width: 80%; float:right; margin: 50px 50px 0px 0px;">
		<div class=" col-lg-12 col-md-12 col-xs-12 col-sm-12">
		<?php
				$taikhoan = '';
				$matkhau = '';
				$xn_matkhau = '';
				$hoten = '';
				$diachi = '';
				$email = '';
				$sdt = '';
				$trangthai = '';

			if (isset($_GET['id']) && filter_var($_GET['id'],FILTER_VALIDATE_INT,array('min_range'=>1))) {
				$id = $_GET['id'];
				include('../inc/myconnect.php');
				include('../inc/function.php');
				$sql = "select taikhoan,matkhau,hoten,email,sđt,diachi,status from tbluser where id = {$id}";
				$result = mysqli_query($dbc, $sql);
				kt_query($result, $sql);
				if(mysqli_num_rows($result) == 1){
					list($taikhoan,$matkhau,$hoten,$email,$sdt,$diachi,$trangthai) = mysqli_fetch_array($result);
				}else{
					echo '<script> alert("ID không tồn tại!"); </script>';
				}
			}else{
				header('location: list_user.php');
			}

			$errors = array();
			if($_SERVER['REQUEST_METHOD']=='POST'){

				$taikhoan = $_POST['taikhoan'];
				$matkhau = md5($_POST['matkhau']);
				$xn_matkhau = md5($_POST['matkhau']);
				$hoten = $_POST['hoten'];
				$diachi = $_POST['diachi'];
				$email = $_POST['email'];
				$sdt = $_POST['sdt'];
				$trangthai = $_POST['trangthai'];

				// $sql1 = "select taikhoan from tbluser where taikhoan='{$taikhoan}' and id != {$id}";
				// $result1 = mysqli_query($dbc,$sql1);
				// kt_query($result1,$sql1);

				$sql2 = "select email from tbluser where email='{$email}' and id != {$id}";
				$result2 = mysqli_query($dbc,$sql2);
				kt_query($result2,$sql2);

				// if(mysqli_num_rows($result1) == 1){
				// 	$errors[] = 'taikhoan';
				// }

				if(mysqli_num_rows($result2) == 1){
					$errors[] = 'email';
				}

				if(empty($errors)){
					$sql3 = "update tbluser set hoten='{$hoten}',diachi='{$diachi}',email='{$email}',sđt='{$sdt}',status={$trangthai} where id = {$id}";
					$result3 = mysqli_query($dbc,$sql3);
					kt_query($result3,$sql3);
					header('location: list_user.php');
				}else{
					echo 'còn lỗi'.'<br>';
					echo var_dump($errors);
				}

				
			}
		?>

		<form name="frm_adduser" method="post" action="">
			<h3>Sửa User: <?php echo $hoten; ?></h3>

			<div class="form-group">
				<label>Tài khoản</label>
				<input type="text" name="taikhoan" class="form-control" value="<?php echo isset($taikhoan)?$taikhoan:'';?>" placeholder="Tài khoản" required=" " disabled="true">
			</div>


			<div class="form-group">
				<label>Họ tên</label>
				<input type="text" name="hoten" class="form-control" value="<?php echo isset($hoten)?$hoten:'';?>" placeholder="Họ tên" required=" " >
			</div>
			<div class="form-group">
				<label>Địa chỉ</label>
				<input type="text" name="diachi" class="form-control" value="<?php echo isset($diachi)?$diachi:'';?>" placeholder="Địa chỉ" required=" " >
			</div>
			<div class="form-group">
				<label>Email</label>
				<input type="Email" name="email" class="form-control" value="<?php echo isset($email)?$email:'';?>" placeholder="Email" required=" ">
				<?php if(isset($errors) && in_array('email',$errors)){ echo '<p style="color:red;">Email đã tồn tại</p>';} ?>
			</div>
			<div class="form-group">
				<label>Số điện thoại</label>
				<input type="number" name="sdt" class="form-control" value="<?php echo isset($sdt)?$sdt:'';?>" placeholder="Số điện thoại" required=" ">
			</div>


			<div class="form-group">
				<?php
					$hienthi = '';
					$khonghienthi = '';
					if($trangthai == 1){
						$hienthi = 'checked';
					}
					if($trangthai == 0){
						$khonghienthi = 'checked';
					}
				?>
				<label style="display: block;">Trạng thái</label>
				<label class="radio-inline"><input type="radio" name="trangthai"  value="1" <?php echo $hienthi; ?> >Hiển thị</label>
				<label class="radio-inline"><input type="radio" name="trangthai" value="0" <?php echo $khonghienthi; ?> >Không hiển thị</label>
			</div>


			<div class="form-group">
				<input type="submit" name="submit" value="Sửa user" class="btn btn-primary">
			</div>

		</form>
			
		</div>
	</div>



<?php include('includes/footer.php'); ?>