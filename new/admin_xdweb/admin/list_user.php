<!-- <style type="text/css">
	th{
		text-align: center;
	}
</style> -->
<?php include('includes/header.php'); ?>
	<div class="row" style="width: 80%; margin: 50px 50px 0px 0px; float: right;">
		<h3>Danh sách User</h3>
		<table class="table table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Họ tên</th>
					<th>Tài khoản</th>
					<th>Mật khẩu</th>
					<th>Email</th>
					<th>SĐT</th>
					<th align="center">Status</th>
					<th>Reset</th>
					<th>Sửa</th>
					<th>Xóa</th>
				</tr>
			</thead>

			<tbody>
				<?php
					include('../inc/myconnect.php');
					include('../inc/function.php');
					// đặt số bản ghi cần hiển thị
					$limit = 2;
					// xác định vị trí bắt đầu
					if (isset($_GET['s']) && filter_var($_GET['s'],FILTER_VALIDATE_INT,array('min_range'=>1))){
						$start = $_GET['s'];
						
					}else{
						$start = 0;
					}
					// xác định số trang dự vào truyền biến hoặc truy xuất từ dữ liệu
					if (isset($_GET['p']) && filter_var($_GET['p'],FILTER_VALIDATE_INT, array('min_range'=>1))){
						$per_page = $_GET['p'];
					}else{

						// nếu p không có, sẽ truy vấn csdl để tìm xem có bao nhiêu page
						$sql_pg = "select count(id) from tbluser";
						$result_pg = mysqli_query($dbc,$sql_pg);
						kt_query($result_pg, $sql_pg);
						list($record) = mysqli_fetch_array($result_pg);
						// tìm số trang bằng cách chia số dữ liệu cho limit
						if($record > $limit){
							$per_page = ceil($record/$limit);
							//echo $per_page;

						}else{
							$per_page = 1;
						}
					}


					$sql = "select * from tbluser order by id ASC limit {$start},{$limit}";
					$result = mysqli_query($dbc, $sql);
					kt_query($result, $sql);
					while($user = mysqli_fetch_array($result)){

						//}
				?>
<!-- 
	$stats là biến bắt đầu lấy dữ liệu
	$limit là số phần tử hiển thị trên 1 trang
	$per_page là số trang
	$record là tổng số phần tử
	curent_page là số trang ở hiện tại
	hàm ceil() là hàm làm tròn

 -->
				<tr>
					<td><?php echo $user['id']; ?></td>
					<td><?php echo $user['hoten']; ?></td>
					<td><?php echo $user['taikhoan']; ?></td>
					<td><?php echo $user['matkhau']; ?></td>
					<td><?php echo $user['email']; ?></td>
					<td><?php echo $user['sđt']; ?></td>
					<td>

						<?php 
							if ($user['status'] == 1) {
								echo 'Hiển thị';
							}else{
								echo 'Không hiển thị';
							}
						//echo $user['status']; 
						?>
						
					</td>
					<td align="center"><a href="reset_user.php?id=<?php echo $user['id']; ?>"><img width="16" src="../images/reset1.png"></a></td>
					<td align="center"><a href="edit_user.php?id=<?php echo $user['id']; ?>"><img width="16" src="../images/icon_edit.png"></a></td>
					<td align="center"><a onclick="return confirm('Bạn có thật sự muốn xóa!');" href="delete_user.php?id=<?php echo $user['id']; ?>"><img width="16" src="../images/icon_delete.png"></a></td>
				</tr>
				<?php
					}
				?>
			</tbody>
		</table>

		<?php
			echo '<ul class="pagination">';
			if($per_page > 1){

				$current_page = ($start/$limit) + 1;
				// nếu không phải là trang đầu thì hiển thị trang trc
				if($current_page != 1){
					echo '<li><a href="list_user.php?s='.($start - $limit).'&p='.$per_page.'"><<</a></li>';
				}

				// hiển thị những phần còn lại của trang
				for ($i=1; $i <= $per_page ; $i++) { 
					if($i != $current_page){
					

						echo '<li><a href="list_user.php?s='.($limit*($i-1)).'&p='.$per_page.'">'.$i.'</a></li>';
					}else{
						echo '<li class="active"><a>'.$i.'</a></li>';
					}
				}

				// nếu nó không phải là trang cuối thi hiển thị nút next
				if($current_page != $per_page){
					
					echo '<li><a href="list_user.php?s='.($start + $limit).'&p='.$per_page.'">>></a></li>';
				}

			}
			echo '</ul>';
		?>
		
	</div>
<?php include('includes/footer.php'); ?>