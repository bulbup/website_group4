<?php
/**
  * @var \App\View\AppView $this
  */
?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <!-- <div class="row"> -->
<!-- <div class="users index col-sm-12 col-lg-12 columns content"> -->
<br><br><br>
        <div class="row">
      <div class="col-sm-2"> <h3><?= __('Chủ Đề') ?></h3></div>
      <div class="col-sm-3">
          <button><?= $this->Html->link(__('Tạo Chủ Đề'), ['action' => 'add','class'=>'btn btn-sm btn-success']) ?></button>
      </div> 
       <div class="col-sm-2" ></div>
      <div class="col-sm-5" >
        <!-- tìm kiếm -->
        <form method="post"  name="frmsearch" action="<?= $this->url->build(['controller'=>'topics', 'action'=>'index']) ?>">
            <div class="row">    
                <div class="col-sm-11"> 
                    <input type="text" name="search" placeholder="Bạn muốn tìm gì?">
                </div>
                <div class="col-sm-1"> 

                    <button style="display: inline-block !important ;" type="submit" name="btnsearch" class="fa fa-search"></button>
                </div>
            </div>
        </form> 
        <!-- .tìm kiếm -->
    </div>   
</div>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('topic_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('topic_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('level_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('describe_short') ?></th>
                <th scope="col"><?= $this->Paginator->sort('image') ?></th>
                <th scope="col"><?= $this->Paginator->sort('create_time') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($topics as $topic): ?>
            <tr>
                <td><?= h($topic ->topic_id) ?></td>
                <td><?= h($topic->topic_name) ?></td>
                <td><?= h($topic->level_name) ?></td>
                <td><?= h($topic->describe_short) ?></td>
                <td><?= h($topic->image) ?></td>
                <td><?= h($topic->create_time) ?></td>
                <td class="actions">
                    <a href="<?= $this->url->build(['controller'=>'Topics','action' => 'view', $topic->topic_id]) ?>" class="fa fa-eye"></a>
                  <a href="<?= $this->url->build(['action' => 'edit', $topic->topic_id]) ?>" class="fa fa-pencil-square-o"></a>   
                  <a href="<?= $this->url->build(['action' => 'delete', $topic->topic_id]) ?>" class="fa fa-trash"></a>  
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
    
</div>
