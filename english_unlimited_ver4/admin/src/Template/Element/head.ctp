<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Lumino - Tables</title>

<!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->
<!-- <link href="css/datepicker3.css" rel="stylesheet"> -->
<!-- <link href="css/bootstrap-table.css" rel="stylesheet"> -->
<!-- <link href="css/styles.css" rel="stylesheet"> -->
 <!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">     -->
<!-- <link href="css/cake.css" rel="stylesheet"> -->
<!-- <link href="css/base.css" rel="stylesheet"> -->
<!-- Icons -->
<!-- <script src="js/lumino.glyphs.js"></script> -->
<?php
echo $this->Html->css(array(
	'bootstrap.min.css','datepicker3.css','bootstrap-table.css','styles.css','//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css','base.css'
));
?>
<?php
echo $this->Html->script(array(
	'lumino.glyphs.js'
));
?>