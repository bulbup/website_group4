<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <!-- <div class="row"> -->
<!-- <div class="users index col-sm-12 col-lg-12 columns content"> -->
<br><br><br>

   
    <form action="<?php echo $this->url->build(['controller'=>'Lessons', 'action'=>'add']); ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        <fieldset>

            <legend>Thêm bài học</legend>
            <div class="form-group">
                <label for="chude">Chủ đề</label>
                <select name="topic_id" class="form-control">
                    <option value="0">Chọn chủ đề</option>}
                <?php foreach ($topics as $value) { ?>
                    <option value="<?= h($value->topic_id) ?>"><?= h($value->topic_name)?></option>
                <?php } ?>
                </select>
            </div>

            <div class="form-group">
                <label for="name">Tên bài học</label>
                <input type="text" name="lesson_name" class="form-control">
            </div>
            <div class="form-group">
                <label for="file">Hình ảnh</label>
                <input type="file" name="file" class="form-control">
            </div>
            <input type="submit" name="submit" class="btn btn-primary" value="Thêm mới">
    
        </fieldset>
    </form>

    
</div>
