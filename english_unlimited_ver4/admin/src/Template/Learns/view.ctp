<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Learn'), ['action' => 'edit', $learn->learn_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Learn'), ['action' => 'delete', $learn->learn_id], ['confirm' => __('Are you sure you want to delete # {0}?', $learn->learn_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Learns'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Learn'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Learns'), ['controller' => 'Learns', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Learn'), ['controller' => 'Learns', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Lessons'), ['controller' => 'Lessons', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Lesson'), ['controller' => 'Lessons', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="learns view large-9 medium-8 columns content">
    <h3><?= h($learn->learn_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Learn') ?></th>
            <td><?= $learn->has('learn') ? $this->Html->link($learn->learn->learn_id, ['controller' => 'Learns', 'action' => 'view', $learn->learn->learn_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $learn->has('user') ? $this->Html->link($learn->user->id, ['controller' => 'Users', 'action' => 'view', $learn->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lesson') ?></th>
            <td><?= $learn->has('lesson') ? $this->Html->link($learn->lesson->lesson_id, ['controller' => 'Lessons', 'action' => 'view', $learn->lesson->lesson_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Level Name') ?></th>
            <td><?= h($learn->level_name) ?></td>
        </tr>
    </table>
</div>
