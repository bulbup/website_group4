  
<?php
/**
* @var \App\View\AppView $this
*/
?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <!-- <div class="row"> -->
<!-- <div class="users index col-sm-12 col-lg-12 columns content"> -->
<br><br><br>
        <div class="row">
      <div class="col-sm-2"> <h3><?= __('Học Viên') ?></h3></div>
      <div class="col-sm-3">
          <button><?= $this->Html->link(__('Tạo Học Viên'), ['action' => 'add','class'=>'btn btn-sm btn-success']) ?></button>
      </div> 
       <div class="col-sm-2" ></div>
      <div class="col-sm-5" >
        <!-- tìm kiếm -->
        <form method="post"  name="frmsearch" action="<?= $this->url->build(['controller'=>'topics', 'action'=>'index']) ?>">
            <div class="row">    
                <div class="col-sm-11"> 
                    <input type="text" name="search" placeholder="Bạn muốn tìm gì?">
                </div>
                <div class="col-sm-1"> 

                    <button style="display: inline-block !important ;" type="submit" name="btnsearch" class="fa fa-search"></button>
                </div>
            </div>
        </form> 
        <!-- .tìm kiếm -->
    </div>   
</div>
    
       <table class="table table-striped">
    
     <thead>
    <tr>
        <th scope="col"><?= $this->Paginator->sort('Mã Số') ?></th>
        <!-- <th scope="col"><?php //$this->Paginator->sort('role_id') ?></th> -->
        <th scope="col"><?= $this->Paginator->sort('Tên') ?></th>
        <th scope="col"><?= $this->Paginator->sort('Họ') ?></th>
        <th scope="col"><?= $this->Paginator->sort('Email') ?></th>
        <th scope="col"><?= $this->Paginator->sort('Mật Khẩu') ?></th>
  
        <th scope="col" class="actions"><?= __('Actions') ?></th>
    </tr>
     </thead>   
     <tbody>
         <?php foreach ($users as $user): ?>
            <tr>
                <td><?= h($user ->user_id) ?></td>
                <!-- <td><?php //$user->has('role') ? $this->Html->link($user->role->role_id, ['controller' => 'Roles', 'action' => 'view', $user->role->role_id]) : '' ?></td> -->
                <td><?= h($user->first_name) ?></td>
                <td><?= h($user->last_name) ?></td>
                <td><?= h($user->email) ?></td>
                <td><?= h($user->password) ?></td>
                
                <td class="actions">

                    <!-- <?php //$this->Html->link(__('View'), ['action' => 'view', $user->user_id]) ?>
                    <?php //$this->Html->link(__('Edit'), ['action' => 'edit', $user->user_id]) ?>
                    <?php //$this->Form->postLink(__('Delete'), ['action' => 'delete', $user->user_id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->user_id)]) ?> -->


                    <a href="<?= $this->url->build(['controller'=>'Users','action' => 'view', $user->user_id]) ?>" class="fa fa-eye"></a>
                    <a href="<?= $this->url->build(['controller'=>'Users','action' => 'edit', $user->user_id]) ?>" class="fa fa-pencil-square-o"></a>
                     <a href="<?= $this->url->build(['controller'=>'Users','action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->user_id)]) ?>" class="fa fa-trash"></a>
                    <!-- <i class="fa fa-trash" aria-hidden="true"></i> -->

                </td>
            </tr>
            <?php endforeach; ?>
       
      

     </tbody>
  </table>
  <!-- Phân trang -->
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
 
</div>
<!-- </div> -->
<!-- </div> -->
<!-- </div> -->



        
    </div><!--/.main-->