<?php
namespace App\Controller;

use App\Controller\AppController;
//namespace App\Model\Table;

use Cake\ORM\Table;


/**
 * Lessons Controller
 *
 * @property \App\Model\Table\LessonsTable $Lessons
 *
 * @method \App\Model\Entity\Lesson[] paginate($object = null, array $settings = [])
 */
class LessonsController extends AppController
{

    public function initialize(){
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->viewBuilder()->layout('backend');
        //$this->addBehavior('Timestamp');
    }

    public function index()
    {
        $this->paginate = [
            'contain' => [ 'Topics']
        ];
        $lessons = $this->paginate($this->Lessons);
        //$lessons = $this->Lessons->find('all');


        $this->set(compact('lessons'));
        $this->set('_serialize', ['lessons']);
    }

    

    // public function initialize(array $config){
    //      $this->addBehavior('Timestamp');
    // }
   
    public function view($id = null)
    {
        $lesson = $this->Lessons->get($id, [
            'contain' => [ 'Topics']
        ]);

        $this->set('lesson', $lesson);
        $this->set('_serialize', ['lesson']);
    }

    public function add()
    {
        // $lesson = $this->Lessons->newEntity();
        // if ($this->request->is('post')) {
        //     $lesson = $this->Lessons->patchEntity($lesson, $this->request->getData());
        //     if ($this->Lessons->save($lesson)) {
        //         $this->Flash->success(__('The lesson has been saved.'));

        //         return $this->redirect(['action' => 'index']);
        //     }
        //     $this->Flash->error(__('The lesson could not be saved. Please, try again.'));
        // }
        // $lessons = $this->Lessons->Lessons->find('list', ['limit' => 200]);
        // $topics = $this->Lessons->Topics->find('list', ['limit' => 200]);
        // $this->set(compact('lesson', 'lessons', 'topics'));
        // $this->set('_serialize', ['lesson']);

        ////////////////////////
        $uploadData = '';
        if ($this->request->is('post')) {
            if(!empty($this->request->data['file']['name'])){
                $fileName = $this->request->data['file']['name'];
                $uploadPath = 'uploads/files/';
                $uploadFile = $uploadPath.$fileName;

                if(move_uploaded_file($this->request->data['file']['tmp_name'],$uploadFile)){
                    $uploadData = $this->Lessons->newEntity();
                    //$uploadData->name = $fileName;
                    // $uploadData->path = $uploadPath;
                    $uploadData->image = $uploadFile;
                    $uploadData->topic_id = $this->request->getData('topic_id');
                    $uploadData->lesson_name = $this->request->getData('lesson_name');
                   
                    //$uploadData->lesson_id = 21;
							//$uploadData = $this->Lessons->patchEntity($uploadData, $this->request->getData());
                    // $uploadData->created = date("Y-m-d H:i:s");
                    // $uploadData->modified = date("Y-m-d H:i:s");

                    //pr($this->request->getData()); exit();
                    if ($this->Lessons->save($uploadData)) {
                        echo $fileName;
                        echo $uploadFile;
                        //echo $uploadPath;
                        echo $this->request->data['file']['tmp_name'];
                        
                        $this->Flash->success(__('File has been uploaded and inserted successfully.'));
                        echo 'thành công';
                        //exit();
                    }else{
                        $this->Flash->error(__('Unable to upload file, please try again.'));
                        echo "thất bại";
                        //exit();
                    }
                }else{
                    $this->Flash->error(__('Unable to upload file, please try again.'));
                }
            }else{
                $this->Flash->error(__('Please choose a file to upload.'));
            }
            
        }
        $this->set('uploadData', $uploadData);
        
        // $files = $this->Lessons->find('all', ['order' => ['Lessons.Ma' => 'DESC']]);
        // $filesRowNum = $files->count();
        // $this->set('files',$files);
        // $this->set('filesRowNum',$filesRowNum);
    
        ////////////////////////
        $lessons = $this->Lessons->Lessons->find('list', ['limit' => 200]);
        $topics = $this->Lessons->Topics->find('all', ['limit' => 200]);
        $this->set(compact('lesson', 'lessons', 'topics'));
        $this->set('_serialize', ['lesson']);
        //pr($topics); exit();
    }


    public function edit($id = null)
    {
        $lesson = $this->Lessons->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $lesson = $this->Lessons->patchEntity($lesson, $this->request->getData());
            if ($this->Lessons->save($lesson)) {
                $this->Flash->success(__('The lesson has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The lesson could not be saved. Please, try again.'));
        }
        $lessons = $this->Lessons->Lessons->find('list', ['limit' => 200]);
        $topics = $this->Lessons->Topics->find('list', ['limit' => 200]);
        $this->set(compact('lesson', 'lessons', 'topics'));
        $this->set('_serialize', ['lesson']);
    }

   
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $lesson = $this->Lessons->get($id);
        if ($this->Lessons->delete($lesson)) {
            $this->Flash->success(__('The lesson has been deleted.'));
        } else {
            $this->Flash->error(__('The lesson could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
