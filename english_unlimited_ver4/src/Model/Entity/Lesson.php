<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Lesson Entity
 *
 * @property int $lesson_id
 * @property int $topic_id
 * @property string $lesson_name
 * @property string $image
 *
 * @property \App\Model\Entity\Lesson $lesson
 * @property \App\Model\Entity\Topic $topic
 */
class Lesson extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'lesson_id' => false
    ];
}
