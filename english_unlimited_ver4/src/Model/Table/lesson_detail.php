<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class LessonsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('lesson_detail');
        $this->setDisplayField('lesson_detail_id');
        $this->setPrimaryKey('lesson_detail_id');

        // $this->belongsTo('Lessons', [
        //     'foreignKey' => 'lesson_id',
        //     'joinType' => 'INNER'
        // ]);

        $this->belongsTo('Lessons', [
            'foreignKey' => 'lesson_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Levels', [
            'foreignKey' => 'level_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Listwords', [
            'foreignKey' => 'listword_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('file_of_lesson', [
            'foreignKey' => 'file_of_lesson_id',
            'joinType' => 'INNER'
        ]);
        
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('lesson_name', 'create')
            ->notEmpty('lesson_name');

        $validator
            ->requirePresence('image', 'create')
            ->notEmpty('image');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['lesson_id'], 'Lessons'));
        $rules->add($rules->existsIn(['topic_id'], 'Topics'));

        return $rules;
    }
}
