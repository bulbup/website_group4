    <title>Sign-Up/Login Form</title>
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <?= $this->Html->css('login/style') ?>

    <div class="form">
        <ul class="tab-group">
          <li class="tab"><a href="#quenmatkhau">Quên mật khẩu</a></li>
        </ul>
    <div class="tab-content">
        <div id="quenmatkhau" >
            <h1>Vui lòng nhập email đã đăng ký</h1>
            <form action="<?= $this->url->build(['controller'=>'Users', 'action'=>'password']) ?>" method="post">
                <div class="field-wrap">
                    <label>
                      Email <span class="req">*</span>
                    </label>
                    <input type="email" name="email" required autocomplete="off" autofocus />
                </div>
                <!-- <p class="tab forgot"><a href="#login">Quay lại?</a></p> -->
          
                <button type="submit" class="button button-block"/>Gửi xác nhận</button>
            </form>
        </div>
    </div><!-- tab-content -->

  </div> <!-- /form -->

  <?= $this->Html->script('login/jquery.min') ?>
  <?= $this->Html->script('login/index') ?>
