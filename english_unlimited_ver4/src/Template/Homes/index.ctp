   <!-- Slider Section -->
   <section id="home" class="slider-section">
      <div id="tt-carousel" class="carousel tt-carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#tt-carousel" data-slide-to="0" class="active"></li>
          <li data-target="#tt-carousel" data-slide-to="1"></li>
      </ol> 

      <!-- Wrapper for slides -->
      <div class="carousel-inner">
          <div class="item active">
            <div class="container">
              <div class="row">
                <div class="col-md-6 col-sm-7">
                    <div class="carousel-intro">
                      <h1 class="animated fadeInDown animation-delay-6 carousel-title">Chào mừng bạn đến với</h1>

                      <h2 class="animated fadeInDown animation-delay-4 crousel-subtitle">English Fun</h2>

                      <p class="animated fadeInUp animation-delay-10">Nào! chuẩn bị hành trang để bay vào chặn đường chinh phục tiếng anh cùng chúng tôi</p>
                      <a href="#" class=" btn btn-default animated fadeInUp animation-delay-10">Learn more</a> 
                  </div><!-- /.carousel-intro -->
              </div><!-- /.col-md-6 -->

              <div class="col-md-6 col-sm-5 hidden-xs">
                  <div class="carousel-img layer-one">
                      <?= $this->Html->image('layer-1.png',['class'=>'img-responsive animated bounceInDown animation-delay-3'])?>

                  </div><!-- /.carousel-img-->
              </div><!-- /.col-md-6 -->
          </div><!-- /.row -->
      </div><!-- /.container -->
  </div><!-- /.item -->

  <div class="item">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-8">
            <div class="carousel-intro">
              <h1 class="animated fadeInDown animation-delay-6 carousel-title">Chào mừng bạn đến với</h1>

              <h2 class="animated fadeInDown animation-delay-4  crousel-subtitle">English Fun </h2>

              <p class="animated fadeInUp animation-delay-10">Chương trình học được thiết kế đặt biệt đáp ứng được nhu cầu phù hợp với người dùng </p>
              <a href="#" class=" btn btn-default animated fadeInUp animation-delay-10">Learn more</a> 
          </div><!-- /.carousel-intro -->
      </div><!-- /.col-md-6 -->

      <div class="col-md-6 col-sm-4 hidden-xs carousel-img-wrap">
          <div class="carousel-img layer-two">
             <?= $this->Html->image('girl-2.png',['class'=>'img-responsive animated fadeIn animation-delay-3'])?>
         </div><!-- /.carousel-img-->
     </div><!-- /.col-md-6 -->
 </div><!-- /.row -->
</div><!-- /.container -->
</div><!-- /.item -->

</div><!-- /.carousel-inner -->
</div>
</section>
<!-- Slider Section End -->



<!-- Feature Section -->
<section id="how" style="padding-bottom: 0px" class="feature-section section-padding">
    <div class="container">
        <div class="section-title">
            <h2>Học tiếng anh <span class="light-text">như thế nào</span></h2>
            <div class="title-border-container">
                <div class="title-border"></div>
            </div>
        </div><!-- /.section-title -->

        <div class="row mt-70">
            <div class="col-md-6">
                <div class="feature-img">
                   <?= $this->Html->image('hoc-tieng-anh-ntn.jpg',['class'=>'img-responsive'])?>
               </div><!-- /.post-img -->
           </div>

           <div class="col-md-6">
            <div class="featured-item mb-30">
                <div class="icon square">
                    <i class="fa fa-desktop"></i>
                </div>
                <div class="title">
                    <h3>Học theo chủ đề</h3>
                </div>
                <div class="desc">
                    <p>Bạn có thể lựa chọn bất kì chủ đề nào để học theo sở thích và mong muốn.</p>
                </div>
            </div><!-- /.featured-item -->

            <div class="featured-item mb-30">
                <div class="icon square">
                    <i class="fa fa-code"></i>
                </div>
                <div class="title">
                    <h3>Phương pháp học thông minh</h3>
                </div>
                <div class="desc">
                    <p>Mỗi kĩ năng: nghe, nói, đọc, viết đều được áp dụng phương pháp học thông minh như: mindmap, video, flashcard</p>
                </div>
            </div><!-- /.featured-item -->

            <div class="featured-item">
                <div class="icon square">
                    <i class="fa fa-pencil"></i>
                </div>
                <div class="title">
                    <h3>Chơi game</h3>
                </div>
                <div class="desc">
                    <p>Chơi game để ôn lại kiến thức là một diều tuyệt vời nếu như bạn không muốn làm những bài test nhàm chán.</p>
                </div>
            </div><!-- /.featured-item -->
        </div>

    </div>
</div><!-- /.container -->
</section>
<!-- Feature Section End -->



<!-- benefit Section -->
<section id="benefit" class="about-section section-padding gray-bg">
    <div class="container">
        <div class="section-title">
            <h2>Lợi ích <span class="light-text">học tiếng anh</span></h2>
            <div class="title-border-container">
                <div class="title-border"></div>
            </div>
        </div>

        <div class="row mt-70">

            <div class="col-md-6">
                <div class="post-desc">

                    <p>Việc bắt đầu với một ngôn ngữ khác không phải tiếng mẹ đẻ chắc hẳn rất khó khăn. Nhưng trong thời kì hội nhập hiện nay học ngoại ngữ đóng một vai trò hết sức quan trọng và đặc biệt là Tiếng Anh. Sau đây là một số lợi ích của việc hoc tieng Anh.</p>

                    <ul class="check-list list-half clearfix mb-20">
                        <li>
                            Giúp bạn đi xa và học hỏi.
                        </li>
                        <li>
                           Có nhiều cơ hội việc làm.
                       </li>
                       <li>
                        Mở rộng kiến thức
                    </li>
                    <li>
                       Thúc đẩy sự nghiệp.
                   </li>
                   <li>
                    Du lịch thế giới
                </li>
                <li>
                   Giao tiếp với người nước ngoài.
               </li>
               <li>
                Không cần thông dịch viên
            </li>
            <li>
               Cảm thấy hài lòng về bản thân.

           </li>
       </ul>

       <div>
        <a href="#" class="btn btn-primary text-uppercase"> Xem Thêm </a>
    </div>
</div><!-- /.post-desc -->
</div><!-- /.col-md-6 -->

<div class="col-md-6">
    <div class="post-img">
        <?= $this->Html->image('mac.png',['class'=>'img-responsive'])?>

    </div><!-- /.post-img -->
</div><!-- /.col-md-6 -->

</div><!-- /.row -->
</div><!-- /.container -->
</section>
<!-- benefit Section End -->


<!-- Video Section -->
<!-- <section class="video-section text-center"> -->
<!-- <div class="dark-overlay"></div> -->
           <!--  <div class="video-intro">
                <h3>Cách học của english unlimited</h3>
                <a class="external-link popup-youtube" href="https://www.youtube.com/watch?v=oi2o2r3Sgc0" title=""><i class="fa fa-play-circle"></i></a>
            </div> -->
            <!-- <div class="team-bg-container text-center"> -->

            <?php //$this->Html->image('team-bg.jpg')?>
            <!-- </div> -->
            <!-- </section> -->
            <!-- Video Section End -->


            <!-- topics Section -->
            <section id="topics" class="service-section section-padding">
                <div class="container">
                    <div class="section-title">
                        <h2>Chủ đề <span class="light-text">yêu thích</span></h2>
                        <div class="title-border-container">
                            <div class="title-border"></div>
                        </div>
                    </div><!-- /.section-title -->

                    <div class="row mt-70">

                        <div class="container">
                            <div class="list-group">
                                <?php 
                                foreach ($topic as $value_topic) {

                                        ?>
                                        <a href="#" class="list-group-item">

                                            <div class="media">
                                                <span class="label label-danger pull-right">Mới</span>
                                                <div class="checkbox pull-left">

                                                </div>
                                                <div class="pull-left">
                                                 
                                                    <?= $this->Html->image(h($value_topic->image),['class'=>'img-responsive media-object'])?>
                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading"><?php echo h($value_topic->topic_name) ?></h4>
                                                    <p><?php echo h($value_topic->describe_short) ?></p>
                                                </div>

                                            </div>                  

                                        </a>
             <!--    <a href="#" class="list-group-item">

                    <div class="media">
                        <span class="label label-danger pull-right">Mới</span>
                        <div class="checkbox pull-left">
                        
                        </div>
                        <div class="pull-left">
                            <img class="media-object" src="http://placehold.it/100x70" alt="Image">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Festival de Tiradentes</h4>
                            <p>Mới</p>
                        </div>
                    </div>           
                    <div class="clearfix"></div>                    
                </a>           -->      
                <?php 
            }
            ?>
        </div>
    </div>





</div><!-- /.row -->


<!--promo theme color box start-->
<div class="promo-box theme-bg mt-50">
    <div class="promo-info">
        <h3 class="white-color">Hãy Đăng Ký Thành Viên Để Khám Phá Những Chủ Đề Mới Này Nhé!</h3>
    </div>
    <div class="promo-btn">
        <a href="#" class="btn btn-default text-uppercase">Đăng ký học ngay</a>
    </div>
</div>
<!--promo theme color box end-->


</div><!-- /.container -->
</section>
<!-- topics Section End -->


<!-- Work Section -->
<section id="work" class="latest-work-section section-padding">
    <div class="container">
        <div class="section-title">
            <h2 class="white-color">Bài học <span class="light-text">mới nhất</span></h2>
            <div class="title-border-container">
                <div class="title-border"></div>
            </div>
        </div><!-- /.section-title -->


        <div class="work-carousel mt-70">
 <?php 
     foreach ($lesson as $value_lesson) {
    ?>

            <figure class="thumb-wrapper">                        
                <?php //$this->Html->image(h($value_lesson->image),['class'=>'img-responsive'])?> 

                <img class="img-responsive" src="<?php echo 'admin/webroot/'.$value_lesson->image; ?>">
                <div class="bg-overlay">
                  <div class="portfolio-content">
                    <!-- Portfolio Icons -->
                    <div class="portfolio-icon-wrapper">
                      <a href="single-project.html"><i class="fa fa-link"></i></a>
                      <a class="tt-lightbox" href="portfolio-1.jpg"><i class="fa fa-search"></i></a>
                  </div>
                  <!-- End Portfolio Icons -->

                  <!-- Portfolio Title & Info -->
                  <div class="portfolio-text">
                      <h3><?php echo h($value_lesson->lesson_name) ?></h3>
                      <div class="portfolio-meta">
                        <a href="#">Vào học</a>
                    </div>
                </div>
                <!-- End Portfolio Title & Info -->
            </div>
        </div>
    </figure>
<?php } ?>

</div><!-- /.latest-work-carousel -->

</div><!-- /.container -->
</section>
<!-- Works Section End -->


<!-- Team Section -->
<section id="team" class="team-section section-padding">
    <div class="container">
        <div class="section-title center">
            <h2>Quản trị <span>website</span></h2>
        </div>
        <div class="team-section-intro text-center mt-30">
            <p>Luôn luôn sáng tạo, luôn luôn đồng hành trong chặn đường phát triển của website, nổ lực vượt trội không ngừng thay đổi đáp ứng nhu cầu học tiếng anh của các bạn.  </p>
        </div>
    </div>

            <!-- <div class="team-bg-container text-center">
               
                <?php //$this->Html->image('team-bg.jpg')?>
            </div> -->

            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="team-member text-center">
                            <div class="team-img">
                               <?= $this->Html->image('luan-nguyen (2).jpg')?>
                               <div class="team-intro">
                                <h3>Nguyễn Văn Luân</h3>
                            </div>
                        </div><!-- /.team-img -->
                        <div class="team-hover">
                            <div class="desk">
                                <h4>Nguyễn Văn Luân</h4>
                                <p>Thành viên phát triển website</p>
                            </div>
                            <div class="social-link">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                            </div>
                        </div><!-- /.team-hover -->
                    </div><!-- /.team-member -->
                </div><!-- /.col-md-3 -->

                <div class="col-md-3 col-sm-6">
                    <div class="team-member text-center">
                        <div class="team-img">

                            <?= $this->Html->image('dung-tran.jpg')?>
                            <div class="team-intro">
                                <h3>Trần Dương Ngọc Dung</h3>
                            </div>
                        </div><!-- /.team-img -->
                        <div class="team-hover">
                            <div class="desk">
                                <h4>Trần Dương Ngọc Dung</h4>
                                <p>Thành viên phát triển website</p>
                            </div>
                            <div class="social-link">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                            </div>
                        </div><!-- /.team-hover -->
                    </div><!-- /.team-member -->
                </div><!-- /.col-md-3 -->

                <div class="col-md-3 col-sm-6">
                    <div class="team-member text-center">
                        <div class="team-img">
                            <?= $this->Html->image('tien (2).jpg')?>
                            <div class="team-intro">
                                <h3>Lê Thị Cẩm Tiên</h3>
                            </div>
                        </div><!-- /.team-img -->
                        <div class="team-hover">
                            <div class="desk">
                                <h4>Lê Thị Cẩm Tiên</h4>
                                <p>Thành viên phát triển website</p>
                            </div>
                            <div class="social-link">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                            </div>
                        </div><!-- /.team-hover -->
                    </div><!-- /.team-member -->
                </div><!-- /.col-md-3 -->

                <div class="col-md-3 col-sm-6">
                    <div class="team-member text-center">
                        <div class="team-img">
                            <?= $this->Html->image('tho-nguyen.jpg')?>
                            <div class="team-intro">
                                <h3>Nguyễn Thị Anh Thơ</h3>
                            </div>
                        </div><!-- /.team-img -->
                        <div class="team-hover">
                            <div class="desk">
                                <h4>Nguyễn Thị Anh Thơ</h4>
                                <p>Thành viên phát triển website</p>
                            </div>
                            <div class="social-link">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                            </div>
                        </div><!-- /.team-hover -->
                    </div><!-- /.team-member -->
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->

    </section>
    <!-- Team Section End -->



    <!-- Client Section -->
    <section class="client-section section-padding gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="client-testimonial">
                        <h2>Người hướng dẫn thực hiện</h2>
                        <ul class="testimonial mt-30">
                            <li>
                                <div class="avatar">
                                    <a href="#"><?= $this->Html->image('an-nguyen.jpg')?></a>
                                </div>
                                <div class="content">
                                    <div class="testimonial-meta">
                                        TS. Nguyễn Thị Thu An
                                        <span>thuan.com</span>
                                    </div>

                                    <p>Nhà phát triển, giám sát và kiểm tra dự án phát triển website</p>
                                </div>
                            </li>

                            <li>
                                <div class="avatar">
                                    <a href="#"><?= $this->Html->image('quyen-le.jpg')?></a>
                                </div>
                                <div class="content">
                                    <div class="testimonial-meta">
                                        Lê Quyên
                                        <span>lequyen.com</span>
                                    </div>

                                    <p>Nhà phát triển, giám sát và kiểm tra dự án phát triển website</p>
                                </div>
                            </li>
                        </ul>
                    </div><!-- /.client-testimonial -->
                </div><!-- /.col-md-6 -->

                <div class="col-md-6">
                    <div class="client-logo">
                        <h2>Some of our best clinets</h2>
                        <ul class="clients plus-box grid-3 mt-30">
                            <li><a href="#"><?= $this->Html->image('client-logo-1.png')?></a></li>
                            <li><a href="#"><?= $this->Html->image('client-logo-2.png')?></a></li>
                            <li><a href="#"><?= $this->Html->image('client-logo-3.png')?></a></li>
                            <li><a href="#"><?= $this->Html->image('client-logo-4.png')?></a></li>
                            <li><a href="#"><?= $this->Html->image('client-logo-5.png')?></a></li>
                            <li><a href="#"><?= $this->Html->image('client-logo-6.png')?></a></li>
                        </ul>
                    </div><!-- /.client-logo -->
                </div><!-- /.col-md-6 -->

            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
    <!-- Client Section End -->



    <!-- Counter Section -->
    <section class="counter-section">
        <div class="dark-overlay"></div>
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-3 col-xs-3 counter-wrap">
                  <i class="fa fa-pencil"></i>
                  <span class="timer">1249</span>
                  <span class="count-description">Coffe Cups</span>
              </div> <!-- /.col-xs-3 -->

              <div class="col-lg-3 col-xs-3 counter-wrap">
                  <i class="fa fa-code"></i>
                  <span class="timer">5164</span>
                  <span class="count-description">Lines Code</span>
              </div><!-- /.col-xs-3 -->

              <div class="col-lg-3 col-xs-3 counter-wrap">
                  <i class="fa fa-pencil"></i>
                  <span class="timer">2142</span>
                  <span class="count-description">Pixel Design </span>
              </div><!-- /.col-xs-3 -->

              <div class="col-lg-3 col-xs-3 counter-wrap">
                  <i class="fa fa-user"></i>
                  <span class="timer">1142</span>
                  <span class="count-description">Happy Clients</span>
              </div><!-- /.col-xs-3 -->
          </div>
      </div><!-- /.container -->
  </section>
  <!-- Counter Section -->



<section class="map-section">
    <!-- <div class="text-center"> -->
        <!-- <h2>Get worldwide support</h2> -->
    <!-- </div> -->
    <div id="world-map-markers"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3928.828957116066!2d105.76671541389511!3d10.030969375254674!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31a0883d0dac6b15%3A0xf6ae5b1bd18625!2sCan+Tho+University!5e0!3m2!1sen!2sin!4v1501581321301" width=100% frameborder="0" height="500px" style="border:0" allowfullscreen></iframe></div>
</section>



<!-- Contact Section -->
<section id="contact" class="contact-section">
    <div class="container">
        <div class="text-center">
            <h3 class="text-uppercase white-color">Liên hệ với chúng tôi</h3>
        </div>

        <div class="contact-form-wrapper text-center">
            <form name="contact-form" id="contactForm" action="sendemail.php" method="POST">

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                          <label class="sr-only" for="name">Name</label>
                          <input type="text" name="name" class="form-control" id="name" placeholder="Name">
                      </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label class="sr-only" for="email">Email</label>
                      <input type="email" name="email" class="form-control" id="email" placeholder="Email">
                  </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label class="sr-only" for="subject">Subject</label>
                  <input type="text" name="subject" class="form-control" id="subject" placeholder="Subject">
              </div>
          </div>
      </div>

      <div class="form-group">
          <label class="sr-only" for="message">Message</label>
          <textarea name="message" class="form-control" id="message" rows="7" placeholder="Write Message"></textarea>
      </div>

      <button type="submit" name="submit" class="btn btn-primary btn-lg text-uppercase">Gửi</button>
  </form>
</div>

</div><!-- /.container -->
</section>
<!-- Contact Section End-->
