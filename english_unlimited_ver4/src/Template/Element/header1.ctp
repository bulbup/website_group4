		<!-- Top menu -->
		<nav>
			<div class="container">
				<div class="row">
					<div class="col-sm-2 nav-logo">
						<a href="">English Unlimited</a>
					</div>
					<div class="col-sm-10 nav-links">
						<a class="scroll-link button-1 active" href="#top-content">Trang Chủ</a>
						<a class="scroll-link button-1" href="#portfolio">Tại Sao?</a>
                        <a class="scroll-link button-1" href="#">Chủ Đề</a>
                        <!-- <a class="scroll-link button-1" href="#blog">Blog</a> -->
                           <!--  <a class="scroll-link button-1" href="#reviews">Reviews</a> -->
						<a class="scroll-link button-1" href="#team">Quản trị viên</a>		
						<a class="scroll-link button-1" href="#contact-us">Liên Hệ</a>
                            <a class="scroll-link button-1" href="#">Đăng Nhập</a>
						<div class="show-menu"><span></span></div>
					</div>
				</div>
			</div>
		</nav>
	<!-- <hr size="2">	 -->
        <!-- Top content -->
        <div class="top-content-container">
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 top-content-text">
                        	<h1>
                        		Bạn muốn giỏi tiếng anh<br>
                        		<span class="top-content-text-big">Hãy học cùng chúng tôi</span>
                        	</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 top-content-computer">
                        	<div class="top-content-computer-container">
                        		<?= $this->Html->image('macbook.png',['class'=>'img-responsive'])?>
                        		<div class="top-content-video">
                        			<?= $this->Html->image('play.png')?>
                        			<p>Play case study</p>
                        			<iframe src="//player.vimeo.com/video/84910153?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;api=1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        		</div>
                        	</div>
                        </div>
                    </div>
                    <div class="row">
                    	<div class="col-sm-12 top-content-select">
                        	<select class="custom-select">
                        		<option class="custom-select-web-design" value="web-design">Web design</option>
                       			<option class="custom-select-image-design" value="image-design">Image design</option>
                       			<option class="custom-select-branding" value="branding">Branding</option>
                       			<option class="custom-select-dtp" value="dtp">DTP</option>
                       			<option class="custom-select-tv-campanies" value="tv-campanies">TV campanies</option>
                       		</select>
                       		<a class="button-2" href="#">See the works</a>
                    	</div>
                    </div>
                </div>
            </div>
        </div>
