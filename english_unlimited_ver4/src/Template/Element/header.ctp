        <!-- top-bar -->
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-7">
                        <div class="cta-wrapper">
                            <ul class="list-inline">
                                <li><a href="#"><i class="fa fa-phone"></i>090 129 901</a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i> englishfun@gmail.com</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-5">
                        <div class="social-wrapper text-right">
                            <ul class="list-inline">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-tumblr"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- top-bar end-->


        <!-- Navigation start -->
        <div class="navbar navbar-custom" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><?= $this->Html->image('logo.png')?></a>
                </div>

                <div class="collapse navbar-collapse" id="custom-collapse">
                   
                    <ul class="nav navbar-nav navbar-right">


                        <li class="active"><a class="page-scroll" href="#home">Trang Chủ</a></li>
                        <li><a class="page-scroll" href="#how">Học Thế Nào?</a></li>
                        <li><a class="page-scroll" href="#benefit">Lợi ích</a></li>
                        <li><a class="page-scroll" href="#topics">Chủ Đề</a></li>
                        <li><a class="page-scroll" href="#team">Quản Trị</a></li>
                        <!-- <li><a class="page-scroll" href="#blog">Blog</a></li> -->  
                        <li><a class="page-scroll" href="#contact">Contact</a></li>
                        <li><a class="page-scroll" href="Users">Đăng Ký</a></li>

                      <!--  <div class="input-group search">
                <input type="text" class="form-control" placeholder="Search Here">
                <span class="input-group-btn">
                <button class="btn btn-primary " type="button"><i class="fa fa-search"></i></button>
                </span></div>  -->
                    </ul>

                  
                </div>

            </div><!-- .container -->
        </div>
        <!-- Navigation end -->