  <!-- Footer Section -->
        <footer class="footer-section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        Copy right <i class="fa fa-love"></i><a href="https://bootstrapthemes.co">English Unlimited</a>
                    </div>
                    <div class="col-sm-6 text-right">
                        <ul class="footer-menu list-inline">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Privacy</a></li>
                            <li><a href="#">Sitemape</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Section End -->


        <!-- Preloader -->
        <div id="preloader">
            <div id="status">
                <div class="status-mes"></div>
            </div>
        </div>
        <!-- End Preloader -->



