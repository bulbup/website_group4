<?php
use Cake\ORM\Query;
?>




	<div class="container" style=" padding: 0px; width: 95%;"> 
		<div class="row" style="margin-left: 5px">
			
			<div class="col-lg-8">

				<div id="myCarousel" class="carousel slide" data-ride="carousel">

					<!-- Wrapper for slides -->
					<div class="carousel-inner">

						<div class="item active">
							<img src="http://placehold.it/760x400/cccccc/ffffff">
							<div class="carousel-caption">
								<h4><a href="#">Lorem ipsum dolor sit amet consetetur sadipscing</a></h4>
								<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat. <a class="label label-primary" href="http://sevenx.de/demo/bootstrap-carousel/" target="_blank">Free Bootstrap Carousel Collection</a></p>
							</div>
						</div><!-- End Item -->

						<div class="item">
							<img src="http://placehold.it/760x400/999999/cccccc">
							<div class="carousel-caption">
								<h4><a href="#">consetetur sadipscing elitr, sed diam nonumy eirmod</a></h4>
								<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat. <a class="label label-primary" href="http://sevenx.de/demo/bootstrap-carousel/" target="_blank">Free Bootstrap Carousel Collection</a></p>
							</div>
						</div><!-- End Item -->

						<div class="item">
							<img src="http://placehold.it/760x400/dddddd/333333">
							<div class="carousel-caption">
								<h4><a href="#">tempor invidunt ut labore et dolore</a></h4>
								<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat. <a class="label label-primary" href="http://sevenx.de/demo/bootstrap-carousel/" target="_blank">Free Bootstrap Carousel Collection</a></p>
							</div>
						</div><!-- End Item -->

						<div class="item">
							<img src="http://placehold.it/760x400/999999/cccccc">
							<div class="carousel-caption">
								<h4><a href="#">magna aliquyam erat, sed diam voluptua</a></h4>
								<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat. <a class="label label-primary" href="http://sevenx.de/demo/bootstrap-carousel/" target="_blank">Free Bootstrap Carousel Collection</a></p>
							</div>
						</div><!-- End Item -->

						<div class="item">
							<img src="http://placehold.it/760x400/dddddd/333333">
							<div class="carousel-caption">
								<h4><a href="#">tempor invidunt ut labore et dolore magna aliquyam erat</a></h4>
								<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat. <a class="label label-primary" href="http://sevenx.de/demo/bootstrap-carousel/" target="_blank">Free Bootstrap Carousel Collection</a></p>
							</div>
						</div><!-- End Item -->

					</div><!-- End Carousel Inner -->


					<ul class="list-group col-sm-4">
						<li data-target="#myCarousel" data-slide-to="0" class="list-group-item active"><h4>Lorem ipsum dolor sit amet consetetur sadipscing</h4></li>
						<li data-target="#myCarousel" data-slide-to="1" class="list-group-item"><h4>consetetur sadipscing elitr, sed diam nonumy eirmod</h4></li>
						<li data-target="#myCarousel" data-slide-to="2" class="list-group-item"><h4>tempor invidunt ut labore et dolore</h4></li>
						<li data-target="#myCarousel" data-slide-to="3" class="list-group-item"><h4>magna aliquyam erat, sed diam voluptua</h4></li>
						<li data-target="#myCarousel" data-slide-to="4" class="list-group-item"><h4>tempor invidunt ut labore et dolore magna aliquyam erat</h4></li>
					</ul>

					<!-- Controls -->
					<div class="carousel-controls">
						<a class="left carousel-control" href="#myCarousel" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"></span>
						</a>
						<a class="right carousel-control" href="#myCarousel" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</a>
					</div>

				</div> <!-- End Carousel -->
				<div style="padding-top: 50px">
					<h2><span class="label label-default">Các chủ đề</span></h2>
						<style type="text/css">
							.ten_chu_de {
								background: url(../../../webroot/img/separator-horizontal.png) no-repeat center center;
								color: #fbfdf8;
								font-family: 'HammersmithOneRegular';
								font-weight: normal;
								line-height: 30px;
								margin: 0 0 24px;
								padding: 0 24px;
								text-transform: uppercase;
							}
							.ten_chu_de span {
								background-color: #97b847;
								display: block;
								height: 29px;
								width: 171px;
								padding: 4px 0 2px;
								text-align: center;
							}
						</style>
					<hr/>
				</div> <!-- end new popular -->
				<!-- chủ đề mới -->

				<div class="row">

					<div class="col-lg-12">
					<?php
					
							foreach ($topics as $topic) {

								?>
								
						<div class="col-md-3">

							<div class="view">
								<div class="caption">
									<h3>Wiseberry</h3>
									<a href="" rel="tooltip" title="Add to Favorites"><span class="fa fa-heart-o fa-2x"></span></a>

									<br/><br/><button type="button" class="btn btn-danger">Vào học</button>
								</div>
								<div class="topic_name"><?php echo h($topic->topic_name)?></div>
								<img class="img-responsive" alt="200x100" style="width: 100%; height: 200px;" src="<?php echo h($topic->image); ?>">
								
							</div>
							<div class="info">
								<p class="small" style="text-overflow: ellipsis">CASTLE HILL</p>
								<p class="small wb-red">30/1-7 Hume Ave</p>
								
							</div>

							<div class="progress" style="width: 100%">
								<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%">40%
								</div>
							</div>

						</div>
						<?php
					}
					?>	
						
						</div>


					</div>
				</div><!-- end row chủ đề mới -->

			<!-- end cột phai-->
			<div class="col-lg-4 col-md-6 col-md-offset-3 col-lg-offset-0" style="margin: 0px">
				<!-- search -->
				<div class="well">
					<h2 align="center"><span class="label label-default">Tìm kiếm</span></h2>
					<form class="form-horizontal">
						<div class="form-group">
							<div class="row">
							<div class="col-lg-6">
							<label for="location1" class="control-label">Cấp độ</label>
							<select class="form-control" name="" id="location1">
								<option value="">Cơ bản</option>
								<option value="">Nâng cao</option>
							</select>
							</div>
							<div class="col-lg-6">
							<label for="type1" class="control-label">Chủ đề</label>
							<select class="form-control" name="" id="type1">
								<option value="">Gia đình</option>
								<option value="">Thể thao</option>
								<option value="">Thời tiết</option>
							</select>
							</div>
							</div>
						</div>
						

						<p class="text-center"><a href="#" class="btn btn-danger glyphicon glyphicon-search" role="button"></a></p>
					</form>
				</div>
				<!-- end search -->
				<!-- trái dưới -->
			


					<h2><span class="label label-default">Chủ đề mới</span></h2>
					<?php
							foreach ($topics as $topic) {

								?>
								<?php
			//}
								?>
						<div class="col-md-6" style="padding-left: 3px; padding-right: 3px">
							

							<div class="view">
								<div class="caption">
									<h3>Wiseberry</h3>
									<a href="" rel="tooltip" title="Add to Favorites"><span class="fa fa-heart-o fa-2x"></span></a>

									<br/><br/><button type="button" class="btn btn-danger">Vào học</button>
								</div>
								<div class="topic_name"><?php echo h($topic->topic_name)?></div>
								<img class="img-responsive" alt="200x100" style="width: 100%; height: 200px;" src="<?php echo h($topic->image); ?>">
								
							</div>
							<div class="info" style="margin-bottom: 15px">
								<p class="small" style="text-overflow: ellipsis">CASTLE HILL</p>
								<p class="small wb-red">30/1-7 Hume Ave</p>
								
							</div>

							

						</div>
						<?php
					}
					?>	
				<br>
				<br>
				<!-- end trái dưới -->
				<!-- chủ đề phổ biến -->
					<h2><span class="label label-default" ">Chủ đề được yêu thích</span></h2>
					<?php
							foreach ($topics as $topic) {

								?>
								<?php
			//}
								?>
						<div class="col-md-6" style="padding-left: 3px; padding-right: 3px">
							

							<div class="view">
								<div class="caption">
									<h3>Wiseberry</h3>
									<a href="" rel="tooltip" title="Add to Favorites"><span class="fa fa-heart-o fa-2x"></span></a>

									<br/><br/><button type="button" class="btn btn-danger">Vào học</button>
								</div>
								<div class="topic_name"><?php echo h($topic->topic_name)?></div>
								<img class="img-responsive" alt="200x100" style="width: 100%; height: 200px;" src="<?php echo h($topic->image); ?>">
								
							</div>
							<div class="info" style="margin-bottom: 15px">
								<p class="small" style="text-overflow: ellipsis">CASTLE HILL</p>
								<p class="small wb-red">30/1-7 Hume Ave</p>
								
							</div>

							

						</div>
						<?php
					}
					?>	
				<!-- end chủ đề phổ biến -->
			</div>
			<!-- end cột trái -->

			</div> <!-- end cột phai-->


			</div> <!-- end cột phai-->

		</div> <!-- end row --> 	
	</div><!-- end container -->
<!-- end fh5co-main -->

