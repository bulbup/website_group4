<!-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> -->
<?php
//header("content-type:application/csv;charset=UTF-8");
//header('Content-Type: text/xml, charset=UTF-8; encoding=UTF-8');

?>

<div class="container body">
    <div class="row home">
        <div class="col-md-9 col-lg-9 col-xs-9 col-sm-9 content">
            <div class="">
                <div class="panel panel-default" style="margin-top: 2%; margin-bottom: 2%">
                
                    <div class="panel-heading">
                        <h5 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                <h3 class="title">
                                    <?php 
                                    echo $chuong->topic_name.' ';
                                    foreach ($lesson as $key => $value) {
                                    if(($baihoc->lesson_id == $value->Ma) && ($chuong->topic_id == $value->topic_id)){ 
                                        echo ' '.$value->lesson_name;
                                        }
                                    }
                                    ?>
                                </h3>
                            </a>
                        </h5>
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#coban">Cơ bản</a></li>
                            <li><a data-toggle="tab" href="#nangcao">Nâng cao</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="coban" class="tab-pane fade in active">
                              <?php
                              foreach ($lesson as $key => $value) {
                                if(($baihoc->lesson_id == $value->Ma) && ($chuong->topic_id == $value->topic_id)){ 
                                    ?>
                                    <img src="<?php echo $value->image ?>" class="img-thumbnail" style="width: 100%;" />

                                    <?php
                                }
                            }
                            ?>


                            <p class="loading">

                                <em><img src="http://www.ifmo.ru/images/loader.gif" style="width: 3%;" alt="Initializing audio"> Loading audio…
                                    <?php //$this->Html->image('loader.gif',['class'=>'img-responsive'])?></em>
                                </p>

                                <p class="passage-audio" >
                                <!--  <audio controls id="passage-audio" class="passage">
                                    <source src="../media/Luke.2.1-Luke.2.20.mp3" type="audio/mp3">
                                    <source src="../media/Luke.2.1-Luke.2.20.ogg" type="audio/ogg"> 
                                    <source src="../media/Luke.2.1-Luke.2.20.wav" type="audio/wav"> 
                                        <em class="error"><strong>Error:</strong> Your browser doesn't appear to support HTML5 Audio.</em> 
                                    </audio>  -->
                                    <?=  $this->Html->media(['audio/Luke.2.1-Luke.2.20.mp3'],['controls']); ?>

                                </p>
                                <p class="passage-audio-unavailable" hidden>
                                    <em class="error"><strong>Error:</strong> You will not be able to do the read-along audio because your browser is not able to play MP3, Ogg, or WAV audio formats.</em>
                                </p>

                                <p class="playback-rate" hidden title="Note that increaseing the reading rate will decrease accuracy of word highlights">
                                    <label for="playback-rate">Reading rate:</label>
                                    <input id="playback-rate" type="range" min="0.5" max="2.0" value="1.0" step="0.1" style="width: 25% !important;" disabled onchange='this.nextElementSibling.textContent = String(Math.round(this.valueAsNumber * 10) / 10) + "\u00D7";'> <!-- Chỗ tính này là seo??? -->
                                    <output>1&times;</output>
                                </p>
                                <p class="playback-rate-unavailable" hidden>
                                    <em>(It seems your browser does not support <code>HTMLMediaElement.playbackRate</code>, so you will not be able to change the speech rate.)</em>
                                </p>
                                <p class="autofocus-current-word" hidden>
                                    <input type="checkbox" id="autofocus-current-word" checked>
                                    <label for="autofocus-current-word">Auto-focus/auto-scroll</label>
                                </p>

                                <noscript>
                                    <p class="error"><em><strong>Notice:</strong> You must have JavaScript enabled/available to try this HTML5 Audio read along.</em></p>
                                </noscript>

                                <div id="passage-text" class="passage">


                                    <h3 class="section-heading">The Birth of Jesus Christ</h3>

                        <!-- bắt đầu ở giây thứ 'data-begin' và kéo dài trong 'data-dur' giây, từ sau sẽ bắt đầu ở giây 'data-begin'+'data-dur' của từ trước đó
                        
                    -->
                    <p><sup class="verse-start">1</sup><span data-dur="0.154" data-begin="0.775">In</span> <span data-dur="0.28" data-begin="0.929">those</span> <span data-dur="0.29" data-begin="1.218">days</span> <span data-dur="0.131" data-begin="1.508">a</span> <span data-dur="0.525" data-begin="1.639">decree</span> <span data-dur="0.191" data-begin="2.165">went</span> <span data-dur="0.225" data-begin="2.355">out</span> <span data-dur="0.245" data-begin="2.583">from</span> <span data-dur="0.438" data-begin="2.828">Caesar</span> <span data-dur="0.637" data-begin="3.267">Augustus</span> <span data-dur="0.166" data-begin="4.03">that</span> <span data-dur="0.268" data-begin="4.216">all</span> <span data-dur="0.111" data-begin="4.486">the</span> <span data-dur="0.411" data-begin="4.594">world</span> <span data-dur="0.205" data-begin="5.006">should</span> <span data-dur="0.134" data-begin="5.211">be</span> <span data-dur="0.529" data-begin="5.344">registered</span>. <sup class="verse-start">2</sup><span data-dur="0.201" data-begin="6.675">This</span> <span data-dur="0.124" data-begin="6.876">was</span> <span data-dur="0.11" data-begin="7">the</span> <span data-dur="0.321" data-begin="7.11">first</span> <span data-dur="0.762" data-begin="7.431">registration</span> <span data-dur="0.164" data-begin="8.193">when</span> <span data-dur="0.474" data-begin="8.357">Quirinius</span> <span data-dur="0.206" data-begin="8.834">was</span> <span data-dur="0.338" data-begin="9.041">governor</span> <span data-dur="0.082" data-begin="9.379">of</span> <span data-dur="0.477" data-begin="9.46">Syria</span>. <sup class="verse-start">3</sup><span data-dur="0.119" data-begin="10.676">And</span> <span data-dur="0.24" data-begin="10.794">all</span> <span data-dur="0.186" data-begin="11.034">went</span> <span data-dur="0.087" data-begin="11.22">to</span> <span data-dur="0.139" data-begin="11.307">be</span> <span data-dur="0.592" data-begin="11.446">registered</span>, <span data-dur="0.251" data-begin="12.284">each</span> <span data-dur="0.093" data-begin="12.572">to</span> <span data-dur="0.134" data-begin="12.665">his</span> <span data-dur="0.275" data-begin="12.799">own</span> <span data-dur="0.467" data-begin="13.074">town</span>. <sup class="verse-start">4</sup><span data-dur="0.184" data-begin="14.369">And</span> <span data-dur="0.358" data-begin="14.553">Joseph</span> <span data-dur="0.351" data-begin="14.911">also</span> <span data-dur="0.128" data-begin="15.262">went</span> <span data-dur="0.152" data-begin="15.39">up</span> <span data-dur="0.215" data-begin="15.595">from</span> <span data-dur="0.541" data-begin="15.811">Galilee</span>, <span data-dur="0.074" data-begin="16.557">from</span> <span data-dur="0.121" data-begin="16.632">the</span> <span data-dur="0.236" data-begin="16.752">town</span> <span data-dur="0.097" data-begin="16.988">of</span> <span data-dur="0.559" data-begin="17.085">Nazareth</span>, <span data-dur="0.154" data-begin="17.966">to</span> <span data-dur="0.575" data-begin="18.12">Judea</span>, <span data-dur="0.129" data-begin="18.823">to</span> <span data-dur="0.059" data-begin="18.952">the</span> <span data-dur="0.31" data-begin="19.011">city</span> <span data-dur="0.166" data-begin="19.321">of</span> <span data-dur="0.393" data-begin="19.487">David</span>, <span data-dur="0.161" data-begin="20.029">which</span> <span data-dur="0.109" data-begin="20.19">is</span> <span data-dur="0.307" data-begin="20.321">called</span> <span data-dur="0.642" data-begin="20.628">Bethlehem</span>, <span data-dur="0.317" data-begin="21.76">because</span> <span data-dur="0.116" data-begin="22.077">he</span> <span data-dur="0.104" data-begin="22.193">was</span> <span data-dur="0.166" data-begin="22.297">of</span> <span data-dur="0.059" data-begin="22.463">the</span> <span data-dur="0.412" data-begin="22.522">house</span> <span data-dur="0.155" data-begin="22.935">and</span> <span data-dur="0.384" data-begin="23.09">lineage</span> <span data-dur="0.175" data-begin="23.474">of</span> <span data-dur="0.421" data-begin="23.648">David</span>, <sup class="verse-start">5</sup><span data-dur="0.127" data-begin="24.714">to</span> <span data-dur="0.172" data-begin="24.84">be</span> <span data-dur="0.53" data-begin="25.013">registered</span> <span data-dur="0.125" data-begin="25.543">with</span> <span data-dur="0.515" data-begin="25.668">Mary</span>, <span data-dur="0.172" data-begin="26.183">his</span> <span data-dur="0.607" data-begin="26.355">betrothed</span>, <span data-dur="0.123" data-begin="27.134">who</span> <span data-dur="0.166" data-begin="27.257">was</span> <span data-dur="0.167" data-begin="27.423">with</span> <span data-dur="0.513" data-begin="27.59">child</span>. <sup class="verse-start">6</sup><span data-dur="0.161" data-begin="29.448">And</span> <span data-dur="0.362" data-begin="29.609">while</span> <span data-dur="0.159" data-begin="29.97">they</span> <span data-dur="0.166" data-begin="30.129">were</span> <span data-dur="0.436" data-begin="30.295">there</span>, <span data-dur="0.159" data-begin="31.072">the</span> <span data-dur="0.431" data-begin="31.231">time</span> <span data-dur="0.277" data-begin="31.662">came</span> <span data-dur="0.161" data-begin="31.939">for</span> <span data-dur="0.093" data-begin="32.1">her</span> <span data-dur="0.107" data-begin="32.193">to</span> <span data-dur="0.233" data-begin="32.299">give</span> <span data-dur="0.352" data-begin="32.522">birth</span>. <sup class="verse-start">7</sup><span data-dur="0.133" data-begin="33.972">And</span> <span data-dur="0.213" data-begin="34.105">she</span> <span data-dur="0.277" data-begin="34.318">gave</span> <span data-dur="0.253" data-begin="34.596">birth</span> <span data-dur="0.069" data-begin="34.888">to</span> <span data-dur="0.171" data-begin="34.957">her</span> <span data-dur="0.602" data-begin="35.128">firstborn</span> <span data-dur="0.56" data-begin="35.73">son</span> <span data-dur="0.166" data-begin="36.491">and</span> <span data-dur="0.342" data-begin="36.657">wrapped</span> <span data-dur="0.153" data-begin="36.998">him</span> <span data-dur="0.119" data-begin="37.152">in</span> <span data-dur="0.55" data-begin="37.271">swaddling</span> <span data-dur="0.542" data-begin="37.82">cloths</span> <span data-dur="0.154" data-begin="38.644">and</span> <span data-dur="0.287" data-begin="38.798">laid</span> <span data-dur="0.176" data-begin="39.085">him</span> <span data-dur="0.087" data-begin="39.261">in</span> <span data-dur="0.092" data-begin="39.348">a</span> <span data-dur="0.604" data-begin="39.44">manger</span>, <span data-dur="0.277" data-begin="40.182">because</span> <span data-dur="0.131" data-begin="40.46">there</span> <span data-dur="0.151" data-begin="40.591">was</span> <span data-dur="0.213" data-begin="40.742">no</span> <span data-dur="0.312" data-begin="40.975">place</span> <span data-dur="0.121" data-begin="41.287">for</span> <span data-dur="0.158" data-begin="41.408">them</span> <span data-dur="0.116" data-begin="41.566">in</span> <span data-dur="0.111" data-begin="41.683">the</span> <span data-dur="0.406" data-begin="41.794">inn</span>.</p>
                    <button type="button" class="btn btn-danger" onclick="anphude();">Xem phụ đề</button>                  
                    <div id="hide_transcript" class="collapse">
                    <div class="warning" style="background-color: #ffffcc;
    border-left: 6px solid #ffeb3b; font-size: 20px; margin-top: 10px; margin-bottom: 10px"><a href="#" download><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Tải phụ đề</a></div>
                    <?php

                    foreach ($data_read as $key => $value) {
                        if ($value['id'] == $baihoc->Ma) {

                            ?>
                            <p>

                                <?php echo $value['id'] ?>
                                <?php echo $value['content'] ?>
                                <?php //echo $value['file_id'] ?>
                                <?php //echo $value['answer'] ?>
                                
                                
                            </p>
                            <?php
                        }
                    }
                    ?>

                            </div>
                            </div>
                            <!-- end doan text -->
                            <!-- audio tra loi -->
                            <div class="section1">
                                <h3>Nghe và trả lời</h3>
                                <ul>
                                    <script>
                                        function play(){
                                           var audio = document.getElementById("audio");
                                           audio.play();
                                       }
                                   </script>

                                   <!-- speaking -->
                                   <div class="panel panel-default">
                                    <div class="row">
                                        <div class="col-sm-7">
                                        <!--  
                                        <div class="cauhoi">
                                            <?php //$this->Html->image('speaker-icon.png',['onclick'=>"play()"]); ?>
                                            <?php //$this->Html->media('audio/what-is-your-name.mp3',['id'=>'audio'])?>
                                            
                                            <?php //echo $this->Html->image('microphone-icon.png'); ?>
                                            <input type="text" x-webkit-speech>
                                            <?php //echo $this->Html->image('false.jpg'); ?>
                                        </div>
                                     

                                        <div class="cauhoi">
                                            <?php //$this->Html->image('speaker-icon.png',['onclick'=>"play()"]); ?>
                                            <?php //$this->Html->media('audio/what-is-your-name.mp3',['id'=>'audio'])?>

                                            <?php //echo $this->Html->image('microphone-icon.png'); ?>
                                            <input type="text" x-webkit-speech>
                                            <?php //echo $this->Html->image('false.jpg'); ?>
                                        </div>

                                        <div class="cauhoi">
                                            <?php //$this->Html->image('speaker-icon.png',['onclick'=>"play()"]); ?>
                                            <?php //$this->Html->media('audio/what-is-your-name.mp3',['id'=>'audio'])?>

                                            <?php //echo $this->Html->image('microphone-icon.png'); ?>
                                            <input type="text" x-webkit-speech>
                                            <?php //echo $this->Html->image('true.jpg'); ?>
                                            
                                        </div>
                                    -->
                                    <?php
                                    $count = 0;
                                    foreach ($data_read as $key => $value) {
                                        $count++;

                                        ?>
                                        <!-- cau noi 4 -->
                                        <div class="cauhoi">
                                            <?php //$this->Html->image('speaker-icon.png',['onclick'=>"play()"]); ?>
                                            <button onclick="play(<?php echo $count; ?>)" class="fa fa-headphones" style="background-color: #fff !important; "></button>

                                            <?php //$this->Html->media('audio/what-is-your-name.mp3',['id'=>'audio'])?>
                                            <?= $this->Html->media($value['file_id'],['id'=>$count])?>

                                            <!-- <button onclick="startConverting(<?php //echo $count; ?>);setTimeout(check(<?php //echo $count; ?>), 10000);" class="fa fa-microphone btn-record" style="background-color: #fff !important;"></button> -->
                                            <button onclick="startConverting(<?php echo $count; ?>);" class="fa fa-microphone btn-record" style="background-color: #fff !important;"></button>

                                            <?php //echo $this->Html->image('microphone-icon.png'); ?>
                                            <!-- <input type="text" x-webkit-speech>            -->
                                            <textarea class="txtResult" id="result<?php echo $count; ?>" style="height: 40px; width: 250px"></textarea>
                                            <input type="hidden" value="<?php echo $count; ?>" class="stt">
                                            <!-- <?php //echo $this->Html->image('false.jpg'); ?> -->
                                            <div class="true">

                                            </div>
                                        </div>
                                        <!-- end cot 1 -->
                                        <!-- end cau noi 4 -->


                                        <?php
                                    }
                                    ?>
                                    <!-- audio -->
                                    <script>
                                        function play(id){
                                         var audio = document.getElementById(id);
                                         audio.play();
                                     }
                                 </script>


                                 <!-- ghi âm giọng nói -->
                                 <script type="text/javascript">

                                        //var r = document.getElementById('result'+<?php //echo $count; ?>);

                                        function startConverting (id) {
                                            //alert(id);
                                            var r = document.getElementById('result'+id);
                                            if('webkitSpeechRecognition' in window){
                                                var speechRecognizer = new webkitSpeechRecognition();
                                                speechRecognizer.continuous = true;
                                                speechRecognizer.interimResults = true;
                                                speechRecognizer.lang = 'en-IN';
                                                speechRecognizer.start();

                                                var finalTranscripts = '';

                                                speechRecognizer.onresult = function(event){
                                                    var interimTranscripts = '';
                                                    for(var i = event.resultIndex; i < event.results.length; i++){
                                                        var transcript = event.results[i][0].transcript;
                                                        transcript.replace("\n", "<br>");
                                                        if(event.results[i].isFinal){
                                                            finalTranscripts += transcript;
                                                        }else{
                                                            interimTranscripts += transcript;
                                                        }
                                                    }
                                                    // r.innerHTML = finalTranscripts + '<span style="color:#999">' + interimTranscripts + '</span>';
                                                    r.innerHTML = finalTranscripts + interimTranscripts;
                                                    
                                                };

                                                
                                                speechRecognizer.onerror = function (event) {
                                                };
                                            }else{
                                                r.innerHTML = 'Your browser is not supported. If google chrome, please upgrade!';
                                            }
                                        }

                                        // function check(id){
                                        //     var check = document.getElementById('result'+ id);
                                        //     alert(check.innerHTML);
                                        //     if(check.innerHTML != ''){
                                        //         if(check.innerHTML == '<?php //echo $value['answer']; ?>'){
                                        //             alert('đúng');
                                        //         }else{
                                        //             alert('sai');
                                        //         }
                                        //     }else{
                                        //         alert('chưa có dữ liệu');
                                        //     }
                                        // }

                                        $(".btn-record").on( "click", function() {
                                          var btn=$(this);
                                          setTimeout(
                                            function() {
                                                var answer=btn.parent().find("textarea").val();
                                                var stt=btn.parent().find(".stt").val();
                                                <?php foreach ($data_read as $key => $value) { ?>
                                                    if((answer == '') && (stt == '<?php echo $value['stt']; ?>')){
                                                        alert('Chưa nhận được giọng nói. Vui lòng trả lời lại!');
                                                        var img = btn.parent().find(".true").html('<img src="https://t3.ftcdn.net/jpg/00/12/88/28/500_F_12882805_6WZBYZZj9ZijCRTfCRGLpgMNh0GFI3TF.jpg" width="50" height="50">');
                                                    }else if((answer == '<?php echo $value['answer']; ?>') && (stt == '<?php echo $value['stt']; ?>')){
                                                            //$(".true").html('<img src="img/true.jpg">');
                                                            var img = btn.parent().find(".true").html('<img src="http://www.gaestezimmer-hagen.de/images/haken_rot.gif" width="50" height="50">');
                                                        }else if((answer != '<?php echo $value['answer']; ?>') && (stt == '<?php echo $value['stt']; ?>')){
                                                            var img = btn.parent().find(".true").html('<img src="https://yt3.ggpht.com/-b7vGFzVjJwg/AAAAAAAAAAI/AAAAAAAAAAA/msUtINENTWA/s900-c-k-no-mo-rj-c0xffffff/photo.jpg" width="50" height="50">');
                                                        }
                                                        

                                                        <?php } ?>
                                                    },
                                                    5000);
                                      });

                                  </script>
                                  <!-- .ghi âm giọng nói -->

                              </div>
                                <div class="col-sm-5">
                                <!-- <img src="images/chibi.png" class="img-responsive"> -->
                                <?= $this->Html->image('chibi.png',['class'=>'img-responsive'])?>
                                </div>
                            </div>



                            <!--.speaking -->
                            </div> <!-- het cot 2 -->
                            </ul>
                            </div>
                            <!-- end audio -->
                            </div>
                            <!-- end cơ bản -->
                            <div id="nangcao" class="tab-pane fade">
                                <?php
                              foreach ($lesson as $key => $value) {
                                if(($baihoc->lesson_id == $value->Ma) && ($chuong->topic_id == $value->topic_id)){ 
                                    ?>
                                    <img src="<?php echo $value->image ?>" class="img-thumbnail" style="width: 100%;" />

                                    <?php
                                }
                            }
                            ?>


                            <p class="loading">

                                <em><img src="http://www.ifmo.ru/images/loader.gif" style="width: 3%;" alt="Initializing audio"> Loading audio…
                                    <?php //$this->Html->image('loader.gif',['class'=>'img-responsive'])?></em>
                                </p>

                                <p class="passage-audio" >
                                <!--  <audio controls id="passage-audio" class="passage">
                                    <source src="../media/Luke.2.1-Luke.2.20.mp3" type="audio/mp3">
                                    <source src="../media/Luke.2.1-Luke.2.20.ogg" type="audio/ogg"> 
                                    <source src="../media/Luke.2.1-Luke.2.20.wav" type="audio/wav"> 
                                        <em class="error"><strong>Error:</strong> Your browser doesn't appear to support HTML5 Audio.</em> 
                                    </audio>  -->
                                    <?=  $this->Html->media(['audio/Luke.2.1-Luke.2.20.mp3'],['controls']); ?>

                                </p>
                                <p class="passage-audio-unavailable" hidden>
                                    <em class="error"><strong>Error:</strong> You will not be able to do the read-along audio because your browser is not able to play MP3, Ogg, or WAV audio formats.</em>
                                </p>

                                <p class="playback-rate" hidden title="Note that increaseing the reading rate will decrease accuracy of word highlights">
                                    <label for="playback-rate">Reading rate:</label>
                                    <input id="playback-rate" type="range" min="0.5" max="2.0" value="1.0" step="0.1" style="width: 25% !important;" disabled onchange='this.nextElementSibling.textContent = String(Math.round(this.valueAsNumber * 10) / 10) + "\u00D7";'> <!-- Chỗ tính này là seo??? -->
                                    <output>1&times;</output>
                                </p>
                                <p class="playback-rate-unavailable" hidden>
                                    <em>(It seems your browser does not support <code>HTMLMediaElement.playbackRate</code>, so you will not be able to change the speech rate.)</em>
                                </p>
                                <p class="autofocus-current-word" hidden>
                                    <input type="checkbox" id="autofocus-current-word" checked>
                                    <label for="autofocus-current-word">Auto-focus/auto-scroll</label>
                                </p>

                                <noscript>
                                    <p class="error"><em><strong>Notice:</strong> You must have JavaScript enabled/available to try this HTML5 Audio read along.</em></p>
                                </noscript>

                                <div id="passage-text" class="passage">


                                    <h3 class="section-heading">The Birth of Jesus Christ</h3>

                        <!-- bắt đầu ở giây thứ 'data-begin' và kéo dài trong 'data-dur' giây, từ sau sẽ bắt đầu ở giây 'data-begin'+'data-dur' của từ trước đó
                        
                    -->
                    <p><sup class="verse-start">1</sup><span data-dur="0.154" data-begin="0.775">In</span> <span data-dur="0.28" data-begin="0.929">those</span> <span data-dur="0.29" data-begin="1.218">days</span> <span data-dur="0.131" data-begin="1.508">a</span> <span data-dur="0.525" data-begin="1.639">decree</span> <span data-dur="0.191" data-begin="2.165">went</span> <span data-dur="0.225" data-begin="2.355">out</span> <span data-dur="0.245" data-begin="2.583">from</span> <span data-dur="0.438" data-begin="2.828">Caesar</span> <span data-dur="0.637" data-begin="3.267">Augustus</span> <span data-dur="0.166" data-begin="4.03">that</span> <span data-dur="0.268" data-begin="4.216">all</span> <span data-dur="0.111" data-begin="4.486">the</span> <span data-dur="0.411" data-begin="4.594">world</span> <span data-dur="0.205" data-begin="5.006">should</span> <span data-dur="0.134" data-begin="5.211">be</span> <span data-dur="0.529" data-begin="5.344">registered</span>. <sup class="verse-start">2</sup><span data-dur="0.201" data-begin="6.675">This</span> <span data-dur="0.124" data-begin="6.876">was</span> <span data-dur="0.11" data-begin="7">the</span> <span data-dur="0.321" data-begin="7.11">first</span> <span data-dur="0.762" data-begin="7.431">registration</span> <span data-dur="0.164" data-begin="8.193">when</span> <span data-dur="0.474" data-begin="8.357">Quirinius</span> <span data-dur="0.206" data-begin="8.834">was</span> <span data-dur="0.338" data-begin="9.041">governor</span> <span data-dur="0.082" data-begin="9.379">of</span> <span data-dur="0.477" data-begin="9.46">Syria</span>. <sup class="verse-start">3</sup><span data-dur="0.119" data-begin="10.676">And</span> <span data-dur="0.24" data-begin="10.794">all</span> <span data-dur="0.186" data-begin="11.034">went</span> <span data-dur="0.087" data-begin="11.22">to</span> <span data-dur="0.139" data-begin="11.307">be</span> <span data-dur="0.592" data-begin="11.446">registered</span>, <span data-dur="0.251" data-begin="12.284">each</span> <span data-dur="0.093" data-begin="12.572">to</span> <span data-dur="0.134" data-begin="12.665">his</span> <span data-dur="0.275" data-begin="12.799">own</span> <span data-dur="0.467" data-begin="13.074">town</span>. <sup class="verse-start">4</sup><span data-dur="0.184" data-begin="14.369">And</span> <span data-dur="0.358" data-begin="14.553">Joseph</span> <span data-dur="0.351" data-begin="14.911">also</span> <span data-dur="0.128" data-begin="15.262">went</span> <span data-dur="0.152" data-begin="15.39">up</span> <span data-dur="0.215" data-begin="15.595">from</span> <span data-dur="0.541" data-begin="15.811">Galilee</span>, <span data-dur="0.074" data-begin="16.557">from</span> <span data-dur="0.121" data-begin="16.632">the</span> <span data-dur="0.236" data-begin="16.752">town</span> <span data-dur="0.097" data-begin="16.988">of</span> <span data-dur="0.559" data-begin="17.085">Nazareth</span>, <span data-dur="0.154" data-begin="17.966">to</span> <span data-dur="0.575" data-begin="18.12">Judea</span>, <span data-dur="0.129" data-begin="18.823">to</span> <span data-dur="0.059" data-begin="18.952">the</span> <span data-dur="0.31" data-begin="19.011">city</span> <span data-dur="0.166" data-begin="19.321">of</span> <span data-dur="0.393" data-begin="19.487">David</span>, <span data-dur="0.161" data-begin="20.029">which</span> <span data-dur="0.109" data-begin="20.19">is</span> <span data-dur="0.307" data-begin="20.321">called</span> <span data-dur="0.642" data-begin="20.628">Bethlehem</span>, <span data-dur="0.317" data-begin="21.76">because</span> <span data-dur="0.116" data-begin="22.077">he</span> <span data-dur="0.104" data-begin="22.193">was</span> <span data-dur="0.166" data-begin="22.297">of</span> <span data-dur="0.059" data-begin="22.463">the</span> <span data-dur="0.412" data-begin="22.522">house</span> <span data-dur="0.155" data-begin="22.935">and</span> <span data-dur="0.384" data-begin="23.09">lineage</span> <span data-dur="0.175" data-begin="23.474">of</span> <span data-dur="0.421" data-begin="23.648">David</span>, <sup class="verse-start">5</sup><span data-dur="0.127" data-begin="24.714">to</span> <span data-dur="0.172" data-begin="24.84">be</span> <span data-dur="0.53" data-begin="25.013">registered</span> <span data-dur="0.125" data-begin="25.543">with</span> <span data-dur="0.515" data-begin="25.668">Mary</span>, <span data-dur="0.172" data-begin="26.183">his</span> <span data-dur="0.607" data-begin="26.355">betrothed</span>, <span data-dur="0.123" data-begin="27.134">who</span> <span data-dur="0.166" data-begin="27.257">was</span> <span data-dur="0.167" data-begin="27.423">with</span> <span data-dur="0.513" data-begin="27.59">child</span>. <sup class="verse-start">6</sup><span data-dur="0.161" data-begin="29.448">And</span> <span data-dur="0.362" data-begin="29.609">while</span> <span data-dur="0.159" data-begin="29.97">they</span> <span data-dur="0.166" data-begin="30.129">were</span> <span data-dur="0.436" data-begin="30.295">there</span>, <span data-dur="0.159" data-begin="31.072">the</span> <span data-dur="0.431" data-begin="31.231">time</span> <span data-dur="0.277" data-begin="31.662">came</span> <span data-dur="0.161" data-begin="31.939">for</span> <span data-dur="0.093" data-begin="32.1">her</span> <span data-dur="0.107" data-begin="32.193">to</span> <span data-dur="0.233" data-begin="32.299">give</span> <span data-dur="0.352" data-begin="32.522">birth</span>. <sup class="verse-start">7</sup><span data-dur="0.133" data-begin="33.972">And</span> <span data-dur="0.213" data-begin="34.105">she</span> <span data-dur="0.277" data-begin="34.318">gave</span> <span data-dur="0.253" data-begin="34.596">birth</span> <span data-dur="0.069" data-begin="34.888">to</span> <span data-dur="0.171" data-begin="34.957">her</span> <span data-dur="0.602" data-begin="35.128">firstborn</span> <span data-dur="0.56" data-begin="35.73">son</span> <span data-dur="0.166" data-begin="36.491">and</span> <span data-dur="0.342" data-begin="36.657">wrapped</span> <span data-dur="0.153" data-begin="36.998">him</span> <span data-dur="0.119" data-begin="37.152">in</span> <span data-dur="0.55" data-begin="37.271">swaddling</span> <span data-dur="0.542" data-begin="37.82">cloths</span> <span data-dur="0.154" data-begin="38.644">and</span> <span data-dur="0.287" data-begin="38.798">laid</span> <span data-dur="0.176" data-begin="39.085">him</span> <span data-dur="0.087" data-begin="39.261">in</span> <span data-dur="0.092" data-begin="39.348">a</span> <span data-dur="0.604" data-begin="39.44">manger</span>, <span data-dur="0.277" data-begin="40.182">because</span> <span data-dur="0.131" data-begin="40.46">there</span> <span data-dur="0.151" data-begin="40.591">was</span> <span data-dur="0.213" data-begin="40.742">no</span> <span data-dur="0.312" data-begin="40.975">place</span> <span data-dur="0.121" data-begin="41.287">for</span> <span data-dur="0.158" data-begin="41.408">them</span> <span data-dur="0.116" data-begin="41.566">in</span> <span data-dur="0.111" data-begin="41.683">the</span> <span data-dur="0.406" data-begin="41.794">inn</span>.</p>
                    
                    <?php

                    foreach ($data_read as $key => $value) {
                        if ($value['id'] == $baihoc->Ma) {

                            ?>
                            <p>

                                <?php echo $value['id'] ?>
                                <?php echo $value['content'] ?>
                                <?php //echo $value['file_id'] ?>
                                <?php //echo $value['answer'] ?>
                                
                                
                            </p>
                            <?php
                        }
                    }
                    ?>
                    
                            </div>
                            <!-- end doan text -->
                            <!-- audio tra loi -->
                            <div class="section1">
                                <h3>Nghe và trả lời</h3>
                                <ul>
                                    <script>
                                        function play(){
                                           var audio = document.getElementById("audio");
                                           audio.play();
                                       }
                                   </script>

                                   <!-- speaking -->
                                   <div class="panel panel-default">
                                    <div class="row">
                                        <div class="col-sm-7">
                                        <!--  
                                        <div class="cauhoi">
                                            <?php //$this->Html->image('speaker-icon.png',['onclick'=>"play()"]); ?>
                                            <?php //$this->Html->media('audio/what-is-your-name.mp3',['id'=>'audio'])?>
                                            
                                            <?php //echo $this->Html->image('microphone-icon.png'); ?>
                                            <input type="text" x-webkit-speech>
                                            <?php //echo $this->Html->image('false.jpg'); ?>
                                        </div>
                                     

                                        <div class="cauhoi">
                                            <?php //$this->Html->image('speaker-icon.png',['onclick'=>"play()"]); ?>
                                            <?php //$this->Html->media('audio/what-is-your-name.mp3',['id'=>'audio'])?>

                                            <?php //echo $this->Html->image('microphone-icon.png'); ?>
                                            <input type="text" x-webkit-speech>
                                            <?php //echo $this->Html->image('false.jpg'); ?>
                                        </div>

                                        <div class="cauhoi">
                                            <?php //$this->Html->image('speaker-icon.png',['onclick'=>"play()"]); ?>
                                            <?php //$this->Html->media('audio/what-is-your-name.mp3',['id'=>'audio'])?>

                                            <?php //echo $this->Html->image('microphone-icon.png'); ?>
                                            <input type="text" x-webkit-speech>
                                            <?php //echo $this->Html->image('true.jpg'); ?>
                                            
                                        </div>
                                    -->
                                    <?php
                                    $count = 0;
                                    foreach ($data_read as $key => $value) {
                                        $count++;

                                        ?>
                                        <!-- cau noi 4 -->
                                        <div class="cauhoi">
                                            <?php //$this->Html->image('speaker-icon.png',['onclick'=>"play()"]); ?>
                                            <button onclick="play(<?php echo $count; ?>)" class="fa fa-headphones" style="background-color: #fff !important; "></button>

                                            <?php //$this->Html->media('audio/what-is-your-name.mp3',['id'=>'audio'])?>
                                            <?= $this->Html->media($value['file_id'],['id'=>$count])?>

                                            <!-- <button onclick="startConverting(<?php //echo $count; ?>);setTimeout(check(<?php //echo $count; ?>), 10000);" class="fa fa-microphone btn-record" style="background-color: #fff !important;"></button> -->
                                            <button onclick="startConverting(<?php echo $count; ?>);" class="fa fa-microphone btn-record" style="background-color: #fff !important;"></button>

                                            <?php //echo $this->Html->image('microphone-icon.png'); ?>
                                            <!-- <input type="text" x-webkit-speech>            -->
                                            <textarea class="txtResult" id="result<?php echo $count; ?>" style="height: 40px; width: 250px"></textarea>
                                            <input type="hidden" value="<?php echo $count; ?>" class="stt">
                                            <!-- <?php //echo $this->Html->image('false.jpg'); ?> -->
                                            <div class="true">

                                            </div>
                                        </div>
                                        <!-- end cot 1 -->
                                        <!-- end cau noi 4 -->


                                        <?php
                                    }
                                    ?>
                                    <!-- audio -->
                                    <script>
                                        function play(id){
                                         var audio = document.getElementById(id);
                                         audio.play();
                                     }
                                 </script>


                                 <!-- ghi âm giọng nói -->
                                 <script type="text/javascript">

                                        //var r = document.getElementById('result'+<?php //echo $count; ?>);

                                        function startConverting (id) {
                                            //alert(id);
                                            var r = document.getElementById('result'+id);
                                            if('webkitSpeechRecognition' in window){
                                                var speechRecognizer = new webkitSpeechRecognition();
                                                speechRecognizer.continuous = true;
                                                speechRecognizer.interimResults = true;
                                                speechRecognizer.lang = 'en-IN';
                                                speechRecognizer.start();

                                                var finalTranscripts = '';

                                                speechRecognizer.onresult = function(event){
                                                    var interimTranscripts = '';
                                                    for(var i = event.resultIndex; i < event.results.length; i++){
                                                        var transcript = event.results[i][0].transcript;
                                                        transcript.replace("\n", "<br>");
                                                        if(event.results[i].isFinal){
                                                            finalTranscripts += transcript;
                                                        }else{
                                                            interimTranscripts += transcript;
                                                        }
                                                    }
                                                    // r.innerHTML = finalTranscripts + '<span style="color:#999">' + interimTranscripts + '</span>';
                                                    r.innerHTML = finalTranscripts + interimTranscripts;
                                                    
                                                };

                                                
                                                speechRecognizer.onerror = function (event) {
                                                };
                                            }else{
                                                r.innerHTML = 'Your browser is not supported. If google chrome, please upgrade!';
                                            }
                                        }

                                        // function check(id){
                                        //     var check = document.getElementById('result'+ id);
                                        //     alert(check.innerHTML);
                                        //     if(check.innerHTML != ''){
                                        //         if(check.innerHTML == '<?php //echo $value['answer']; ?>'){
                                        //             alert('đúng');
                                        //         }else{
                                        //             alert('sai');
                                        //         }
                                        //     }else{
                                        //         alert('chưa có dữ liệu');
                                        //     }
                                        // }

                                        $(".btn-record").on( "click", function() {
                                          var btn=$(this);
                                          setTimeout(
                                            function() {
                                                var answer=btn.parent().find("textarea").val();
                                                var stt=btn.parent().find(".stt").val();
                                                <?php foreach ($data_read as $key => $value) { ?>
                                                    if((answer == '') && (stt == '<?php echo $value['stt']; ?>')){
                                                        alert('Chưa nhận được giọng nói. Vui lòng trả lời lại!');
                                                        var img = btn.parent().find(".true").html('<img src="https://t3.ftcdn.net/jpg/00/12/88/28/500_F_12882805_6WZBYZZj9ZijCRTfCRGLpgMNh0GFI3TF.jpg" width="50" height="50">');
                                                    }else if((answer == '<?php echo $value['answer']; ?>') && (stt == '<?php echo $value['stt']; ?>')){
                                                            //$(".true").html('<img src="img/true.jpg">');
                                                            var img = btn.parent().find(".true").html('<img src="http://www.gaestezimmer-hagen.de/images/haken_rot.gif" width="50" height="50">');
                                                        }else if((answer != '<?php echo $value['answer']; ?>') && (stt == '<?php echo $value['stt']; ?>')){
                                                            var img = btn.parent().find(".true").html('<img src="https://yt3.ggpht.com/-b7vGFzVjJwg/AAAAAAAAAAI/AAAAAAAAAAA/msUtINENTWA/s900-c-k-no-mo-rj-c0xffffff/photo.jpg" width="50" height="50">');
                                                        }
                                                        

                                                        <?php } ?>
                                                    },
                                                    5000);
                                      });

                                  </script>
                                  <!-- .ghi âm giọng nói -->

                              </div>
                                <div class="col-sm-5">
                                <!-- <img src="images/chibi.png" class="img-responsive"> -->
                                <?= $this->Html->image('chibi.png',['class'=>'img-responsive'])?>
                                </div>
                            </div>



                            <!--.speaking -->
                            </div> <!-- het cot 2 -->
                            </ul>
                            </div>
                            </div>
                            <!-- end nang cao -->
                        </div>
                        <!-- end tab-content -->
                        <!-- minmap -->
                        <div class="section1">
                            <h3>Học cùng Mind Map</h3>
                            <ul>
                             <div id="draggableElement1">Drag Me 1</div>
                             <div id="draggableElement2">Drag Me 2</div>
                             <div id="draggableElement3">Drag Me 3</div>
                             <div id="draggableElement4">Drag Me 4</div>
                             <div id="draggableElement5">Drag Me 5</div>
                             <div id="droppableElement11">Just accept Drag Me 1</div>
                             <div id="droppableElement22">Just accept Drag Me 2</div>
                             <div id="droppableElement33">Just accept Drag Me 3</div>
                             <div id="droppableElement44">Just accept Drag Me 4</div>
                             <div id="droppableElement55">Just accept Drag Me 5</div>
                             <!-- xử lý drag và drop... chưa để qa file lessons.js mà nó k ăn nên để đây 
                             <script type="text/javascript">
                                $("#draggableElement1").draggable({
                                  'cursor': 'move',
                                  'snap': '#droppableElement'
                              });
                                $("#draggableElement2").draggable({
                                  'cursor': 'move',
                                  'snap': '#droppableElement'
                              });
                                $("#draggableElement3").draggable({
                                  'cursor': 'move',
                                  'snap': '#droppableElement'
                              });
                                $("#draggableElement4").draggable({
                                  'cursor': 'move',
                                  'snap': '#droppableElement'
                              });
                                $("#draggableElement5").draggable({
                                  'cursor': 'move',
                                  'snap': '#droppableElement'
                              });
                                $("#droppableElement11").droppable({
                                  accept: '#draggableElement1',
                                  drop: drop,
                                  hoverClass: 'hovered'
                              });
                                $("#droppableElement22").droppable({
                                  accept: '#draggableElement2',
                                  drop: drop,
                                  hoverClass: 'hovered'
                              });
                                $("#droppableElement33").droppable({
                                  accept: '#draggableElement3',
                                  drop: drop,
                                  hoverClass: 'hovered'
                              });
                                $("#droppableElement44").droppable({
                                  accept: '#draggableElement4',
                                  drop: drop,
                                  hoverClass: 'hovered'
                              });
                                $("#droppableElement55").droppable({
                                  accept: '#draggableElement5',
                                  drop: drop,
                                  hoverClass: 'hovered'
                              });

                                function drop(event, ui) {
                                  alert(ui.draggable.attr('id') + ' dropped on me');
                              }
                          </script>-->
                        </ul>
                        </div>
                        <!-- end mipmap -->
                        <!-- hoc tu vung -->
                        <div class="section1">
                            <h3>Học từ vựng</h3>
                            <ul>
                                <div class="warning" style="background-color: #f5f5f5;
    border-left: 6px solid #ffeb3b; font-size: 20px"><p style="margin-left: 10px">Chọn hình ảnh đúng với nghĩa của từ </p></div>
                                <div style="font-size: 20px">Câu 1: Đàn Piano</div><br>
                                                
                                    <label for="default" class="btn btn-default"><?= $this->Html->image('piano.jpg')?>
                                        <input type="radio" name="options" id="default" class="badgebox"><span class="badge"><i class="fa fa-circle" aria-hidden="true"></i></span></label>
                                        <label for="primary" class="btn btn-default"><?= $this->Html->image('guitar.jpg')?>
                                            <input type="radio" name="options" id="primary" class="badgebox"><span class="badge"><i class="fa fa-circle" aria-hidden="true"></i></span></label>
                                            <label for="info" class="btn btn-default"><?= $this->Html->image('violin.jpg')?>
                                                <input type="radio" name="options" id="info" class="badgebox"><span class="badge"><i class="fa fa-circle" aria-hidden="true"></i></span></label>
                                    
                                    
                                    <input type="submit" value="Kiểm tra" w>
                                    
                                
                                            <!-- css của học từ vựng -->
                                <style type="text/css">
                                    .badgebox
                                    {
                                        opacity: 0;
                                    }

                                    .badgebox + .badge
                                    {
                                        /* Move the check mark away when unchecked */
                                        text-indent: -999999px;
                                        /* Makes the badge's width stay the same checked and unchecked */
                                        width: 27px;
                                    }

                                    .badgebox:focus + .badge
                                    {
                                        /* Set something to make the badge looks focused */
                                        /* This really depends on the application, in my case it was: */
                                        
                                        /* Adding a light border */
                                        box-shadow: inset 0px 0px 5px;
                                        /* Taking the difference out of the padding */
                                    }

                                    .badgebox:checked + .badge
                                    {
                                        /* Move the check mark back when checked */
                                        text-indent: 0;
                                    }
                                </style>
                                <!-- end css của học từ vựng -->
                            </ul>
                        </div>
                        <!-- end hoc tu vung -->
                        <!-- trả lời câu hỏi -->
                        <div class="section1">
                            <h3>Đọc hiểu</h3>
                            <ul>
                                <div class="warning" style="background-color: #f5f5f5;
    border-left: 6px solid #ffeb3b; font-size: 20px"><p style="margin-left: 10px">Hãy trả lời các câu hỏi sau: </p></div>
                                <li>Câu hỏi 1: Con chó có mấy chân?</li><br/>
                                <style>
input[type=text], select {
    width: 80%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

input[type=submit] {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

input[type=submit]:hover {
    background-color: #45a049;
}


</style>
                                <div>
                                    <form action="" >
                                    <input type="text" id="fname" name="firstname" placeholder="Gõ đáp án của bạn vào đây">
                                    <input type="submit" value="Kiểm tra">
                                    </form>
                                </div>
                                <li>Câu 2: Con chuột lông màu gì?</li><br/>
                                <div>
                                    <form action="" >
                                    <input type="text" id="fname" name="firstname" placeholder="Gõ đáp án của bạn vào đây">
                                    <input type="submit" value="Kiểm tra">
                                    </form>
                                </div>
                            </ul>
                        </div>
                        <!-- end trả lời câu hỏi -->
                        <!-- goiy -->
                        <div class="section1">
                            <h3>Bài học gợi ý</h3>
                            <ul>
                                <li>
                                    <span>Piano</span> <a href="programs.html"><?= $this->Html->image('piano.jpg')?></a>

                                </li>
                                <li>
                                    <span>Violin</span> <a href="violin.html"><?= $this->Html->image('violin.jpg')?></a>
                                </li>
                                <li>
                                    <span>Guitar</span><a href="guitar.html"><?= $this->Html->image('guitar.jpg')?></a>
                                </li>
                                <li>
                                    <span>Saxophone</span> <a href="saxophone.html"><?= $this->Html->image('saxophone.jpg')?></a>
                                </li>
                                <li>
                                    <span>Drums</span> <a href="drums.html"><?= $this->Html->image('drums.jpg')?></a>
                                </li>
                                <li>
                                    <span>Voice Lesson</span> <a href="vioce-lesson.html"><?= $this->Html->image('voice-lesson.jpg')?></a>
                                </li>
                                <li>
                                    <span>Drums</span> <a href="drums.html"><?= $this->Html->image('drums.jpg')?></a>
                                </li>
                                <li>
                                    <span>Voice Lesson</span> <a href="vioce-lesson.html"><?= $this->Html->image('voice-lesson.jpg')?></a>
                                </li>
                            </ul>
                        </div>
                        <!-- end gioy -->
                    </div>  
                    <!-- end panel-heading -->
                </div>
                <!-- end panel panel-default -->
            </div>
            <!-- end class rỗng -->
        </div>

        <!-- end cot trai -->
        <!-- cột phải bắt đầu -->
        <div class="col-md-3 col-lg-3 col-xs-3 col-sm-3  sidebar" style="margin-top: 2%; margin-bottom: 2%">
            <a data-toggle="collapse" id="title_chu_de" data-parent="#accordion">Chủ Đề</a>
            <?php 
            foreach ($topic as $value_topic) {

                ?>   


                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $value_topic->topic_id; ?>"><?php echo h($value_topic->topic_name) ?></a>


                <div id="collapse<?php echo $value_topic->topic_id; ?>" class="collapse" >


<a href="blog.html">more posts</a>
<!-- <div id="tu_da_chon" > 
<h3 id="title_tu_da_chon">Từ Đã Chọn</h3>
  <textarea  name="txtMessage" id="txtMessage" class="txtDropTarget" ></textarea>
</div> -->

                   <?php 
                   foreach ($lesson as $value_lesson) {
                    if($value_topic->topic_id == $value_lesson->topic_id){


                        ?>

                        <a href="<?php echo $this->url->build(['controller'=>'lessons', 'action'=>'index', $value_topic->topic_id, $value_lesson->Ma]) ?>" style="color: black;"><h5><?php echo h($value_lesson->lesson_name) ?></h5></a>
                        <?php
                    }
                }
                ?>
                </div>
            <?php 

            }

            ?>
            <!-- tu da chon -->
                <div id="tu_da_chon"> 
                    <h3 id="title_tu_da_chon">Từ Đã Chọn</h3>
                    <textarea name="txtMessage" id="txtMessage" class="txtDropTarget"></textarea>
                </div>
            <!-- end tu da chon -->
        </div>
        <!-- end cột phải -->
    </div>
    <!-- end row chính -->
</div>