<?php
namespace App\Controller;
//use Cake\Mailer\Email;
//use Cake\Network\Email\Email;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher; //include this line
use Cake\Routing\Router;
use Cake\Mailer\Email;

use App\Controller\AppController;

class UsersController extends AppController
{
    public function index()
    {
        // $this->paginate = [
        //     'contain' => ['Users', 'Roles']
        // ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    public function initialize()
    {
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'users',
                'action' => 'login'
            ],
            'unauthorizedRedirect' => $this->referer() // If unauthorized, return them to page they were just on
        ]);

        // Allow the display action so our pages controller
        // continues to work.

        $this->Auth->allow(['display','password','register','login']);
            // 18 Chapter 2. Quick Start Guide
            // CakePHP Cookbook Documentation, Release 3.4
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {

        // $user = $this->Users->get($id, [
        //     'contain' => ['Users', 'Roles']
        // ]);

         $user = $this->Users->get($id, [
            'contain' => ['Roles']
         ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $users = $this->Users->Users->find('list', ['limit' => 200]);
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'users', 'roles'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $users = $this->Users->Users->find('list', ['limit' => 200]);
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'users', 'roles'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }



    public function login()
    {
        $user = $this->Auth->user('user_id');
        if (!empty($user)) {
            return $this->redirect(['controller'=>'Topics']);
        }else{
            if ($this->request->is('post')) {
                $user = $this->Auth->identify();
                if ($user) {
                    $this->Auth->setUser($user); 
                    return $this->redirect(['controller'=>'Topics']);
                }
                $this->Flash->error('Your username or password is incorrect.');
            }
        }
    }

    public function password()
    { 
        if ($this->request->is('post')) {
            $query = $this->Users->findByEmail($this->request->data['email']);
            $user = $query->first();
            if (is_null($user)) {
                $this->Flash->error('Email address does not exist. Please try again');
            } else {
                $passkey = uniqid();
                $url = Router::Url(['controller' => 'users', 'action' => 'reset'], true) . '/' . $passkey;
                $timeout = time() + DAY;
                if ($this->Users->updateAll(['passkey' => $passkey, 'created' => $timeout], ['user_id' => $user->user_id])){
                    $this->sendResetEmail($url, $user);
                    $this->redirect(['action' => 'login']);
                } else {
                    $this->Flash->error('Error saving reset passkey/timeout');
                }
            }
        }
    }

    private function sendResetEmail($url, $user) {
        $email = new Email();
        $email->template('resetpw');
        $email->emailFormat('both');
        $email->from('nvluan15101995@gmail.com');
        $email->to($user->email, $user->first_name);
        $email->subject('Reset your password');
        $email->viewVars(['url' => $url, 'username' => $user->email]);
        if ($email->send()) {
            $this->Flash->success(__('Check your email for your reset password link'));
        } else {
            $this->Flash->error(__('Error sending email: ') . $email->smtpError);
        }
    }

    public function reset($passkey = null) {
        if ($passkey) {
            $query = $this->Users->find('all', ['conditions' => ['passkey' => $passkey, 'created' => time()]]);
            $user = $query->first();
            if ($user) {
                if (!empty($this->request->data)) {
                    // Clear passkey and timeout
                    $this->request->data['passkey'] = null;
                    $this->request->data['created'] = null;
                    $user = $this->Users->patchEntity($user, $this->request->data);
                    if ($this->Users->save($user)) {
                        $this->Flash->set(__('Your password has been updated.'));
                        return $this->redirect(array('action' => 'login'));
                    } else {
                        $this->Flash->error(__('The password could not be updated. Please, try again.'));
                    }
                }
            } else {
                $this->Flash->error('Invalid or expired passkey. Please check your email or try again');
                $this->redirect(['action' => 'password']);
            }
            unset($user->password);
            $this->set(compact('user'));
        } else {
            $this->redirect('/');
        }
    }
    // public function forgetPW(){
    //     if($this->request->is('post')){
    //         $email1 = $this->request->getData('email');
    //         $emails = TableRegistry::get('Users'); 
    //         $user = $emails->find()->where(['email' => $email1 ])->first();

    //         if (!$user) {
    //             $this->Flash->error(__('No user with that email found.'));
    //             return $this->redirect(['controller' => 'Users','action' => 'login']);
    //         }else{

    //             // $random = 'a';
    //             $user = $this->Users->newEntity();
    //             $user->role_id = 'nv';
    //             $random = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',6)),0,6);

    //             $hasher = new DefaultPasswordHasher();
    //             $val = $hasher->hash($random);
                
    //             $user['password'] =  $val; 
    //             $data =  $user['password'];
               
    //             if ($this->Users->save($user)) {
                    
    //                 $this->Flash->success(__('Password changed Succesfully.'));
    //                  return $this->redirect(['controller' => 'Users','action' => 'login']);

    //             }

    //         }
    //     }
    //     //echo '<script> alert("Vui lòng vào email để lấy lại mật khẩu!") </script>';
    //     //return $this->redirect(['controller'=>'Users', 'action'=>'login']);
    // }

    public function register(){

       $user = $this->Users->newEntity(); // tạo đối tượng $user với trường dữ liệu có trong csdl
       
        if ($this->request->is('post')) {
           $user->role_id = 'nv';
            $user = $this->Users->patchEntity($user, $this->request->getData()); // đổ dữ liệu trên form vào đối tượng $user
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['controller' => 'Users', 'action' => 'login']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }


      public function userprofile(){
        $userId = $this->Auth->user('user_id');

        $user = $this->Users->get($userId, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }


    public function logout()
    {
        // $this->loadComponent('Auth');
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
        // return $this->redirect('/Signs/login');
    }
}
