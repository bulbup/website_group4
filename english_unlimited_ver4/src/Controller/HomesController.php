<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * homes Controller
 *
 * @property \App\Model\Table\homesTable $homes
 *
 * @method \App\Model\Entity\home[] paginate($object = null, array $settings = [])
 */
class HomesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        // $homes = $this->paginate($this->Homes);
         $this->set(compact('homes'));
        // $this->set('_serialize', ['homes']);
        $this->loadModel('Topics');
        $this->loadModel('Lessons');
        $topic = $this->Topics->find('all');
        $lesson = $this->Lessons->find('all',array(
            'order' => array('lesson_id'=>'desc'),
            'limit' => 9,
            ));
        $this->set(compact('topic'));
        $this->set(compact('lesson'));

    }
public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
          $this->viewBuilder()->layout('frontend');
    }
    
  
 
}
