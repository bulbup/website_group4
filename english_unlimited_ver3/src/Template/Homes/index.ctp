
     <!-- Services -->
        <div class="services-container">
	        <div class="container">
	            <div class="row team">
	             <h2>Chúng tôi giúp bạn<br /> học tiếng anh như thế nào...</h2>
	                <div class="col-sm-4 services-box">
	                	<div class="services-box-icon"><?= $this->Html->image('love.png')?></div>
	                    <h3>Học chủ đề yêu thích</h3>
	                    <p>Bạn có thể lựa chọn bất kì chủ đề nào để học theo sở thích và mong muốn.</p>
	                </div>
	                <div class="col-sm-4 services-box">
	                	<div class="services-box-icon"><?= $this->Html->image('services-2.png')?></div>
	                    <h3>Học bằng mindmap, audio, voice</h3>
	                    <p>Mỗi kĩ năng: nghe, nói, đọc, viết đều được áp dụng phương pháp học thông minh như: mindmap, video, flashcard</p>
	                </div>
	                <div class="col-sm-4 services-box">
	                	<div class="services-box-icon"><?= $this->Html->image('gameicontho.png')?></div>
	                    <h3>Vừa học vừa chơi</h3>
	                    <p>Chơi game để ôn lại kiến thức là một diều tuyệt vời nếu như bạn không muốn làm những bài test nhàm chán.</p>
	                </div>
	            </div>
	        </div>
        </div>
        
        <!-- Portfolio -->
        <div class="portfolio-container">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-12 portfolio">
	                    <h2>Tại sao phải học tiếng anh?</h2>
	                    <!-- <p>
	                    	Voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis 
	                    	et quasi architecto beatae vitae.
	                    </p> -->
	                </div>
	            </div>
	            <!-- <div class="row">
	            	<div class="col-sm-12 portfolio-filters">
	            		<a href="#" class="filter-all active">All</a> 
	            		<a href="#" class="filter-web-design">Web design</a> 
	            		<a href="#" class="filter-image-design">Image design</a> 
	            		<a href="#" class="filter-branding">Branding</a> 
	            		<a href="#" class="filter-dtp">DTP</a> 
	            		<a href="#" class="filter-tv-campanies">TV campanies</a>
	            	</div> -->
	            </div>

		               
		               
		               

		                	<?= $this->Html->image('why.png')?>
		                	
		           
		         
	                </div>
	            </div>
	        </div>
        </div>


		<!-- Blog -->
        <div class="blog-container">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-12 blog">
	                    <h2>Chủ Đề Mới Nhất</h2>
	                    <!-- <p>Quia dolor sit ame consectetur adipisci velit sed quia non numquam lorem dolor.</p> -->
	                </div>

	            </div>
	            <br><br>
    <div class="row">
        <div class="col-sm-3 col-md-3">
            <div class="post">
                <div class="post-img-content">
                    <img src="http://placehold.it/460x250/e67e22/ffffff&text=HTML5" class="img-responsive" />
                    <span class="post-title"><b>Chủ đề động vật</b><br />
                        <!-- <b>CSS3 Blur</b></span> -->
                </div>
                <div class="content">
                    <div class="author">
                        By <b>Bhaumik</b> |
                        <time datetime="2014-01-20">January 20th, 2014</time>
                    </div>
                    <div>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                        unknown printer took a galley of type and scrambled it to make a type specimen book.
                    </div>
                    <div>
                        <a href="http://www.jquery2dotnet.com/2014/01/jquery-highlight-table-row-and-column.html" class="btn btn-warning btn-sm">Read more</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="post">
                <div class="post-img-content">
                    <img src="http://placehold.it/460x250/2980b9/ffffff&text=CSS3" class="img-responsive" />
                    <span class="post-title"><b>chủ đề thiên nhiên</b><br />
                        <!-- <b>CSS3 Blur</b></span> -->
                </div>
                <div class="content">
                    <div class="author">
                        By <b>Bhaumik</b> |
                        <time datetime="2014-01-20">January 20th, 2014</time>
                    </div>
                    <div>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                        unknown printer took a galley of type and scrambled it to make a type specimen book.
                    </div>
                    <div>
                        <a href="http://www.jquery2dotnet.com/2013/11/share-social-media-round-buttons.html" class="btn btn-primary btn-sm">Read more</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="post">
                <div class="post-img-content">
                    <img src="http://placehold.it/460x250/2980b9/ffffff&text=CSS3" class="img-responsive" />
                    <span class="post-title"><b>Chủ đề bạn bè</b><br />
                        <!-- <b>CSS3 Blur</b></span> -->
                </div>
                <div class="content">
                    <div class="author">
                        By <b>Bhaumik</b> |
                        <time datetime="2014-01-20">January 20th, 2014</time>
                    </div>
                    <div>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                        unknown printer took a galley of type and scrambled it to make a type specimen book.
                    </div>
                    <div>
                        <a href="http://www.jquery2dotnet.com/2013/11/share-social-media-round-buttons.html" class="btn btn-primary btn-sm">Read more</a>
                    </div>
                </div>
            </div>
        </div>
                <div class="col-sm-3 col-md-3">
            <div class="post">
                <div class="post-img-content">
                    <img src="http://placehold.it/460x250/e67e22/ffffff&text=HTML5" class="img-responsive" />
                    <span class="post-title"><b>Chủ đề trường học</b><br />
                        <!-- <b>CSS3 Blur</b></span> -->
                </div>
                <div class="content">
                    <div class="author">
                        By <b>Bhaumik</b> |
                        <time datetime="2014-01-20">January 20th, 2014</time>
                    </div>
                    <div>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                        unknown printer took a galley of type and scrambled it to make a type specimen book.
                    </div>
                    <div>
                        <a href="http://www.jquery2dotnet.com/2014/01/jquery-highlight-table-row-and-column.html" class="btn btn-warning btn-sm">Read more</a>
                    </div>
                </div>
            </div>
        </div><br><br><br>
	            <div class="blog-all-posts"><a class="button-2" href="#">Show all posts</a></div>
	        </div>
        </div>
        <br><br>
        <!-- Customer reviews -->
        <div class="reviews-container">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-12 reviews">
	                    <h2>Châm Ngôn</h2>
	                    <div class="review-active"></div>

	                    <div class="review-single">
	                    	<p>
	                    		&ldquo;Life is 10% what happens to you and 90% how you respond to it &rdquo;
	                    		<br><br>
	                    		<a class="review-author" href="">Lou Holtz</a>
	                    	</p>
	                    	<div class="review-single-pagination active"><span></span></div>
	                    </div>

	                    <div class="review-single">
	                    	<p>
	                    		&ldquo;Never give up. Today is hard, tomorrow will be worse, but the day after tomorrow will be sunshine. &rdquo;
	                    		<br><br>
	                    		<a class="review-author" href="">JACK MA</a>
	                    	</p>
	                    	<div class="review-single-pagination"><span></span></div>
	                    </div>

	                    <div class="review-single">
	                    	<p>
	                    		&ldquo;Success is only meaningful and enjoynable if it fells like your own&rdquo;
	                    		<br><br>
	                    		<a class="review-author" href=""> Michelle Obama</a>
	                    	</p>
	                    	<div class="review-single-pagination"><span></span></div>
	                    </div>

	                    <div class="review-single">
	                    	<p>
	                    		"Neque porro quisquam est qui dolorem ipsum quia dolor sit ame consectetur adipisci velit sed quia non numquam 
	                    		lorem dolor excepturi tempore"
	                    		<br><br>
	                    		<a class="review-author" href="">Jane Doe, Designer</a>
	                    	</p>
	                    	<div class="review-single-pagination"><span></span></div>
	                    </div>

	                    <div class="review-single">
	                    	<p>
	                    		"Accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti 
	                    		quos dolores et quas molestias excepturi tempore"
	                    		<br><br>
	                    		<a class="review-author" href="">Steven Johnson, CEO</a>
	                    	</p>
	                    	<div class="review-single-pagination"><span></span></div>
	                    </div>

	                </div>
	            </div>
	        </div>
        </div>
        
		<!-- Our team -->
        <div class="team-container">
	        <div class="container">
	            <div class="row">
	                <div class="col-sm-12 team">
	                    <h2>Quản trị website</h2>
	                   <!--  <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit ame consectetur adipisci velit sed quia non numquam lorem dolor.</p> -->
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-sm-6">
	                	<div class="team-box">
		                	<div class="team-box-photo"><?= $this->Html->image('luan.jpg',['class'=>'img-responsive'])?></div>
		                	<div class="team-box-text">
		                		<h3>Nguyễn Văn Luân</h3>
		                		<p>Xử lý dữ liệu và các chức năng</p>
		                		<div class="team-box-social">
		                			<a class="button-social-1 button-facebook-1" href="#"></a>
		                			<a class="button-social-1 button-google-plus-1" href="#"></a>
		                			<a class="button-social-1 button-twitter-1" href="#"></a>
		                			<a class="button-social-1 button-pinterest-1" href="#"></a>
		                		</div>
							</div>
						</div>
	                </div>
	                <div class="col-sm-6">
	                	<div class="team-box">
		                	<div class="team-box-photo"><?= $this->Html->image('dung.jpg',['class'=>'img-responsive'])?></div>
		                	<div class="team-box-text">
		                		<h3>Trần Dương Ngọc Dung</h3>
		                		<p>Xây dựng giao diện website</p>
		                		<div class="team-box-social">
		                			<a class="button-social-1 button-facebook-1" href="#"></a>
		                			<a class="button-social-1 button-google-plus-1" href="#"></a>
		                			<a class="button-social-1 button-twitter-1" href="#"></a>
		                			<a class="button-social-1 button-pinterest-1" href="#"></a>
		                		</div>
							</div>
						</div>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-sm-6">
	                	<div class="team-box">
		                	<div class="team-box-photo"><?= $this->Html->image('tien.jpg',['class'=>'img-responsive'])?></div>
		                	<div class="team-box-text">
		                		<h3>Lê Thị Cẩm Tiên</h3>
		                		<p>Áp dụng các phương pháp học tư duy vào bài học</p>
		                		<div class="team-box-social">
		                			<a class="button-social-1 button-facebook-1" href="#"></a>
		                			<a class="button-social-1 button-google-plus-1" href="#"></a>
		                			<a class="button-social-1 button-twitter-1" href="#"></a>
		                			<a class="button-social-1 button-pinterest-1" href="#"></a>
		                		</div>
							</div>
						</div>
	                </div>
	                <div class="col-sm-6">
	                	<div class="team-box">
		                	<div class="team-box-photo">
		                	<!-- <img src="assets/img/team/4.jpg" alt="" data-at2x="assets/img/team/4.jpg"> -->
		                	<?= $this->Html->image('tho.jpg',['class'=>'img-responsive'])?></div>
		                	<div class="team-box-text">
		                		<h3>Nguyễn Thị Anh Thơ</h3>
		                		<p>Tạo ra những game thú vị giúp bạn hứng thú khi học tiếng anh</p>
		                		<div class="team-box-social">
		                			<a class="button-social-1 button-facebook-1" href="#"></a>
		                			<a class="button-social-1 button-google-plus-1" href="#"></a>
		                			<a class="button-social-1 button-twitter-1" href="#"></a>
		                			<a class="button-social-1 button-pinterest-1" href="#"></a>
		                		</div>
							</div>
						</div>
	                </div>
	            </div>
	        </div>
        </div>

		<!-- Image slider -->
        <div class="slider-container">
	        <!-- <div class="container"> -->
	            <div class="row">
	                <div class="col-sm-12">
	                	<div class="slider" >
	                		<div id="slider-1" class="carousel slide" data-ride="carousel">
								<!-- Wrapper for slides -->
								<div class="carousel-inner">
									<div class="item active">
										<?= $this->Html->image('study.jpg',['class'=>'img-responsive'])?>
									</div>
									<div class="item">
										<?= $this->Html->image('group.jpg',['class'=>'img-responsive'])?>
									</div>
								</div>
								<!-- Controls -->
								<a class="left carousel-control" href="#slider-1" data-slide="prev"></a>
								<a class="right carousel-control" href="#slider-1" data-slide="next"></a>
							</div>
	                	</div>
	                </div>
	            </div>
	        <!-- </div> -->
        </div>

<br>

		<!-- Map -->
        <div class="map-container">
	      
        	<!-- <div class="container"> -->
	              
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3928.8289571160703!2d105.7667154140422!3d10.030969375254312!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31a0883d0dac6b15%3A0xf6ae5b1bd18625!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBD4bqnbiBUaMah!5e0!3m2!1svi!2s!4v1498103427630" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	 
        <!-- </div> -->

		<!-- Contact us -->
        <div class="contact-us-container">
	        <div class="container" >
	            <div class="row">
	                <div class="col-sm-12 contact-us">
	                    <h2 style="color:black">Liên Hệ</h2>
	                   <!--  <p>Voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae.</p> -->
	                   <p class="above">
	                   	
	                   </p>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-sm-12 contact-us-form">
	                	<form action="" method="post">
	                		<div class="contact-us-form-left">
	                			<div class="contact-us-input-container">
		                			<label for="contact-us-name" class="contact-us-name-label"></label>
		                			<input type="text" id="contact-us-name" name="contact-us-name" placeholder="Họ tên...">
	                       		</div>
	                       		<div class="contact-us-input-container">
		                			<label for="contact-us-email" class="contact-us-email-label"></label>
		                			<input type="text" id="contact-us-email" name="contact-us-email" placeholder="Email...">
	                       		</div>
	                       		<div class="contact-us-input-container">
		                			<label for="contact-us-phone" class="contact-us-phone-label"></label>
		                			<input type="text" id="contact-us-phone" name="contact-us-phone" placeholder="Số điện thoại...">
	                       		</div>
	                		</div>
	                		<div class="contact-us-form-right">
	                			<div class="contact-us-input-container">
		                			<label for="contact-us-message" class="contact-us-message-label"></label>		                			
		                			<textarea id="contact-us-message" name="contact-us-message" placeholder="Thông điệp..."></textarea>
	                       		</div>
	                			<input type="submit" value="Gửi đi">
	                		</div>
	                	</form>
	                </div>
	            </div>
	        </div>
        </div>