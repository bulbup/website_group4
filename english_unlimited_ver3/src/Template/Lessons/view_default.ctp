<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Lesson'), ['action' => 'edit', $lesson->lesson_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Lesson'), ['action' => 'delete', $lesson->lesson_id], ['confirm' => __('Are you sure you want to delete # {0}?', $lesson->lesson_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Lessons'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Lesson'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Lessons'), ['controller' => 'Lessons', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Lesson'), ['controller' => 'Lessons', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Topics'), ['controller' => 'Topics', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Topic'), ['controller' => 'Topics', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="lessons view large-9 medium-8 columns content">
    <h3><?= h($lesson->lesson_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Lesson') ?></th>
            <td><?= $lesson->has('lesson') ? $this->Html->link($lesson->lesson->lesson_id, ['controller' => 'Lessons', 'action' => 'view', $lesson->lesson->lesson_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Topic') ?></th>
            <td><?= $lesson->has('topic') ? $this->Html->link($lesson->topic->topic_id, ['controller' => 'Topics', 'action' => 'view', $lesson->topic->topic_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lesson Name') ?></th>
            <td><?= h($lesson->lesson_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Image') ?></th>
            <td><?= h($lesson->image) ?></td>
        </tr>
    </table>
</div>
