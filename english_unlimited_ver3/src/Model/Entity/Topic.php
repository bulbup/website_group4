<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Topic Entity
 *
 * @property int $topic_id
 * @property string $level_name
 * @property string $describe_short
 * @property string $image
 * @property \Cake\I18n\FrozenTime $create_time
 *
 * @property \App\Model\Entity\Topic $topic
 */
class Topic extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'topic_id' => false
    ];
}
