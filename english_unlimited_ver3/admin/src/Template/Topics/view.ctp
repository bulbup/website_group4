<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
<br><br><br>
    <h3><?= h($topic->topic_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Topic') ?></th>
            <td><?= $topic->has('topic') ? $this->Html->link($topic->topic->topic_id, ['controller' => 'Topics', 'action' => 'view', $topic->topic->topic_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Topic Name') ?></th>
            <td><?= h($topic->topic_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Level Name') ?></th>
            <td><?= h($topic->level_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Describe Short') ?></th>
            <td><?= h($topic->describe_short) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Image') ?></th>
            <td><?= h($topic->image) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Create Time') ?></th>
            <td><?= h($topic->create_time) ?></td>
        </tr>
    </table>
</div>
