<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">    <div class="row">
<div class="users index col-sm-12 col-lg-12 columns content">

<!-- <div class="users form large-9 medium-8 columns content"> -->
  <br><br> 
     <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Thêm Học Viên') ?></legend>
        <?php
            echo $this->Form->control('role_id', ['options' => $roles]);
            echo $this->Form->control('first_name');
            echo $this->Form->control('last_name');
            echo $this->Form->control('email');
            echo $this->Form->control('password');
            echo $this->Form->control('birthday', ['empty' => true]);
            echo $this->Form->control('gender');
            echo $this->Form->control('job');
            echo $this->Form->control('image');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Thêm')) ?>
    <?= $this->Form->end() ?>
  .....
<!-- </div> -->
</div></div></div>