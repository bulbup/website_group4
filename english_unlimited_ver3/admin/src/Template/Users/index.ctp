  
<?php
/**
* @var \App\View\AppView $this
*/
?>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <!-- <div class="row"> -->
<!-- <div class="users index col-sm-12 col-lg-12 columns content"> -->
<br><br><br>
    <h3><?= __('Học Viên') ?></h3>  
    
       <table class="table table-striped">
    
     <thead>
    <tr>
        <th scope="col"><?= $this->Paginator->sort('Mã Số') ?></th>
        <!-- <th scope="col"><?php //$this->Paginator->sort('role_id') ?></th> -->
        <th scope="col"><?= $this->Paginator->sort('Tên') ?></th>
        <th scope="col"><?= $this->Paginator->sort('Họ') ?></th>
        <th scope="col"><?= $this->Paginator->sort('Email') ?></th>
        <th scope="col"><?= $this->Paginator->sort('Mật Khẩu') ?></th>
  
        <th scope="col" class="actions"><?= __('Actions') ?></th>
    </tr>
     </thead>   
     <tbody>
         <?php foreach ($users as $user): ?>
            <tr>
                <td><?= h($user ->user_id) ?></td>
                <!-- <td><?php //$user->has('role') ? $this->Html->link($user->role->role_id, ['controller' => 'Roles', 'action' => 'view', $user->role->role_id]) : '' ?></td> -->
                <td><?= h($user->first_name) ?></td>
                <td><?= h($user->last_name) ?></td>
                <td><?= h($user->email) ?></td>
                <td><?= h($user->password) ?></td>
                
                <td class="actions">

                    <!-- <?php //$this->Html->link(__('View'), ['action' => 'view', $user->user_id]) ?>
                    <?php //$this->Html->link(__('Edit'), ['action' => 'edit', $user->user_id]) ?>
                    <?php //$this->Form->postLink(__('Delete'), ['action' => 'delete', $user->user_id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->user_id)]) ?> -->


                    <?= $this->Html->link(__('Xem'), ['action' => 'view', $user->user_id]) ?>
                    <?= $this->Html->link(__('Sửa'), ['action' => 'edit', $user->user_id]) ?>
                    <?= $this->Form->postLink(__('Xóa'),['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->user_id)]) ?>
                    <!-- <i class="fa fa-trash" aria-hidden="true"></i> -->

                </td>
            </tr>
            <?php endforeach; ?>
       
      
        <tr class="row-content">
           <td class="check" "> <label><input type="checkbox" value=""></label></td>
           <td> <span class="label label-default"> New </span></td>
           <td>FA106</td>
           <td>Twinkle</td>
           <td>Pen</td>
           <td>Very Happy.. <i class="fa fa-smile-o" aria-hidden="true"></i></td>
           <td>780/-</td>
           <td>
              <a class="btn btn-danger edit" href="path/to/settings" aria-label="Settings">
                <i class="fa fa-trash" aria-hidden="true"></i>
              </a>
              &nbsp 
              <a class="btn btn-info edit" href="path/to/settings" aria-label="Settings">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
              </a> 
           </td>
        </tr>
     </tbody>
  </table>
  <!-- Phân trang -->
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
    <button class="btn btn-sm btn-success"><?= $this->Html->link(__('Tạo Người Dùng'), ['action' => 'add']) ?></button>
</div>
<!-- </div> -->
<!-- </div> -->
<!-- </div> -->



        
    </div><!--/.main-->