<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * homes Controller
 *
 * @property \App\Model\Table\homesTable $homes
 *
 * @method \App\Model\Entity\home[] paginate($object = null, array $settings = [])
 */
class TestsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    
public function initialize()
    {
        parent::initialize();

       // $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
          $this->viewBuilder()->layout('default');
    }
    public function index()
    {
        // $homes = $this->paginate($this->Homes);

         $this->set(compact('tests'));
        // $this->set('_serialize', ['homes']);
    }
  
  

   
 
}
