<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * homes Controller
 *
 * @property \App\Model\Table\homesTable $homes
 *
 * @method \App\Model\Entity\home[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    
public function initialize()
    {
        parent::initialize();

       // $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
          $this->viewBuilder()->layout('default');
    }

    public function index()
    {
        // $homes = $this->paginate($this->Homes);

         //$this->set(compact('users'));
        // $this->set('_serialize', ['homes']);
    }

     public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify(); // mã hóa dữ liệu
            // var_dump($user);
            // exit();
            if ($user) {
                $this->Auth->setUser($user);
                //return $this->redirect($this->Auth->redirectUrl());
                return $this->redirect(
                    //['Controller' => 'Slants', 'action' => 'index']
                    '/Homes'
                    );
            }
            $this->Flash->error('Your username or password is incorrect.');
        }
    }

    public function logout()
    {
        // $this->loadComponent('Auth');
        //$this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
        // return $this->redirect('/Signs/login');
    }

        public function userprofile(){
        $userId = $this->Auth->user('id');

        $user = $this->Users->get($userId, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        //$this->set('users',$user);
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

  
  

   
 
}
