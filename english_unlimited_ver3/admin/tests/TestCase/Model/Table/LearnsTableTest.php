<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LearnsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LearnsTable Test Case
 */
class LearnsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LearnsTable
     */
    public $Learns;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.learns',
        'app.users',
        'app.roles',
        'app.lessons'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Learns') ? [] : ['className' => 'App\Model\Table\LearnsTable'];
        $this->Learns = TableRegistry::get('Learns', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Learns);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
