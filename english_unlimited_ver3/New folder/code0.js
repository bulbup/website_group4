gdjs.GameSceneCode = {};


gdjs.GameSceneCode.GDHUB_95SCOREObjects1= [];
gdjs.GameSceneCode.GDHUB_95SCOREObjects2= [];
gdjs.GameSceneCode.GDHUB_95SCOREObjects3= [];
gdjs.GameSceneCode.GDHUB_95SCOREObjects4= [];
gdjs.GameSceneCode.GDCharacter1Objects1= [];
gdjs.GameSceneCode.GDCharacter1Objects2= [];
gdjs.GameSceneCode.GDCharacter1Objects3= [];
gdjs.GameSceneCode.GDCharacter1Objects4= [];
gdjs.GameSceneCode.GDCoinGoldObjects1= [];
gdjs.GameSceneCode.GDCoinGoldObjects2= [];
gdjs.GameSceneCode.GDCoinGoldObjects3= [];
gdjs.GameSceneCode.GDCoinGoldObjects4= [];
gdjs.GameSceneCode.GDGrassCenterTextureObjects1= [];
gdjs.GameSceneCode.GDGrassCenterTextureObjects2= [];
gdjs.GameSceneCode.GDGrassCenterTextureObjects3= [];
gdjs.GameSceneCode.GDGrassCenterTextureObjects4= [];
gdjs.GameSceneCode.GDGrassTextureObjects1= [];
gdjs.GameSceneCode.GDGrassTextureObjects2= [];
gdjs.GameSceneCode.GDGrassTextureObjects3= [];
gdjs.GameSceneCode.GDGrassTextureObjects4= [];
gdjs.GameSceneCode.GDBoxObjects1= [];
gdjs.GameSceneCode.GDBoxObjects2= [];
gdjs.GameSceneCode.GDBoxObjects3= [];
gdjs.GameSceneCode.GDBoxObjects4= [];
gdjs.GameSceneCode.GDCloud2Objects1= [];
gdjs.GameSceneCode.GDCloud2Objects2= [];
gdjs.GameSceneCode.GDCloud2Objects3= [];
gdjs.GameSceneCode.GDCloud2Objects4= [];
gdjs.GameSceneCode.GDCharacter3Objects1= [];
gdjs.GameSceneCode.GDCharacter3Objects2= [];
gdjs.GameSceneCode.GDCharacter3Objects3= [];
gdjs.GameSceneCode.GDCharacter3Objects4= [];

gdjs.GameSceneCode.conditionTrue_0 = {val:false};
gdjs.GameSceneCode.condition0IsTrue_0 = {val:false};
gdjs.GameSceneCode.condition1IsTrue_0 = {val:false};
gdjs.GameSceneCode.condition2IsTrue_0 = {val:false};
gdjs.GameSceneCode.conditionTrue_1 = {val:false};
gdjs.GameSceneCode.condition0IsTrue_1 = {val:false};
gdjs.GameSceneCode.condition1IsTrue_1 = {val:false};
gdjs.GameSceneCode.condition2IsTrue_1 = {val:false};

gdjs.GameSceneCode.func = function(runtimeScene, context) {
context.startNewFrame();
gdjs.GameSceneCode.GDHUB_95SCOREObjects1.length = 0;
gdjs.GameSceneCode.GDHUB_95SCOREObjects2.length = 0;
gdjs.GameSceneCode.GDHUB_95SCOREObjects3.length = 0;
gdjs.GameSceneCode.GDHUB_95SCOREObjects4.length = 0;
gdjs.GameSceneCode.GDCharacter1Objects1.length = 0;
gdjs.GameSceneCode.GDCharacter1Objects2.length = 0;
gdjs.GameSceneCode.GDCharacter1Objects3.length = 0;
gdjs.GameSceneCode.GDCharacter1Objects4.length = 0;
gdjs.GameSceneCode.GDCoinGoldObjects1.length = 0;
gdjs.GameSceneCode.GDCoinGoldObjects2.length = 0;
gdjs.GameSceneCode.GDCoinGoldObjects3.length = 0;
gdjs.GameSceneCode.GDCoinGoldObjects4.length = 0;
gdjs.GameSceneCode.GDGrassCenterTextureObjects1.length = 0;
gdjs.GameSceneCode.GDGrassCenterTextureObjects2.length = 0;
gdjs.GameSceneCode.GDGrassCenterTextureObjects3.length = 0;
gdjs.GameSceneCode.GDGrassCenterTextureObjects4.length = 0;
gdjs.GameSceneCode.GDGrassTextureObjects1.length = 0;
gdjs.GameSceneCode.GDGrassTextureObjects2.length = 0;
gdjs.GameSceneCode.GDGrassTextureObjects3.length = 0;
gdjs.GameSceneCode.GDGrassTextureObjects4.length = 0;
gdjs.GameSceneCode.GDBoxObjects1.length = 0;
gdjs.GameSceneCode.GDBoxObjects2.length = 0;
gdjs.GameSceneCode.GDBoxObjects3.length = 0;
gdjs.GameSceneCode.GDBoxObjects4.length = 0;
gdjs.GameSceneCode.GDCloud2Objects1.length = 0;
gdjs.GameSceneCode.GDCloud2Objects2.length = 0;
gdjs.GameSceneCode.GDCloud2Objects3.length = 0;
gdjs.GameSceneCode.GDCloud2Objects4.length = 0;
gdjs.GameSceneCode.GDCharacter3Objects1.length = 0;
gdjs.GameSceneCode.GDCharacter3Objects2.length = 0;
gdjs.GameSceneCode.GDCharacter3Objects3.length = 0;
gdjs.GameSceneCode.GDCharacter3Objects4.length = 0;


{



}


{


gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
gdjs.GameSceneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("GameOver")) == 0;
}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {

{ //Subevents

{

gdjs.GameSceneCode.GDCharacter3Objects2.createFrom(runtimeScene.getObjects("Character3"));

gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
gdjs.GameSceneCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameSceneCode.GDCharacter3Objects2.length ;i < len;++i) {
    gdjs.GameSceneCode.GDCharacter3Objects2[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


{

gdjs.GameSceneCode.GDCharacter3Objects2.createFrom(runtimeScene.getObjects("Character3"));

{for(var i = 0, len = gdjs.GameSceneCode.GDCharacter3Objects2.length ;i < len;++i) {
    gdjs.GameSceneCode.GDCharacter3Objects2[i].getBehavior("PlatformerObject").simulateRightKey();
}
}
}


{

gdjs.GameSceneCode.GDCharacter3Objects2.createFrom(runtimeScene.getObjects("Character3"));

{gdjs.evtTools.camera.setCameraX(runtimeScene, (( gdjs.GameSceneCode.GDCharacter3Objects2.length === 0 ) ? 0 :gdjs.GameSceneCode.GDCharacter3Objects2[0].getPointX("")) + 300, "", 0);
}
{ //Subevents

{

gdjs.GameSceneCode.GDCoinGoldObjects3.createFrom(runtimeScene.getObjects("CoinGold"));

gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameSceneCode.GDCoinGoldObjects3.length;i<l;++i) {
    if ( gdjs.GameSceneCode.GDCoinGoldObjects3[i].getOpacity() < 255 ) {
        gdjs.GameSceneCode.condition0IsTrue_0.val = true;
        gdjs.GameSceneCode.GDCoinGoldObjects3[k] = gdjs.GameSceneCode.GDCoinGoldObjects3[i];
        ++k;
    }
}
gdjs.GameSceneCode.GDCoinGoldObjects3.length = k;}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameSceneCode.GDCoinGoldObjects3.length ;i < len;++i) {
    gdjs.GameSceneCode.GDCoinGoldObjects3[i].setAnimation(gdjs.GameSceneCode.GDCoinGoldObjects3[i].getAnimation() - (0));
}
}{for(var i = 0, len = gdjs.GameSceneCode.GDCoinGoldObjects3.length ;i < len;++i) {
    gdjs.GameSceneCode.GDCoinGoldObjects3[i].addForce(0, -100, 0);
}
}
{ //Subevents

{

gdjs.GameSceneCode.GDCharacter3Objects4.createFrom(gdjs.GameSceneCode.GDCharacter3Objects2);
gdjs.GameSceneCode.GDCoinGoldObjects4.createFrom(gdjs.GameSceneCode.GDCoinGoldObjects3);

gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
gdjs.GameSceneCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("Character3", gdjs.GameSceneCode.GDCharacter3Objects4).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("CoinGold", gdjs.GameSceneCode.GDCoinGoldObjects4).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameSceneCode.GDCoinGoldObjects4.length ;i < len;++i) {
    gdjs.GameSceneCode.GDCoinGoldObjects4[i].deleteFromScene(runtimeScene);
}
}}

}

} //End of subevents
}

}

} //End of subevents

}

} //End of subevents
}

}


{



}


{

gdjs.GameSceneCode.GDCharacter3Objects1.createFrom(runtimeScene.getObjects("Character3"));

gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameSceneCode.GDCharacter3Objects1.length;i<l;++i) {
    if ( gdjs.GameSceneCode.GDCharacter3Objects1[i].getY() > 600 ) {
        gdjs.GameSceneCode.condition0IsTrue_0.val = true;
        gdjs.GameSceneCode.GDCharacter3Objects1[k] = gdjs.GameSceneCode.GDCharacter3Objects1[i];
        ++k;
    }
}
gdjs.GameSceneCode.GDCharacter3Objects1.length = k;}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("GameOver").setNumber(1);
}
{ //Subevents

{


gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
{gdjs.GameSceneCode.conditionTrue_1 = gdjs.GameSceneCode.condition0IsTrue_0;
gdjs.GameSceneCode.conditionTrue_1.val = context.triggerOnce(6536980);
}
}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "GameOver");
}{gdjs.evtTools.sound.playSound(runtimeScene, "spaceTrash1.ogg", false, 100, 1);
}}

}


{


gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
gdjs.GameSceneCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "GameOver");
}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "GameScene", false);
}}

}

} //End of subevents
}

}


{



}


{

gdjs.GameSceneCode.GDCharacter3Objects1.createFrom(runtimeScene.getObjects("Character3"));

gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameSceneCode.GDCharacter3Objects1.length;i<l;++i) {
    if ( gdjs.GameSceneCode.GDCharacter3Objects1[i].getBehavior("PlatformerObject").isJumping() ) {
        gdjs.GameSceneCode.condition0IsTrue_0.val = true;
        gdjs.GameSceneCode.GDCharacter3Objects1[k] = gdjs.GameSceneCode.GDCharacter3Objects1[i];
        ++k;
    }
}
gdjs.GameSceneCode.GDCharacter3Objects1.length = k;}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameSceneCode.GDCharacter3Objects1.length ;i < len;++i) {
    gdjs.GameSceneCode.GDCharacter3Objects1[i].setAnimation(2);
}
}
{ //Subevents

{


gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
{gdjs.GameSceneCode.conditionTrue_1 = gdjs.GameSceneCode.condition0IsTrue_0;
gdjs.GameSceneCode.conditionTrue_1.val = context.triggerOnce(6367460);
}
}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "highUp.ogg", false, 100, 1);
}}

}

} //End of subevents
}

}


{

gdjs.GameSceneCode.GDCharacter3Objects1.createFrom(runtimeScene.getObjects("Character3"));

gdjs.GameSceneCode.condition0IsTrue_0.val = false;
gdjs.GameSceneCode.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameSceneCode.GDCharacter3Objects1.length;i<l;++i) {
    if ( gdjs.GameSceneCode.GDCharacter3Objects1[i].getBehavior("PlatformerObject").isMoving() ) {
        gdjs.GameSceneCode.condition0IsTrue_0.val = true;
        gdjs.GameSceneCode.GDCharacter3Objects1[k] = gdjs.GameSceneCode.GDCharacter3Objects1[i];
        ++k;
    }
}
gdjs.GameSceneCode.GDCharacter3Objects1.length = k;}if ( gdjs.GameSceneCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.GameSceneCode.GDCharacter3Objects1.length;i<l;++i) {
    if ( gdjs.GameSceneCode.GDCharacter3Objects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.GameSceneCode.condition1IsTrue_0.val = true;
        gdjs.GameSceneCode.GDCharacter3Objects1[k] = gdjs.GameSceneCode.GDCharacter3Objects1[i];
        ++k;
    }
}
gdjs.GameSceneCode.GDCharacter3Objects1.length = k;}}
if (gdjs.GameSceneCode.condition1IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameSceneCode.GDCharacter3Objects1.length ;i < len;++i) {
    gdjs.GameSceneCode.GDCharacter3Objects1[i].setAnimation(1);
}
}
{ //Subevents

{

gdjs.GameSceneCode.GDCharacter3Objects2.createFrom(gdjs.GameSceneCode.GDCharacter3Objects1);

gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameSceneCode.GDCharacter3Objects2.length;i<l;++i) {
    if ( gdjs.GameSceneCode.GDCharacter3Objects2[i].getBehavior("PlatformerObject").isJumping() ) {
        gdjs.GameSceneCode.condition0IsTrue_0.val = true;
        gdjs.GameSceneCode.GDCharacter3Objects2[k] = gdjs.GameSceneCode.GDCharacter3Objects2[i];
        ++k;
    }
}
gdjs.GameSceneCode.GDCharacter3Objects2.length = k;}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameSceneCode.GDCharacter3Objects2.length ;i < len;++i) {
    gdjs.GameSceneCode.GDCharacter3Objects2[i].setAnimation(2);
}
}
{ //Subevents

{



}

} //End of subevents
}

}

} //End of subevents
}

}


{



}


{


gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
gdjs.GameSceneCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("LastPlatformX").setNumber(400);
}{runtimeScene.getVariables().get("LastPlatformY").setNumber(400);
}}

}


{

gdjs.GameSceneCode.GDCharacter3Objects1.createFrom(runtimeScene.getObjects("Character3"));

gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameSceneCode.GDCharacter3Objects1.length;i<l;++i) {
    if ( gdjs.GameSceneCode.GDCharacter3Objects1[i].getX() > gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("LastPlatformX")) - 600 ) {
        gdjs.GameSceneCode.condition0IsTrue_0.val = true;
        gdjs.GameSceneCode.GDCharacter3Objects1[k] = gdjs.GameSceneCode.GDCharacter3Objects1[i];
        ++k;
    }
}
gdjs.GameSceneCode.GDCharacter3Objects1.length = k;}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("LastPlatformX").add(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("LastPlatformWidth")) + (1 + gdjs.random(5)) * 40);
}{runtimeScene.getVariables().get("LastPlatformY").setNumber(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("LastPlatformY")) + (gdjs.random(6) - 3) * 30);
}
{ //Subevents

{


gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
gdjs.GameSceneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("LastPlatformY")) > 500;
}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("LastPlatformY").setNumber(500);
}}

}


{


gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
gdjs.GameSceneCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("LastPlatformY")) < 300;
}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("LastPlatformY").setNumber(300);
}}

}


{

gdjs.GameSceneCode.GDGrassCenterTextureObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, context.clearEventsObjectsMap().addObjectsToEventsMap("GrassCenterTexture", gdjs.GameSceneCode.GDGrassCenterTextureObjects2).getEventsObjectsMap(), gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("LastPlatformX")), gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("LastPlatformY")), "");
}{for(var i = 0, len = gdjs.GameSceneCode.GDGrassCenterTextureObjects2.length ;i < len;++i) {
    gdjs.GameSceneCode.GDGrassCenterTextureObjects2[i].setWidth(150+gdjs.random(400));
}
}{runtimeScene.getVariables().get("LastPlatformWidth").setNumber((( gdjs.GameSceneCode.GDGrassCenterTextureObjects2.length === 0 ) ? 0 :gdjs.GameSceneCode.GDGrassCenterTextureObjects2[0].getWidth()));
}{for(var i = 0, len = gdjs.GameSceneCode.GDGrassCenterTextureObjects2.length ;i < len;++i) {
    gdjs.GameSceneCode.GDGrassCenterTextureObjects2[i].setHeight(500);
}
}{for(var i = 0, len = gdjs.GameSceneCode.GDGrassCenterTextureObjects2.length ;i < len;++i) {
    gdjs.GameSceneCode.GDGrassCenterTextureObjects2[i].setZOrder(-1);
}
}
}


{

gdjs.GameSceneCode.GDGrassTextureObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, context.clearEventsObjectsMap().addObjectsToEventsMap("GrassTexture", gdjs.GameSceneCode.GDGrassTextureObjects2).getEventsObjectsMap(), gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("LastPlatformX")), gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("LastPlatformY")), "");
}{for(var i = 0, len = gdjs.GameSceneCode.GDGrassTextureObjects2.length ;i < len;++i) {
    gdjs.GameSceneCode.GDGrassTextureObjects2[i].setWidth(gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("LastPlatformWidth")));
}
}
}

} //End of subevents
}

}


{



}


{

gdjs.GameSceneCode.GDCharacter3Objects1.createFrom(runtimeScene.getObjects("Character3"));

gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
gdjs.GameSceneCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 1, "Acceleration");
}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Acceleration");
}{for(var i = 0, len = gdjs.GameSceneCode.GDCharacter3Objects1.length ;i < len;++i) {
    gdjs.GameSceneCode.GDCharacter3Objects1[i].getBehavior("PlatformerObject").setMaxSpeed(gdjs.GameSceneCode.GDCharacter3Objects1[i].getBehavior("PlatformerObject").getMaxSpeed() + (5));
}
}}

}


{



}


{

gdjs.GameSceneCode.GDCharacter3Objects1.createFrom(runtimeScene.getObjects("Character3"));
gdjs.GameSceneCode.GDGrassCenterTextureObjects1.createFrom(runtimeScene.getObjects("GrassCenterTexture"));

gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameSceneCode.GDGrassCenterTextureObjects1.length;i<l;++i) {
    if ( gdjs.GameSceneCode.GDGrassCenterTextureObjects1[i].getX() < (( gdjs.GameSceneCode.GDCharacter3Objects1.length === 0 ) ? 0 :gdjs.GameSceneCode.GDCharacter3Objects1[0].getPointX("")) - 1000 ) {
        gdjs.GameSceneCode.condition0IsTrue_0.val = true;
        gdjs.GameSceneCode.GDGrassCenterTextureObjects1[k] = gdjs.GameSceneCode.GDGrassCenterTextureObjects1[i];
        ++k;
    }
}
gdjs.GameSceneCode.GDGrassCenterTextureObjects1.length = k;}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameSceneCode.GDGrassCenterTextureObjects1.length ;i < len;++i) {
    gdjs.GameSceneCode.GDGrassCenterTextureObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.GameSceneCode.GDCharacter3Objects1.createFrom(runtimeScene.getObjects("Character3"));
gdjs.GameSceneCode.GDCloud2Objects1.createFrom(runtimeScene.getObjects("Cloud2"));

gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameSceneCode.GDCloud2Objects1.length;i<l;++i) {
    if ( gdjs.GameSceneCode.GDCloud2Objects1[i].getX() < (( gdjs.GameSceneCode.GDCharacter3Objects1.length === 0 ) ? 0 :gdjs.GameSceneCode.GDCharacter3Objects1[0].getPointX("")) - 1000 ) {
        gdjs.GameSceneCode.condition0IsTrue_0.val = true;
        gdjs.GameSceneCode.GDCloud2Objects1[k] = gdjs.GameSceneCode.GDCloud2Objects1[i];
        ++k;
    }
}
gdjs.GameSceneCode.GDCloud2Objects1.length = k;}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameSceneCode.GDCloud2Objects1.length ;i < len;++i) {
    gdjs.GameSceneCode.GDCloud2Objects1[i].deleteFromScene(runtimeScene);
}
}}

}


{

gdjs.GameSceneCode.GDCharacter3Objects1.createFrom(runtimeScene.getObjects("Character3"));
gdjs.GameSceneCode.GDCloud2Objects1.createFrom(runtimeScene.getObjects("Cloud2"));

gdjs.GameSceneCode.condition0IsTrue_0.val = false;
gdjs.GameSceneCode.condition1IsTrue_0.val = false;
{
gdjs.GameSceneCode.condition0IsTrue_0.val = gdjs.evtTools.object.pickedObjectsCount(context.clearEventsObjectsMap().addObjectsToEventsMap("Cloud2", gdjs.GameSceneCode.GDCloud2Objects1).getEventsObjectsMap()) < 20;
}if ( gdjs.GameSceneCode.condition0IsTrue_0.val ) {
{
gdjs.GameSceneCode.condition1IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 1, "Cloud");
}}
if (gdjs.GameSceneCode.condition1IsTrue_0.val) {
{gdjs.evtTools.object.createObjectOnScene(runtimeScene, context.clearEventsObjectsMap().addObjectsToEventsMap("Cloud2", gdjs.GameSceneCode.GDCloud2Objects1).getEventsObjectsMap(), (( gdjs.GameSceneCode.GDCharacter3Objects1.length === 0 ) ? 0 :gdjs.GameSceneCode.GDCharacter3Objects1[0].getPointX("")) + 800 + gdjs.random(400), gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("LastPlatformY")) - gdjs.random(400), "");
}{for(var i = 0, len = gdjs.GameSceneCode.GDCloud2Objects1.length ;i < len;++i) {
    gdjs.GameSceneCode.GDCloud2Objects1[i].setZOrder(-3);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "Cloud");
}
{ //Subevents

{

gdjs.GameSceneCode.GDCharacter3Objects2.createFrom(gdjs.GameSceneCode.GDCharacter3Objects1);
gdjs.GameSceneCode.GDCoinGoldObjects2.createFrom(runtimeScene.getObjects("CoinGold"));

gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
gdjs.GameSceneCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("Character3", gdjs.GameSceneCode.GDCharacter3Objects2).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("CoinGold", gdjs.GameSceneCode.GDCoinGoldObjects2).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameSceneCode.GDCoinGoldObjects2.length ;i < len;++i) {
    gdjs.GameSceneCode.GDCoinGoldObjects2[i].deleteFromScene(runtimeScene);
}
}
{ //Subevents

{


gdjs.GameSceneCode.condition0IsTrue_0.val = false;
if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{}
{ //Subevents

{

gdjs.GameSceneCode.GDCharacter3Objects4.createFrom(gdjs.GameSceneCode.GDCharacter3Objects2);

gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameSceneCode.GDCharacter3Objects4.length;i<l;++i) {
    if ( gdjs.GameSceneCode.GDCharacter3Objects4[i].getBehavior("PlatformerObject").isJumping() ) {
        gdjs.GameSceneCode.condition0IsTrue_0.val = true;
        gdjs.GameSceneCode.GDCharacter3Objects4[k] = gdjs.GameSceneCode.GDCharacter3Objects4[i];
        ++k;
    }
}
gdjs.GameSceneCode.GDCharacter3Objects4.length = k;}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameSceneCode.GDCharacter3Objects4.length ;i < len;++i) {
    gdjs.GameSceneCode.GDCharacter3Objects4[i].setAnimation(2);
}
}}

}

} //End of subevents
}

}

} //End of subevents
}

}

} //End of subevents
}

}


{

gdjs.GameSceneCode.GDCharacter3Objects1.createFrom(runtimeScene.getObjects("Character3"));
gdjs.GameSceneCode.GDCoinGoldObjects1.createFrom(runtimeScene.getObjects("CoinGold"));

gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
gdjs.GameSceneCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(context.clearEventsObjectsMap().addObjectsToEventsMap("Character3", gdjs.GameSceneCode.GDCharacter3Objects1).getEventsObjectsMap(), context.clearEventsObjectsMap().addObjectsToEventsMap("CoinGold", gdjs.GameSceneCode.GDCoinGoldObjects1).getEventsObjectsMap(), false, runtimeScene);
}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {
{for(var i = 0, len = gdjs.GameSceneCode.GDCoinGoldObjects1.length ;i < len;++i) {
    gdjs.GameSceneCode.GDCoinGoldObjects1[i].deleteFromScene(runtimeScene);
}
}{gdjs.evtTools.sound.playSound(runtimeScene, "phaseJump2.ogg", false, 100, 2);
}{runtimeScene.getVariables().get("PlayerScore").add(10);
}}

}


{



}


{

gdjs.GameSceneCode.GDHUB_95SCOREObjects1.createFrom(runtimeScene.getObjects("HUB_SCORE"));

gdjs.GameSceneCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.GameSceneCode.GDHUB_95SCOREObjects1.length;i<l;++i) {
    if ( gdjs.GameSceneCode.GDHUB_95SCOREObjects1[i].getString() == "" ) {
        gdjs.GameSceneCode.condition0IsTrue_0.val = true;
        gdjs.GameSceneCode.GDHUB_95SCOREObjects1[k] = gdjs.GameSceneCode.GDHUB_95SCOREObjects1[i];
        ++k;
    }
}
gdjs.GameSceneCode.GDHUB_95SCOREObjects1.length = k;}if (gdjs.GameSceneCode.condition0IsTrue_0.val) {

{ //Subevents

{

gdjs.GameSceneCode.GDHUB_95SCOREObjects2.createFrom(gdjs.GameSceneCode.GDHUB_95SCOREObjects1);

{for(var i = 0, len = gdjs.GameSceneCode.GDHUB_95SCOREObjects2.length ;i < len;++i) {
    gdjs.GameSceneCode.GDHUB_95SCOREObjects2[i].setString("");
}
}
{ //Subevents

{

gdjs.GameSceneCode.GDHUB_95SCOREObjects3.createFrom(gdjs.GameSceneCode.GDHUB_95SCOREObjects2);

{gdjs.evtTools.object.createObjectOnScene(runtimeScene, context.clearEventsObjectsMap().addObjectsToEventsMap("HUB_SCORE", gdjs.GameSceneCode.GDHUB_95SCOREObjects3).getEventsObjectsMap(), 0, 0, "");
}
}

} //End of subevents

}

} //End of subevents
}

}

return;
}
gdjs['GameSceneCode']= gdjs.GameSceneCode;
