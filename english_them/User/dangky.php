<form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>Đăng ký thành viên</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="hôten">Họ và tên</label>  
  <div class="col-md-8">
  <input id="hôten" name="hôten" type="text" placeholder="Họ và tên" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">Email</label>  
  <div class="col-md-8">
  <input id="email" name="email" type="text" placeholder="Email" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="pass">Password</label>
  <div class="col-md-8">
    <input id="pass" name="pass" type="password" placeholder="password" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="dangky"></label>
  <div class="col-md-4">
    <button id="dangky" name="dangky" class="btn btn-info">Đăng ký</button>
  </div>
</div>

</fieldset>
</form>
