<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Slant &mdash; A free HTML5 Template by FREEHTML5.CO</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
<meta name="author" content="FREEHTML5.CO" />
<!-- Facebook and Twitter integration -->
<meta property="og:title" content=""/>
<meta property="og:image" content=""/>
<meta property="og:url" content=""/>
<meta property="og:site_name" content=""/>
<meta property="og:description" content=""/>
<meta name="twitter:title" content="" />
<meta name="twitter:image" content="" />
<meta name="twitter:url" content="" />
<meta name="twitter:card" content="" />

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="favicon.ico">

<!-- Google Webfont -->
<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
<!-- Themify Icons -->
<link rel="stylesheet" href="css/themify-icons.css">
<!-- Bootstrap -->
<link rel="stylesheet" href="css/bootstrap.css">
<!-- Owl Carousel -->
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">
<!-- Magnific Popup -->
<link rel="stylesheet" href="css/magnific-popup.css">
<!-- Superfish -->
<link rel="stylesheet" href="css/superfish.css">
<!-- Easy Responsive Tabs -->
<link rel="stylesheet" href="css/easy-responsive-tabs.css">
<!-- Animate.css -->
<link rel="stylesheet" href="css/animate.css">
<!-- Theme Style -->
<link rel="stylesheet" href="css/style.css">

<!-- Modernizr JS -->
<script src="js/modernizr-2.6.2.min.js"></script>

<!-- jQuery -->
<script src="js/jquery-1.10.2.min.js"></script>
<!-- jQuery Easing -->
<script src="js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.js"></script>
<!-- Owl carousel -->
<script src="js/owl.carousel.min.js"></script>
<!-- Magnific Popup -->
<script src="js/jquery.magnific-popup.min.js"></script>
<!-- Superfish -->
<script src="js/hoverIntent.js"></script>
<script src="js/superfish.js"></script>
<!-- Easy Responsive Tabs -->
<script src="js/easyResponsiveTabs.js"></script>
<!-- FastClick for Mobile/Tablets -->
<script src="js/fastclick.js"></script>
<!-- Parallax -->
<!-- <script src="js/jquery.parallax-scroll.min.js"></script> -->
<!-- Waypoints -->
<script src="js/jquery.waypoints.min.js"></script>
<!-- Main JS -->
<script src="js/main.js"></script>




<link rel="stylesheet" type="text/css" href="css/style.css">
<!-- START #fh5co-header -->
<header id="fh5co-header-section" role="header" class="" >
	<div class="container">



		<!-- <div id="fh5co-menu-logo"> -->
		<!-- START #fh5co-logo -->
		<h1 id="fh5co-logo" class="pull-left"><a href="index.php"><img src="images/logo2.png" alt="Slant Free HTML5 Template">english unlimited</a></h1>

		<!-- START #fh5co-menu-wrap -->
		<nav id="fh5co-menu-wrap" role="navigation">


			<ul class="sf-menu" id="fh5co-primary-menu">
				<li><a href="index.php">Trang Chủ</a></li>
				<li><a href="chu_de.php" >Chủ đề</a></li>
				<li><a href="lien_he.php">Liên hệ</a></li>
				<li><a href="elements.html">Giới thiệu</a></li>
				<li class="fh5co-special"><a href="dang_nhap.php">Đăng nhập</a></li>
				<!-- <li class="fh5co-special" style="margin-left: 15px"><a href="contact.html">Đăng nhập</a></li> -->
			</ul>
		</nav>
		<!-- </div> -->

	</div>
</header>

		<div id="fh5co-hero">
			<a href="#fh5co-main" class="smoothscroll fh5co-arrow to-animate hero-animate-4"><i class="ti-angle-down"></i></a>
			<!-- End fh5co-arrow -->
			<div class="container">
				<div class="col-md-8 col-md-offset-2">
					<div class="fh5co-hero-wrap">
						<div class="fh5co-hero-intro">
							<h1 class="to-animate hero-animate-1">Đôi nét về chúng tôi </h1>
							<h1 class="to-animate hero-animate-1" style="background-color: #173e43;margin-top: 40px;padding: 10px">English Unlimited</h1>
							<h2 class="to-animate hero-animate-2">Cùng xem nhé</h2>
							<!-- <p class="to-animate hero-animate-3"><a href="#" class="btn btn-outline btn-lg">Vào thư viện</a></p> -->
						</div>
					</div>
				</div>
			</div>		
		</div>