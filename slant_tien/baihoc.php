<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Slant &mdash; A free HTML5 Template by FREEHTML5.CO</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="FREEHTML5.CO" />

  	<!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
	
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 	https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
-->

<!-- Facebook and Twitter integration -->
<meta property="og:title" content=""/>
<meta property="og:image" content=""/>
<meta property="og:url" content=""/>
<meta property="og:site_name" content=""/>
<meta property="og:description" content=""/>
<meta name="twitter:title" content="" />
<meta name="twitter:image" content="" />
<meta name="twitter:url" content="" />
<meta name="twitter:card" content="" />

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="favicon.ico">

<!-- Google Webfont -->
<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
<!-- Themify Icons -->
<link rel="stylesheet" href="css/themify-icons.css">
<!-- Bootstrap -->
<link rel="stylesheet" href="css/bootstrap.css">
<!-- Owl Carousel -->
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">
<!-- Magnific Popup -->
<link rel="stylesheet" href="css/magnific-popup.css">
<!-- Superfish -->
<link rel="stylesheet" href="css/superfish.css">
<!-- Easy Responsive Tabs -->
<link rel="stylesheet" href="css/easy-responsive-tabs.css">
<!-- Animate.css -->
<link rel="stylesheet" href="css/animate.css">
<!-- Theme Style -->
<link rel="stylesheet" href="css/style.css">

<!-- Modernizr JS -->
<script src="js/modernizr-2.6.2.min.js"></script>
<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	<!-- START #fh5co-header -->
	<!-- header -->
	<?php include('header.html');?>
	<!-- .header -->

	<div id="fh5co-main">

		<div class="container">
			<div class="row" id="fh5co-features">
				<h2 class="dung" style="text-align: center;">4 Yếu tố học tốt tiếng anh</h2>
				<div class="fh5co-spacer fh5co-spacer-sm"></div>
				<div class="col-md-3 col-sm-6 text-center fh5co-feature feature-box">
					<div class="fh5co-feature-icon">
						<i class="ti-mobile"></i>
					</div>
					<h3 class="heading">Niềm tin</h3>
					<p>Niềm tin là yếu tố quan trọng nhất giúp bạn thực hiện bất kì công việc gì trong mọi lĩnh vực. Bạn sẽ chẳng thể thành công nều luôn để sự nghi ngờ tồn tại trong tâm trí. Vì nó chính là rào cản lớn nhất khiến bạn không thể tự tin đưa ra và thực thi bất cứ dự định gì. Vì vậy, hãy loại bỏ những suy nghĩ như “ mình dốt”, “ mình không có năng khiếu”,… ra khỏi đầu và đặt niềm tin vào chính bản thân mình.</p>
				</div>

				<div class="col-md-3 col-sm-6 text-center fh5co-feature feature-box"> 
					<div class="fh5co-feature-icon">
						<i class="ti-lock"></i>
					</div>
					<h3 class="heading">Kế họach</h3>
					<p>Liệt kê các công việc cần phải làm khi học tiếng anh, các mục tiêu cần hướng tới theo một trình tự nhất định và được thực hiện trong một khoảng thời gian nhất định, cụ thể. </p>
				</div>

				<!-- 	<div class="clearfix visible-sm-block"></div> -->

				<div class="col-md-3 col-sm-6 text-center fh5co-feature feature-box"> 
					<div class="fh5co-feature-icon">
						<i class="ti-video-camera"></i>
					</div>
					<h3 class="heading">Thực hiện</h3>
					<p> Nếu bạn đã hoàn thành được 2 bước đầu thì việc khó khăn cuối cùng chỉ là bạn hãy thực hiện và kiên trì với kế hoạch của mình. Bí quyết để thực hiện được việc khó khăn này là hãy luôn suy nghĩ tích cực và đặt niềm tin vào chính bản thân mình.</p>
				</div>

				<!-- <div class="clearfix visible-md-block visible-lg-block"></div> -->

				<div class="col-md-3 col-sm-6 text-center fh5co-feature feature-box">
					<div class="fh5co-feature-icon">
						<i class="ti-shopping-cart"></i>
					</div>
					<h3 class="heading">Động lực và đam mê</h3>
					<p>Chắc hẳn nếu bạn đã đọc đến đây thì có lẽ bạn đã thực sự mong muốn tự học tiếng anh. Để luôn giữ ngọn lửa đam mê trong mình hãy liệt kê hết những lý do bạn phải học tiếng anh như: thăng tiến trong công việc, đi du lịch nước ngoài, xem bất kì chương trình tiếng anh mà không cần phụ đề…Hãy chọn ra một vài lý do mà bạn thực sự tâm đắc nhất để lấy đó làm kim chỉ nam cho những bước đi tiếp theo trên con đường chinh phục tiếng Anh. </p>
				</div>

				<div class="clearfix visible-sm-block"></div>

			<!-- 		<div class="col-md-4 col-sm-6 text-center fh5co-feature feature-box"> 
						<div class="fh5co-feature-icon">
							<i class="ti-palette"></i>
						</div>
						<h3 class="heading">Pallete</h3>
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste sunt porro delectus cum officia magnam.</p>
					</div>
					<div class="col-md-4 col-sm-6 text-center fh5co-feature feature-box"> 
						<div class="fh5co-feature-icon">
							<i class="ti-truck"></i>
						</div>
						<h3 class="heading">Deliver</h3>
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste sunt porro delectus cum officia magnam.</p>
					</div> -->
				</div>
				<!-- END row -->

				<div class="fh5co-spacer fh5co-spacer-md"></div>
				<!-- End Spacer -->

				<div class="row" id="fh5co-works">
					<div class="col-md-8 col-md-offset-2 text-center fh5co-section-heading work-box">
						<h2 class="fh5co-lead">3 Bước học hiệu quả của<br> English Unlimited</h2>
						<!-- <p class="fh5co-sub">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit est facilis maiores, perspiciatis accusamus asperiores sint consequuntur debitis. Ut, dolores sit amet consectetur adipisicing elit.</p> -->
						<!-- <div class="fh5co-spacer fh5co-spacer-sm"></div> -->
					</div>

					<div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 text-center fh5co-work-item work-box">
						<div class="container">
							<blockquote class="quote-box">
								<p class="quotation-mark">
									“
								</p>
								<p class="quote-text">
									Bạn có thể lựa chọn các chủ đề để học theo sở thích và mong muốn. 
								</p>

								<hr>
								<div class="blog-post-actions">
									<p class="blog-post-bottom pull-left">
										Học theo chủ đề
									</p>
									<p class="blog-post-bottom pull-right">
										<!-- <span class="badge quote-badge">896</span>  ❤ -->
									</p>
								</div>
							</blockquote>
						</div>
					</div>

					<div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 text-center fh5co-work-item work-box"> 
						<div class="container">
							<blockquote class="quote-box">
								<p class="quotation-mark">
									“
								</p>
								<p class="quote-text">
										Với mỗi bài học mà english unlimited đem lại có đầy đủ các kĩ năng: nghe, nói, đọc, viết. Mỗi kĩ năng được áp dụng phương pháp học thông minh như: mindmap, video, flashcard  
								</p>

								<hr>
								<div class="blog-post-actions">
									<p class="blog-post-bottom pull-left">
									Học theo Kỹ năng
									</p>
									<p class="blog-post-bottom pull-right">
										<!-- <span class="badge quote-badge">896</span>  ❤ -->
									</p>
								</div>
							</blockquote>
						</div>
					</div>

					<div class="clearfix visible-sm-block visible-xs-block"></div>

					<div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 text-center fh5co-work-item work-box"> 
						<div class="container">
							<blockquote class="quote-box">
								<p class="quotation-mark">
									“
								</p>
								<p class="quote-text">
									Bài kiểm tra giúp đánh giá tiến độ của bạn.
So sánh trước và sau khi học một cách rõ ràng và cụ thể.
								</p>

								<hr>
								<div class="blog-post-actions">
									<p class="blog-post-bottom pull-left">
										Kiểm tra
									</p>
									<p class="blog-post-bottom pull-right">
										<!-- <span class="badge quote-badge">896</span>  ❤ -->
									</p>
								</div>
							</blockquote>
						</div>
					</div>

					<div class="clearfix visible-md-block visible-lg-block"></div>

			<br><br>

					<div class="col-md-4 col-md-offset-4 text-center work-box">
						<p style="color: black"><a href="#" class="btn btn-outline btn-md">Học ngay thôi</a></p>
					</div>
				</div>
				<!-- END row -->
				
				<div class="fh5co-spacer fh5co-spacer-md"></div>
				<div class="row">
					<!-- Start Slider Testimonial -->
					<h2 class="fh5co-uppercase-heading-sm text-center animate-box">Châm ngôn của chúng tôi</h2>
					<div class="fh5co-spacer fh5co-spacer-xs"></div>
					<div class="owl-carousel-fullwidth animate-box">
						<div class="item">
							<p class="text-center quote" style="color: black">&ldquo;Life is 10% what happens to you and 90% how you respond to it &rdquo; <cite class="author">&mdash; Lou Holtz</cite></p>
						</div>
						<div class="item">
							<p class="text-center quote " style="color: black">&ldquo;Never give up. Today is hard, tomorrow will be worse, but the day after tomorrow will be sunshine. &rdquo;<cite class="author">&mdash;  JACK MA</cite></p>
						</div>
						<div class="item">
							<p class="text-center quote" style="color: black">&ldquo;Success is only meaningful and enjoynable if it fells like your own&rdquo;<cite class="author">&mdash; Michelle Obama</cite></p>
						</div>
					</div>
					<!-- End Slider Testimonial -->
				</div>
				<!-- END row -->
				<div class="fh5co-spacer fh5co-spacer-md"></div>

			</div>
			<!-- END container -->


		</div>
		<!-- END fhtco-main -->

		<!-- footer -->
		<?php include('footer.html');?>
		<!--.footer  -->

		<!-- jQuery -->
		<script src="js/jquery-1.10.2.min.js"></script>
		<!-- jQuery Easing -->
		<script src="js/jquery.easing.1.3.js"></script>
		<!-- Bootstrap -->
		<script src="js/bootstrap.js"></script>
		<!-- Owl carousel -->
		<script src="js/owl.carousel.min.js"></script>
		<!-- Magnific Popup -->
		<script src="js/jquery.magnific-popup.min.js"></script>
		<!-- Superfish -->
		<script src="js/hoverIntent.js"></script>
		<script src="js/superfish.js"></script>
		<!-- Easy Responsive Tabs -->
		<script src="js/easyResponsiveTabs.js"></script>
		<!-- FastClick for Mobile/Tablets -->
		<script src="js/fastclick.js"></script>
		<!-- Parallax -->
		<script src="js/jquery.parallax-scroll.min.js"></script>
		<!-- Waypoints -->
		<script src="js/jquery.waypoints.min.js"></script>
		<!-- Main JS -->
		<script src="js/main.js"></script>

	</body>
	</html>
