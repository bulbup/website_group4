<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>HTML5 Audio Read-Along Demo</title>
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link rel="stylesheet" href="style.css">

        <link rel="shortcut icon" href="favicon.ico">

<!-- Google Webfont -->
<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
<!-- Themify Icons -->
<link rel="stylesheet" href="../css/themify-icons.css">
<!-- Bootstrap -->
<link rel="stylesheet" href="../css/bootstrap.css">
<!-- Owl Carousel -->
<link rel="stylesheet" href="../css/owl.carousel.min.css">
<link rel="stylesheet" href="../css/owl.theme.default.min.css">
<!-- Magnific Popup -->
<link rel="stylesheet" href="../css/magnific-popup.css">
<!-- Superfish -->
<link rel="stylesheet" href="../css/superfish.css">
<!-- Easy Responsive Tabs -->
<link rel="stylesheet" href="../css/easy-responsive-tabs.css">
<!-- Animate.css -->
<link rel="stylesheet" href="../css/animate.css">
<!-- Theme Style -->
<link rel="stylesheet" href="../css/style.css">

<!-- Modernizr JS -->
<script src="../js/modernizr-2.6.2.min.js"></script>

<!-- jQuery -->
<script src="../js/jquery-1.10.2.min.js"></script>
<!-- jQuery Easing -->
<script src="../js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="../js/bootstrap.js"></script>
<!-- Owl carousel -->
<script src="../js/owl.carousel.min.js"></script>
<!-- Magnific Popup -->
<script src="../js/jquery.magnific-popup.min.js"></script>
<!-- Superfish -->
<script src="../js/hoverIntent.js"></script>
<script src="../js/superfish.js"></script>
<!-- Easy Responsive Tabs -->
<script src="../js/easyResponsiveTabs.js"></script>
<!-- FastClick for Mobile/Tablets -->
<script src="../js/fastclick.js"></script>
<!-- Parallax -->
<!-- <script src="js/jquery.parallax-scroll.min.js"></script> -->
<!-- Waypoints -->
<script src="../js/jquery.waypoints.min.js"></script>
<!-- Main JS -->
<script src="../js/main.js"></script>
    </head>
    <body>
        

        <!-- @todo The link could pull in README.md via Ajax and render it inline -->
        
        <?php include('../header_short.php');?>
        <article>
            

            <p class="loading">
                <em><img src="loader.gif" alt="Initializing audio"> Loading audio…</em>
            </p>

            <p class="passage-audio" hidden>
                <audio id="passage-audio" class="passage" controls>
                    <!-- @todo WebM? -->
                    <source src="audio/BCSW-s01-e01.mp3" type="audio/mp3">
                    <source src="audio/Luke.2.1-Luke.2.20.ogg" type="audio/ogg">
                    <source src="audio/Luke.2.1-Luke.2.20.wav" type="audio/wav">
                    <em class="error"><strong>Error:</strong> Your browser doesn't appear to support HTML5 Audio.</em>
                </audio>
            </p>
            <p class="passage-audio-unavailable" hidden>
                <em class="error"><strong>Error:</strong> You will not be able to do the read-along audio because your browser is not able to play MP3, Ogg, or WAV audio formats.</em>
            </p>

            <p class="playback-rate" hidden title="Note that increaseing the reading rate will decrease accuracy of word highlights">
                <label for="playback-rate">Reading rate:</label>
                <input id="playback-rate" type="range" min="0.5" max="2.0" value="1.0" step="0.1" disabled onchange='this.nextElementSibling.textContent = String(Math.round(this.valueAsNumber * 10) / 10) + "\u00D7";'> <!-- Chỗ tính này là seo??? -->
                <output>1&times;</output>
            </p>
            <p class="playback-rate-unavailable" hidden>
                <em>(It seems your browser does not support <code>HTMLMediaElement.playbackRate</code>, so you will not be able to change the speech rate.)</em>
            </p>
            <p class="autofocus-current-word" hidden>
                <input type="checkbox" id="autofocus-current-word" checked>
                <label for="autofocus-current-word">Auto-focus/auto-scroll</label>
            </p>

            <noscript>
                <p class="error"><em><strong>Notice:</strong> You must have JavaScript enabled/available to try this HTML5 Audio read along.</em></p>
            </noscript>

            <div id="passage-text" class="passage">
                <h3 class="section-heading">The Birth of Jesus Christ</h3>
                <!-- bắt đầu ở giây thứ 'data-begin' và kéo dài trong 'data-dur' giây, từ sau sẽ bắt đầu ở giây 'data-begin'+'data-dur' của từ trước đó
                
                 -->
                <p><sup class="verse-start">1</sup><span data-dur="3" data-begin="0">Hello and welcome to big city small world</span> <span data-dur="3" data-begin="3">talk to you by British Coucil</span><span data-dur="4" data-begin="6">Espisode 1</span><span data-dur="23" data-begin="10">(beat)</span> <span data-dur="1" data-begin="33">Uhm</span><span data-dur="4" data-begin="34">Hello, hello</span><span data-dur="4" data-begin="38">Uhm, excuse me, excuse me</span><span data-dur="5" data-begin="42"> just sit down anyway, the don't have way to in here</span>.</p>
            
            </div>

            
        </article>
        <footer>
            <?php include('../footer.html');?>
        </footer>

        <script src="read-along.js"></script>
        <script src="main.js"></script>
    </body>
</html>
